package util

import java.util.Collections

fun <T> Sequence<T>.takeWhileInclusive(predicate: (T) -> Boolean): Sequence<T> {
    var shouldContinue = true
    return takeWhile {
        val result = shouldContinue
        shouldContinue = predicate(it)
        result
    }
}

fun <T> Iterable<T>.second() =
    when (this) {
        is List -> this.second()
        else -> {
            val iterator = iterator()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection is empty.")
            iterator.next()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection has just one element.")
            iterator.next()
        }
    }

fun <T> List<T>.second(): T {
    if (size < 2)
        throw NoSuchElementException("List has not enough elements: $size.")
    return this[1]
}

fun <T> Iterable<T>.secondLast() =
    when (this) {
        is List -> this.secondLast()
        else -> {
            val iterator = iterator()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection is empty.")
            var secondToLast = iterator.next()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection has just one element.")
            var last = iterator.next()
            while (iterator.hasNext()) {
                secondToLast = last
                last = iterator.next()
            }
            secondToLast
        }
    }


fun <T> List<T>.secondLast(): T {
    if (size < 2)
        throw NoSuchElementException("List has not enough elements: $size.")
    return this[lastIndex - 1]
}

fun <T> Iterable<T>.third() =
    when (this) {
        is List -> this.third()
        else -> {
            val iterator = iterator()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection is empty.")
            iterator.next()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection has just one element.")
            iterator.next()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection has just two elements.")
            iterator.next()
        }
    }

fun <T> List<T>.third(): T {
    if (size < 3)
        throw NoSuchElementException("List has not enough elements: $size.")
    return this[2]
}

fun <T> Iterable<T>.fourth() =
    when (this) {
        is List -> this.fourth()
        else -> {
            val iterator = iterator()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection is empty.")
            iterator.next()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection has just one element.")
            iterator.next()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection has just two elements.")
            iterator.next()
            if (!iterator.hasNext())
                throw NoSuchElementException("Collection has just three elements.")
            iterator.next()
        }
    }

fun <T> List<T>.fourth(): T {
    if (size < 4)
        throw NoSuchElementException("List has not enough elements: $size.")
    return this[3]
}

fun <T> List<T>.middle(): T {
    if (isEmpty())
        throw NoSuchElementException("List has not enough elements: $size.")
    return this[size / 2]
}

fun Iterable<Int>.product() = fold(1) { a, b -> a * b }

fun Iterable<Long>.product() = fold(1L) { a, b -> a * b }

fun Iterable<Decimal>.product() = fold(Decimal.ONE) { a, b -> a * b }

inline fun Iterable<Int>.product(predicate: (Int) -> Boolean): Int = this.filter(predicate).product()

inline fun Iterable<Long>.product(predicate: (Long) -> Boolean): Long = this.filter(predicate).product()

inline fun Iterable<Decimal>.product(predicate: (Decimal) -> Boolean): Decimal = this.filter(predicate).product()

inline fun <T> Iterable<T>.productOf(function: (T) -> Long): Long = this.map(function).product()

fun Collection<*>.allDistinct() = this is Set || size == distinct().size

infix fun <T> Collection<T>.isDisjoint(other: Collection<T>) = Collections.disjoint(this, other)

fun <T> List<List<T>>.deepModifiableCopy() = this.map { it.toMutableList() }.toMutableList()

fun <T> List<T>.oneBasedIndexOf(element: T) = 1 + indexOf(element)

infix fun <T, S> Iterable<T>.x(other: Iterable<S>): List<Pair<T, S>> {
    return flatMap { t -> other.map { s -> t to s } }
}

fun <T : Any> Iterable<T>.distinctPairs(): Sequence<Pair<T, T>> = sequence {
    val iterator = this@distinctPairs.iterator()
    if (!iterator.hasNext()) return@sequence
    val previous = mutableListOf(iterator.next())
    while(iterator.hasNext()) {
        val second = iterator.next()
        for (first in previous) yield(first to second)
        previous.add(second)
    }
}