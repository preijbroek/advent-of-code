package util

fun String.second(): Char {
    if (length < 2)
        throw StringIndexOutOfBoundsException("String is too short: $length")
    return this[1]
}

fun String.secondLast(): Char {
    if (length < 2)
        throw StringIndexOutOfBoundsException("String is too short: $length")
    return this[lastIndex - 1]
}

fun List<String>.toCharMap(): Map<Position, Char> {
    return flatMapIndexed { y, s ->
        s.mapIndexed { x, c ->
            Position(x, y) to c
        }
    }.toMap()
}