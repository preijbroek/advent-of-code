package util

data class PositionRangeFromCentre(
    val centre: Position,
    val maxDistance: Long,
) {

    constructor(centre: Position, maxDistance: Int) : this(centre, maxDistance.toLong())

    operator fun contains(position: Position): Boolean {
        return position.distanceTo(centre) <= maxDistance
    }

    fun outerBorder2d(): Sequence<Position> {
        return (-maxDistance - 1..maxDistance + 1).asSequence().flatMap { x ->
            setOf(
                Position(centre.x + x, centre.y - (maxDistance - x.abs() + 1)),
                Position(centre.x + x, centre.y + (maxDistance - x.abs() + 1))
            )
        }
    }

    infix fun intersect(positions: Set<Position>): Set<Position> {
        return positions.filter { it in this }.toSet()
    }

    fun intersectY(y: Long): LongRange {
        val distance = (centre.y - y).abs()
        return (centre.x - (maxDistance - distance))..(centre.x + (maxDistance - distance))
    }
}
