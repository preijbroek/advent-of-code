@file:JvmName(name = "Comparators")

package util

import java.util.function.Function

fun <T : Comparable<T>?> minOf(ts: Collection<T>): T {
    return ts.stream().min { obj: T, o: T -> obj!!.compareTo(o) }.orElseThrow { IllegalStateException() }
}

fun <T, U : Comparable<U?>?> minOf(ts: Collection<T>, keyExtractor: Function<in T, out U?>?): T {
    return ts.stream().min(Comparator.comparing(keyExtractor)).orElseThrow { IllegalStateException() }
}

fun <T> minOf(ts: Collection<T>, comparator: Comparator<T>?): T {
    return ts.stream().min(comparator).orElseThrow { IllegalStateException() }
}

fun <T : Comparable<T>?> maxOf(ts: Collection<T>): T {
    return ts.stream().max { obj: T, o: T -> obj!!.compareTo(o) }.orElseThrow { IllegalStateException() }
}

fun <T, U : Comparable<U?>?> maxOf(
    ts: Collection<T>,
    keyExtractor: Function<in T, out U?>?
): T {
    return ts.stream().max(Comparator.comparing(keyExtractor)).orElseThrow { IllegalStateException() }
}

fun <T> maxOf(ts: Collection<T>, comparator: Comparator<T>?): T {
    return ts.stream().max(comparator).orElseThrow { IllegalStateException() }
}