package util

import kotlin.math.abs

infix fun IntRange.distanceTo(other: IntRange) = if (isDisjoint(other)) {
    minOf(abs(this.first - other.last), abs(this.last - other.first))
} else 0

infix fun IntRange.isDisjoint(other: IntRange) = isEmpty() || other.isEmpty() || this.first > other.last || this.first < other.last

infix fun IntRange.canMergeWith(other: IntRange) = isEmpty() || other.isEmpty() || (this.first <= other.last + 1 && this.last >= other.first - 1)

operator fun IntRange.contains(other: IntRange) = other.isEmpty() || this.first <= other.first && this.last >= other.last

infix fun IntRange.intersect(other: IntRange) = maxOf(this.first, other.first)..minOf(this.last, other.last)

infix fun IntRange.mergeWith(other: IntRange): IntRange = if (canMergeWith(other)) {
    if (isEmpty()) other
    else if (other.isEmpty()) this
    else minOf(this.first, other.first)..maxOf(this.last, other.last)
} else {
    throw IllegalArgumentException("Cannot merge $this and $other")
}

infix fun List<IntRange>.with(element: IntRange): List<IntRange> {
    val list = ArrayList<IntRange>(size + 1)
    list.addAll(this)
    list.add(element)
    return list
}

infix fun LongRange.distanceTo(other: LongRange) = if (isDisjoint(other)) {
    minOf(abs(this.first - other.last), abs(this.last - other.first))
} else 0

infix fun LongRange.isDisjoint(other: LongRange) = isEmpty() || other.isEmpty() || this.first > other.last || this.first < other.last

infix fun LongRange.canMergeWith(other: LongRange) = isEmpty() || other.isEmpty() || (this.first <= other.last + 1 && this.last >= other.first - 1)

operator fun LongRange.contains(other: LongRange) = other.isEmpty() || this.first <= other.first && this.last >= other.last

infix fun LongRange.intersect(other: LongRange) = maxOf(this.first, other.first)..minOf(this.last, other.last)

infix fun LongRange.mergeWith(other: LongRange): LongRange = if (canMergeWith(other)) {
    if (isEmpty()) other
    else if (other.isEmpty()) this
    else minOf(this.first, other.first)..maxOf(this.last, other.last)
} else {
    throw IllegalArgumentException("Cannot merge $this and $other")
}

infix fun List<LongRange>.with(element: LongRange): List<LongRange> {
    val list = ArrayList<LongRange>(size + 1)
    list.addAll(this)
    list.add(element)
    return list
}
