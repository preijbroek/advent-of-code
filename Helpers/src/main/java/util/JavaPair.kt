package util

import java.util.Collections
import java.util.Objects

data class JavaPair<T>(val left: T, val right: T) {

    fun values(): List<T> {
        return listOf(left, right)
    }

    operator fun contains(t: T): Boolean {
        return values().contains(t)
    }

    fun containsAll(vararg ts: Any?) = values().containsAll(ts.toList())

    fun hasOverlap(pair: JavaPair<T?>): Boolean {
        return !Collections.disjoint(values(), pair.values())
    }

    fun hasPartialOverlap(pair: JavaPair<T?>): Boolean {
        return !equals(pair) && hasOverlap(pair)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val pair = other as JavaPair<*>
        return hasSameElements(pair)
    }

    private fun hasSameElements(pair: JavaPair<*>): Boolean {
        return containsAll(pair.left, pair.right)
                && pair.containsAll(left, right)
    }

    override fun hashCode(): Int {
        return Objects.hash(left, right) + Objects.hash(right, left)
    }

}