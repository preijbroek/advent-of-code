package util

import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

data class Box(val corner1: Position, val corner2: Position) {

    private val xRange = min(corner1.x, corner2.x)..max(corner1.x, corner2.x)
    private val yRange = min(corner1.y, corner2.y)..max(corner1.y, corner2.y)
    private val zRange = min(corner1.z, corner2.z)..max(corner1.z, corner2.z)
    private val wRange = min(corner1.w, corner2.w)..max(corner1.w, corner2.w)

    val minCorner = Position(xRange.first, yRange.first, zRange.first, wRange.first)
    val maxCorner = Position(xRange.last, yRange.last, zRange.last, wRange.last)

    operator fun contains(position: Position): Boolean {
        return position.x in xRange && position.y in yRange && position.z in zRange && position.w in wRange
    }

    val size = xRange.toSet().size.toLong() *
            yRange.toSet().size.toLong() *
            zRange.toSet().size.toLong() *
            wRange.toSet().size.toLong()

    fun split(x: Long? = null, y: Long? = null, z: Long? = null, w: Long? = null, includeLeft: Boolean): List<Box?> {
        return (x?.let { split(x, xRange, includeLeft) { newX -> copy(x = newX) } } ?: listOf(this))
            .flatMap { box -> y?.let { box?.split(y, yRange, includeLeft) { copy(y = it) } } ?: listOf(box) }
            .flatMap { box -> z?.let { box?.split(z, zRange, includeLeft) { copy(z = it) } } ?: listOf(box) }
            .flatMap { box -> w?.let { box?.split(w, wRange, includeLeft) { copy(w = it) } } ?: listOf(box) }
    }

    private fun split(
        line: Long,
        range: LongRange,
        includeLeft: Boolean,
        corner: Position.(Long) -> Position,
    ): List<Box?> = when {
        line == range.first ->
            if (includeLeft) {
                listOf(Box(minCorner, maxCorner.corner(line)), Box(minCorner.corner(line + 1), maxCorner))
            } else {
                listOf(null, this)
            }
        line == range.last ->
            if (includeLeft) {
                listOf(this, null)
            } else {
                listOf(Box(minCorner, maxCorner.corner(line - 1)), Box(minCorner.corner(line), maxCorner))
            }
        line < range.first -> listOf(null, this)
        line > range.last -> listOf(this, null)
        includeLeft -> listOf(Box(minCorner, maxCorner.corner(line)), Box(minCorner.corner(line + 1), maxCorner))
        else -> listOf(Box(minCorner, maxCorner.corner(line - 1)), Box(minCorner.corner(line), maxCorner))
    }

    fun toSet(): Set<Position> = xRange.flatMap { x ->
        yRange.flatMap { y ->
            zRange.flatMap { z ->
                wRange.map { w ->
                    Position(x, y, z, w)
                }
            }
        }
    }.toSet()

    fun distanceToCentre() = distanceTo(Position.CENTRE)

    fun distanceTo(position: Position): Long =
        (position.x.takeIf { it !in xRange }?.let { minOf(abs(xRange.first - it), abs(xRange.last - it)) } ?: 0) +
        (position.y.takeIf { it !in yRange }?.let { minOf(abs(yRange.first - it), abs(yRange.last - it)) } ?: 0) +
        (position.z.takeIf { it !in zRange }?.let { minOf(abs(zRange.first - it), abs(zRange.last - it)) } ?: 0) +
        (position.w.takeIf { it !in wRange }?.let { minOf(abs(wRange.first - it), abs(wRange.last - it)) } ?: 0)

    fun distanceTo(other: Box): Long =
        (xRange distanceTo other.xRange) +
        (yRange distanceTo other.yRange) +
        (zRange distanceTo other.zRange) +
        (wRange distanceTo other.wRange)
}

infix fun Position.box(position: Position) = Box(this, position)