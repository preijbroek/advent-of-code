package util

import java.util.Comparator.comparing
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

data class Position
@JvmOverloads
constructor(val x: Long, val y: Long, val z: Long = 0, val w: Long = 0) {

    @JvmOverloads
    constructor(x: Int, y: Int, z: Int = 0, w: Int = 0) : this(x.toLong(), y.toLong(), z.toLong(), w.toLong())

    fun sortedNeighbours(): List<Position> {
        return listOf(south(), west(), east(), north())
    }

    fun neighbours(): List<Position> {
        return listOf(north(), east(), south(), west())
    }

    fun withNeighbours(): List<Position> {
        return neighbours() + this
    }

    fun allNeighbours(): List<Position> {
        return listOf(
            north(), north().east(), east(), south().east(),
            south(), south().west(), west(), north().west()
        )
    }

    fun withAllNeighbours(): List<Position> {
        return listOf(
            south().west(), south(), south().east(),
            west(), this, east(),
            north().west(), north(), north().east()
        )
    }

    fun neighbours3d(): List<Position> {
        return listOf(north(), east(), south(), west(), plusZ(), minZ())
    }

    fun allNeighbours3d(): List<Position> {
        val neighbours = listOf(this.minX(), this, this.plusX())
            .flatMap { listOf(it.minY(), it, it.plusY()) }
            .flatMap { listOf(it.minZ(), it, it.plusZ()) }
            .toMutableSet()
        neighbours.remove(this)
        return neighbours.toList()
    }

    fun allNeighbours4d(): List<Position> {
        val neighbours = listOf(this.minX(), this, this.plusX())
            .flatMap { listOf(it.minY(), it, it.plusY()) }
            .flatMap { listOf(it.minZ(), it, it.plusZ()) }
            .flatMap { listOf(it.minW(), it, it.plusW()) }
            .toMutableSet()
        neighbours.remove(this)
        return neighbours.toList()
    }

    fun north(dY: Int): Position {
        return north(dY.toLong())
    }

    @JvmOverloads
    fun north(dY: Long = 1L): Position {
        return plusY(dY)
    }

    fun east(dX: Int): Position {
        return east(dX.toLong())
    }

    @JvmOverloads
    fun east(dX: Long = 1L): Position {
        return plusX(dX)
    }

    @JvmOverloads
    fun south(dY: Int): Position {
        return south(dY.toLong())
    }


    @JvmOverloads
    fun south(dY: Long = 1): Position {
        return minY(dY)
    }

    @JvmOverloads
    fun west(dX: Int): Position {
        return west(dX)
    }

    @JvmOverloads
    fun west(dX: Long = 1L): Position {
        return minX(dX)
    }

    @JvmOverloads
    fun forward(dZ: Int = 1): Position {
        return plusZ(dZ)
    }

    @JvmOverloads
    fun backward(dZ: Int = 1): Position {
        return minZ(dZ)
    }

    fun plus(dX: Int = 0, dY: Int = 0, dZ: Int = 0, dW: Int = 0) =
        Position(x = x + dX, y = y + dY, z = z + dZ, w = w + dW)

    fun plusX(dX: Int) = copy(x = x + dX)

    @JvmOverloads
    fun plusX(dX: Long = 1L) = copy(x = x + dX)

    fun plusY(dY: Int) = copy(y = y + dY)

    @JvmOverloads
    fun plusY(dY: Long = 1L) = copy(y = y + dY)

    fun plusZ(dZ: Int) = copy(z = z + dZ)

    @JvmOverloads
    fun plusZ(dZ: Long = 1L) = copy(z = z + dZ)

    fun plusW(dW: Int) = copy(w = w + dW)

    @JvmOverloads
    fun plusW(dW: Long = 1L) = copy(w = w + dW)

    fun minX(dX: Int) = copy(x = x - dX)

    @JvmOverloads
    fun minX(dX: Long = 1L) = copy(x = x - dX)

    fun minY(dY: Int) = copy(y = y - dY)

    @JvmOverloads
    fun minY(dY: Long = 1L) = copy(y = y - dY)

    fun minZ(dZ: Int) = copy(z = z - dZ)

    @JvmOverloads
    fun minZ(dZ: Long = 1L) = copy(z = z - dZ)

    fun minW(dW: Int) = copy(w = w - dW)

    @JvmOverloads
    fun minW(dW: Long = 1L) = copy(w = w - dW)

    fun to(direction: Direction, delta: Int): Position {
        return direction.move(this, delta)
    }

    @JvmOverloads
    fun to(direction: Direction, delta: Long = 1L): Position {
        return direction.move(this, delta)
    }

    fun to(movement: Movement, delta: Int): Position {
        return movement.move(this, delta)
    }

    @JvmOverloads
    fun to(movement: Movement, delta: Long = 1L): Position {
        return movement.move(this, delta)
    }

    @JvmOverloads
    fun toFlipped(direction: Direction, delta: Int = 1): Position {
        return direction.moveFlipped(this, delta)
    }

    infix fun distanceTo(p: Position): Long {
        return abs(x - p.x) + abs(y - p.y) + abs(z - p.z) + abs(w - p.w)
    }

    fun distanceToCentre(): Long {
        return distanceTo(CENTRE)
    }

    fun add(p: Position): Position {
        return Position(x + p.x, y + p.y, z + p.z, w + p.w)
    }

    fun subtract(p: Position): Position {
        return Position(x - p.x, y - p.y, z - p.z, w - p.w)
    }

    fun isInRange(minX: Int, maxX: Int, minY: Int, maxY: Int): Boolean {
        return x in minX..maxX && y in minY..maxY
    }

    fun isInRange(minX: Int, maxX: Int, minY: Int, maxY: Int, minZ: Int, maxZ: Int): Boolean {
        return x in minX..maxX && y in minY..maxY && z in minZ..maxZ
    }

    override fun toString(): String {
        return "[$x,$y,$z,$w]"
    }

    operator fun rangeTo(other: Position): Set<Position> {
        return (min(x, other.x)..max(x, other.x)).flatMap { x ->
            (min(y, other.y)..max(y, other.y)).flatMap { y ->
                (min(z, other.z)..max(z, other.z)).flatMap { z ->
                    (min(w, other.w)..max(w, other.w)).map { w ->
                        Position(x, y, z, w)
                    }
                }
            }
        }.toSet()
    }

    fun range2d(distance: Int): Set<Position> {
        return (-distance..distance).flatMap { x ->
            ((-distance + x.abs())..(distance - x.abs())).map { y ->
                Position(this.x + x, this.y + y)
            }
        }.toSet()
    }

    infix fun determinant2d(other: Position): Long {
        return this.x * other.y - other.x * this.y
    }

    companion object {

        @JvmField
        val CENTRE = Position(0, 0)

        @JvmStatic
        fun byX(): Comparator<Position> {
            return comparing(Position::x)
        }

        @JvmStatic
        fun byY(): Comparator<Position> {
            return comparing(Position::y)
        }

        @JvmStatic
        fun byZ(): Comparator<Position> {
            return comparing(Position::z)
        }

        @JvmStatic
        fun byW(): Comparator<Position> {
            return comparing(Position::w)
        }
    }
}

operator fun Position.times(position: Position): Position {
    return Position(x * position.x, y * position.y, z * position.z, w * position.w)
}

operator fun Position.plus(position: Position): Position {
    return this.add(position)
}

operator fun Position.plusAssign(position: Position) {
    add(position)
}

operator fun Position.times(int: Int): Position {
    return times(int.toLong())
}

operator fun Position.times(long: Long): Position {
    return Position(this.x * long, this.y * long, this.z * long, this.w * long)
}

operator fun Int.times(position: Position): Position {
    return toLong().times(position)
}

operator fun Long.times(position: Position): Position {
    return position.times(this)
}

operator fun Position.minus(position: Position): Position = subtract(position)

operator fun Position.minusAssign(position: Position) {
    subtract(position)
}

operator fun Position.div(int: Int): Position {
    return div(int.toLong())
}

operator fun Position.div(long: Long): Position {
    return Position(this.x / long, this.y / long, this.z / long, this.w / long)
}

operator fun Position.rem(position: Position): Position {
    require(position.x > 0) { "x must be bigger than 0" }
    require(position.y > 0) { "y must be bigger than 0" }
    return Position(
        x = x modulo position.x,
        y = y modulo position.y,
        z = if (position.z > 0) z modulo position.z else z,
        w = if (position.w > 0) w modulo position.w else w,
    )
}

operator fun Position.rem(i: Int): Position {
    require(i > 0) { "integer must be bigger than 0" }
    return Position(
        x = x modulo i,
        y = y modulo i,
        z = z modulo i,
        w = w modulo i,
    )
}

fun Position.normalize(): Position {
    val gcm = x gcd y
    return Position((x / gcm).abs(), (y / gcm).abs())
}

fun Position.asBox() = this box this