package util

import java.io.File

fun getInputForYear(year: Int, day: Int) = getInputFor("year-$year", "day${day}exercise")

fun getInputFor(project: String, resource: String) = file(project, resource).readLines(Charsets.UTF_8)

fun getInputBlocksForYear(year: Int, day: Int) = getInputBlocksFor("year-$year", "day${day}exercise")

fun getInputBlocksFor(project: String, resource: String) = file(project, resource).readText(Charsets.UTF_8).split("\n\n")

private fun file(item: String, resource: String) = File("$item/src/main/resources/$resource.csv")