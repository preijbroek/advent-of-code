package util

import java.math.BigDecimal

operator fun Boolean.plus(i: Int) = i + this

operator fun Int.plus(boolean: Boolean) = this + boolean.toInt()

operator fun Boolean.plus(i: Long): Long = i + this

operator fun Long.plus(boolean: Boolean): Long = this + boolean.toLong()

operator fun Boolean.plus(i: BigDecimal): BigDecimal = i + this

operator fun BigDecimal.plus(boolean: Boolean): BigDecimal = this + boolean.toBigDecimal()

operator fun Boolean.minus(i: Int) = i - this

operator fun Int.minus(boolean: Boolean) = this - boolean.toInt()

operator fun Boolean.minus(i: Long): Long = i - this

operator fun Long.minus(boolean: Boolean): Long = this - boolean.toLong()

operator fun Boolean.minus(i: BigDecimal): BigDecimal = i - this

operator fun BigDecimal.minus(boolean: Boolean): BigDecimal = this - boolean.toBigDecimal()

operator fun Boolean.times(i: Int) = i * this

operator fun Int.times(boolean: Boolean) = if (boolean) this else 0

operator fun Boolean.times(i: Long): Long = i * this

operator fun Long.times(boolean: Boolean): Long = if (boolean) this else 0L

operator fun Boolean.times(i: BigDecimal): BigDecimal = i * this

operator fun BigDecimal.times(boolean: Boolean): BigDecimal = if (boolean) this else BigDecimal.ZERO

fun Boolean.toDigitChar() = times(1).digitToChar()

fun Boolean.toInt() = if (this) 1 else 0

fun Boolean.toSignedInt() = if (this) 1 else -1

fun Boolean.toLong() = if (this) 1L else 0L

fun Boolean.toBigDecimal(): BigDecimal = if (this) BigDecimal.ONE else BigDecimal.ZERO

fun Int.toBoolean() = if (this > 0) true else false

fun Long.toBoolean() = if (this > 0L) true else false

fun BigDecimal.toBoolean(): Boolean = if (this > BigDecimal.ZERO) true else false
