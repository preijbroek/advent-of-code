package util

import java.util.function.BiFunction

enum class Direction(
    internal val move: BiFunction<Position, Long, Position>,
    private val moveFlipped: BiFunction<Position, Long, Position>
) {
    EAST(
        BiFunction { obj: Position, dX: Long -> obj.east(dX) },
        BiFunction { obj: Position, dX: Long -> obj.east(dX) }
    ),
    SOUTH(
        BiFunction { obj: Position, dY: Long -> obj.south(dY) },
        BiFunction { obj: Position, dY: Long -> obj.north(dY) }
    ),
    WEST(
        BiFunction { obj: Position, dY: Long -> obj.west(dY) },
        BiFunction { obj: Position, dY: Long -> obj.west(dY) }
    ),
    NORTH(
        BiFunction { obj: Position, dY: Long -> obj.north(dY) },
        BiFunction { obj: Position, dY: Long -> obj.south(dY) }
    );

    fun move(position: Position, amount: Int): Position {
        return move(position, amount.toLong())
    }

    @JvmOverloads
    fun move(position: Position, amount: Long = 1L): Position {
        return move.apply(position, amount)
    }

    fun moveFlipped(position: Position, amount: Int): Position {
        return moveFlipped(position, amount.toLong())
    }

    @JvmOverloads
    fun moveFlipped(position: Position, amount: Long = 1L): Position {
        return moveFlipped.apply(position, amount)
    }

    fun left() = cachedValues[(this.ordinal - 1).mod(cachedValues.size)]

    fun right() = cachedValues[(this.ordinal + 1).mod(cachedValues.size)]

    fun back() = cachedValues[(this.ordinal + 2).mod(cachedValues.size)]

    companion object {
        private val cachedValues = entries.toTypedArray()
    }
}

enum class Movement(val direction: Direction) {
    RIGHT(Direction.EAST),
    DOWN(Direction.NORTH),
    LEFT(Direction.WEST),
    UP(Direction.SOUTH),
    ;

    fun left() = cachedValues[(this.ordinal - 1).mod(cachedValues.size)]

    fun right() = cachedValues[(this.ordinal + 1).mod(cachedValues.size)]

    fun back() = cachedValues[(this.ordinal + 2).mod(cachedValues.size)]

    fun move(position: Position, amount: Int): Position {
        return move(position, amount.toLong())
    }

    @JvmOverloads
    fun move(position: Position, amount: Long = 1L): Position {
        return direction.move.apply(position, amount)
    }

    companion object {
        private val cachedValues = entries.toTypedArray()
    }
}