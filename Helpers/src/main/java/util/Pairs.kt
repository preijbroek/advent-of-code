package util

fun <T, R> Pair<T, T>.map(function: (T) -> R): Pair<R, R> {
    return function(first) to function(second)
}

infix fun <T, R, S> Pair<T, R>.triple(s: S) = Triple(this.first, this.second, s)

infix fun <T, R, S> S.triple(pair: Pair<T, R>) = Triple(this, pair.first, pair.second)