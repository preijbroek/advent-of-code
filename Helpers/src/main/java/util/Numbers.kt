package util

import java.math.BigInteger

infix fun Int.gcd(int: Int): Int = if (int == 0) this else if (this == 0) int else int.gcd(this % int)

fun Int.abs() = if (this < 0) -this else this

fun Long.abs() = if (this < 0L) -this else this

infix fun Long.gcd(long: Long): Long = if (long == 0L) this else long.gcd(this % long)

infix fun Long.lcm(long: Long) = this / this.gcd(long) * long

infix fun Int.modulo(int: Int): Int {
    val result = this % int
    return if (result < 0) {
        result + int
    } else {
        result
    }
}

infix fun Long.modulo(l: Long): Long {
    val result = this % l
    return if (result < 0) {
        result + l
    } else {
        result
    }
}

infix fun Long.modulo(i: Int): Int {
    val result = this % i.toLong()
    return if (result < 0) {
        result + i
    } else {
        result
    }.toInt()
}

infix fun Int.nonZeroMod(int: Int): Int {
    val result = this % int
    return if (result <= 0) {
        result + int
    } else {
        result
    }
}

infix fun Long.nonZeroMod(long: Long): Long {
    val result = this % long
    return if (result <= 0) {
        result + long
    } else {
        result
    }
}

infix fun Long.nonZeroMod(int: Int): Int {
    val result = this % int.toLong()
    return if (result <= 0) {
        result + int
    } else {
        result
    }.toInt()
}

infix fun Long.modInverse(l: Long): Long {
    return BigInteger.valueOf(this).modInverse(BigInteger.valueOf(l)).toLong()
}

infix fun Int.pow(n: Int): Int {
    return if (n < 0) {
        throw IllegalArgumentException(n.toString())
    } else if (n == 0) {
        1
    } else {
        this * (this pow (n - 1))
    }
}

infix fun Long.pow(n: Int): Long {
    return if (n < 0) {
        throw IllegalArgumentException(n.toString())
    } else if (n == 0) {
        1
    } else {
        this * (this pow (n - 1))
    }
}

fun Int.isEven() = (this and 1) == 0

fun Int.isOdd() = !isEven()

fun Long.isEven() = (this and 1L) == 0L

fun Long.isOdd() = !isEven()