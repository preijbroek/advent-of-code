package util

data class DecimalPosition(
    val x: Decimal,
    val y: Decimal,
    val z: Decimal = Decimal.ZERO,
    val w: Decimal = Decimal.ZERO,
) {

    @JvmOverloads
    constructor(x: Long, y: Long, z: Long = 0L, w: Long = 0L) :
            this(x.toDecimal(), y.toDecimal(), z.toDecimal(), w.toDecimal())

    @JvmOverloads
    constructor(x: Int, y: Int, z: Int = 0, w: Int = 0) :
            this(x.toDecimal(), y.toDecimal(), z.toDecimal(), w.toDecimal())
}

operator fun DecimalPosition.plus(other: DecimalPosition): DecimalPosition {
    return DecimalPosition(x = x + other.x, y = y + other.y, z = z + other.z, w = w + other.w)
}

operator fun DecimalPosition.times(long: Long): DecimalPosition {
    return DecimalPosition(x = long * x, y = long * y, z = long * z, w = long * w)
}

operator fun Long.times(position: DecimalPosition): DecimalPosition {
    return position * this
}

operator fun DecimalPosition.times(decimal: Decimal): DecimalPosition {
    return DecimalPosition(x = decimal * x, y = decimal * y, z = decimal * z, w = decimal * w)
}

operator fun Decimal.times(position: DecimalPosition): DecimalPosition {
    return position * this
}