package util

import util.Decimal.Companion.ONE
import util.Decimal.Companion.ZERO
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode

private const val calculationScale = 34

sealed class Decimal : Number(), Comparable<Decimal> {

    companion object {
        val POSITIVE_INFINITY: Decimal = PositiveInfinityDecimal
        val NEGATIVE_INFINITY: Decimal = NegativeInfinityDecimal
        val NaN: Decimal = NaNDecimal
        val ZERO: Decimal = RegularDecimal(BigDecimal.ZERO)
        val ONE: Decimal = RegularDecimal(BigDecimal.ONE)
        val TWO: Decimal = RegularDecimal(BigDecimal(2))
    }

    override fun toByte(): Byte {
        throw UnsupportedOperationException("toByte()")
    }

    override fun toDouble(): Double {
        throw UnsupportedOperationException("toShort()")
    }

    override fun toFloat(): Float {
        throw UnsupportedOperationException("toFloat()")
    }

    override fun toInt(): Int {
        throw UnsupportedOperationException("toInt()")
    }

    override fun toLong(): Long {
        throw UnsupportedOperationException("toLong()")
    }

    override fun toShort(): Short {
        throw UnsupportedOperationException("toShort()")
    }

    fun toBigDecimal(): BigDecimal {
        throw UnsupportedOperationException("toBigDecimal()")
    }

    open fun integerString(): String {
        throw UnsupportedOperationException("integerString()")
    }

    open fun asString(scale: Int = calculationScale, roundingMode: RoundingMode = RoundingMode.HALF_EVEN): String {
        throw UnsupportedOperationException("asString()")
    }

    open fun isInteger(): Boolean = false
}

internal object PositiveInfinityDecimal : Decimal() {

    override fun compareTo(other: Decimal): Int {
        return when (other) {
            is PositiveInfinityDecimal -> 0
            is NegativeInfinityDecimal -> 1
            is NaNDecimal -> -1
            is RegularDecimal -> 1
        }
    }

    override fun toDouble(): Double {
        return Double.POSITIVE_INFINITY
    }

    override fun toFloat(): Float {
        return Float.POSITIVE_INFINITY
    }
}

internal object NegativeInfinityDecimal : Decimal() {

    override fun compareTo(other: Decimal): Int {
        return when (other) {
            is PositiveInfinityDecimal -> -1
            is NegativeInfinityDecimal -> 0
            is NaNDecimal -> -1
            is RegularDecimal -> -1
        }
    }

    override fun toDouble(): Double {
        return Double.NEGATIVE_INFINITY
    }

    override fun toFloat(): Float {
        return Float.NEGATIVE_INFINITY
    }
}

internal object NaNDecimal : Decimal() {

    override fun equals(other: Any?) = false

    override fun compareTo(other: Decimal): Int {
        return when(other) {
            is PositiveInfinityDecimal -> 1
            is NegativeInfinityDecimal -> 1
            is NaNDecimal -> 0
            is RegularDecimal -> 1
        }
    }

    override fun toDouble(): Double {
        return Double.NaN
    }

    override fun toFloat(): Float {
        return Float.NaN
    }
}

internal class RegularDecimal(value: BigDecimal) : Decimal() {

    val value: BigDecimal = value.setScale(calculationScale, RoundingMode.DOWN)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as RegularDecimal
        return (value == other.value)
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun compareTo(other: Decimal): Int {
        return when (other) {
            is PositiveInfinityDecimal -> -1
            is NegativeInfinityDecimal -> 1
            is NaNDecimal -> -1
            is RegularDecimal -> value.compareTo(other.value)
        }
    }

    override fun toByte(): Byte {
        return value.toByte()
    }

    override fun toDouble(): Double {
        return value.toDouble()
    }

    override fun toFloat(): Float {
        return value.toFloat()
    }

    override fun toInt(): Int {
        return value.toInt()
    }

    override fun toLong(): Long {
        return value.toLong()
    }

    override fun toShort(): Short {
        return value.toShort()
    }

    override fun integerString(): String = asString(scale = 0)

    override fun asString(scale: Int, roundingMode: RoundingMode): String =
        value.setScale(scale, roundingMode).toString()

    override fun isInteger(): Boolean = this == floor()
}

fun Decimal.isNaN() = this is NaNDecimal

fun Decimal.isNotNaN() = !isNaN()

fun Decimal.sqrt(): Decimal = when (this) {
    is PositiveInfinityDecimal -> PositiveInfinityDecimal
    is NegativeInfinityDecimal -> throw ArithmeticException("Attempted square root of negative infinity")
    is NaNDecimal -> NaNDecimal
    is RegularDecimal -> if (value.signum() == -1) {
        throw ArithmeticException("Attempted square root of negative Decimal")
    } else {
        RegularDecimal(value.sqrt(MathContext(calculationScale, RoundingMode.HALF_EVEN)))
    }
}

fun Decimal.floor(): Decimal = when (this) {
    is PositiveInfinityDecimal -> PositiveInfinityDecimal
    is NegativeInfinityDecimal -> NegativeInfinityDecimal
    is NaNDecimal -> NaNDecimal
    is RegularDecimal -> RegularDecimal(value.setScale(0, RoundingMode.DOWN))
}

fun Decimal.floorUp(): Decimal = floor().let { it + (this == it) }

fun Decimal.floorDown(): Decimal = floor().let { it - (this == it) }

fun Decimal.ceil(): Decimal = when (this) {
    is PositiveInfinityDecimal -> PositiveInfinityDecimal
    is NegativeInfinityDecimal -> NegativeInfinityDecimal
    is NaNDecimal -> NaNDecimal
    is RegularDecimal -> RegularDecimal(value.setScale(0, RoundingMode.UP))
}

fun Decimal.ceilUp(): Decimal = ceil().let { it + (this == it) }

fun Decimal.ceilDown(): Decimal = ceil().let { it - (this == it) }

fun Decimal.pow(n: Int): Decimal {
    if (n < 0) throw ArithmeticException("Negative powers not supported for Decimal")
    return when (this) {
        is PositiveInfinityDecimal -> if (n == 0) ONE else PositiveInfinityDecimal
        is NegativeInfinityDecimal -> throw ArithmeticException("Powers of negative infinity not supported for Decimal")
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> RegularDecimal(value.pow(n))
    }
}

operator fun Decimal.unaryPlus(): Decimal = this

operator fun Decimal.unaryMinus(): Decimal = when (this) {
    is PositiveInfinityDecimal -> PositiveInfinityDecimal
    is NegativeInfinityDecimal -> NegativeInfinityDecimal
    is NaNDecimal -> NaNDecimal
    is RegularDecimal -> RegularDecimal(-value)
}

operator fun Decimal.inc(): Decimal = when (this) {
    is PositiveInfinityDecimal -> PositiveInfinityDecimal
    is NegativeInfinityDecimal -> NegativeInfinityDecimal
    is NaNDecimal -> throw ArithmeticException("NaN cannot be incremented")
    is RegularDecimal -> RegularDecimal(value + BigDecimal.ONE)
}

operator fun Decimal.dec(): Decimal = when (this) {
    is PositiveInfinityDecimal -> PositiveInfinityDecimal
    is NegativeInfinityDecimal -> NegativeInfinityDecimal
    is NaNDecimal -> throw ArithmeticException("NaN cannot be decremented")
    is RegularDecimal -> RegularDecimal(value - BigDecimal.ONE)
}

operator fun Decimal.plus(other: Decimal): Decimal = when (this) {
    is PositiveInfinityDecimal -> when (other) {
        is PositiveInfinityDecimal -> PositiveInfinityDecimal
        is NegativeInfinityDecimal -> NaNDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> PositiveInfinityDecimal
    }
    is NegativeInfinityDecimal -> when (other) {
        is PositiveInfinityDecimal -> NaNDecimal
        is NegativeInfinityDecimal -> NegativeInfinityDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> NegativeInfinityDecimal
    }
    is NaNDecimal -> NaNDecimal
    is RegularDecimal -> when (other) {
        is PositiveInfinityDecimal -> PositiveInfinityDecimal
        is NegativeInfinityDecimal -> NegativeInfinityDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> RegularDecimal(value + other.value)
    }
}

operator fun Decimal.plus(int: Int): Decimal = this + int.toDecimal()

operator fun Int.plus(d: Decimal): Decimal = d + this

operator fun Decimal.plus(long: Long): Decimal = this + long.toDecimal()

operator fun Long.plus(d: Decimal): Decimal = d + this

operator fun Decimal.plus(double: Double): Decimal = this + double.toDecimal()

operator fun Double.plus(d: Decimal): Decimal = d + this

operator fun Decimal.plus(boolean: Boolean): Decimal = if (boolean) this + ONE else this

operator fun Boolean.plus(d: Decimal): Decimal = d + this

operator fun Decimal.minus(other: Decimal): Decimal = when (this) {
    is PositiveInfinityDecimal -> when (other) {
        is PositiveInfinityDecimal -> NaNDecimal
        is NegativeInfinityDecimal -> PositiveInfinityDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> PositiveInfinityDecimal
    }
    is NegativeInfinityDecimal -> when (other) {
        is PositiveInfinityDecimal -> NegativeInfinityDecimal
        is NegativeInfinityDecimal -> NaNDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> NegativeInfinityDecimal
    }
    is NaNDecimal -> NaNDecimal
    is RegularDecimal -> when (other) {
        is PositiveInfinityDecimal -> NegativeInfinityDecimal
        is NegativeInfinityDecimal -> PositiveInfinityDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> RegularDecimal(value - other.value)
    }
}

operator fun Decimal.minus(int: Int): Decimal = this - int.toDecimal()

operator fun Int.minus(d: Decimal): Decimal = d - this

operator fun Decimal.minus(long: Long): Decimal = this - long.toDecimal()

operator fun Long.minus(d: Decimal): Decimal = d - this

operator fun Decimal.minus(double: Double): Decimal = this - double.toDecimal()

operator fun Double.minus(d: Decimal): Decimal = d - this

operator fun Decimal.minus(boolean: Boolean): Decimal = if (boolean) this - ONE else this

operator fun Boolean.minus(d: Decimal): Decimal = d - this

operator fun Decimal.times(other: Decimal): Decimal = when (this) {
    is PositiveInfinityDecimal -> when (other) {
        is PositiveInfinityDecimal -> PositiveInfinityDecimal
        is NegativeInfinityDecimal -> NegativeInfinityDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> when (other.value.signum()) {
            -1 -> NegativeInfinityDecimal
            0 -> NaNDecimal
            1 -> PositiveInfinityDecimal
            else -> willNeverReach()
        }
    }
    is NegativeInfinityDecimal -> when (other) {
        is PositiveInfinityDecimal -> NegativeInfinityDecimal
        is NegativeInfinityDecimal -> PositiveInfinityDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> when (other.value.signum()) {
            -1 -> PositiveInfinityDecimal
            0 -> NaNDecimal
            1 -> NegativeInfinityDecimal
            else -> willNeverReach()
        }
    }
    is NaNDecimal -> NaNDecimal
    is RegularDecimal -> when (other) {
        is PositiveInfinityDecimal -> when (value.signum()) {
            -1 -> NegativeInfinityDecimal
            0 -> NaNDecimal
            1 -> PositiveInfinityDecimal
            else -> willNeverReach()
        }
        is NegativeInfinityDecimal -> when (value.signum()) {
            -1 -> PositiveInfinityDecimal
            0 -> NaNDecimal
            1 -> NegativeInfinityDecimal
            else -> willNeverReach()
        }
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> RegularDecimal(value * other.value)
    }
}

operator fun Decimal.times(int: Int): Decimal = this * int.toDecimal()

operator fun Int.times(d: Decimal): Decimal = d * this

operator fun Decimal.times(long: Long): Decimal = this * long.toDecimal()

operator fun Long.times(d: Decimal): Decimal = d * this

operator fun Decimal.times(double: Double): Decimal = this * double.toDecimal()

operator fun Double.times(d: Decimal): Decimal = d * this

operator fun Decimal.times(boolean: Boolean): Decimal = when (this) {
    is PositiveInfinityDecimal -> if (boolean) this else NaNDecimal
    is NegativeInfinityDecimal -> if (boolean) this else NaNDecimal
    is NaNDecimal -> NaNDecimal
    is RegularDecimal -> if (boolean) this else ZERO
}

operator fun Boolean.times(d: Decimal): Decimal = d * this

operator fun Decimal.div(other: Decimal): Decimal = when (this) {
    is PositiveInfinityDecimal -> when (other) {
        is PositiveInfinityDecimal -> NaNDecimal
        is NegativeInfinityDecimal -> NaNDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> when (other.value.signum()) {
            -1 -> NegativeInfinityDecimal
            0 -> PositiveInfinityDecimal
            1 -> PositiveInfinityDecimal
            else -> willNeverReach()
        }
    }
    is NegativeInfinityDecimal -> when (other) {
        is PositiveInfinityDecimal -> NaNDecimal
        is NegativeInfinityDecimal -> NaNDecimal
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> when (other.value.signum()) {
            -1 -> PositiveInfinityDecimal
            0 -> NegativeInfinityDecimal
            1 -> NegativeInfinityDecimal
            else -> willNeverReach()
        }
    }
    is NaNDecimal -> NaNDecimal
    is RegularDecimal -> when (other) {
        is PositiveInfinityDecimal -> ZERO
        is NegativeInfinityDecimal -> ZERO
        is NaNDecimal -> NaNDecimal
        is RegularDecimal -> if (other.value.signum() == 0) {
            when (value.signum()) {
                -1 -> NegativeInfinityDecimal
                0 -> NaNDecimal
                1 -> PositiveInfinityDecimal
                else -> willNeverReach()
            }
        } else {
            RegularDecimal(value / other.value)
        }
    }
}

operator fun Decimal.div(int: Int): Decimal = this / int.toDecimal()

operator fun Int.div(d: Decimal): Decimal = d / this

operator fun Decimal.div(long: Long): Decimal = this / long.toDecimal()

operator fun Long.div(d: Decimal): Decimal = d / this

operator fun Decimal.div(double: Double): Decimal = this / double.toDecimal()

operator fun Double.div(d: Decimal): Decimal = d / this

fun BigDecimal.toDecimal(): Decimal = RegularDecimal(this)

fun String.toDecimal() = toBigDecimal().toDecimal()

fun Int.toDecimal() = toBigDecimal().toDecimal()

fun Long.toDecimal() = toBigDecimal().toDecimal()

fun Float.toDecimal() = toBigDecimal().toDecimal()

fun Double.toDecimal() = toBigDecimal().toDecimal()

fun Boolean.toDecimal() = if (this) ONE else ZERO

private fun willNeverReach(): Nothing {
    throw IllegalStateException("Will never reach here")
}

operator fun Decimal.rangeTo(other: Decimal) = DecimalRange(this, other)

class DecimalRange(
    override val start: Decimal,
    override val endInclusive: Decimal,
    private val progression: Decimal = ONE
) : ClosedRange<Decimal>, Iterable<Decimal> {

    val first = start
    val last = endInclusive

    override fun isEmpty() = first > last

    override fun iterator() = DecimalIterator(first, last, progression)
}

class DecimalIterator(first: Decimal, last: Decimal, private val step: Decimal) : Iterator<Decimal> {
    init {
        require(step != ZERO) { "Iterator can't work with step ZERO" }
    }

    private val increases = step > ZERO
    private val finalElement: Decimal = if (increases) last else first
    private var hasNext: Boolean = if (step > ZERO) first <= last else first >= last
    private var next: Decimal = if (hasNext) first else finalElement

    override fun hasNext(): Boolean = hasNext

    override fun next(): Decimal {
        val value = next
        if (!hasNext || (increases && value > finalElement) || (!increases && value < finalElement)) {
            throw kotlin.NoSuchElementException()
        } else if (value == finalElement) {
            hasNext = false
        } else {
            next += step
        }
        return value
    }
}
