package advent.day23;

import advent.InputProvider;
import advent.RegisterComputer;

import java.util.List;
import java.util.stream.IntStream;

public final class Day23Exercise {

    private Day23Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(23);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        RegisterComputer computer = new RegisterComputer(input);
        computer.runSingle();
        System.out.println(computer.getHasRunMulCommand());
    }

    private static void part2(final List<String> input) {
        int h = 0;
        for(int b = 106700; b <= 123700; b += 17) {
            if (!isPrime(b)) h++;
        }
        System.out.println(h);
    }

    private static boolean isPrime(final int i) {
        int sqrt = (int) Math.sqrt(i);
        return IntStream.rangeClosed(2, sqrt).noneMatch(j -> i % j == 0) && i != 1;
    }

}
