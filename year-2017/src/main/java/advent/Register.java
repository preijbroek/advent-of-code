package advent;

import java.util.Objects;

public final class Register {

    private final String id;
    private long value;

    public Register(final String id) {
        this.id = id;
        this.value = 0;
    }

    public void addValue(final long value) {
        this.value += value;
    }

    public void addValue(final Register register) {
        addValue(register.value);
    }

    public void subtractValue(final long value) {
        this.value -= value;
    }

    public void subtractValue(final Register register) {
        subtractValue(register.value);
    }

    public void multiplyValue(final long value) {
        this.value *= value;
    }

    public void multiplyValue(final Register register) {
        multiplyValue(register.value);
    }

    public long getValue() {
        return value;
    }

    public void setValue(final long value) {
        this.value = value;
    }

    public void setValue(final Register register) {
        setValue(register.value);
    }

    public void modulo(final long value) {
        this.value %= value;
    }

    public void modulo(final Register register) {
        modulo(register.value);
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Register register = (Register) o;
        return Objects.equals(id, register.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
