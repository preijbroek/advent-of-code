package advent.day2;

import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

import static util.Comparators.maxOf;
import static util.Comparators.minOf;

public final class SpreadSheet<DATA extends Comparable<DATA>> {

    private final List<List<DATA>> data;

    public SpreadSheet(final List<List<DATA>> data) {
        this.data = data;
    }

    public DATA max(final int row) {
        return maxOf(data.get(row));
    }

    public DATA min(final int row) {
        return minOf(data.get(row));
    }

    public DATA getAnyMatching(final int row,
                               final BiPredicate<DATA, DATA> predicate,
                               final BiFunction<DATA, DATA, DATA> biFunction) {
        return streamRow(row).map(r1 ->
                streamRow(row).filter(r2 -> !r1.equals(r2))
                        .filter(r2 -> predicate.test(r1, r2))
                        .map(r2 -> biFunction.apply(r1, r2))
                        .findAny()
                        .orElse(null))
                .filter(Objects::nonNull)
                .findAny()
                .orElseThrow(IllegalStateException::new);
    }

    private Stream<DATA> streamRow(final int line) {
        return data.get(line).stream();
    }

    public int rows() {
        return data.size();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpreadSheet<?> spreadSheet = (SpreadSheet<?>) o;
        return Objects.equals(data, spreadSheet.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }
}
