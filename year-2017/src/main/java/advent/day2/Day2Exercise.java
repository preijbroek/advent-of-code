package advent.day2;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public final class Day2Exercise {

    private Day2Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(2);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<List<Integer>> data = input.stream()
                .map(line ->
                        Arrays.stream(line.split("\\s+"))
                                .map(Integer::parseInt)
                                .collect(toList()))
                .collect(toList());
        SpreadSheet<Integer> spreadSheet = new SpreadSheet<>(data);
        System.out.println(IntStream.range(0, spreadSheet.rows())
                .map(i -> spreadSheet.max(i) - spreadSheet.min(i))
                .sum());
    }

    private static void part2(final List<String> input) {
        List<List<Integer>> data = input.stream()
                .map(line ->
                        Arrays.stream(line.split("\\s+"))
                                .map(Integer::parseInt)
                                .collect(toList()))
                .collect(toList());
        SpreadSheet<Integer> spreadSheet = new SpreadSheet<>(data);
        System.out.println(IntStream.range(0, spreadSheet.rows())
                .map(i -> spreadSheet.getAnyMatching(i, (n, m) -> (n % m) == 0, (n, m) -> n / m))
                .sum());
    }

}
