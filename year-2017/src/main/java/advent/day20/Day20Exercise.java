package advent.day20;

import advent.InputProvider;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static util.Comparators.minOf;

public final class Day20Exercise {

    private Day20Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(20);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<Particle> particles = input.stream()
                .map(Particle::from)
                .collect(Collectors.toList());
        int minA = minOf(particles, Particle::totalAcceleration).totalAcceleration();
        List<Particle> lowestAccelerations = particles.stream()
                .filter(particle -> particle.totalAcceleration() == minA)
                .collect(Collectors.toList());
        if (lowestAccelerations.size() == 1) {
            System.out.println(particles.indexOf(lowestAccelerations.get(0)));
        } else if (lowestAccelerations.isEmpty()) {
            throw new IllegalStateException("No input has been provided!");
        } else {
            while (lowestAccelerations.stream().anyMatch(particle -> !particle.hasPassedBeyondZero())) {
                lowestAccelerations.forEach(Particle::prolong);
            }
            Particle closestParticle = minOf(lowestAccelerations,
                    Comparator.comparing(Particle::totalAcceleration)
                            .thenComparing(Particle::totalVelocity)
                            .thenComparing(Particle::totalPosition));
            System.out.println(particles.indexOf(closestParticle));
        }
    }

    private static void part2(final List<String> input) {
        List<Particle> particles = input.stream()
                .map(Particle::from)
                .collect(Collectors.toCollection(ArrayList::new));
        removeCollisions(particles);
        while (mayHaveFutureCollisions(particles)) {
            particles.forEach(Particle::prolong);
            removeCollisions(particles);
        }
        System.out.println(particles.size());
    }

    private static boolean mayHaveFutureCollisions(final List<Particle> particles) {
        return particles.stream()
                .anyMatch(particle ->
                        !particles.stream()
                                .filter(particle1 -> particle1 != particle)
                                .allMatch(particle::willNeverReach)
                );
    }

    private static void removeCollisions(final List<Particle> particles) {
        new ArrayList<>(particles)
                .forEach(particle -> new ArrayList<>(particles)
                        .stream()
                        .filter(particle1 -> particle1 != particle)
                        .forEach(particle1 -> {
                            if (particle.hasSamePosition(particle1)) {
                                particles.remove(particle);
                                particles.remove(particle1);
                            }
                        })
                );
    }

}
