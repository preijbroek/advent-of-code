package advent.day20;

import util.Position;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class Particle {

    private final Position acceleration;
    private Position velocity;
    private Position position;

    public Particle(final Position acceleration, final Position velocity, final Position position) {
        this.acceleration = acceleration;
        this.velocity = velocity;
        this.position = position;
    }

    public static Particle from(final String s) {
        String[] parameters = s.split(",\\s");
        List<Position> params = Arrays.stream(parameters)
                .map(parameter -> parameter.substring(3, parameter.length() - 1).split(","))
                .map(array -> new Position(Integer.parseInt(array[0]), Integer.parseInt(array[1]), Integer.parseInt(array[2])))
                .collect(Collectors.toList());
        return new Particle(params.get(2), params.get(1), params.get(0));
    }

    public long totalAcceleration() {
        return acceleration.distanceToCentre();
    }

    public long totalVelocity() {
        return velocity.distanceToCentre();
    }

    public long totalPosition() {
        return position.distanceToCentre();
    }

    public boolean hasPassedBeyondZero() {
        return areAllBelowOrAboveZero(acceleration.getX(), velocity.getX(), position.getX())
                && areAllBelowOrAboveZero(acceleration.getY(), velocity.getY(), position.getY())
                && areAllBelowOrAboveZero(acceleration.getZ(), velocity.getZ(), position.getZ());
    }

    private static boolean areAllBelowOrAboveZero(final long f1, final long f2, final long f3) {
        return (f1 >= 0 && f2 >= 0 && f3 >= 0) || (f1 <= 0 && f2 <= 0 && f3 <= 0);
    }

    public void prolong() {
        velocity = velocity.add(acceleration);
        position = position.add(velocity);
    }

    public boolean hasSamePosition(final Particle particle) {
        return Objects.equals(this.position, particle.position);
    }

    public boolean willNeverReach(final Particle particle) {
        return isFurtherThan(this, particle) || isFurtherThan(particle, this);
    }

    private static boolean isFurtherThan(final Particle thisParticle, final Particle otherParticle) {
        return (thisParticle.acceleration.getX() >= otherParticle.acceleration.getX()
                && thisParticle.velocity.getX() >= otherParticle.velocity.getX()
                && thisParticle.position.getX() >= otherParticle.position.getX())
                || (thisParticle.acceleration.getY() >= otherParticle.acceleration.getY()
                && thisParticle.velocity.getY() >= otherParticle.velocity.getY()
                && thisParticle.position.getY() >= otherParticle.position.getY())
                || (thisParticle.acceleration.getZ() >= otherParticle.acceleration.getZ()
                && thisParticle.velocity.getZ() >= otherParticle.velocity.getZ()
                && thisParticle.position.getZ() >= otherParticle.position.getZ());
    }


    @Override
    public String toString() {
        return "Particle{" +
                "acceleration=" + acceleration +
                ", velocity=" + velocity +
                ", position=" + position +
                '}';
    }
}
