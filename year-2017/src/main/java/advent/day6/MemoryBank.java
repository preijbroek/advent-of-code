package advent.day6;

public final class MemoryBank {

    private int blocks;

    public MemoryBank(final int blocks) {
        this.blocks = blocks;
    }

    public void addBlock() {
        blocks++;
    }

    public void clear() {
        blocks = 0;
    }

    public int getBlocks() {
        return blocks;
    }
}
