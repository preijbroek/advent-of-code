package advent.day6;

import advent.InputProvider;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public final class Day6Exercise {

    private Day6Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(6).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        List<MemoryBank> banks = Arrays.stream(input.split("\\s+"))
                .map(Integer::parseInt)
                .map(MemoryBank::new)
                .collect(Collectors.toList());
        Set<List<Integer>> memory = new HashSet<>();
        List<Integer> currentState = banks.stream()
                .map(MemoryBank::getBlocks)
                .collect(Collectors.toList());
        int steps = 0;
        while (!memory.contains(currentState)) {
            memory.add(currentState);
            currentState = nextState(banks);
            steps++;
        }
        System.out.println(steps);
    }

    private static void part2(final String input) {
        List<MemoryBank> banks = Arrays.stream(input.split("\\s+"))
                .map(Integer::parseInt)
                .map(MemoryBank::new)
                .collect(Collectors.toList());
        Map<List<Integer>, Integer> memory = new HashMap<>();
        List<Integer> currentState = banks.stream()
                .map(MemoryBank::getBlocks)
                .collect(Collectors.toList());
        int steps = 0;
        while (!memory.containsKey(currentState)) {
            memory.put(currentState, steps);
            currentState = nextState(banks);
            steps++;
        }
        System.out.println(steps - memory.get(currentState));
    }

    private static List<Integer> nextState(final List<MemoryBank> banks) {
        MemoryBank max = banks.stream().max(Comparator.comparing(MemoryBank::getBlocks)).orElseThrow(IllegalStateException::new);
        int blocks = max.getBlocks();
        max.clear();
        int bankToReceive = (banks.indexOf(max) + 1) % banks.size();
        for (int i = 0; i < blocks; i++) {
            banks.get(bankToReceive).addBlock();
            bankToReceive = (bankToReceive + 1) % banks.size();
        }
        return banks.stream().map(MemoryBank::getBlocks).collect(Collectors.toList());
    }

}
