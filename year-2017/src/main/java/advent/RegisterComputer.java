package advent;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public final class RegisterComputer {

    private final List<String> input;
    private final List<Register> registers;
    private final Queue<Long> outputs = new ArrayDeque<>();
    private final Queue<Long> inputs = new ArrayDeque<>();

    private boolean canRun = true;
    private int timesAValueIsSent = 0;
    private int position = 0;
    private int hasRunMulCommand = 0;

    public RegisterComputer(final List<String> input) {
        this.input = input;
        this.registers = input.stream()
                .map(s -> s.split(" ")[1])
                .map(Register::new)
                .distinct()
                .collect(Collectors.toList());
    }

    public void runSingle() {
        long lastSound = 0;
        while (position < input.size() && position >= 0) {
            String[] instruction = input.get(position).split(" ");
            Register register = getRegister(instruction[1], registers);
            if ("set".equals(instruction[0])) {
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.setValue(getRegister(instruction[2], registers));
                } else {
                    register.setValue(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("mul".equals(instruction[0])) {
                hasRunMulCommand++;
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.multiplyValue(getRegister(instruction[2], registers));
                } else {
                    register.multiplyValue(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("jgz".equals(instruction[0])) {
                long checkValue = Character.isLetter(instruction[1].charAt(0))
                        ? getRegister(instruction[1], registers).getValue()
                        : Long.parseLong(instruction[1]);
                long moveValue = Character.isLetter(instruction[2].charAt(0))
                        ? getRegister(instruction[2], registers).getValue()
                        : Long.parseLong(instruction[2]);
                if (checkValue > 0) {
                    position += moveValue;
                } else {
                    position++;
                }
            } else if ("jnz".equals(instruction[0])) {
                long checkValue = Character.isLetter(instruction[1].charAt(0))
                        ? getRegister(instruction[1], registers).getValue()
                        : Long.parseLong(instruction[1]);
                long moveValue = Character.isLetter(instruction[2].charAt(0))
                        ? getRegister(instruction[2], registers).getValue()
                        : Long.parseLong(instruction[2]);
                if (checkValue != 0) {
                    position += moveValue;
                } else {
                    position++;
                }
            } else if ("add".equals(instruction[0])) {
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.addValue(getRegister(instruction[2], registers));
                } else {
                    register.addValue(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("sub".equals(instruction[0])) {
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.subtractValue(getRegister(instruction[2], registers));
                } else {
                    register.subtractValue(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("mod".equals(instruction[0])) {
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.modulo(getRegister(instruction[2], registers));
                } else {
                    register.modulo(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("snd".equals(instruction[0])) {
                lastSound = getRegister(instruction[1], registers).getValue();
                position++;
            } else if ("rcv".equals(instruction[0])) {
                if (register.getValue() != 0) {
                    System.out.println(lastSound);
                    break;
                }
                position++;
            } else {
                throw new IllegalArgumentException(input.get(position));
            }
        }
    }

    public void runMultiple() {
        while (position < input.size() && position >= 0) {
            String[] instruction = input.get(position).split(" ");
            Register register = getRegister(instruction[1], registers);
            if ("set".equals(instruction[0])) {
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.setValue(getRegister(instruction[2], registers));
                } else {
                    register.setValue(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("mul".equals(instruction[0])) {
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.multiplyValue(getRegister(instruction[2], registers));
                } else {
                    register.multiplyValue(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("jgz".equals(instruction[0])) {
                long checkValue = Character.isLetter(instruction[1].charAt(0))
                        ? getRegister(instruction[1], registers).getValue()
                        : Long.parseLong(instruction[1]);
                long moveValue = Character.isLetter(instruction[2].charAt(0))
                        ? getRegister(instruction[2], registers).getValue()
                        : Long.parseLong(instruction[2]);
                if (checkValue > 0) {
                    position += moveValue;
                } else {
                    position++;
                }
            } else if ("jnz".equals(instruction[0])) {
                long checkValue = Character.isLetter(instruction[1].charAt(0))
                        ? getRegister(instruction[1], registers).getValue()
                        : Long.parseLong(instruction[1]);
                long moveValue = Character.isLetter(instruction[2].charAt(0))
                        ? getRegister(instruction[2], registers).getValue()
                        : Long.parseLong(instruction[2]);
                if (checkValue != 0) {
                    position += moveValue;
                } else {
                    position++;
                }
            } else if ("add".equals(instruction[0])) {
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.addValue(getRegister(instruction[2], registers));
                } else {
                    register.addValue(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("sub".equals(instruction[0])) {
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.subtractValue(getRegister(instruction[2], registers));
                } else {
                    register.subtractValue(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("mod".equals(instruction[0])) {
                if (Character.isLetter(instruction[2].charAt(0))) {
                    register.modulo(getRegister(instruction[2], registers));
                } else {
                    register.modulo(Integer.parseInt(instruction[2]));
                }
                position++;
            } else if ("snd".equals(instruction[0])) {
                outputs.offer(getRegister(instruction[1], registers).getValue());
                position++;
                timesAValueIsSent++;
            } else if ("rcv".equals(instruction[0])) {
                if (!hasInputs()) {
                    canRun = false;
                    break;
                }
                getRegister(instruction[1], registers).setValue(inputs.poll());
                position++;
            } else {
                throw new IllegalArgumentException(input.get(position));
            }
        }
        canRun = false;
    }

    private static Register getRegister(final String id, final List<Register> registers) {
        return registers.stream()
                .filter(register -> id.equals(register.getId()))
                .findAny()
                .orElse(null);
    }

    public boolean hasInputs() {
        return !inputs.isEmpty();
    }

    public void receiveInputs(final Collection<Long> inputs) {
        this.inputs.addAll(inputs);
        if (hasInputs()) {
            this.canRun = true;
        }
    }

    public Queue<Long> sendOutputs() {
        return outputs;
    }

    public void clearOutputs() {
        outputs.clear();
    }

    public void setRegisterP(final int value) {
        getRegister("p", registers).setValue(value);
    }

    public boolean canRun() {
        return canRun;
    }

    public int getTimesAValueIsSent() {
        return timesAValueIsSent;
    }

    public int getHasRunMulCommand() {
        return hasRunMulCommand;
    }

    public void setRegister(final String register, final int value) {
        getRegister(register, registers).setValue(value);
    }

    public Register getRegister(final String id) {
        return getRegister(id, registers);
    }
}
