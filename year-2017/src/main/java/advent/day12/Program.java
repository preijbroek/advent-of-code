package advent.day12;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public final class Program {

    private final int id;
    private final Set<Integer> connections;

    public Program(final int id, final Collection<Integer> connections) {
        this.id = id;
        this.connections = new HashSet<>(connections);
    }

    public int getId() {
        return id;
    }

    public Set<Integer> getConnections() {
        return connections;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Program program = (Program) o;
        return id == program.id &&
                Objects.equals(connections, program.connections);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, connections);
    }
}
