package advent.day12;

import advent.InputProvider;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static util.Comparators.minOf;

public final class Day12Exercise {

    private Day12Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(12);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<Program> programs = input.stream()
                .map(s -> s.split(" <-> "))
                .map(array ->
                        new Program(Integer.parseInt(array[0]),
                                Arrays.stream(array[1].split(", "))
                                        .map(Integer::parseInt)
                                        .collect(Collectors.toSet())))
                .collect(Collectors.toList());
        System.out.println(allOfGroup(programs, 0).size());
    }

    private static void part2(final List<String> input) {
        List<Program> programs = input.stream()
                .map(s -> s.split(" <-> "))
                .map(array ->
                        new Program(Integer.parseInt(array[0]),
                                Arrays.stream(array[1].split(", "))
                                        .map(Integer::parseInt)
                                        .collect(Collectors.toSet())))
                .collect(Collectors.toList());
        Set<Integer> ids = programs.stream().map(Program::getId).collect(Collectors.toCollection(HashSet::new));
        int groups = 0;
        while (!ids.isEmpty()) {
            Integer minId = minOf(ids);
            ids.removeAll(allOfGroup(programs, minId));
            groups++;
        }
        System.out.println(groups);
    }

    private static Set<Integer> allOfGroup(final List<Program> programs, final int programId) {
        Set<Integer> connectedIds = new HashSet<>();
        connectedIds.add(programId);
        connectedIds.addAll(programs.get(programId).getConnections());
        int previousSize = 1;
        while (previousSize < connectedIds.size()) {
            previousSize = connectedIds.size();
            new HashSet<>(connectedIds).forEach(id -> connectedIds.addAll(programs.get(id).getConnections()));
        }
        return connectedIds;
    }

}
