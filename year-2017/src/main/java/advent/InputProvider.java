package advent;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class InputProvider {

    private InputProvider() {
    }

    public static List<String> getInput(final int day) {
        try {
            return Files.readAllLines(Paths.get("year-2017/src/main/resources/day" + day + "exercise.csv"));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.valueOf(day), e);
        }
    }
}