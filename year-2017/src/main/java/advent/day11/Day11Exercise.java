package advent.day11;

import advent.InputProvider;

public final class Day11Exercise {

    private Day11Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(11).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        HexagonalPosition startPosition = new HexagonalPosition(0, 0);
        HexagonalPosition currentPosition = startPosition;
        for (String s : input.split(",")) {
            currentPosition = currentPosition.to(s);
        }
        System.out.println(currentPosition.distanceTo(startPosition));
    }

    private static void part2(final String input) {
        HexagonalPosition startPosition = new HexagonalPosition(0, 0);
        HexagonalPosition currentPosition = startPosition;
        int maxDistance = 0;
        for (String s : input.split(",")) {
            currentPosition = currentPosition.to(s);
            maxDistance = Math.max(maxDistance, currentPosition.distanceTo(startPosition));
        }
        System.out.println(maxDistance);
    }

}
