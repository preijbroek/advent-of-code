package advent.day11;

import java.util.Objects;

public final class HexagonalPosition {

    private final int x;
    private final int y;

    public HexagonalPosition(final int x, final int y) {
        if (x % 3 != 0) {
            throw new IllegalArgumentException("y must be divisible by 3!");
        }
        this.x = x;
        this.y = y;
    }

    public HexagonalPosition north() {
        return new HexagonalPosition(x, y + 2);
    }

    public HexagonalPosition northEast() {
        return new HexagonalPosition(x + 3, y + 1);
    }

    public HexagonalPosition southEast() {
        return new HexagonalPosition(x + 3, y - 1);
    }

    public HexagonalPosition south() {
        return new HexagonalPosition(x, y - 2);
    }

    public HexagonalPosition southWest() {
        return new HexagonalPosition(x - 3, y - 1);
    }

    public HexagonalPosition northWest() {
        return new HexagonalPosition(x - 3, y + 1);
    }

    public HexagonalPosition to(final String s) {
        switch (s) {
            case "n": return north();
            case "ne": return northEast();
            case "se": return southEast();
            case "s": return south();
            case "sw": return southWest();
            case "nw": return northWest();
            default: throw new IllegalArgumentException(s);
        }
    }

    public int distanceTo(final HexagonalPosition hp) {
        int xDistance = Math.abs((this.x - hp.x) / 3);
        return xDistance + Math.max(0, (Math.abs(this.y - hp.y) - xDistance) >> 1);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HexagonalPosition hexagonalPosition = (HexagonalPosition) o;
        return x == hexagonalPosition.x &&
                y == hexagonalPosition.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
