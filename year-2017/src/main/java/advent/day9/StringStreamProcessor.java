package advent.day9;

public final class StringStreamProcessor {

    private StringStreamProcessor() {
    }

    public static int groupScore(final String s) {
        String filtered = removeGarbage(s);
        int groupScore = 0;
        int currentScore = 0;
        for (int i = 0; i < filtered.length(); i++) {
            if (filtered.charAt(i) == '{') {
                currentScore++;
            } else if (filtered.charAt(i) == '}') {
                groupScore += currentScore;
                currentScore--;
            }
        }
        return groupScore;
    }

    public static int removedGarbageAmount(final String s) {
        String filtered = exclamationMarksFiltered(s);
        int removedGarbageAmount = 0;
        boolean inGarbage = false;
        int index = 0;
        while (index < filtered.length()) {
            if (inGarbage) {
                if (filtered.charAt(index) == '>') {
                    inGarbage = false;
                } else {
                    removedGarbageAmount++;
                }
            } else if (filtered.charAt(index) == '<') {
                inGarbage = true;
            }
            index++;
        }
        return removedGarbageAmount;
    }

    private static String removeGarbage(final String s) {
        String filtered = exclamationMarksFiltered(s);
        StringBuilder result = new StringBuilder();
        int index = 0;
        boolean inGarbage = false;
        while (index < filtered.length()) {
            if (index == 0) {
                result.append(filtered.charAt(index));
            } else if (!inGarbage && (filtered.charAt(index) != '<')) {
                result.append(filtered.charAt(index));
            } else if (!inGarbage && filtered.charAt(index) == '<') {
                inGarbage = true;
            } else if (inGarbage && filtered.charAt(index) == '>') {
                inGarbage = false;
            }
            index++;
        }
        return result.toString();
    }

    private static String exclamationMarksFiltered(final String s) {
        StringBuilder result = new StringBuilder();
        int index = 0;
        while (index < s.length()) {
            if (s.charAt(index) != '!') {
                result.append(s.charAt(index));
                index++;
            } else {
                index += 2;
            }
        }
        return result.toString();
    }

}
