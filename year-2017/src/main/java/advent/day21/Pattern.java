package advent.day21;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Pattern {

    private final Set<Configuration> configurations;

    Pattern(final Set<Configuration> configurations) {
        this.configurations = configurations;
    }

    public static Pattern from(final String s) {
        Configuration configuration = new Configuration(Arrays.asList(s.split("/")));
        Set<Configuration> configurations = Stream.of(configuration, configuration.rotate(), configuration.rotate().rotate(),
                configuration.rotate().rotate().rotate(), configuration.mirror(),
                configuration.mirror().rotate(), configuration.mirror().rotate().rotate(),
                configuration.mirror().rotate().rotate().rotate())
                .collect(Collectors.toSet());
        return new Pattern(configurations);
    }

    public boolean contains(final Configuration configuration) {
        return configurations.contains(configuration);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pattern pattern = (Pattern) o;
        return Objects.equals(configurations, pattern.configurations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configurations);
    }

    @Override
    public String toString() {
        return "Pattern{" +
                "configurations=" + configurations +
                '}';
    }
}
