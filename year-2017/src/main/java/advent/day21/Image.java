package advent.day21;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public final class Image {

    private final List<String> strings;

    public Image(final List<String> strings) {
        this.strings = strings;
    }

    public Image enhance(final Map<Pattern, Configuration> patternConfigurationMap) {
        if ((strings.size() & 1) == 0) {
            return enhanceTwo(patternConfigurationMap);
        } else if (strings.size() % 3 == 0) {
            return enhanceThree(patternConfigurationMap);
        } else {
            throw new IllegalStateException(strings.toString());
        }
    }

    private Image enhanceTwo(final Map<Pattern, Configuration> patternConfigurationMap) {
        List<String> newImageStrings = new ArrayList<>();
        for (int i = 0; i < strings.size(); i += 2) {
            StringBuilder line1 = new StringBuilder();
            StringBuilder line2 = new StringBuilder();
            StringBuilder line3 = new StringBuilder();
            for (int j = 0; j < strings.size(); j += 2) {
                Pattern pattern = Pattern.from(
                        strings.get(i).substring(j, j + 2)
                                + "/"
                                + strings.get(i + 1).substring(j, j + 2)
                );
                List<String> lines = patternConfigurationMap.get(pattern).getLines();
                line1.append(lines.get(0));
                line2.append(lines.get(1));
                line3.append(lines.get(2));
            }
            newImageStrings.add(line1.toString());
            newImageStrings.add(line2.toString());
            newImageStrings.add(line3.toString());
        }
        return new Image(newImageStrings);
    }

    private Image enhanceThree(final Map<Pattern, Configuration> patternConfigurationMap) {
        List<String> newImageStrings = new ArrayList<>();
        for (int i = 0; i < strings.size(); i += 3) {
            StringBuilder line1 = new StringBuilder();
            StringBuilder line2 = new StringBuilder();
            StringBuilder line3 = new StringBuilder();
            StringBuilder line4 = new StringBuilder();
            for (int j = 0; j < strings.size(); j += 3) {
                Pattern pattern = Pattern.from(
                        strings.get(i).substring(j, j + 3)
                                + "/"
                                + strings.get(i + 1).substring(j, j + 3)
                                + "/"
                                + strings.get(i + 2).substring(j, j + 3)
                );
                List<String> lines = patternConfigurationMap.get(pattern).getLines();
                line1.append(lines.get(0));
                line2.append(lines.get(1));
                line3.append(lines.get(2));
                line4.append(lines.get(3));
            }
            newImageStrings.add(line1.toString());
            newImageStrings.add(line2.toString());
            newImageStrings.add(line3.toString());
            newImageStrings.add(line4.toString());
        }
        return new Image(newImageStrings);
    }

    public long countPixelsOn() {
        return strings.stream()
                .flatMapToInt(CharSequence::chars)
                .filter(i -> i == '#')
                .count();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return Objects.equals(strings, image.strings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(strings);
    }

    @Override
    public String toString() {
        return "Image{" +
                "strings=" + strings +
                '}';
    }
}
