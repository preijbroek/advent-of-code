package advent.day21;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class Configuration {

    private final List<String> lines;

    public Configuration(final List<String> lines) {
        this.lines = lines;
    }

    public static Configuration from(final String s) {
        return new Configuration(Arrays.stream(s.split("/")).collect(Collectors.toList()));
    }

    public Configuration mirror() {
        List<String> copy = new ArrayList<>(lines);
        Collections.reverse(copy);
        return new Configuration(copy);
    }

    public Configuration rotate() {
        char[][] chars = lines.stream().map(String::toCharArray).toArray(char[][]::new);
        int length = chars.length;
        char[][] rotatedChars = new char[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                rotatedChars[i][j] = chars[length - j - 1][i];
            }
        }
        List<String> rotated = Arrays.stream(rotatedChars)
                .map(String::new)
                .collect(Collectors.toList());
        return new Configuration(rotated);
    }

    public List<String> getLines() {
        return lines;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Configuration configuration = (Configuration) o;
        return Objects.equals(lines, configuration.lines);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lines);
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "lines=" + lines +
                '}';
    }
}
