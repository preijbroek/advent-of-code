package advent.day21;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class Day21Exercise {

    private Day21Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(21);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Map<Pattern, Configuration> patternConfigurationMap = input.stream()
                .map(s -> s.split(" => "))
                .collect(Collectors.toMap(strings -> Pattern.from(strings[0]),
                        strings -> Configuration.from(strings[1])));
        Image image = new Image(Arrays.asList(".#.", "..#", "###"));
        for (int i = 0; i < 5; i++) {
            image = image.enhance(patternConfigurationMap);
        }
        System.out.println(image.countPixelsOn());
    }

    private static void part2(final List<String> input) {
        Map<Pattern, Configuration> patternConfigurationMap = input.stream()
                .map(s -> s.split(" => "))
                .collect(Collectors.toMap(strings -> Pattern.from(strings[0]),
                        strings -> Configuration.from(strings[1])));
        Image image = new Image(Arrays.asList(".#.", "..#", "###"));
        for (int i = 0; i < 18; i++) {
            image = image.enhance(patternConfigurationMap);
        }
        System.out.println(image.countPixelsOn());
    }

}
