package advent.day13;

import advent.InputProvider;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public final class Day13Exercise {

    private Day13Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(13);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<Layer> layers = input.stream()
                .map(s -> s.split(": "))
                .map(array -> new Layer(Integer.parseInt(array[0]), Integer.parseInt(array[1])))
                .collect(Collectors.toList());
        int severity = layers.stream()
                .filter(layer -> layer.isAtZeroAtTime(layer.depth()))
                .mapToInt(Layer::severity)
                .sum();
        System.out.println(severity);
    }

    private static void part2(final List<String> input) {
        List<Layer> layers = input.stream()
                .map(s -> s.split(": "))
                .map(array -> new Layer(Integer.parseInt(array[0]), Integer.parseInt(array[1])))
                .collect(Collectors.toList());
        AtomicInteger index = new AtomicInteger(0);
        while (layers.stream().anyMatch(layer -> layer.isAtZeroAtTime(layer.depth() + index.get()))) {
            index.getAndIncrement();
        }
        System.out.println(index.get());
    }
}
