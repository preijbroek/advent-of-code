package advent.day13;

import java.util.Objects;

public final class Layer {

    private final int depth;
    private final int range;

    public Layer(final int depth, final int range) {
        this.depth = depth;
        this.range = range;
    }

    public boolean isAtZeroAtTime(final int t) {
        return t % (period()) == 0;
    }

    public int period() {
        return 2 * (range - 1);
    }

    public int depth() {
        return depth;
    }

    public int severity() {
        return depth * range;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Layer layer = (Layer) o;
        return depth == layer.depth &&
                range == layer.range;
    }

    @Override
    public int hashCode() {
        return Objects.hash(depth, range);
    }
}
