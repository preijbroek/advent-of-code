package advent.day5;

import advent.InputProvider;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toCollection;

public final class Day5Exercise {

    private Day5Exercise() {
    }

    public static void main(String[] args) {
        List<Integer> input = InputProvider.getInput(5).stream().map(Integer::parseInt).collect(toCollection(ArrayList::new));
        part1(input);
        part2(input);
    }

    private static void part1(final List<Integer> input) {
        List<Integer> inputCopy = new ArrayList<>(input);
        int position = 0;
        int steps = 0;
        while (position >= 0 && position < inputCopy.size()) {
            Integer jump = inputCopy.get(position);
            inputCopy.set(position, jump + 1);
            position += jump;
            steps++;
        }
        System.out.println(steps);
    }

    private static void part2(final List<Integer> input) {
        List<Integer> inputCopy = new ArrayList<>(input);
        int position = 0;
        int steps = 0;
        while (position >= 0 && position < inputCopy.size()) {
            Integer jump = inputCopy.get(position);
            inputCopy.set(position, jump >= 3 ? jump - 1 : jump + 1);
            position += jump;
            steps++;
        }
        System.out.println(steps);
    }

}
