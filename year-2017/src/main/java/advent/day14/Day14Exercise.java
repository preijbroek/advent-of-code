package advent.day14;

import advent.Hasher;
import advent.InputProvider;
import util.Position;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Day14Exercise {

    private Day14Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(14).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        int usedSpace = 0;
        for (int i = 0; i < 128; i++) {
            Hasher hasher = new Hasher(input + "-" + i);
            hasher.runFullHash();
            String hash = hasher.denseHash();
            usedSpace += IntStream.range(0, hash.length())
                    .mapToObj(hash::charAt)
                    .map(String::valueOf)
                    .mapToInt(s -> Integer.parseInt(s, 16))
                    .map(Integer::bitCount)
                    .sum();
        }
        System.out.println(usedSpace);
    }

    private static void part2(final String input) {
        Set<Position> usedSpacePositions = new HashSet<>();
        for (int i = 0; i < 128; i++) {
            Hasher hasher = new Hasher(input + "-" + i);
            hasher.runFullHash();
            String hash = hasher.denseHash();
            String row = IntStream.range(0, hash.length())
                    .mapToObj(hash::charAt)
                    .map(String::valueOf)
                    .mapToInt(s -> Integer.parseInt(s, 16))
                    .mapToObj(Integer::toBinaryString)
                    .map(s -> ("0000" + s).substring(s.length()))
                    .collect(Collectors.joining());
            int finalI = i;
            IntStream.range(0, row.length())
                    .filter(cPos -> row.charAt(cPos) == '1')
                    .forEach(cPos -> usedSpacePositions.add(new Position(cPos, finalI)));
        }
        int regions = 0;
        while (!usedSpacePositions.isEmpty()) {
            Position next = usedSpacePositions.stream().findAny().orElseThrow(IllegalStateException::new);
            Set<Position> region = new HashSet<>();
            region.add(next);
            int regionSize = 0;
            while (regionSize != region.size()) {
                regionSize = region.size();
                Set<Position> additions = region.stream()
                        .flatMap(position -> position.neighbours().stream().filter(usedSpacePositions::contains))
                        .collect(Collectors.toSet());
                region.addAll(additions);
            }
            usedSpacePositions.removeAll(region);
            regions++;
        }
        System.out.println(regions);
    }

}
