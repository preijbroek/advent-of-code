package advent.day4;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;

public final class Day4Exercise {

    private Day4Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(4);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        System.out.println(input.stream()
                .map(line -> line.split("\\s+"))
                .filter(array -> array.length == Arrays.stream(array).distinct().count())
                .count());
    }

    private static void part2(final List<String> input) {
        System.out.println(input.stream()
                .map(line -> line.split("\\s+"))
                .filter(Day4Exercise::containsNoAnagram)
                .count());
    }

    private static boolean containsNoAnagram(final String... strings) {
        return Arrays.stream(strings)
                .noneMatch(s -> Arrays.stream(strings)
                        .filter(str -> s != str)
                        .anyMatch(str -> areAnagrams(str, s))
                );
    }

    private static boolean areAnagrams(final String str, final String s) {
        char[] charFromWord = str.toCharArray();
        char[] charFromAnagram = s.toCharArray();
        Arrays.sort(charFromWord);
        Arrays.sort(charFromAnagram);
        return Arrays.equals(charFromWord, charFromAnagram);
    }

}
