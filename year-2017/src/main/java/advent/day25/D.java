package advent.day25;

public final class D implements State {

    @Override
    public int valueToSetFor(final int currentValue) {
        return 1;
    }

    @Override
    public int positionForValue(final int currentValue, final int position) {
        return position - 1;
    }

    @Override
    public State nextStateForValue(final int currentValue) {
        return currentValue == 0 ? StateFactory.getE() : StateFactory.getF();
    }
}
