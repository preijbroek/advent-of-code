package advent.day25;

public final class B implements State {

    @Override
    public int valueToSetFor(final int currentValue) {
        return currentValue == 0 ? 0 : 1;
    }

    @Override
    public int positionForValue(final int currentValue, final int position) {
        return currentValue == 0 ? position + 1 : position - 1;
    }

    @Override
    public State nextStateForValue(final int currentValue) {
        return currentValue == 0 ? StateFactory.getC() : StateFactory.getB();
    }
}
