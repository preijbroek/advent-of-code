package advent.day25;

public final class StateFactory {

    private static final State A = new A();
    private static final State B = new B();
    private static final State C = new C();
    private static final State D = new D();
    private static final State E = new E();
    private static final State F = new F();

    private StateFactory() {
    }

    public static State getA() {
        return A;
    }

    public static State getB() {
        return B;
    }

    public static State getC() {
        return C;
    }

    public static State getD() {
        return D;
    }

    public static State getE() {
        return E;
    }

    public static State getF() {
        return F;
    }

}
