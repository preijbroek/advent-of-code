package advent.day25;

import java.util.HashMap;
import java.util.Map;

public class TuringMachine {

    private final Map<Integer, Integer> turingMachine = new HashMap<>();

    public void runMachine(int times) {
        turingMachine.clear();
        int position = 0;
        State currentState = StateFactory.getA();
        for (int i = 0; i < times; i++) {
            int currentValue = turingMachine.computeIfAbsent(position, n -> 0);
            turingMachine.put(position, currentState.valueToSetFor(currentValue));
            position = currentState.positionForValue(currentValue, position);
            currentState = currentState.nextStateForValue(currentValue);
        }
    }

    public long countOnes() {
        return turingMachine.values()
                .stream()
                .filter(value -> value == 1)
                .count();
    }
}
