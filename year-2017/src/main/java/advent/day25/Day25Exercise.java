package advent.day25;

import advent.InputProvider;

import java.util.List;

public final class Day25Exercise {

    private Day25Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(25);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        TuringMachine turingMachine = new TuringMachine();
        turingMachine.runMachine(12629077);
        System.out.println(turingMachine.countOnes());
    }

    private static void part2(final List<String> input) {

    }

}
