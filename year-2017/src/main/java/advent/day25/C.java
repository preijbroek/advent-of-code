package advent.day25;

public final class C implements State {

    @Override
    public int valueToSetFor(final int currentValue) {
        return currentValue == 0 ? 1 : 0;
    }

    @Override
    public int positionForValue(final int currentValue, final int position) {
        return currentValue == 0 ? position + 1 : position - 1;
    }

    @Override
    public State nextStateForValue(final int currentValue) {
        return currentValue == 0 ? StateFactory.getD() : StateFactory.getA();
    }
}
