package advent.day25;

public interface State {

    int valueToSetFor(final int currentValue);

    int positionForValue(final int currentValue, final int position);

    State nextStateForValue(final int currentValue);
}
