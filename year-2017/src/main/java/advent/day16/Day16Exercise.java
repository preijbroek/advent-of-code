package advent.day16;

import advent.InputProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Day16Exercise {

    private Day16Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(16).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        List<Character> dancingPrograms = dancingPrograms();
        Arrays.stream(input.split(","))
                .forEach(dance -> apply(dance, dancingPrograms));
        dancingPrograms.forEach(System.out::print);
        System.out.println();
    }

    private static void part2(final String input) {
        List<Character> dancingPrograms = dancingPrograms();
        Map<Character, Boolean> hasLooped = dancingPrograms.stream()
                .collect(Collectors.toMap(Function.identity(), c -> false));
        Map<Character, Integer> loops = new HashMap<>();
        int index = 0;
        while (!hasLooped.values().stream().allMatch(value -> value)) {
            Arrays.stream(input.split(","))
                    .forEach(dance -> apply(dance, dancingPrograms));
            index++;
            for (int i = 0; i < dancingPrograms.size(); i++) {
                Character c = dancingPrograms.get(i);
                if (c - 'a' == i && !hasLooped.get(c)) {
                    loops.put(c, index);
                    hasLooped.put(c, true);
                }
            }
        }
        int lcm = loops.values().stream()
                .mapToInt(i -> i)
                .reduce(1, Day16Exercise::lcm);
        int rounds = 1_000_000_000 % lcm;
        List<Character> lastDancingPrograms = dancingPrograms();
        for (int i = 0; i < rounds; i++) {
            Arrays.stream(input.split(","))
                    .forEach(dance -> apply(dance, lastDancingPrograms));
        }
        lastDancingPrograms.forEach(System.out::print);
    }

    private static List<Character> dancingPrograms() {
        return IntStream.rangeClosed(0, 15)
                .mapToObj(value -> (char) ('a' + value))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    private static void apply(final String dance, final List<Character> dancingPrograms) {
        char c = dance.charAt(0);
        if (c == 's') {
            for (int i = 0; i < Integer.parseInt(dance.substring(1)); i++) {
                dancingPrograms.add(0, dancingPrograms.remove(dancingPrograms.size() - 1));
            }
        } else if (c == 'x') {
            int splitter = dance.indexOf('/');
            int indexOfA = Integer.parseInt(dance.substring(1, splitter));
            char a = dancingPrograms.get(indexOfA);
            int indexOfB = Integer.parseInt(dance.substring(splitter + 1));
            char b = dancingPrograms.get(indexOfB);
            dancingPrograms.set(indexOfA, b);
            dancingPrograms.set(indexOfB, a);
        } else if (c == 'p') {
            char a = dance.charAt(1);
            int indexOfA = dancingPrograms.indexOf(a);
            char b = dance.charAt(3);
            int indexOfB = dancingPrograms.indexOf(b);
            dancingPrograms.set(indexOfA, b);
            dancingPrograms.set(indexOfB, a);
        } else {
            throw new IllegalArgumentException(dance);
        }
    }

    private static Integer lcm(final int m, final int n) {
        int max = Math.max(m, n);
        int min = Math.min(m, n);
        int lcm = max;
        while (lcm % min != 0) {
            lcm += max;
        }
        return max;
    }

}
