package advent.day3;

import advent.InputProvider;
import util.Direction;
import util.Position;

import java.util.HashMap;
import java.util.Map;

public final class Day3Exercise {

    private Day3Exercise() {
    }

    public static void main(String[] args) {
        int input = Integer.parseInt(InputProvider.getInput(3).get(0));
        part1(input);
        part2(input);
    }

    private static void part1(final int input) {
        int square = 1;
        int squaredValue = 1;
        while (squaredValue < input) {
            square += 2;
            squaredValue = square * square;
        }
        int difference = squaredValue - input;
        while (difference - (square - 1) > input) {
            squaredValue -= (square - 1);
            difference = squaredValue - input;
        }
        if (difference > square >> 1) {
            difference = square - difference;
        }
        System.out.println((square >> 1) + (square >> 1) - difference);
    }

    private static void part2(final int input) {
        Map<Position, Integer> circleSequence = new HashMap<>();
        circleSequence.put(Position.CENTRE, 1);
        circleSequence.put(new Position(1, 0), 1);
        Direction currentDirection = Direction.EAST;
        Position currentPosition = new Position(1, 0);
        while (circleSequence.values().stream().noneMatch(value -> value > input)) {
            if (!circleSequence.containsKey(currentPosition.to(currentDirection.left()))) {
                currentDirection = currentDirection.left();
            }
            currentPosition = currentPosition.to(currentDirection);
            int currentPositionValue = currentPosition.allNeighbours()
                    .stream()
                    .mapToInt(neighbour -> circleSequence.getOrDefault(neighbour, 0))
                    .sum();
            if (currentPositionValue > input) {
                System.out.println(currentPositionValue);
            }
            circleSequence.put(currentPosition, currentPositionValue);
        }
    }

}
