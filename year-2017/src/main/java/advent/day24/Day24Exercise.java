package advent.day24;

import advent.InputProvider;
import util.JavaPair;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

public final class Day24Exercise {

    private Day24Exercise() {
    }

    public static void main(final String[] args) {
        List<String> input = InputProvider.getInput(24);
        List<Bridge> pairs = input.stream()
                .map(s -> s.split("/"))
                .map(array -> Arrays.stream(array).mapToInt(Integer::parseInt).toArray())
                .map(intArray -> new JavaPair<>(intArray[0], intArray[1]))
                .map(Bridge::new)
                .collect(toList());
        Set<BridgeChain> possibleBridgeChains = findAllBridgeChains(pairs);
        part1(possibleBridgeChains);
        part2(possibleBridgeChains);
    }

    private static void part1(final Set<BridgeChain> possibleBridgeChains) {
        System.out.println(possibleBridgeChains.stream()
                .mapToInt(BridgeChain::strength)
                .max()
                .orElseThrow(IllegalStateException::new));
    }

    private static void part2(final Set<BridgeChain> possibleBridgeChains) {
        System.out.println(possibleBridgeChains.stream()
                .max(Comparator.comparing(BridgeChain::length).thenComparing(BridgeChain::strength))
                .map(BridgeChain::strength)
                .orElseThrow(IllegalStateException::new));
    }

    private static Set<BridgeChain> findAllBridgeChains(final List<Bridge> pairs) {
        Set<BridgeChain> possibleBridgeChains = new HashSet<>();
        pairs.stream()
                .filter(Bridge::isStartingBridge)
                .map(BridgeChain::new)
                .forEachOrdered(possibleBridgeChains::add);
        Set<BridgeChain> currentBridgeChains = new HashSet<>(possibleBridgeChains);
        while (!currentBridgeChains.isEmpty()) {
            Set<BridgeChain> newBridgeChains = new HashSet<>();
            for (BridgeChain bridgeChain : currentBridgeChains) {
                pairs.stream()
                        .filter(bridge -> !bridgeChain.contains(bridge))
                        .filter(bridge -> bridge.containsValue(bridgeChain.lastPort()))
                        .forEachOrdered(bridge -> {
                            BridgeChain newBridgeChain = new BridgeChain(bridgeChain);
                            newBridgeChain.add(bridge);
                            newBridgeChains.add(newBridgeChain);
                        });
            }
            currentBridgeChains.clear();
            currentBridgeChains.addAll(newBridgeChains);
            possibleBridgeChains.addAll(newBridgeChains);
        }
        return possibleBridgeChains;
    }
}
