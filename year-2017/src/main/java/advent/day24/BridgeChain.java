package advent.day24;

import util.JavaPair;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class BridgeChain {

    private final List<Bridge> bridges = new ArrayList<>();
    private final List<Integer> bridgeValues = new ArrayList<>();

    public BridgeChain(final Bridge bridge) {
        if (!bridge.isStartingBridge()) {
            throw new IllegalArgumentException(bridge.toString());
        }
        bridges.add(bridge);
        bridgeValues.addAll(bridge.orderOfConnectionBasedOn(new Bridge(new JavaPair<>(0, 0))));
    }

    public BridgeChain(final BridgeChain bridgeChain) {
        bridges.addAll(bridgeChain.bridges);
        bridgeValues.addAll(bridgeChain.bridgeValues);
    }

    public boolean contains(final Bridge bridge) {
        return bridges.contains(bridge);
    }

    public int lastPort() {
        return bridgeValues.get(bridgeValues.size() - 1);
    }

    public int strength() {
        return bridgeValues.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public int length() {
        return bridges.size();
    }

    public void add(final Bridge bridge) {
        Bridge lastAddedBridge = bridges.get(bridges.size() - 1);
        bridges.add(bridge);
        bridgeValues.addAll(bridge.orderOfConnectionBasedOn(lastAddedBridge));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BridgeChain bridgeChain = (BridgeChain) o;
        return Objects.equals(bridges, bridgeChain.bridges) &&
                Objects.equals(bridgeValues, bridgeChain.bridgeValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bridges, bridgeValues);
    }

    @Override
    public String toString() {
        return "BridgeChain{" +
                "bridges=" + bridges +
                ", bridgeValues=" + bridgeValues +
                '}';
    }
}
