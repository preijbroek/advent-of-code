package advent.day24;

import util.JavaPair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class Bridge {

    private final JavaPair<Integer> values;

    public Bridge(final JavaPair<Integer> values) {
        this.values = values;
    }

    public int strength() {
        return values.values()
                .stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public List<Integer> orderOfConnectionBasedOn(final Bridge previousBridge) {
        Integer first = values.values().stream()
                .filter(previousBridge.values::contains)
                .findAny()
                .orElseThrow(IllegalStateException::new);
        List<Integer> nextValues = new ArrayList<>(values.values());
        nextValues.remove(first);
        return Arrays.asList(first, nextValues.get(0));
    }

    public boolean isStartingBridge() {
        return values.contains(0);
    }

    public boolean containsValue(final int lastPort) {
        return values.contains(lastPort);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bridge bridge = (Bridge) o;
        return Objects.equals(values, bridge.values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(values);
    }

    @Override
    public String toString() {
        return "Bridge{" +
                "values=" + values +
                '}';
    }
}
