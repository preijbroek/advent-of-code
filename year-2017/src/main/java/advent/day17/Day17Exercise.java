package advent.day17;

import advent.InputProvider;

import java.util.ArrayList;
import java.util.List;

public final class Day17Exercise {

    private Day17Exercise() {
    }

    public static void main(String[] args) {
        int input = Integer.parseInt(InputProvider.getInput(17).get(0));
        part1(input);
        part2(input);
    }

    private static void part1(final int input) {
        List<Integer> values = new ArrayList<>(2018);
        values.add(0);
        int currentPosition = 0;
        for (int i = 1; i < 2018; i++) {
            int insertPosition = (currentPosition + input) % values.size() + 1;
            values.add(insertPosition, i);
            currentPosition = insertPosition;
        }
        System.out.println(values.get(values.indexOf(2017) + 1));
    }

    private static void part2(final int input) {
        int valueAfterZero = 0;
        int currentStep = 0;
        for (int i = 1; i <= 50_000_000; i++) {
            currentStep = ((currentStep + input) % i) + 1;
            if (currentStep == 1) {
                valueAfterZero = i;
            }
        }
        System.out.println(valueAfterZero);
    }

}
