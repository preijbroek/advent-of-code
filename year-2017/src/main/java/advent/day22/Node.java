package advent.day22;

import util.Position;

import java.util.Objects;

public class Node {

    private final Position position;
    private boolean isInfected;
    private State state;

    public Node(final Position position, final boolean isInfected) {
        this.position = position;
        this.isInfected = isInfected;
        this.state = isInfected ? State.INFECTED : State.CLEAN;
    }

    public void changeInfectionStatus() {
        this.isInfected = !this.isInfected;
        this.state = this.state.next();
    }

    public Position getPosition() {
        return position;
    }

    public boolean isInfected() {
        return isInfected;
    }

    public State state() {
        return state;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Objects.equals(position, node.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }

    public enum State {
        CLEAN,
        WEAKENED,
        INFECTED,
        FLAGGED;

        private static final State[] ALL_STATES = values();

        private State next() {
            return ALL_STATES[(ordinal() + 1) % values().length];
        }
    }
}
