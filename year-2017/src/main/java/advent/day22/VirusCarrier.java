package advent.day22;

import util.Direction;
import util.Position;

import java.util.Map;

public final class VirusCarrier {

    private final Map<Position, Node> infectedNodes;
    private Position currentPosition;
    private Direction currentDirection;

    public VirusCarrier(final Map<Position, Node> infectedNodes, final Position currentPosition, final Direction currentDirection) {
        this.infectedNodes = infectedNodes;
        this.currentPosition = currentPosition;
        this.currentDirection = currentDirection;
    }

    public int countInfectedNodesForBursts(final int bursts) {
        int infectedNodesNumber = 0;
        for (int i = 0; i < bursts; i++) {
            Node currentNode = infectedNodes.computeIfAbsent(currentPosition, position -> new Node(position, false));
            if (currentNode.isInfected()) {
                currentDirection = currentDirection.right();
            } else {
                currentDirection = currentDirection.left();
                infectedNodesNumber++;
            }
            currentNode.changeInfectionStatus();
            currentPosition = currentPosition.toFlipped(currentDirection);
        }
        return infectedNodesNumber;
    }

    public int countInfectedNodesWithMultipleStatesForBursts(final int bursts) {
        int infectedNodesNumber = 0;
        for (int i = 0; i < bursts; i++) {
            Node currentNode = infectedNodes.computeIfAbsent(currentPosition, position -> new Node(position, false));
            Node.State state = currentNode.state();
            switch (state) {
                case CLEAN:
                    currentDirection = currentDirection.left();
                    break;
                case WEAKENED:
                    infectedNodesNumber++;
                    break;
                case INFECTED:
                    currentDirection = currentDirection.right();
                    break;
                case FLAGGED:
                    currentDirection = currentDirection.back();
                    break;
                default:
                    throw new IllegalArgumentException(state.toString());
            }
            currentNode.changeInfectionStatus();
            currentPosition = currentPosition.toFlipped(currentDirection);
        }
        return infectedNodesNumber;
    }


}
