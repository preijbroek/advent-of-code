package advent.day22;

import advent.InputProvider;
import util.Direction;
import util.Position;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Day22Exercise {

    private Day22Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(22);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Map<Position, Node> infectedNodes = getPositionNodeMap(input);
        VirusCarrier virusCarrier = new VirusCarrier(infectedNodes, new Position(12, 12), Direction.NORTH);
        System.out.println(virusCarrier.countInfectedNodesForBursts(10_000));
    }

    private static void part2(final List<String> input) {
        Map<Position, Node> infectedNodes = getPositionNodeMap(input);
        VirusCarrier virusCarrier = new VirusCarrier(infectedNodes, new Position(12, 12), Direction.NORTH);
        System.out.println(virusCarrier.countInfectedNodesWithMultipleStatesForBursts(10_000_000));
    }

    private static Map<Position, Node> getPositionNodeMap(final List<String> input) {
        Map<Position, Node> infectedNodes = new HashMap<>();
        for (int i = 0; i < input.size(); i++) {
            for (int j = 0; j < input.get(i).length(); j++) {
                Position nodePosition = new Position(j, i);
                boolean isInfected = input.get(i).charAt(j) == '#';
                infectedNodes.put(nodePosition, new Node(nodePosition, isInfected));
            }
        }
        return infectedNodes;
    }

}
