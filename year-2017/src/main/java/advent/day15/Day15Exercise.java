package advent.day15;

import advent.InputProvider;

import java.util.List;
import java.util.stream.Collectors;

public final class Day15Exercise {

    private Day15Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(15);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<Integer> initialValues = input.stream()
                .map(s -> s.split(" ")[4])
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        Generator generatorA = new Generator(16807L, initialValues.get(0));
        Generator generatorB = new Generator(48271L, initialValues.get(1));
        int equalities = 0;
        for (int i = 0; i < 40_000_000; i++) {
            generatorA.createNextValue();
            generatorB.createNextValue();
            if (generatorA.hasSame16LowestBits(generatorB)) {
                equalities++;
            }
        }
        System.out.println(equalities);
    }

    private static void part2(final List<String> input) {
        List<Integer> initialValues = input.stream()
                .map(s -> s.split(" ")[4])
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        Generator generatorA = new Generator(16807L, initialValues.get(0)); // 3 % 4
        Generator generatorB = new Generator(48271L, initialValues.get(1)); // 7 % 8
        int equalities = 0;
        for (int i = 0; i < 5_000_000; i++) {
            generatorA.nextFourMultipleValue();
            generatorB.nextEightMultipleValue();
            if (generatorA.hasSame16LowestBits(generatorB)) {
                equalities++;
            }
        }
        System.out.println(equalities);
    }

}
