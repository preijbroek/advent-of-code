package advent.day15;

import java.util.Objects;
import java.util.stream.IntStream;

public final class Generator {

    private static final long MODULO = Integer.MAX_VALUE;
    private final long factor;
    private long value;

    public Generator(final long factor, final long initialValue) {
        this.factor = factor;
        this.value = initialValue;
    }

    public void createNextValue() {
        value = (value * factor) % MODULO;
    }

    public void nextFourMultipleValue() {
        do {
            createNextValue();
        } while ((value & 1) != 0 || (((value >> 1) & 1) != 0));
    }

    public void nextEightMultipleValue() {
        do {
            nextFourMultipleValue();
        } while (((value >> 2) & 1) != 0);
    }

    public boolean hasSame16LowestBits(final Generator generator) {
        return IntStream.rangeClosed(0, 15)
                .allMatch(i -> ((value >> i) & 1) == ((generator.value >> i) & 1));
    }

    public long getValue() {
        return value;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Generator generator = (Generator) o;
        return factor == generator.factor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(factor);
    }
}
