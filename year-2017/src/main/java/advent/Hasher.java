package advent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public final class Hasher {

    private final List<Integer> numbers = IntStream.range(0, 256)
            .boxed()
            .collect(Collectors.toCollection(ArrayList::new));
    private static final int SIZE = 256;
    private final List<Integer> lengths;

    private int currentPosition = 0;
    private int skipSize = 0;

    public Hasher(final List<Integer> lengths) {
        this.lengths = lengths;
    }

    public Hasher(final String input) {
        this.lengths = Stream.concat(input.chars().boxed(), Stream.of(17, 31, 73, 47, 23)).collect(toList());
    }

    public void runHash() {
        for (Integer length : lengths) {
            int end = (currentPosition + length) % SIZE;
            if (end < currentPosition) {
                List<Integer> toReverse = new ArrayList<>(numbers.subList(currentPosition, SIZE));
                toReverse.addAll(numbers.subList(0, length - (SIZE - currentPosition)));
                numbers.removeAll(toReverse);
                Collections.reverse(toReverse);
                numbers.addAll(0, toReverse.subList(SIZE - currentPosition, toReverse.size()));
                numbers.addAll(toReverse.subList(0, SIZE - currentPosition));
            } else {
                Collections.reverse(numbers.subList(currentPosition, end));
            }
            currentPosition = (currentPosition + length + skipSize) % SIZE;
            skipSize++;
        }
    }

    public void runFullHash() {
        for (int i = 0; i < 64; i++) {
            runHash();
        }
    }

    public int productOfFirstTwoElements() {
        return numbers.get(0) * numbers.get(1);
    }

    public String denseHash() {
        List<Integer> denseHashElements = new ArrayList<>(16);
        for (int i = 0; i < SIZE; i += 16) {
            int element = IntStream.range(i, i + 16)
                    .map(numbers::get)
                    .reduce((m, n) -> m ^ n)
                    .orElseThrow(IllegalStateException::new);
            denseHashElements.add(element);
        }
        return denseHashElements.stream()
                .map(Integer::toHexString)
                .map(s -> ("00" + s).substring(s.length()))
                .collect(Collectors.joining());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hasher hasher = (Hasher) o;
        return Objects.equals(lengths, hasher.lengths);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lengths);
    }
}
