package advent.day18;

import advent.InputProvider;
import advent.RegisterComputer;

import java.util.List;

public final class Day18Exercise {

    private Day18Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(18);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        RegisterComputer computer = new RegisterComputer(input);
        computer.runSingle();
    }

    private static void part2(final List<String> input) {
        RegisterComputer computerA = new RegisterComputer(input);
        RegisterComputer computerB = new RegisterComputer(input);
        computerB.setRegisterP(1);
        while (computerA.canRun() || computerB.canRun()) {
            computerA.runMultiple();
            computerB.receiveInputs(computerA.sendOutputs());
            computerA.clearOutputs();
            computerB.runMultiple();
            computerA.receiveInputs(computerB.sendOutputs());
            computerB.clearOutputs();
        }
        System.out.println(computerB.getTimesAValueIsSent());
    }

}
