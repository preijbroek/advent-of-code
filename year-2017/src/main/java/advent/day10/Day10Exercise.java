package advent.day10;

import advent.Hasher;
import advent.InputProvider;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class Day10Exercise {

    private Day10Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(10).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        List<Integer> lengths = Arrays.stream(input.split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        Hasher hasher = new Hasher(lengths);
        hasher.runHash();
        System.out.println(hasher.productOfFirstTwoElements());
    }

    private static void part2(final String input) {
        Hasher hasher = new Hasher(input);
        hasher.runFullHash();
        System.out.println(hasher.denseHash());
    }

}
