package advent.day7;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class Day7Exercise {

    private Day7Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(7);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Set<Tower> towers = input.stream()
                .map(s -> s.split(" -> ")[0].split(" "))
                .map(array -> new Tower(array[0], Integer.parseInt(array[1].substring(1, array[1].length() - 1))))
                .collect(Collectors.toSet());
        Set<String> supportingTowers = input.stream()
                .map(s -> s.split(" -> "))
                .filter(array -> array.length > 1)
                .map(array -> array[1])
                .flatMap(subs -> Arrays.stream(subs.split(", ")))
                .collect(Collectors.toSet());
        List<String> bottomTowers = towers.stream().map(Tower::getName)
                .filter(name -> !supportingTowers.contains(name))
                .collect(Collectors.toList());
        if (bottomTowers.size() != 1) {
            throw new IllegalStateException("Wrong number of bottom towers: " + bottomTowers);
        }
        System.out.println(bottomTowers.get(0));
    }

    private static void part2(final List<String> input) {
        Set<Tower> towers = input.stream()
                .map(s -> s.split(" -> ")[0].split(" "))
                .map(array -> new Tower(array[0], Integer.parseInt(array[1].substring(1, array[1].length() - 1))))
                .collect(Collectors.toSet());
        Map<String, Tower> towerMap = towers.stream().collect(Collectors.toMap(Tower::getName, Function.identity()));
        input.stream()
                .map(s -> s.split(" -> "))
                .filter(array -> array.length > 1)
                .forEach(array -> {
                    Tower supportingTower = towerMap.get(array[0].split(" ")[0]);
                    Arrays.stream(array[1].split(", ")).map(towerMap::get).forEach(supportingTower::addTower);
                });
        Tower oddTower = towers.stream().filter(Tower::hasConflictingWeights).findAny().orElseThrow(IllegalStateException::new);
        List<Tower> subTowers = oddTower.getSubTowers();
        Tower oddOneOut = subTowers.stream().filter(tower ->
                subTowers.stream()
                        .filter(tower1 -> tower1.totalWeight() == tower.totalWeight())
                        .count() == 1L)
                .findFirst()
                .orElseThrow(IllegalStateException::new);
        Tower regularTower = subTowers.stream()
                .filter(tower -> !tower.equals(oddOneOut))
                .findAny()
                .orElseThrow(IllegalStateException::new);
        System.out.println(regularTower.totalWeight() - oddOneOut.childrenWeight());
    }

}
