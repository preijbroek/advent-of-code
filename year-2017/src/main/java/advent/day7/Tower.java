package advent.day7;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Tower {

    private final List<Tower> subTowers = new ArrayList<>();
    private final String name;
    private final int weight;

    public Tower(final String name, final int weight) {
        this.name = name;
        this.weight = weight;
    }

    public boolean addTower(final Tower tower) {
        return subTowers.add(tower);
    }

    public String getName() {
        return name;
    }

    public boolean hasConflictingWeights() {
        return subTowers.stream().map(Tower::totalWeight).distinct().count() > 1L;
    }

    public int totalWeight() {
        return weight + childrenWeight();
    }

    public int childrenWeight() {
        return subTowers.stream().mapToInt(Tower::totalWeight).sum();
    }

    public List<Tower> getSubTowers() {
        return new ArrayList<>(subTowers);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tower tower = (Tower) o;
        return name == tower.name &&
                weight == tower.weight &&
                Objects.equals(subTowers, tower.subTowers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subTowers, name, weight);
    }
}
