package advent.day19;

import advent.InputProvider;

import java.util.List;

public final class Day19Exercise {

    private Day19Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(19);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Network network = new Network(input);
        System.out.println(network.routeCode());
    }

    private static void part2(final List<String> input) {
        Network network = new Network(input);
        System.out.println(network.routeSteps());
    }

}
