package advent.day19;

import util.Direction;
import util.Position;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public final class Network {

    private final Map<Position, Character> networkMap;
    private final Position startPoint;
    private Direction currentDirection = Direction.SOUTH;

    public Network(final List<String> input) {
        Map<Position, Character> networkMap = new HashMap<>();
        for (int i = 0; i < input.size(); i++) {
            String line = input.get(i);
            for (int j = 0; j < line.length(); j++) {
                networkMap.put(new Position(j, i), line.charAt(j));
            }
        }
        this.networkMap = networkMap;
        this.startPoint = networkMap.entrySet()
                .stream()
                .filter(entry -> entry.getKey().getY() == 0)
                .filter(entry -> !Character.isWhitespace(entry.getValue()))
                .map(Map.Entry::getKey)
                .findAny()
                .orElseThrow(IllegalStateException::new);
    }


    public String routeCode() {
        StringBuilder code = new StringBuilder();
        Position currentPoint = startPoint;
        while (true) {
            currentPoint = currentPoint.toFlipped(currentDirection);
            Character currentChar = networkMap.get(currentPoint);
            if (Character.isLetter(currentChar)) {
                code.append(currentChar);
            } else if (currentChar == '+') {
                Position finalCurrentPoint = currentPoint;
                currentDirection = Stream.of(
                        currentDirection.left(),
                        currentDirection.right()
                )
                        .filter(dir -> networkMap.get(finalCurrentPoint.toFlipped(dir)) != null)
                        .filter(dir -> !Character.isWhitespace(networkMap.get(finalCurrentPoint.toFlipped(dir))))
                        .filter(dir ->
                                Character.isLetter(currentChar)
                                        || networkMap.get(finalCurrentPoint.toFlipped(dir)) == '-'
                                        || networkMap.get(finalCurrentPoint.toFlipped(dir)) == '|')
                        .findFirst()
                        .orElseThrow(IllegalStateException::new);
            } else if (Character.isWhitespace(currentChar)) {
                break;
            } else if (currentChar != '-' && currentChar != '|') {
                throw new IllegalStateException(currentPoint.toString());
            }
        }
        return code.toString();
    }

    public int routeSteps() {
        int steps = 0;
        Position currentPoint = startPoint;
        while (true) {
            currentPoint = currentPoint.toFlipped(currentDirection);
            steps++;
            Character currentChar = networkMap.get(currentPoint);
            if (currentChar == '+') {
                Position finalCurrentPoint = currentPoint;
                currentDirection = Stream.of(
                        currentDirection.left(),
                        currentDirection.right()
                )
                        .filter(dir -> networkMap.get(finalCurrentPoint.toFlipped(dir)) != null)
                        .filter(dir -> !Character.isWhitespace(networkMap.get(finalCurrentPoint.toFlipped(dir))))
                        .filter(dir ->
                                Character.isLetter(currentChar)
                                        || networkMap.get(finalCurrentPoint.toFlipped(dir)) == '-'
                                        || networkMap.get(finalCurrentPoint.toFlipped(dir)) == '|')
                        .findFirst()
                        .orElseThrow(IllegalStateException::new);
            } else if (Character.isWhitespace(currentChar)) {
                break;
            } else if (currentChar != '-' && currentChar != '|' && !Character.isLetter(currentChar)) {
                throw new IllegalStateException(currentPoint.toString());
            }
        }
        return steps;
    }
}
