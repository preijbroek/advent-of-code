package advent.day8;

import advent.InputProvider;
import advent.Register;

import java.util.Comparator;
import java.util.List;

public final class Day8Exercise {

    private Day8Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(8);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        JumpComputer jumpComputer = new JumpComputer();
        jumpComputer.run(input);
        System.out.println(jumpComputer.getRegisters()
                .values().stream()
                .max(Comparator.comparing(Register::getValue))
                .map(Register::getValue)
                .orElseThrow(IllegalStateException::new));
    }

    private static void part2(final List<String> input) {
        JumpComputer jumpComputer = new JumpComputer();
        jumpComputer.run(input);
        System.out.println(jumpComputer.getMaxValueEver());
    }

}
