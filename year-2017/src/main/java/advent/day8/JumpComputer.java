package advent.day8;

import advent.Register;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public final class JumpComputer {

    private final Map<String, Register> registers = new HashMap<>();
    private long maxValueEver = Integer.MIN_VALUE;

    public JumpComputer() {
    }

    public Map<String, Register> getRegisters() {
        return Collections.unmodifiableMap(registers);
    }

    public void run(final List<String> instructions) {
        AtomicLong atomicLong = new AtomicLong(0);
        instructions.stream()
                .map(s -> s.split(" "))
                .forEach(array -> {
                    registers.computeIfAbsent(array[0], Register::new);
                    registers.computeIfAbsent(array[4], Register::new);
                    registers.get(array[0]).addValue(getValue(array[1], array[2], array[4], array[5], array[6]));
                    long maxValue = registers.values().stream().mapToLong(Register::getValue).max().orElseThrow(IllegalStateException::new);
                    if (maxValue > atomicLong.get()) {
                        atomicLong.set(maxValue);
                    }
                });
        maxValueEver = atomicLong.get();
    }

    private int getValue(final String factorArgument,
                         final String weight, final String registerToCompare,
                         final String equality,
                         final String compareWeight) {
        boolean condition;
        switch (equality) {
            case "<":
                condition = registers.get(registerToCompare).getValue() < Integer.parseInt(compareWeight);
                break;
            case "<=":
                condition = registers.get(registerToCompare).getValue() <= Integer.parseInt(compareWeight);
                break;
            case "==":
                condition = registers.get(registerToCompare).getValue() == Integer.parseInt(compareWeight);
                break;
            case "!=":
                condition = registers.get(registerToCompare).getValue() != Integer.parseInt(compareWeight);
                break;
            case ">=":
                condition = registers.get(registerToCompare).getValue() >= Integer.parseInt(compareWeight);
                break;
            case ">":
                condition = registers.get(registerToCompare).getValue() > Integer.parseInt(compareWeight);
                break;
            default: throw new IllegalArgumentException(equality);
        }
        return condition ? getFactor(factorArgument) * Integer.parseInt(weight) : 0;
    }

    public long getMaxValueEver() {
        return maxValueEver;
    }

    private static int getFactor(final String s) {
        if ("dec".equals(s)) {
            return -1;
        } else if ("inc".equals(s)) {
            return 1;
        } else {
            throw new IllegalArgumentException(s);
        }
    }
}
