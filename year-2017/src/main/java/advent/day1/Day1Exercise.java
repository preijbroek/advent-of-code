package advent.day1;

import advent.InputProvider;

import java.util.stream.IntStream;

public final class Day1Exercise {

    private Day1Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(1).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        System.out.println(IntStream.range(0, input.length())
                .filter(i -> input.charAt(i) == input.charAt((i + 1) % input.length()))
                .mapToObj(input::charAt)
                .map(String::valueOf)
                .mapToInt(Integer::parseInt)
                .sum());
    }

    private static void part2(final String input) {
        int lengthMatch = input.length() >> 1;
        System.out.println(IntStream.range(0, input.length())
                .filter(i -> input.charAt(i) == input.charAt((i + lengthMatch) % input.length()))
                .mapToObj(input::charAt)
                .map(String::valueOf)
                .mapToInt(Integer::parseInt)
                .sum());
    }

}
