package advent.day4;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public final class Password {

    private final String valueString;
    private final Map<Character, Integer> occurrences;

    public Password(final int value) {
        this.valueString = String.valueOf(value);
        this.occurrences = findOccurrences();
    }

    private Map<Character, Integer> findOccurrences() {
        Map<Character, Integer> occurrences = new HashMap<>();
        for (int i = 0; i < valueString.length(); i++) {
            occurrences.put(valueString.charAt(i), occurrences.getOrDefault(valueString.charAt(i), 0) + 1);
        }
        return occurrences;
    }

    public boolean isValid() {
        return isValid(false);
    }

    public boolean isStrictlyValid() {
        return isValid(true);
    }

    private boolean isValid(final boolean strictMode) {
        return isNonDecreasing()
                && hasDoubleOccurrence()
                && (!strictMode || hasExactDoubleOccurrence());
    }

    private boolean isNonDecreasing() {
        return IntStream.range(0, valueString.length() - 1)
                .noneMatch(i -> valueString.charAt(i) > valueString.charAt(i + 1));
    }

    private boolean hasDoubleOccurrence() {
        return occurrences.values().stream()
                .anyMatch(value -> value > 1);
    }

    private boolean hasExactDoubleOccurrence() {
        return occurrences.values().stream()
                .anyMatch(value -> value == 2);
    }

}
