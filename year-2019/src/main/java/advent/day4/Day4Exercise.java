package advent.day4;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public final class Day4Exercise {

    private Day4Exercise() {
    }

    public static void main(final String[] args) {
        List<Integer> boundaries = Arrays.stream(InputProvider.getInput(4).get(0).split("-"))
                .map(Integer::valueOf)
                .collect(toList());
        part1(boundaries.get(0), boundaries.get(1));
        part2(boundaries.get(0), boundaries.get(1));
    }

    private static void part1(final Integer min, final Integer max) {
        long validPasswords = IntStream.rangeClosed(min, max)
                .mapToObj(Password::new)
                .filter(Password::isValid)
                .count();
        System.out.println(validPasswords);
    }

    private static void part2(final Integer min, final Integer max) {
        long validPasswords = IntStream.rangeClosed(min, max)
                .mapToObj(Password::new)
                .filter(Password::isStrictlyValid)
                .count();
        System.out.println(validPasswords);
    }

}
