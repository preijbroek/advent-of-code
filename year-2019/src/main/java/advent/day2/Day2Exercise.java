package advent.day2;

import advent.InputProvider;
import advent.IntCodeComputer;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public final class Day2Exercise {

    private Day2Exercise() {
    }

    public static void main(final String[] args) {
        List<Integer> input = Arrays.stream(InputProvider.getInput(2).get(0).split(","))
                .map(Integer::parseInt)
                .collect(toList());
        part1(input);
        part2(input);
    }

    private static void part1(final List<Integer> input) {
        input.set(1, 12);
        input.set(2, 2);
        String code = input.stream().map(String::valueOf).collect(joining(","));
        IntCodeComputer computer = new IntCodeComputer(code, new ArrayDeque<>(), new ArrayDeque<>());
        computer.run();
        System.out.println(computer.getFirstPosition());
    }

    private static void part2(final List<Integer> input) {
        for (int value1 = 0; value1 < 100; value1++) {
            for (int value2 = 0; value2 < 100; value2++) {
                input.set(1, value1);
                input.set(2, value2);
                String code = input.stream().map(String::valueOf).collect(joining(","));
                IntCodeComputer computer = new IntCodeComputer(code, new ArrayDeque<>(), new ArrayDeque<>());
                computer.run();
                if (computer.getFirstPosition() == 19690720L) {
                    System.out.println(100 * value1 + value2);
                    return;
                }
            }
        }
    }

}
