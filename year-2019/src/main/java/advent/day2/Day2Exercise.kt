package advent.day2

import advent.IntCodeProgram
import advent.getInput
import util.x

fun main() {
    val program = getInput(2).first().let(::IntCodeProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: IntCodeProgram): Long {
    program[1L] = 12L
    program[2L] = 2L
    program.run()
    return program[0L]
}

private fun part2(program: IntCodeProgram): Long {
    val desiredOutput = 19690720L
    return ((0..99L) x (0..99L)).first { (noun, verb) ->
        val initialProgram = program.reset()
        initialProgram[1L] = noun
        initialProgram[2L] = verb
        initialProgram.run()
        initialProgram[0L] == desiredOutput
    }.let { (noun, verb) -> 100L * noun + verb }
}
