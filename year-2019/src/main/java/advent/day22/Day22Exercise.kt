package advent.day22

import advent.getInput
import util.modulo
import java.math.BigInteger
import java.math.BigInteger.ONE

private typealias Operation = (input: Long, modular: Long) -> Long
private typealias BigIntOperation = (input: BigInteger, modular: BigInteger) -> BigInteger

fun main() {
    val operations = getInput(22).map(String::toOperation)
    val inverseOperations = getInput(22).map(String::toInverseOperation)
    println(part1(operations))
    println(part2(inverseOperations))
}

private fun part1(operations: List<Operation>): Long {
    val requiredCard = 2019L
    val deckSize = 10007L
    return operations.reduce(Operation::then).invoke(requiredCard, deckSize)
}

private fun part2(operations: List<BigIntOperation>): BigInteger {
    val deckSize = 119315717514047L.toBigInteger()
    val completeCycle = operations.reduce(BigIntOperation::after)
    val x = 2020L.toBigInteger()
    val y = completeCycle(x, deckSize)
    val z = completeCycle(y, deckSize)
    val a = ((y - z) * ((x - y).modInverse(deckSize)))
    val b = (y - (a * x))
    val cycles = 101741582076661L.toBigInteger()
    val position = 2020L.toBigInteger()
    return ((a.modPow(cycles, deckSize) * position) +
            ((a.modPow(cycles, deckSize) - ONE) * (a - ONE).modInverse(deckSize) * b)).mod(deckSize)
}

private fun String.toOperation(): Operation {
    return when {
        this == "deal into new stack" -> {
            { input, modular -> (-1 - input) modulo modular }
        }
        this.startsWith("cut ") -> {
            { input, modular -> input - this.getCut() modulo modular }
        }
        else -> {
            { input, modular -> input * this.getIncrement() modulo modular }
        }
    }
}

private fun String.toInverseOperation(): BigIntOperation {
    return when {
        this == "deal into new stack" -> {
            { input: BigInteger, modular: BigInteger -> (-input - ONE).mod(modular) }
        }
        this.startsWith("cut ") -> {
            { input, modular -> (input + this.getCut().toBigInteger()).mod(modular) }
        }
        else -> {
            { input, modular -> (input * this.getIncrement().toBigInteger().modInverse(modular)).mod(modular) }
        }
    }
}

private fun String.getCut() = substringAfter("cut ").toLong()

private fun String.getIncrement() = substringAfter("deal with increment ").toLong()

private fun Operation.then(operation: Operation): Operation {
    return { input, modular ->
        operation(this(input, modular), modular)
    }
}

private fun BigIntOperation.after(operation: BigIntOperation): BigIntOperation {
    return { input, modular ->
        this(operation(input, modular), modular)
    }
}
