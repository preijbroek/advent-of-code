package advent.day1;

import advent.InputProvider;

import java.util.List;

import static java.util.stream.Collectors.toList;

public final class Day1Exercise {

    private Day1Exercise() {
    }

    public static void main(final String[] args) {
        List<Integer> input = InputProvider.getInput(1).stream().map(Integer::parseInt).collect(toList());
        part1(input);
        part2(input);
    }

    private static void part1(final List<Integer> input) {
        int totalFuel = input.stream()
                .mapToInt(Day1Exercise::calculateFuel)
                .sum();
        System.out.println(totalFuel);
    }

    private static void part2(final List<Integer> input) {
        int totalFuel = input.stream()
                .mapToInt(Day1Exercise::calculateTotalFuel)
                .sum();
        System.out.println(totalFuel);
    }

    private static int calculateFuel(final int mass) {
        return Math.max(0, (mass / 3) - 2);
    }

    private static int calculateTotalFuel(final int mass) {
        int totalFuel = 0;
        int additionalFuel = mass;
        while (additionalFuel != 0) {
            additionalFuel = calculateFuel(additionalFuel);
            totalFuel += additionalFuel;
        }
        return totalFuel;
    }

}
