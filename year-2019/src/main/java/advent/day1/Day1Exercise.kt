package advent.day1

import advent.getInput

fun main() {
    val masses = getInput(1).map(String::toInt)
    println(part1(masses))
    println(part2(masses))
}

private fun part1(masses: List<Int>): Int {
    return masses.sumOf(Int::requiredFuel)
}


private fun part2(masses: List<Int>): Int {
    return masses.sumOf { mass ->
        generateSequence(mass.requiredFuel(), Int::requiredFuel).takeWhile { it > 0 }.sum()
    }
}

private fun Int.requiredFuel() = maxOf(this / 3 - 2, 0)
