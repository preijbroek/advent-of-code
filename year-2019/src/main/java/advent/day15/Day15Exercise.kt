package advent.day15

import advent.IntCodeProgram
import advent.getInput
import util.Movement
import util.Position

fun main() {
    val program = getInput(15).first().let(::IntCodeProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: IntCodeProgram): Int {
    return program.grid().distanceToOxygenSystem()
}

private fun part2(program: IntCodeProgram): Int {
    val grid = program.grid()
    var currentPositions = listOf(grid.oxygenSystem().key)
    val visitedPositions = currentPositions.associateWith { 0 }.toMutableMap()
    var index = 0
    while (currentPositions.isNotEmpty()) {
        index++
        currentPositions = currentPositions.flatMap { position ->
            position.neighbours().filter {
                it !in visitedPositions && it in grid && grid[it]?.second != 0L
            }.map { it.also { visitedPositions[it] = index } }
        }
    }
    return visitedPositions.maxOf { it.value }
}

private fun Long.toMovement() = when (this) {
    1L -> Movement.UP
    2L -> Movement.DOWN
    3L -> Movement.LEFT
    4L -> Movement.RIGHT
    else -> throw IllegalArgumentException(this.toString())
}

private fun IntCodeProgram.grid(): MutableMap<Position, Pair<Int, Long>> {
    var currentPositions = listOf(this to Position.CENTRE)
    val grid = mutableMapOf(currentPositions.first().second to (0 to 1L))
    var index = 0
    while (currentPositions.isNotEmpty()) {
        index++
        currentPositions = currentPositions.flatMap { (program, position) ->
            listOf(1L, 2L, 3L, 4L).mapNotNull { l ->
                val copy = program.deepCopy()
                copy.addInput(l)
                copy.run()
                val nextPosition = position.to(l.toMovement())
                val output = copy.getOutput()
                val nextValue = listOfNotNull(grid[nextPosition], index to output).minBy { it.first }
                if (grid[nextPosition] != nextValue) {
                    grid[nextPosition] = nextValue
                    output.takeIf { it != 0L }?.let {
                        copy to nextPosition
                    }
                } else {
                    null
                }
            }
        }
    }
    return grid
}

private fun Map<Position, Pair<Int, Long>>.oxygenSystem() = entries.first { it.value.second == 2L }

private fun Map<Position, Pair<Int, Long>>.distanceToOxygenSystem() = oxygenSystem().value.first
