package advent.day16

import advent.getInput
import java.util.Collections.nCopies

fun main() {
    val input = getInput(16).first()
    println(part1(input))
    println(part2(input))
}

private fun part1(input: String): String {
    val transformers = generateSequence(listOf(0, 1, 0, -1)) {
        val length = it.size
        listOf(
            nCopies((length / 4) + 1, it[0]),
            nCopies((length / 4) + 1, it[(length / 4)]),
            nCopies((length / 4) + 1, it[(length / 2)]),
            nCopies((length / 4) + 1, it[3 * (length / 4)]),
        ).flatten()
    }.map { it.take(input.length + 1) }
        .take(input.length)
        .toList()
    var currentInput = input
    repeat(100) {
        currentInput = currentInput.indices.joinToString(separator = "") { index ->
            val transformer = transformers[index]
            val tLength = transformer.size
            (currentInput.mapIndexed { i, digit -> i to digit.digitToInt() }
                .sumOf { (i, digit) -> transformer[(i + 1) % tLength] * digit }).toString().last().toString()
        }
    }
    return currentInput.take(8)
}


private fun part2(input: String): String {
    val startIndex = input.take(7).toInt()
    var substring = nCopies(10000, input).joinToString("").substring(startIndex)
    repeat(100) {
        substring = substring.next()
    }
    return substring.take(8)
}

private fun String.next(): String {
    var value = 0
    return (lastIndex downTo 0).map {
        value += this[it].digitToInt()
        value.mod(10)
    }.reversed().joinToString(separator = "")
}
