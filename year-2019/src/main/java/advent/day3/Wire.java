package advent.day3;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import util.Position;

import java.util.HashMap;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class Wire {

    private final Map<Position, Integer> wireMap;

    public static Wire fromString(final String instruction) {
        String[] instructions = instruction.split(",");
        Map<Position, Integer> wireMap = new HashMap<>();
        Position currentPosition = Position.CENTRE;
        int length = 1;
        for (String s : instructions) {
            String direction = s.substring(0, 1);
            int times = Integer.parseInt(s.substring(1));
            for (int i = 0; i < times; i++) {
                currentPosition = setNextPosition(currentPosition, direction);
                wireMap.putIfAbsent(currentPosition, length++);
            }
        }
        return new Wire(wireMap);
    }

    private static Position setNextPosition(Position currentPosition, final String direction) {
        switch (direction) {
            case "U":
                currentPosition = currentPosition.north();
                break;
            case "D":
                currentPosition = currentPosition.south();
                break;
            case "R":
                currentPosition = currentPosition.east();
                break;
            case "L":
                currentPosition = currentPosition.west();
                break;
            default: throw new IllegalArgumentException(direction);
        }
        return currentPosition;
    }

    public Map<Position, Integer> getCrossings(final Wire wire) {
        return wireMap.entrySet()
                .stream()
                .filter(entry -> wire.wireMap.containsKey(entry.getKey()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public int getLengthFrom(final Position position) {
        return wireMap.get(position);
    }
}
