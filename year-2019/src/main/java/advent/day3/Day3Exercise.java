package advent.day3;

import advent.InputProvider;
import util.Position;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public final class Day3Exercise {

    private Day3Exercise() {
    }

    public static void main(final String[] args) {
        List<String> input = InputProvider.getInput(3);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<Wire> wires = input.stream()
                .map(Wire::fromString)
                .collect(toList());
        Map<Position, Integer> crossings = wires.get(0).getCrossings(wires.get(1));
        Position closestCrossing = crossings.keySet().stream()
                .min(Comparator.comparing(Position::distanceToCentre))
                .orElseThrow(IllegalStateException::new);
        System.out.println(closestCrossing.distanceToCentre());
    }

    private static void part2(final List<String> input) {
        List<Wire> wires = input.stream()
                .map(Wire::fromString)
                .collect(toList());
        Map<Position, Integer> crossings = wires.get(0).getCrossings(wires.get(1));
        Position firstCrossing = crossings.entrySet().stream()
                .min(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .orElseThrow(IllegalStateException::new);
        int combinedLength = wires.stream()
                .mapToInt(wire -> wire.getLengthFrom(firstCrossing))
                .sum();
        System.out.println(combinedLength);
    }

}
