package advent.day6;

import advent.InputProvider;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public final class Day6Exercise {

    private Day6Exercise() {
    }

    public static void main(final String[] args) {
        List<String> input = InputProvider.getInput(6);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Map<String, Star> starMap = input.stream()
                .flatMap(line -> Arrays.stream(line.split("\\)")))
                .map(Star::fromString)
                .collect(toMap(Star::getName, identity(), (s1, s2) -> s1));
        input.stream()
                .map(line -> line.split("\\)"))
                .forEach(orbit -> starMap.get(orbit[0]).addDirectOrbitor(starMap.get(orbit[1])));
        int totalOrbits = starMap.values().stream()
                .mapToInt(Star::getAllOrbitors)
                .sum();
        System.out.println(totalOrbits);
    }

    private static void part2(final List<String> input) {
        Map<String, Star> starMap = input.stream()
                .flatMap(line -> Arrays.stream(line.split("\\)")))
                .map(Star::fromString)
                .collect(toMap(Star::getName, identity(), (s1, s2) -> s1));
        input.stream()
                .map(line -> line.split("\\)"))
                .forEach(orbit -> starMap.get(orbit[0]).addDirectOrbitor(starMap.get(orbit[1])));
        List<Star> pathOfYOUToCOM = getPath(starMap.get("YOU"), starMap);
        List<Star> pathOfSANToCOM = getPath(starMap.get("SAN"), starMap);
        int firstDifferingIndex = getPathSplitMoment(pathOfSANToCOM, pathOfYOUToCOM);
        int pathLength = pathOfSANToCOM.size() + pathOfYOUToCOM.size() - (firstDifferingIndex << 1);
        System.out.println(pathLength);
    }

    private static List<Star> getPath(final Star orbit, final Map<String, Star> starMap) {
        List<Star> pathToCom = new LinkedList<>();
        Star currentStar = orbit;
        while (!"COM".equals(currentStar.getName())) {
            currentStar = starMap.values().stream()
                    .filter(currentStar::orbits)
                    .findAny()
                    .orElseThrow(IllegalStateException::new);
            pathToCom.add(0, currentStar);
        }
        return pathToCom;
    }

    private static int getPathSplitMoment(final List<Star> path1, final List<Star> path2) {
        return IntStream.range(0, Math.min(path1.size(), path2.size()))
                .filter(i -> !path1.get(i).equals(path2.get(i)))
                .findFirst()
                .orElseGet(() -> Math.abs(path1.size() - path2.size()));
    }

}
