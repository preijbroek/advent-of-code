package advent.day6;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class Star {

    @Getter
    private final String name;
    private final Set<Star> directOrbits;

    public static Star fromString(final String name) {
        return new Star(name, new HashSet<>());
    }

    public void addDirectOrbitor(final Star star) {
        directOrbits.add(star);
    }

    public int getAllOrbitors() {
        return getDirectOrbitors() + getIndirectOrbitors();
    }

    private int getDirectOrbitors() {
        return directOrbits.size();
    }

    private int getIndirectOrbitors() {
        return directOrbits.stream()
                .mapToInt(Star::getAllOrbitors)
                .sum();
    }

    public boolean orbits(final Star star) {
        return star.directOrbits.contains(this);
    }

}
