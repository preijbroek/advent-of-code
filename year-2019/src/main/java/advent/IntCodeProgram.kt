package advent

import util.pow
import util.times

data class IntCodeProgram(
    private val code: String,
    private val input: ArrayDeque<Long> = ArrayDeque(),
    private val output: ArrayDeque<Long> = ArrayDeque(),
    private var currentPosition: Long = 0L,
    private var relativeBase: Long = 0L,
    private val addresses: MutableMap<Long, Long> = code.split(",")
        .mapIndexed { index, s -> index.toLong() to s.toLong() }
        .toMap(LinkedHashMap())
        .withDefault { 0L }
) {

    fun reset() = IntCodeProgram(code)

    fun deepCopy() = copy(
        code = code,
        input = ArrayDeque(input),
        output = ArrayDeque(output),
        currentPosition = currentPosition,
        relativeBase = relativeBase,
        addresses = addresses.toMutableMap()
    )

    operator fun set(index: Long, value: Long) {
        addresses[index] = value
    }

    operator fun get(index: Long) = addresses.getValue(index)

    fun hasInputs() = input.isNotEmpty()
    fun hasOutputs() = output.isNotEmpty()

    fun addInput(l: Long) = input.addLast(l)

    fun getOutput() = output.removeFirst()

    fun getAllOutputs() = allOutputs().also { output.clear() }

    fun allOutputs() = output.toList()

    fun isFinished(): Boolean = opCode() == exitCode

    fun run() {
        while (opCode() != exitCode) {
            when (val opCode = opCode()) {
                1L -> runAddition()
                2L -> runMultiplication()
                3L -> {
                    if (input.isEmpty()) return; runInput()
                }
                4L -> runOutput()
                5L -> runJumpIfTrue()
                6L -> runJumpIfFalse()
                7L -> runLessThan()
                8L -> runEquals()
                9L -> runAdjustRelativeBase()
                else -> throw IllegalArgumentException(opCode.toString())
            }
        }

    }

    private fun runAddition() {
        addresses[maybeRelativeBase(delta = 3) + addressDelta(delta = 3L)] =
            mode(delta = 1)(addressDelta(delta = 1L)) + mode(delta = 2)(addressDelta(delta = 2L))
        move(4)
    }

    private fun runMultiplication() {
        addresses[maybeRelativeBase(delta = 3) + addressDelta(delta = 3L)] =
            mode(delta = 1)(addressDelta(delta = 1L)) * mode(delta = 2)(addressDelta(delta = 2L))
        move(4)
    }

    private fun runInput() {
        addresses[maybeRelativeBase(delta = 1) + addressDelta(delta = 1)] = input.removeFirst()
        move(2)
    }

    private fun runOutput() {
        output.addLast(mode(delta = 1)(addressDelta(delta = 1)))
        move(2)
    }

    private fun runJumpIfTrue() {
        currentPosition = if (firstParameterIsZero()) {
            currentPosition + 3
        } else {
            mode(delta = 2)(addressDelta(delta = 2))
        }
    }

    private fun runJumpIfFalse() {
        currentPosition = if (firstParameterIsZero()) {
            mode(delta = 2)(addressDelta(delta = 2))
        } else {
            currentPosition + 3
        }
    }

    private fun runLessThan() {
        addresses[maybeRelativeBase(delta = 3) + addressDelta(3L)] =
            1L * (mode(delta = 1)(addressDelta(delta = 1L)) < mode(delta = 2)(addressDelta(delta = 2L)))
        move(4)
    }

    private fun runEquals() {
        addresses[maybeRelativeBase(delta = 3) + addressDelta(3L)] =
            1L * (mode(delta = 1)(addressDelta(delta = 1L)) == mode(delta = 2)(addressDelta(delta = 2L)))
        move(4)
    }

    private fun firstParameterIsZero() = mode(delta = 1)(addressDelta(delta = 1L)) == 0L

    private fun runAdjustRelativeBase() {
        relativeBase += mode(delta = 1)(addressDelta(delta = 1L))
        move(2)
    }

    private fun mode(delta: Int): (Long) -> Long {
        return when (modeDigit(delta)) {
            0L -> ::address
            1L -> { l -> l }
            2L -> { l -> address(l + relativeBase) }
            else -> throw IllegalArgumentException(addressDelta().toString())
        }
    }

    private fun modeDigit(delta: Int) = (addressDelta() / 10L.pow(1 + delta)) % 10

    private fun maybeRelativeBase(delta: Int) = relativeBase * (modeDigit(delta) == 2L)

    private fun address(position: Long) = addresses.getValue(position)

    private fun addressDelta(delta: Long = 0L) = addresses.getValue(currentPosition + delta)

    private fun opCode() = addressDelta() % 100

    private fun move(delta: Long) {
        currentPosition += delta
    }

    companion object {
        private const val exitCode = 99L
    }
}