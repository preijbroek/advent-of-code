package advent.day17

import advent.ASCIIProgram
import advent.IntCodeProgram
import advent.getInput
import util.Position.Companion.byX
import util.Position.Companion.byY
import util.toCharMap

fun main() {
    val program = getInput(17).first().let(::IntCodeProgram).let(::ASCIIProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: ASCIIProgram): Int {
    val scaffolds = program.apply(ASCIIProgram::run)
        .getAllCharOutputs()
        .joinToString(separator = "")
        .split('\n')
        .toCharMap()
    val (minX, minY) = scaffolds.keys.minWith(byX().thenComparing(byY()))
    return scaffolds.filter { (position, type) ->
        type.isScaffold() && position.neighbours().map(scaffolds::get).all { it.isScaffold() }
    }.keys.sumOf { (x, y) -> (x - minX) * (y - minY) }
}

private fun part2(program: ASCIIProgram): Long {
    program[0L] = 2L
    listOf(
        "A,A,B,C,B,C,B,C,C,A", // main movement
        "L,10,R,8,R,8", // A
        "L,10,L,12,R,8,R,10", // B
        "R,10,L,12,R,10", // C
        "n", // videoFeed
    ).forEach {
        program.addLine(it)
    }
    program.run()
    return program.getAllOutputs().last()
}

private fun Char?.isScaffold() = this in setOf('#', '>', 'v', '<', '^')
