package advent.day13

import advent.IntCodeProgram
import advent.getInput
import util.Position

fun main() {
    val program = getInput(13).first().let(::IntCodeProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: IntCodeProgram): Int {
    program.run()
    return program.allOutputs().chunked(size = 3) { (x, y, tile) ->
        Position(x.toInt(), y.toInt()) to Tile(tile)
    }.count { (_, tile) -> tile == Tile.BLOCK }
}

private fun Map<Position, Tile?>.print() {
    val minX = minOf { (position, _) -> position.x }
    val maxX = maxOf { (position, _) -> position.x }
    val minY = minOf { (position, _) -> position.y }
    val maxY = maxOf { (position, _) -> position.y }
    (minY..maxY).forEach { y ->
        (minX..maxX).forEach { x ->
            print(this[Position(x, y)]?.c ?: ' ')
        }
        println()
    }
}

private fun part2(program: IntCodeProgram): Long {
    program[0L] = 2L
    var score = 0L
    val screen = mutableMapOf<Position, Tile?>()
    var iteration = 0
    while (!program.isFinished()) {
        iteration++
        program.run()
        score = score(program, screen)
    }
    return score
}

private fun score(program: IntCodeProgram, screen: MutableMap<Position, Tile?>): Long {
    var score = 0L
    while (program.allOutputs().isNotEmpty()) {
        val x = program.getOutput()
        val y = program.getOutput()
        val value = program.getOutput()
        if (x == -1L && y == 0L && Tile(value) == null) {
            score = value
        } else {
            screen[Position(x, y)] = Tile(value)
        }
    }
    val ball = screen.entries.firstOrNull { (_, value) -> value == Tile.BALL }?.key?.x
    val paddle = screen.entries.firstOrNull { (_, value) -> value == Tile.PADDLE }?.key?.x
    if (ball != null && paddle != null) {
        program.addInput(ball.compareTo(paddle).toLong())
    }
    return score
}

enum class Tile(private val value: Long, val c: Char) {
    EMPTY(value = 0L, c = ' '),
    WALL(value = 1L, c = '+'),
    BLOCK(value = 2L, c = '#'),
    PADDLE(value = 3L, c = '='),
    BALL(value = 4L, c = 'o'),
    ;

    companion object {
        private val cachedValues = values()

        operator fun invoke(value: Long) = cachedValues.firstOrNull { value == it.value }
    }
}
