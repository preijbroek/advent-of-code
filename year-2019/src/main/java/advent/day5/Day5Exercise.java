package advent.day5;

import advent.InputProvider;
import advent.IntCodeComputer;

import java.util.ArrayDeque;

public final class Day5Exercise {

    private Day5Exercise() {
    }

    public static void main(final String[] args) {
        String input = InputProvider.getInput(5).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        IntCodeComputer computer = new IntCodeComputer(input, new ArrayDeque<>(1), new ArrayDeque<>(20));
        computer.provideInput(1L);
        computer.run();
        Long output;
        while ((output = computer.retrieveFirstOutput()) != null) {
            System.out.println(output);
        }
    }

    private static void part2(final String input) {
        IntCodeComputer computer = new IntCodeComputer(input, new ArrayDeque<>(1), new ArrayDeque<>(1));
        computer.provideInput(5L);
        computer.run();
        System.out.println(computer.retrieveFirstOutput());
    }

}
