package advent.day5

import advent.IntCodeProgram
import advent.getInput

fun main() {
    val program = getInput(5).first().let(::IntCodeProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: IntCodeProgram): Long {
    program.addInput(1L)
    program.run()
    return generateSequence { program.getOutput() }.first { it != 0L }
}

private fun part2(program: IntCodeProgram): Long {
    program.addInput(5L)
    program.run()
    return program.getOutput()
}
