package advent.day9;

import advent.InputProvider;
import advent.IntCodeComputer;

import java.util.ArrayDeque;

public final class Day9Exercise {

    private Day9Exercise() {
    }

    public static void main(final String[] args) {
        part1();
        part2();
    }

    private static void part1() {
        runWithValue(1L);
    }

    private static void part2() {
        runWithValue(2L);
    }

    private static void runWithValue(final long l) {
        String code = InputProvider.getInput(9).get(0);
        IntCodeComputer computer = new IntCodeComputer(code, new ArrayDeque<>(10_000), new ArrayDeque<>(10_000));
        computer.provideInput(l);
        computer.run();
        Long output;
        while ((output = computer.retrieveFirstOutput()) != null) {
            System.out.println(output);
        }
    }
}
