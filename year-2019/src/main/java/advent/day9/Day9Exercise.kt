package advent.day9

import advent.IntCodeProgram
import advent.getInput

fun main() {
    val program = getInput(9).first().let(::IntCodeProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: IntCodeProgram): Long {
    program.addInput(1L)
    program.run()
    return program.getOutput()
}

private fun part2(program: IntCodeProgram): Long {
    program.addInput(2L)
    program.run()
    return program.getOutput()
}