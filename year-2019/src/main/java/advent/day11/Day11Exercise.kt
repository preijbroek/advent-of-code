package advent.day11

import advent.IntCodeProgram
import advent.getInput
import util.Movement
import util.Position
import util.toLong

fun main() {
    val program = getInput(11).first().let(::IntCodeProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: IntCodeProgram): Int {
    return paintedPanels(program, startOnWhite = false).size
}

private fun part2(program: IntCodeProgram): String {
    val paintedPanels = paintedPanels(program, startOnWhite = true)
    val minX = paintedPanels.minOf { it.key.x }
    val maxX = paintedPanels.maxOf { it.key.x }
    val minY = paintedPanels.minOf { it.key.y }
    val maxY = paintedPanels.maxOf { it.key.y }
    return buildString {
        (minY..maxY).forEach { y ->
            (minX..maxX).forEach { x ->
                append(if (paintedPanels.getValue(Position(x,y))) '#' else '.')
            }
            append('\n')
        }
    }
}

private fun paintedPanels(program: IntCodeProgram, startOnWhite: Boolean): MutableMap<Position, Boolean> {
    var currentMovement = Movement.UP
    var currentPosition = Position.CENTRE
    val paintedPanels = mutableMapOf(currentPosition to startOnWhite).withDefault { false }
    while (!program.isFinished()) {
        program.addInput((paintedPanels.getValue(currentPosition)).toLong())
        program.run()
        paintedPanels[currentPosition] = program.getOutput() != 0L
        currentMovement = if (program.getOutput() == 0L) {
            currentMovement.left()
        } else {
            currentMovement.right()
        }
        currentPosition = currentPosition.to(currentMovement)
    }
    return paintedPanels
}
