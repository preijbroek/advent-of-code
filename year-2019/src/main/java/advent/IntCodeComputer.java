package advent;

import lombok.Getter;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.function.Function.identity;

public final class IntCodeComputer {

    private static final long EXIT_CODE = 99L;

    private final String code;
    private final Queue<Long> input;
    private final Queue<Long> output;
    private final Map<Integer, Long> instructions;

    private final int[] positions = new int[3];
    private int instructionPosition = 0;
    private int relativeBase = 0;
    @Getter
    private boolean finished = false;

    public IntCodeComputer(final String code,
                           final Queue<Long> input,
                           final Queue<Long> output) {
        this.code = code;
        this.input = input;
        this.output = output;
        this.instructions = initializeInstructions();
    }

    public void reset() {
        input.clear();
        output.clear();
        instructions.clear();
        instructions.putAll(initializeInstructions());
        finished = false;
        instructionPosition = 0;
        relativeBase = 0;
        Arrays.fill(positions, 0);
    }

    private Map<Integer, Long> initializeInstructions() {
        String[] codes = code.trim().split(",");
        return IntStream.range(0, codes.length)
                .boxed()
                .collect(Collectors.toMap(identity(),
                        i -> Long.parseLong(codes[i]), (a, b) -> b));
    }

    public void provideInput(final Long l) {
        input.offer(l);
    }

    public void provideInputs(final Collection<Long> longs) {
        longs.forEach(this::provideInput);
    }

    public Long retrieveFirstOutput() {
        return output.poll();
    }

    public void run() {
        while (retrieve(instructionPosition) != EXIT_CODE) {
            Long instruction = retrieve(instructionPosition);
            String command = String.format("%05d", instruction);
            int opCode = Integer.parseInt(command.substring(3));
            positions[0] = getParam(command, 1);
            positions[1] = getParam(command, 2);
            positions[2] = getParam(command, 3);
            if (runOpCode(opCode)) {
                return;
            }
        }
        finished = true;
    }

    private boolean runOpCode(final int opCode) {
        switch (opCode) {
            case 1:
                runAdditionOpCode();
                break;
            case 2:
                runMultiplicationOpCode();
                break;
            case 3:
                if (input.isEmpty()) {
                    return true;
                }
                runGetInputOpCode();
                break;
            case 4:
                runAddOutputOpCode();
                break;
            case 5:
                runSetPositionOnConditionOpCode(retrieve(positions[0]) > 0L);
                break;
            case 6:
                runSetPositionOnConditionOpCode(retrieve(positions[0]) == 0L);
                break;
            case 7:
                runChangeValueOnConditionOpCode(retrieve(positions[0]) < retrieve(positions[1]));
                break;
            case 8:
                runChangeValueOnConditionOpCode(retrieve(positions[0]).equals(retrieve(positions[1])));
                break;
            case 9:
                runSetRelativeBaseOpCode();
                break;
            default:
                throw new IllegalStateException("Cannot work with opCode " + opCode);
        }
        return false;
    }

    private void runAdditionOpCode() {
        int destination = positions[2];
        long first = retrieve(positions[0]);
        long second = retrieve(positions[1]);
        setAtPosition(destination, first + second);
        instructionPosition += 4;
    }

    private void runMultiplicationOpCode() {
        int destination = positions[2];
        long first = retrieve(positions[0]);
        long second = retrieve(positions[1]);
        setAtPosition(destination, first * second);
        instructionPosition += 4;
    }

    private void runGetInputOpCode() {
        Long value = input.poll();
        setAtPosition(positions[0], value);
        instructionPosition += 2;
    }

    private void runAddOutputOpCode() {
        Long output = retrieve(positions[0]);
        instructionPosition += 2;
        this.output.offer(output);
    }

    private void runSetPositionOnConditionOpCode(final boolean condition) {
        instructionPosition = condition
                ? retrieve(positions[1]).intValue()
                : instructionPosition + 3;
    }

    private void runChangeValueOnConditionOpCode(final boolean condition) {
        long value = condition ? 1L : 0L;
        setAtPosition(positions[2], value);
        instructionPosition += 4;
    }

    private void runSetRelativeBaseOpCode() {
        relativeBase += retrieve(positions[0]).intValue();
        instructionPosition += 2;
    }

    private int getParam(final String params, final int i) {
        switch (params.charAt(3 - i)) {
            case /*POSITION*/ '0':
                return retrieve(instructionPosition + i).intValue();
            case /*IMMEDIATE*/'1':
                return instructionPosition + i;
            case /* RELATIVE */ '2':
                return Long.valueOf(retrieve(instructionPosition + i) + relativeBase).intValue();
            default:
                throw new IllegalArgumentException("Unsupported opMode");
        }
    }

    private Long retrieve(final int position) {
        return instructions.computeIfAbsent(position, value -> 0L);
    }

    private void setAtPosition(final int position, final Long value) {
        instructions.put(position, value);
    }

    public boolean hasInput() {
        return !input.isEmpty();
    }

    public boolean hasOutput() {
        return !output.isEmpty();
    }

    public Long getFirstPosition() {
        return instructions.get(0);
    }
}
