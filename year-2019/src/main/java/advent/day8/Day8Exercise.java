package advent.day8;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public final class Day8Exercise {

    private static final int IMAGE_SIZE =   150;
    private static final int IMAGE_LENGTH = 6;
    private static final int IMAGE_WIDTH = 25;

    private Day8Exercise() {
    }

    public static void main(final String[] args) {
        String input = InputProvider.getInput(8).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        Long result = IntStream.range(0, input.length() / IMAGE_SIZE)
                .mapToObj(i -> input.substring(IMAGE_SIZE * i, IMAGE_SIZE * (i + 1)))
                .map(Layer::new)
                .min(comparing(Layer::numberOfZeroes))
                .map(Layer::onesTimesTwos)
                .orElseThrow(IllegalStateException::new);
        System.out.println(result);
    }

    private static void part2(final String input) {
        List<Layer> layers = IntStream.range(0, input.length() / IMAGE_SIZE)
                .mapToObj(i -> input.substring(IMAGE_SIZE * i, IMAGE_SIZE * (i + 1)))
                .map(Layer::new)
                .collect(toList());
        String[][] image = new String[IMAGE_LENGTH][IMAGE_WIDTH];
        layers.forEach(layer -> layer.fillImage(image));
        print(image);
    }

    private static void print(final String[][] image) {
        for (String[] line : image) {
            Arrays.stream(line).forEach(System.out::print);
            System.out.println();
        }
    }

}
