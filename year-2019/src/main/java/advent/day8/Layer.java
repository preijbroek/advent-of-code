package advent.day8;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public final class Layer {

    private static final int IMAGE_WIDTH = 25;
    private final String layerString;

    public long numberOfZeroes() {
        return count('0');
    }

    public long onesTimesTwos() {
        return count('1') * count('2');
    }

    private long count(final char c) {
        return layerString.chars()
                .filter(k -> k == c)
                .count();
    }

    public void fillImage(final String[][] image) {
        for (int i = 0; i < layerString.length(); i++) {
            char charAt = layerString.charAt(i);
            if (charAt != '2' && image[i / IMAGE_WIDTH][i % IMAGE_WIDTH] == null) {
                image[i / IMAGE_WIDTH][i % IMAGE_WIDTH] = charAt == '0' ? " " : "#";
            }
        }
    }

}
