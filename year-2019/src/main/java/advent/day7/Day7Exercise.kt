package advent.day7

import advent.IntCodeProgram
import advent.getInput
import java.util.Collections.swap

fun main() {
    val program = getInput(7).first().let(::IntCodeProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: IntCodeProgram): Long {
    return listOf(0L, 1L, 2L, 3L, 4L).permutations().associateWith { Programs(program.reset()) }
        .map { it.key zip it.value.programs }
        .map(::Amplifiers)
        .maxOf(Amplifiers::runSequence)
}

private fun part2(program: IntCodeProgram): Long {
    return listOf(5L, 6L, 7L, 8L, 9L).permutations().associateWith { Programs(program.reset()) }
        .map { it.key zip it.value.programs }
        .map(::Amplifiers)
        .maxOf(Amplifiers::runCyclic)
}

private data class Amplifiers(val computers: List<Pair<Long, IntCodeProgram>>) {
    
    val programs = computers.map { (initialValue, program) ->
        program.addInput(initialValue)
        program
    }

    fun runSequence(): Long {
        return programs.fold(0L) { acc, intCodeProgram ->
            intCodeProgram.addInput(acc)
            intCodeProgram.run()
            intCodeProgram.getOutput()
        }
    }

    fun runCyclic(): Long {
        var value = 0L
        while (programs.any { !it.isFinished() }) {
            value = programs.fold(value) { acc, intCodeProgram ->
                intCodeProgram.addInput(acc)
                intCodeProgram.run()
                intCodeProgram.getOutput()
            }
        }
        return value
    }
}

private data class Programs(private val program: IntCodeProgram) {

    val programs = listOf(
        program.reset(),
        program.reset(),
        program.reset(),
        program.reset(),
        program.reset(),
    )
}

fun List<Long>.permutations(): List<List<Long>> {
    val solutions = mutableListOf<List<Long>>()
    permutationsRecursive(this, 0, solutions)
    return solutions
}

fun permutationsRecursive(input: List<Long>, index: Int, answers: MutableList<List<Long>>) {
    if (index == input.lastIndex) answers.add(input.toList())
    for (i in index .. input.lastIndex) {
        swap(input, index, i)
        permutationsRecursive(input, index + 1, answers)
        swap(input, i, index)
    }
}
