package advent.day7;

import advent.InputProvider;
import advent.IntCodeComputer;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

public final class Day7Exercise {

    private Day7Exercise() {
    }

    public static void main(final String[] args) {
        String input = InputProvider.getInput(7).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        List<IntCodeComputer> computers = IntStream.rangeClosed(0, 4)
                .mapToObj(i -> new IntCodeComputer(input, new ArrayDeque<>(2), new ArrayDeque<>(1)))
                .collect(toList());
        List<Integer> integers = IntStream.rangeClosed(0, 4)
                .boxed()
                .collect(toCollection(ArrayList::new));
        List<List<Integer>> permutations = new ArrayList<>();
        addPermutations(integers, 0, permutations);
        long signal = Long.MIN_VALUE;
        for (List<Integer> permutation : permutations) {
            long currentSignal = 0L;
            for (int i = 0; i < computers.size(); i++) {
                computers.get(i).provideInput((long) permutation.get(i));
                computers.get(i).provideInput(currentSignal);
                computers.get(i).run();
                currentSignal = computers.get(i).retrieveFirstOutput();
            }
            signal = Math.max(signal, currentSignal);
            computers.forEach(IntCodeComputer::reset);
        }
        System.out.println(signal);
    }

    private static void part2(final String input) {
        List<IntCodeComputer> computers = IntStream.rangeClosed(0, 4)
                .mapToObj(i -> new IntCodeComputer(input, new ArrayDeque<>(2), new ArrayDeque<>(1)))
                .collect(toList());
        List<Integer> integers = IntStream.rangeClosed(5, 9)
                .boxed()
                .collect(toCollection(ArrayList::new));
        List<List<Integer>> permutations = new ArrayList<>();
        addPermutations(integers, 0, permutations);
        long signal = Long.MIN_VALUE;
        for (List<Integer> permutation : permutations) {
            signal = Math.max(signal, runComputers(computers, permutation));
        }
        System.out.println(signal);
    }

    private static void addPermutations(final List<Integer> integers, final int index, final List<List<Integer>> permutations) {
        for (int i = index; i < integers.size(); i++) {
            Collections.swap(integers, i, index);
            addPermutations(integers, index + 1, permutations);
            Collections.swap(integers, index, i);
        }
        if (index == integers.size() - 1) {
            permutations.add(new ArrayList<>(integers));
        }
    }

    private static long runComputers(final List<IntCodeComputer> computers, final List<Integer> permutation) {
        for (int i = 0; i < computers.size(); i++) {
            computers.get(i).provideInput((long) permutation.get(i));
        }
        int index = 0;
        long signal = 0L;
        while (!haveAllHalted(computers)) {
            computers.get(index).provideInput(signal);
            computers.get(index).run();
            signal = computers.get(index).retrieveFirstOutput();
            index = (index + 1) % 5;
        }
        computers.forEach(IntCodeComputer::reset);
        return signal;
    }

    private static boolean haveAllHalted(final List<IntCodeComputer> computers) {
        return computers.stream()
                .allMatch(IntCodeComputer::isFinished);
    }

}
