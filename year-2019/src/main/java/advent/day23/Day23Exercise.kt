package advent.day23

import advent.IntCodeProgram
import advent.getInput

fun main() {
    val program = getInput(23).first().let(::IntCodeProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: IntCodeProgram): Long {
    val programs = (0L..<50L).map { program.reset().apply { addInput(it);run() } }
    while (programs.any { !it.isFinished() }) {
        val (_, natY) = programs.fetchNatCoordinates()
        programs.addIdleValuesAndRunAll()
        if (natY != null) return natY
    }
    throw IllegalStateException("No nat address found")
}

private fun part2(program: IntCodeProgram): Long {
    val programs = (0L..<50L).map { program.reset().apply { addInput(it);run() } }
    var lastSentY: Long? = null
    var lastReceivedX: Long? = null
    var lastReceivedY: Long? = null
    while (programs.any { !it.isFinished() }) {
        val isIdle = programs.none { it.hasInputs() && it.hasOutputs() }
        if (isIdle && lastReceivedX != null && lastReceivedY != null) {
            programs.first().run {
                addInput(requireNotNull(lastReceivedX) { "Why has lastReceivedX been changed to null?"})
                addInput(requireNotNull(lastReceivedY) { "Why has lastReceivedY been changed to null?"})
            }
            if (lastSentY == lastReceivedY) return lastSentY
            lastSentY = lastReceivedY
            lastReceivedX = null
            lastReceivedY = null
        }
        val (natX, natY) = programs.fetchNatCoordinates()
        lastReceivedX = natX ?: lastReceivedX
        lastReceivedY = natY ?: lastReceivedY
        programs.addIdleValuesAndRunAll()
    }
    throw IllegalStateException("No consecutive results from nat address found")
}

private fun List<IntCodeProgram>.fetchNatCoordinates(): Pair<Long?, Long?> {
    var natX: Long? = null
    var natY: Long? = null
    forEach { program ->
        while (program.hasOutputs()) {
            val address = program.getOutput()
            val x = program.getOutput()
            val y = program.getOutput()
            if (address == 255L) {
                natX = x
                natY = y
            } else {
                this[address.toInt()].run {
                    addInput(x)
                    addInput(y)
                }
            }
        }
    }
    return natX to natY
}

private fun List<IntCodeProgram>.addIdleValuesAndRunAll() {
    forEach { if (!it.hasInputs()) it.addInput(-1L);it.run() }
}
