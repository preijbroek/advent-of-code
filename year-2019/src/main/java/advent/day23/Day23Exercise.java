package advent.day23;

import advent.InputProvider;
import advent.IntCodeComputer;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public final class Day23Exercise {

    private static final long NAT_ADDRESS = 255L;

    private Day23Exercise() {
    }

    public static void main(String[] args) throws IOException {
        String code = InputProvider.getInput(23).get(0);
        List<IntCodeComputer> computers = createComputers(code);
        Long X = null, Y = null, previousY = null, firstY = null;
        while (!computers.stream().allMatch(IntCodeComputer::isFinished)) {
            boolean isIdle = allIdle(computers);
            if (isIdle && X != null && Y != null) {
                computers.get(0).provideInput(X);
                computers.get(0).provideInput(Y);
                if (firstY == null) {
                    firstY = Y;
                }
                if (Y.equals(previousY)) {
                    break;
                }
                previousY = Y;
            }
            Long[] xy = runComputers(computers);
            if (Arrays.stream(xy).allMatch(Objects::nonNull)) {
                X = xy[0];
                Y = xy[1];
            }
            provideIdleValues(computers);
        }
        System.out.println("Part 1: " + firstY);
        System.out.println("Part 2: " + previousY);
    }

    private static List<IntCodeComputer> createComputers(final String code) {
        Queue<Long> output = new ArrayDeque<>(10);
        List<IntCodeComputer> computers = IntStream.range(0, 50).mapToObj(i ->
                new IntCodeComputer(code, new ArrayDeque<>(1), output))
                .collect(Collectors.toList());
        LongStream.range(0, 50).boxed().forEach(l -> {
            computers.get(l.intValue()).provideInput(l);
            computers.get(l.intValue()).run();
        });
        return computers;
    }

    private static boolean allIdle(final List<IntCodeComputer> computers) {
        return computers.stream().noneMatch(IntCodeComputer::hasInput)
                && computers.stream().noneMatch(IntCodeComputer::hasOutput);
    }

    private static Long[] runComputers(final List<IntCodeComputer> computers) {
        Long[] xAndY = new Long[2];
        for (IntCodeComputer computer : computers) {
            Long[] xy = getXandY(computers, computer);
            if (Arrays.stream(xy).allMatch(Objects::nonNull)) {
                xAndY[0] = xy[0];
                xAndY[1] = xy[1];
            }
        }
        return xAndY;
    }

    private static Long[] getXandY(final List<IntCodeComputer> computers,
                                   final IntCodeComputer computer) {
        Long[] xy = new Long[2];
        while (computer.hasOutput()) {
            Long address = computer.retrieveFirstOutput();
            Long x = computer.retrieveFirstOutput();
            Long y = computer.retrieveFirstOutput();
            if (address.equals(NAT_ADDRESS)) {
                xy[0] = x;
                xy[1] = y;
                continue;
            }
            IntCodeComputer recipient = computers.get(address.intValue());
            recipient.provideInput(x);
            recipient.provideInput(y);
        }
        return xy;
    }

    private static void provideIdleValues(final List<IntCodeComputer> computers) {
        for (IntCodeComputer computer : computers) {
            if (!computer.hasInput()) computer.provideInput(-1L);
            computer.run();
        }
    }
}
