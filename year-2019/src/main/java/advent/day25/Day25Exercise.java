package advent.day25;

import advent.ASCIIComputer;
import advent.InputProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public final class Day25Exercise {

    private static final String TAKE = "take ";
    private static final String DROP = "drop ";
    private static final String EAST = "east";
    private static final String SOUTH = "south";
    private static final String WEST = "west";
    private static final String NORTH = "north";
    private static final String INV = "inv";

    private Day25Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        String code = InputProvider.getInput(25).get(0);
        part1(code);
        part2(code);
    }

    private static void part1(final String code) {
        ASCIIComputer computer = new ASCIIComputer(code);
        List<String> forbidden = Arrays.asList("escape pod", "photons", "infinite loop", "molten lava", "giant electromagnet");
        List<String> directions = Arrays.asList(EAST, SOUTH, WEST, NORTH);
        List<String> inventory = new ArrayList<>();
        List<String> wanted = new ArrayList<>();
        Random random = new Random();
        while (!computer.isFinished()) {
            computer.run();
            String output = computer.readOutput();
            if (output.contains("You can't go that way.")) {
                break;
            }
            if (output.contains("== Pressure-Sensitive Floor ==")) {
                if (output.contains("Alert! Droids on this ship are heavier than the detected value!\"")
                        || output.contains("Alert! Droids on this ship are lighter than the detected value!\"")) {
                    computer.reset();
                    wanted = new ArrayList<>();
                    for (String item : inventory) {
                        if (random.nextBoolean()) {
                            wanted.add(item);
                        }
                    }
                    continue;
                } else {
                    System.out.println(output);
                    break;
                }
            }
            for (String s : output.split("\n")) {
                String param = s;
                if (param.startsWith("- ")) {
                    param = param.substring(2);
                    if (!directions.contains(param) && !forbidden.contains(param) && wanted.contains(param)) {
                        computer.addCommand(TAKE + param);
                    }
                    String paramForStream = param;
                    if (Stream.of(directions, forbidden, inventory).noneMatch(strings -> strings.contains(paramForStream))) {
                        System.out.println("Found new stuff: " + param);
                        computer.addCommand(TAKE + param);
                        inventory.add(param);
                    }
                }
            }
            String direction;
            do {
                int directionInt = random.nextInt(4);
                direction = directions.get(directionInt);
            } while (!output.contains(direction));
            computer.addCommand(direction);
        }
    }

    private static void part2(final String code) {

    }
}
