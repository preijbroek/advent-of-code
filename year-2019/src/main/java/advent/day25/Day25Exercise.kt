package advent.day25

import advent.ASCIIProgram
import advent.IntCodeProgram
import advent.getInput
import util.Direction
import kotlin.random.Random

fun main() {
    val program = getInput(25).first().let(::IntCodeProgram).let(::ASCIIProgram)
    println(part1(program))
}

private fun part1(program: ASCIIProgram): String {
    val directions = Direction.values().map { it.name.lowercase() }
    var currentProgram = program
    val inventory = mutableSetOf<String>()
    // Discovered in trial by error. Taking any of these will keep the program running forever
    val forbiddenItems = listOf(
        "photons",
        "giant electromagnet",
        "molten lava",
        "escape pod",
        "infinite loop"
    )
    var wanted = listOf<String>()
    val random = Random(1L)
    while (!currentProgram.isFinished()) {
        currentProgram.run()
        val output = currentProgram.getAllCharOutputs().joinToString(separator = "")
        if ("== Pressure-Sensitive Floor ==" in output) {
            if ("Analysis complete! You may proceed." in output) {
                return output
            } else {
                currentProgram = currentProgram.reset()
                wanted = inventory.filter { random.nextBoolean() }
            }
        } else {
            output.lines().filter { it.startsWith("- ") }.map { it.drop(2) }
                .filter { it !in directions && it !in forbiddenItems }
                .filter { it in wanted || it !in inventory }
                .forEach {
                    currentProgram.take(it)
                    inventory.add(it)
                }
            generateSequence { random.nextInt(4) }.map { directions[it] }
                .first { it in output }.run { currentProgram.addLine(this) }
        }
    }
    throw IllegalStateException("Could not find correct configuration of items.")
}

private fun ASCIIProgram.take(item: String) = addLine("take $item")
