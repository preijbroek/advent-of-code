package advent.day24;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Tile3D {

    private final int x;
    private final int y;
    private final int z;
    private final boolean hasBug;

    public Tile3D(final int x, final int y, final int z, final boolean hasBug) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.hasBug = hasBug;
    }

    public List<Tile3D> neighbours(final Set<Tile3D> tiles) {
        List<Tile3D> directNeighbours = Stream.of(
                new Tile3D(x - 1, y, z, hasBug),
                new Tile3D(x - 1, y, z, !hasBug),
                new Tile3D(x + 1, y, z, hasBug),
                new Tile3D(x + 1, y, z, !hasBug),
                new Tile3D(x, y - 1, z, hasBug),
                new Tile3D(x, y - 1, z, !hasBug),
                new Tile3D(x, y + 1, z, hasBug),
                new Tile3D(x, y + 1, z, !hasBug)
        )
                .filter(Tile3D::isInGrid)
                .filter(tile -> !tile.isZero())
                .collect(Collectors.toCollection(LinkedList::new));
        List<Tile3D> present = tiles.stream()
                .filter(directNeighbours::contains)
                .collect(Collectors.toList());
        List<Tile3D> neighbours = directNeighbours.stream()
                .filter(tile -> !tile.hasBug)
                .filter(tile -> !tiles.contains(tile))
                .collect(Collectors.toList());
        neighbours.addAll(present);
        neighbours.addAll(innerNeighbours(tiles));
        neighbours.addAll(outerNeighbours(tiles));
        return neighbours;
    }

    private boolean isInGrid() {
        return 0 <= x && x < 5 && 0 <= y && y < 5 && isNotCentre();
    }

    private boolean isNotCentre() {
        return x != 2 || y != 2;
    }

    private List<Tile3D> innerNeighbours(final Set<Tile3D> tiles) {
        List<Tile3D> innerNeighbours = Collections.emptyList();
        if (x == 1 && y == 2) {
            innerNeighbours = Arrays.asList(
                    new Tile3D(0, 0, z - 1, hasBug),
                    new Tile3D(0, 0, z - 1, !hasBug),
                    new Tile3D(0, 1, z - 1, hasBug),
                    new Tile3D(0, 1, z - 1, !hasBug),
                    new Tile3D(0, 2, z - 1, hasBug),
                    new Tile3D(0, 2, z - 1, !hasBug),
                    new Tile3D(0, 3, z - 1, hasBug),
                    new Tile3D(0, 3, z - 1, !hasBug),
                    new Tile3D(0, 4, z - 1, hasBug),
                    new Tile3D(0, 4, z - 1, !hasBug)
            );
        } else if (x == 3 && y == 2) {
            innerNeighbours = Arrays.asList(
                    new Tile3D(4, 0, z - 1, hasBug),
                    new Tile3D(4, 0, z - 1, !hasBug),
                    new Tile3D(4, 1, z - 1, hasBug),
                    new Tile3D(4, 1, z - 1, !hasBug),
                    new Tile3D(4, 2, z - 1, hasBug),
                    new Tile3D(4, 2, z - 1, !hasBug),
                    new Tile3D(4, 3, z - 1, hasBug),
                    new Tile3D(4, 3, z - 1, !hasBug),
                    new Tile3D(4, 4, z - 1, hasBug),
                    new Tile3D(4, 4, z - 1, !hasBug)
            );
        } else if (x == 2 && y == 1) {
            innerNeighbours = Arrays.asList(
                    new Tile3D(0, 0, z - 1, hasBug),
                    new Tile3D(0, 0, z - 1, !hasBug),
                    new Tile3D(1, 0, z - 1, hasBug),
                    new Tile3D(1, 0, z - 1, !hasBug),
                    new Tile3D(2, 0, z - 1, hasBug),
                    new Tile3D(2, 0, z - 1, !hasBug),
                    new Tile3D(3, 0, z - 1, hasBug),
                    new Tile3D(3, 0, z - 1, !hasBug),
                    new Tile3D(4, 0, z - 1, hasBug),
                    new Tile3D(4, 0, z - 1, !hasBug)
            );
        } else if (x == 2 && y == 3) {
            innerNeighbours = Arrays.asList(
                    new Tile3D(0, 4, z - 1, hasBug),
                    new Tile3D(0, 4, z - 1, !hasBug),
                    new Tile3D(1, 4, z - 1, hasBug),
                    new Tile3D(1, 4, z - 1, !hasBug),
                    new Tile3D(2, 4, z - 1, hasBug),
                    new Tile3D(2, 4, z - 1, !hasBug),
                    new Tile3D(3, 4, z - 1, hasBug),
                    new Tile3D(3, 4, z - 1, !hasBug),
                    new Tile3D(4, 4, z - 1, hasBug),
                    new Tile3D(4, 4, z - 1, !hasBug)
            );
        }
        List<Tile3D> present = tiles.stream()
                .filter(innerNeighbours::contains)
                .collect(Collectors.toList());
        innerNeighbours = innerNeighbours.stream()
                .filter(tile3D -> !tile3D.hasBug())
                .filter(tile3D -> !tiles.contains(tile3D))
                .collect(Collectors.toList());
        innerNeighbours.addAll(present);
        return innerNeighbours;
    }

    private List<Tile3D> outerNeighbours(final Set<Tile3D> tiles) {
        List<Tile3D> outerNeighbours = new LinkedList<>();
        if (x == 0) {
            outerNeighbours.add(new Tile3D(1, 2, z + 1, hasBug));
            outerNeighbours.add(new Tile3D(1, 2, z + 1, !hasBug));
        } else if (x == 4) {
            outerNeighbours.add(new Tile3D(3, 2, z + 1, hasBug));
            outerNeighbours.add(new Tile3D(3, 2, z + 1, !hasBug));
        }
        if (y == 0) {
            outerNeighbours.add(new Tile3D(2, 1, z + 1, hasBug));
            outerNeighbours.add(new Tile3D(2, 1, z + 1, !hasBug));
        } else if (y == 4) {
            outerNeighbours.add(new Tile3D(2, 3, z + 1, hasBug));
            outerNeighbours.add(new Tile3D(2, 3, z + 1, !hasBug));
        }
        List<Tile3D> present = tiles.stream()
                .filter(outerNeighbours::contains)
                .collect(Collectors.toList());
        outerNeighbours = outerNeighbours.stream()
                .filter(tile3D -> !tile3D.hasBug())
                .filter(tile3D -> !tiles.contains(tile3D))
                .collect(Collectors.toList());
        outerNeighbours.addAll(present);
        return outerNeighbours;
    }

    public boolean hasBug() {
        return hasBug;
    }

    public boolean isZero() {
        return x == 2 && y == 2;
    }

    public Tile3D withBug() {
        return new Tile3D(x, y, z,true);
    }

    public Tile3D withoutBug() {
        return new Tile3D(x, y, z,false);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tile3D tile3D = (Tile3D) o;
        return x == tile3D.x &&
                y == tile3D.y &&
                z == tile3D.z;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }
}
