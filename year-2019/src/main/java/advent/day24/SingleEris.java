package advent.day24;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class SingleEris {

    private final Set<Tile> tiles = new HashSet<>(25);
    private final Set<Set<Tile>> gridHash = new HashSet<>();
    private boolean hasDuplicate = false;

    public SingleEris(final List<String> code) {
        for (int i = 0; i < code.size(); i++) {
            for (int j = 0; j < code.get(i).length(); j++) {
                tiles.add(new Tile(j, i, code.get(i).charAt(j) == '#'));
            }
        }
        gridHash.add(new HashSet<>(tiles));
        draw();
    }

    public void process() {
        Set<Tile> update = new HashSet<>();
        for (Tile tile : tiles) {
            long adjacentBugs = tile.neighbours(tiles).stream().filter(Tile::hasBug).count();
            if (tile.hasBug()) {
                if (adjacentBugs != 1L) {
                    update.add(tile.withoutBug());
                } else {
                    update.add(tile);
                }
            } else {
                if (adjacentBugs == 1L || adjacentBugs == 2L) {
                    update.add(tile.withBug());
                } else {
                    update.add(tile);
                }
            }
        }
        tiles.clear();
        tiles.addAll(update);
        if (gridHash.contains(tiles)) {
            hasDuplicate = true;
        } else {
            gridHash.add(new HashSet<>(tiles));
        }
        draw();
    }

    public boolean hasDuplicate() {
        return hasDuplicate;
    }

    public long biodiversityRating() {
        return tiles.stream().filter(Tile::hasBug)
                .mapToLong(tile -> tile.getY() * 5L + tile.getX())
                .map(value -> ((long) Math.pow(2L, value)))
                .sum();
    }

    public void draw() {
        List<Tile> draws = tiles.stream().sorted(Comparator.comparing(Tile::getY).thenComparing(Tile::getX))
                .collect(Collectors.toList());
        for (int i = 0; i < draws.size(); i++) {
            if (i > 0 && draws.get(i).getY() > draws.get(i - 1).getY()) {
                System.out.println();
            }
            System.out.print(draws.get(i).hasBug() ? '#' : '.');
        }
        System.out.println();
        System.out.println();
    }
}
