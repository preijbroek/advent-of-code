package advent.day24;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class Day24Exercise {

    private Day24Exercise() {
    }

    public static void main(String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("src/main/resources/day24exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        SingleEris singleEris = new SingleEris(code);
        while (!singleEris.hasDuplicate()) {
            singleEris.process();
        }
        System.out.println("Part 1: " + singleEris.biodiversityRating());
    }

    private static void part2(final List<String> code) {
        MultipleEris multipleEris = new MultipleEris(code);
        for (int i = 0; i < 200; i++) {
            multipleEris.process();
        }
        System.out.println("Part 2: " + multipleEris.countBugs());
    }

}
