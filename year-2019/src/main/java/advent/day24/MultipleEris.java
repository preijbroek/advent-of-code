package advent.day24;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class MultipleEris {

    private final Set<Tile3D> tiles = new HashSet<>();

    public MultipleEris(List<String> code) {
        for (int i = 0; i < code.size(); i++) {
            for (int j = 0; j < code.get(i).length(); j++) {
                tiles.add(new Tile3D(j, i, 0,code.get(i).charAt(j) == '#'));
            }
        }
    }

    public void process() {
        Set<Tile3D> update = new HashSet<>();
        Set<Tile3D> newTiles = tiles.stream()
                .flatMap(tile -> tile.neighbours(tiles).stream())
                .filter(tile -> !tiles.contains(tile))
                .collect(Collectors.toSet());
        tiles.addAll(newTiles);
        for (Tile3D tile : tiles) {
            if (tile.isZero()) {
                continue;
            }
            long adjacentBugs = tile.neighbours(tiles)
                    .stream()
                    .filter(Tile3D::hasBug)
                    .count();
            if (tile.hasBug()) {
                if (adjacentBugs != 1L) {
                    update.add(tile.withoutBug());
                } else {
                    update.add(tile);
                }
            } else {
                if (adjacentBugs == 1L || adjacentBugs == 2L) {
                    update.add(tile.withBug());
                } else {
                    update.add(tile);
                }
            }
        }
        tiles.clear();
        tiles.addAll(update);
    }

    public long countBugs() {
        return tiles.stream().filter(Tile3D::hasBug).count();
    }
}
