package advent.day24;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Tile {

    private final int x;
    private final int y;
    private final boolean hasBug;

    public Tile(final int x, final int y, final boolean hasBug) {
        this.x = x;
        this.y = y;
        this.hasBug = hasBug;
    }

    public List<Tile> neighbours(final Set<Tile> tiles) {
        return Stream.of(
                new Tile(x - 1, y, hasBug),
                new Tile(x - 1, y, !hasBug),
                new Tile(x + 1, y, hasBug),
                new Tile(x + 1, y, !hasBug),
                new Tile(x, y - 1, hasBug),
                new Tile(x, y - 1, !hasBug),
                new Tile(x, y + 1, hasBug),
                new Tile(x, y + 1, !hasBug)
        )
                .filter(tiles::contains)
                .filter(Tile::isInGrid)
                .collect(Collectors.toList());
    }

    private boolean isInGrid() {
        return 0 <= x && x < 5 && 0 <= y && y < 5;
    }

    public Tile withBug() {
        return new Tile(x, y, true);
    }

    public Tile withoutBug() {
        return new Tile(x, y, false);
    }

    public boolean hasBug() {
        return hasBug;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tile tile = (Tile) o;
        return x == tile.x &&
                y == tile.y &&
                hasBug == tile.hasBug;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, hasBug);
    }

    @Override
    public String toString() {
        return "Tile{" +
                "x=" + x +
                ", y=" + y +
                ", hasBug=" + hasBug +
                '}';
    }
}
