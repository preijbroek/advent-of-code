package advent.day21

import advent.ASCIIProgram
import advent.IntCodeProgram
import advent.getInput

fun main() {
    val program = getInput(21).first().let(::IntCodeProgram).let(::ASCIIProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: ASCIIProgram): Long {
    program.addLine("NOT B J")
    program.addLine("NOT C T")
    program.addLine("OR T J")
    program.addLine("AND D J")
    program.addLine("NOT A T")
    program.addLine("OR T J")
    program.addLine("WALK")
    program.run()
    return program.getAllOutputs().last()
}

private fun part2(program: ASCIIProgram): Long {
    program.addLine("NOT B J")
    program.addLine("NOT C T")
    program.addLine("OR T J")
    program.addLine("AND D J")
    program.addLine("AND H J")
    program.addLine("NOT A T")
    program.addLine("OR T J")
    program.addLine("RUN")
    program.run()
    return program.getAllOutputs().last()
}
