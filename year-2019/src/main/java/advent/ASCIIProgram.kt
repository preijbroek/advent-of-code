package advent

data class ASCIIProgram(
    val intCodeProgram: IntCodeProgram,
) {

    fun reset() = ASCIIProgram(intCodeProgram.reset())

    operator fun set(index: Long, value: Long) = intCodeProgram.set(index, value)

    fun addInput(c: Char) = intCodeProgram.addInput(c.code.toLong())

    fun addInput(s: String) = s.forEach { intCodeProgram.addInput(it.code.toLong()) }

    fun addLine(s: String) = addInput("$s\n")

    fun getOutput() = intCodeProgram.getOutput()

    fun getAllOutputs() = intCodeProgram.getAllOutputs()

    fun getAllCharOutputs() = getAllOutputs().map { it.charValue() }

    private fun Long.charValue() = toInt().toChar()

    fun isFinished() = intCodeProgram.isFinished()

    fun run() = intCodeProgram.run()
}
