package advent.day12

import advent.getInput
import util.Position
import util.lcm
import util.second
import util.third

fun main() {
    val moons = getInput(12).map {
        it.drop(1).dropLast(1).split(", ")
            .map { s -> s.drop(2).toInt() }
            .let { (x, y, z) -> Position(x, y, z) }
            .let(::Moon)
    }
    println(part1(moons))
    println(part2(moons))
}

private fun part1(moons: List<Moon>): Int {
    return generateSequence(moons) { it.map { moon -> moon.move(it) } }
        .take(1001)
        .last()
        .sumOf(Moon::totalEnergy)
}

private fun part2(moons: List<Moon>): Long {
    val seenConstellationsX = mutableSetOf<List<Pair<Int, Int>>>()
    val seenConstellationsY = mutableSetOf<List<Pair<Int, Int>>>()
    val seenConstellationsZ = mutableSetOf<List<Pair<Int, Int>>>()
    var firstXRepeat: Long? = null
    var firstYRepeat: Long? = null
    var firstZRepeat: Long? = null
    var index = 0L
    var currentMoons = moons
    do {
        val (xConstellations, yConstellations, zConstellations) = currentMoons.constellationsVector()
        if (firstXRepeat == null && !seenConstellationsX.add(xConstellations)) {
            firstXRepeat = index
        }
        if (firstYRepeat == null && !seenConstellationsY.add(yConstellations)) {
            firstYRepeat = index
        }
        if (firstZRepeat == null && !seenConstellationsZ.add(zConstellations)) {
            firstZRepeat = index
        }
        currentMoons = currentMoons.map { it.move(currentMoons) }
        index++
    } while (firstXRepeat == null || firstYRepeat == null || firstZRepeat == null)
    return firstXRepeat lcm firstYRepeat lcm firstZRepeat
}

private fun List<Moon>.constellationsVector() = listOf(
    map { it.movementPairs().first() },
    map { it.movementPairs().second() },
    map { it.movementPairs().third() },
)
