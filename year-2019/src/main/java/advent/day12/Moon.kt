package advent.day12

import util.Position
import util.plus

data class Moon(
    val position: Position,
    val velocity: Position = Position.CENTRE
) {

    fun move(moons: List<Moon>): Moon {
        val velocityVector = moons.map(::velocityVector).reduce(Position::plus)
        val nextVelocity = velocity + velocityVector
        val nextPosition = position + nextVelocity
        return Moon(
            position = nextPosition,
            velocity = nextVelocity,
        )
    }

    fun totalEnergy() = position.distanceToCentre() * velocity.distanceToCentre()

    fun movementPairs() = listOf(
        position.x to velocity.x,
        position.y to velocity.y,
        position.z to velocity.z,
    )

    private fun velocityVector(other: Moon): Position {
        return Position(
            x = other.position.x.compareTo(position.x),
            y = other.position.y.compareTo(position.y),
            z = other.position.z.compareTo(position.z),
        )
    }
}
