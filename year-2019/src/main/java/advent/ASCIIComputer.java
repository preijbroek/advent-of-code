package advent;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.List;

import static java.util.stream.Collectors.joining;

public final class ASCIIComputer {

    private static final long TERMINATION_COMMAND = 10L;
    private final IntCodeComputer computer;

    public ASCIIComputer(final String code) {
        this.computer = new IntCodeComputer(code, new ArrayDeque<>(100), new ArrayDeque<>(100));
    }

    public void reset() {
        computer.reset();
    }

    public void addCommand(final String command) {
        command.chars()
                .boxed()
                .map(Integer::longValue)
                .forEachOrdered(computer::provideInput);
        computer.provideInput(TERMINATION_COMMAND);
    }

    public void run() {
        computer.run();
    }

    public String readOutput() {
        List<Long> output = new LinkedList<>();
        while (computer.hasOutput()) {
            output.add(computer.retrieveFirstOutput());
        }
        return output.stream()
                .mapToInt(Long::intValue)
                .mapToObj(i -> (char) i)
                .map(String::valueOf)
                .collect(joining());
    }

    public boolean isFinished() {
        return computer.isFinished();
    }
}
