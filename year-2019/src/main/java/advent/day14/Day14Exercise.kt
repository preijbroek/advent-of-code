package advent.day14

import advent.getInput
import util.ceil
import util.div
import util.toDecimal

fun main() {
    val reactions = getInput(14).map {
        it.split(" => ").let { (income, outcome) -> Reaction(outcome, income.split(", ")) }
    }.associateBy { it.produceType }
    println(part1(reactions))
    println(part2(reactions))
}

private fun part1(reactions: Map<String, Reaction>): Long {
    return reactions.calculateRequiredOres(
        target = "FUEL",
        targetAmount = 1L,
        surplus = mutableMapOf<String, Long>().withDefault { 0L }
    )
}

private fun part2(reactions: Map<String, Reaction>): Long {
    var remainingTargetOres = 1000000000000L
    var targetAmount = remainingTargetOres / reactions.calculateRequiredOres(
        target = "FUEL",
        targetAmount = 1L,
        surplus = mutableMapOf<String, Long>().withDefault { 0L }
    )
    var fuel = 0L
    val surplus = mutableMapOf<String, Long>().withDefault { 0L }
    while (remainingTargetOres > 0 && targetAmount > 0) {
        val newSurplus = surplus.toMutableMap().withDefault { 0L }
        val usedOres = reactions.calculateRequiredOres("FUEL", targetAmount, newSurplus)
        if (usedOres > remainingTargetOres) {
            targetAmount /= 2
        } else {
            fuel += targetAmount
            remainingTargetOres -= usedOres
            surplus.clear()
            surplus.putAll(newSurplus)
        }
    }
    return fuel
}

private fun Map<String, Reaction>.calculateRequiredOres(
    target: String,
    targetAmount: Long,
    surplus: MutableMap<String, Long>,
): Long {
    return if (target == "ORE") {
        targetAmount
    } else if (targetAmount <= surplus.getValue(target)) {
        surplus[target] = surplus.getValue(target) - targetAmount
        0L
    } else {
        val nextTargetAmount = targetAmount - surplus.getValue(target)
        surplus[target] = 0L
        var ore = 0L
        val reaction = getValue(target)
        val copies = (nextTargetAmount.toDecimal() / reaction.produceAmount.toDecimal()).ceil().toLong()
        reaction.consumes.forEach { (consumeAmount, consumeType) ->
            val inputAmount = consumeAmount * copies
            ore += calculateRequiredOres(target = consumeType, targetAmount = inputAmount, surplus = surplus)
        }
        surplus[target] = surplus.getValue(target) - nextTargetAmount + reaction.produceAmount * copies
        return ore
    }
}

