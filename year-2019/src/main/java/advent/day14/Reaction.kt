package advent.day14

data class Reaction(
    private val outcome: String,
    private val income: List<String>,
) {

    private val produces = outcome.ingredients()
    val produceType = produces.second
    val produceAmount = produces.first
    val consumes = income.map { it.ingredients() }

    private fun String.ingredients() = split(" ")
        .let { (amount, ingredient) -> amount.toLong() to ingredient }
}
