package advent.day19

import advent.IntCodeProgram
import advent.getInput
import util.Position
import util.x

fun main() {
    val program = getInput(19).first().let(::IntCodeProgram)
    println(part1(program.reset()))
    println(part2(program.reset()))
}

private fun part1(program: IntCodeProgram): Int {
    return ((0L..<50L) x (0L..<50L)).count { (x, y) ->
        program.isInBeam(Position(x, y))
    }
}

private fun part2(program: IntCodeProgram): Long {
    var x = 0L
    var y = 0L
    while (!program.reset().isInBeam(Position(x + 99L, y))) {
        y++
        while (!program.reset().isInBeam(Position(x, y + 99L))) {
            x++
        }
    }
    return 10000 * x + y
}

private fun IntCodeProgram.isInBeam(position: Position) =
    reset().run {
        addInput(position.x.toLong())
        addInput(position.y.toLong())
        run()
        getOutput()
    } == 1L
