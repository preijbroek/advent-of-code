package advent.day10

import advent.getInput
import util.Position
import util.toCharMap

fun main() {
    val asteroids = getInput(10).toCharMap().filterValues { it != '.' }.keys
    println(part1(asteroids))
    println(part2(asteroids))
}

private fun part1(asteroids: Set<Position>): Int {
    return asteroids.map { Asteroid(it, asteroids) }
        .maxOf { it.sightCount() }
}

private fun part2(asteroids: Set<Position>): Int {
    return asteroids.map { Asteroid(it, asteroids) }
        .maxBy { it.sightCount() }
        .shotAsteroid(nth = 200)
        .let { 100 * it.x + it.y }
}
