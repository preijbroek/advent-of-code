package advent.day10

import util.Decimal
import util.Position
import util.div
import util.minus
import util.toDecimal

data class Asteroid(
    private val position: Position,
    private val asteroids: Set<Position>
) {

    private val otherAsteroids: Set<Position> = asteroids - position
    private val rightHalf: Set<Position>
    private val leftHalf: Set<Position>

    init {
        val (right, left) = otherAsteroids.partition { it.x > position.x || (it.x == position.x && it.y > position.y) }
        rightHalf = right.toSet()
        leftHalf = left.toSet()
    }

    fun sightCount(): Int {
        return rightHalf.groupedFromAsteroid().keys.size + leftHalf.groupedFromAsteroid().keys.size
    }

    fun shotAsteroid(nth: Int): Position {
        val asteroidGroups = (rightHalf.groupedFromAsteroid().mapValues { (_, values) -> ArrayDeque(values) }.toList() +
                leftHalf.groupedFromAsteroid().mapValues { (_, values) -> ArrayDeque(values) }.toList())
            .let(::ArrayDeque)
        var shotAsteroids = 0
        var nextShotAsteroid: Position? = null
        while (shotAsteroids != nth && asteroidGroups.isNotEmpty()) {
            shotAsteroids++
            val nextGroup = asteroidGroups.removeFirst()
            val (_, asteroids) = nextGroup
            nextShotAsteroid = asteroids.removeFirst()
            if (asteroids.isNotEmpty()) {
                asteroidGroups.addLast(nextGroup)
            }
        }
        return nextShotAsteroid.takeIf { shotAsteroids == nth }
            ?: throw IllegalArgumentException("$nth asteroids should have been destroyed but only $shotAsteroids were present")
    }

    private fun Set<Position>.groupedFromAsteroid() = groupBy { position / it }
        .entries.sortedBy { it.key }
        .associate { (key, value) -> key to value.sortedBy { it distanceTo position } }

    private operator fun Position.div(other: Position): Decimal {
        val (thisX, thisY) = let { it.x.toDecimal() to it.y.toDecimal() }
        val (otherX, otherY) = other.let { it.x.toDecimal() to it.y.toDecimal() }
        val differenceX = otherX - thisX
        val differenceY = otherY - thisY
        return differenceY / differenceX
    }
}
