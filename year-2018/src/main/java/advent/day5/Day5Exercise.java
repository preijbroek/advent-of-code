package advent.day5;

import advent.InputProvider;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toMap;

public final class Day5Exercise {

    private Day5Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(5).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        Map<Character, Character> charMap = IntStream.range(0, 26)
                .map(i -> i + 'a')
                .mapToObj(i -> (char) i)
                .collect(toMap(Function.identity(), c -> String.valueOf(c).toUpperCase().charAt(0)));
        charMap.putAll(charMap.entrySet().stream().collect(toMap(Map.Entry::getValue, Map.Entry::getKey)));
        String reduced = reduceString(input, charMap);
        System.out.println(reduced.length());
    }

    private static void part2(final String input) {
        Map<Character, Character> charMap = IntStream.range(0, 26)
                .map(i -> i + 'a')
                .mapToObj(i -> (char) i)
                .collect(toMap(Function.identity(), c -> String.valueOf(c).toUpperCase().charAt(0)));
        charMap.putAll(charMap.entrySet().stream().collect(toMap(Map.Entry::getValue, Map.Entry::getKey)));
        int minLength = Integer.MAX_VALUE;
        for (Map.Entry<Character, Character> entry : charMap.entrySet().stream().filter(entry -> Character.isLowerCase(entry.getKey())).collect(Collectors.toSet())) {
            String subtractedString = input.replaceAll(String.valueOf(entry.getKey()), "")
                    .replaceAll(String.valueOf(entry.getValue()), "");
            String reduced = reduceString(subtractedString, charMap);
            minLength = Math.min(minLength, reduced.length());
        }
        System.out.println(minLength);
    }

    private static String reduceString(final String input, final Map<Character, Character> charMap) {
        String unreduced = input;
        String reduced = reducedString(input, charMap);
        while (!reduced.equals(unreduced)) {
            unreduced = reduced;
            reduced = reducedString(reduced, charMap);
        }
        return reduced;
    }

    private static String reducedString(final String s, final Map<Character, Character> charMap) {
        StringBuilder result = new StringBuilder();
        int i = 0;
        while (i < s.length()) {
            if (i == s.length() - 1 || (charMap.get(s.charAt(i)) != s.charAt(i + 1))) {
                result.append(s.charAt(i));
                i++;
            } else {
                i += 2;
            }
        }
        return result.toString();
    }

}
