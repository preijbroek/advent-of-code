package advent.day15;

import lombok.AllArgsConstructor;
import lombok.Value;
import util.Position;

import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

@Value
@AllArgsConstructor
public class PositionWithHistory {

    int distance;
    PositionWithHistory previous;
    Position current;

    public PositionWithHistory(final int distance, final Position current) {
        this(distance, null, current);
    }

    public PositionWithHistory(final Position current) {
        this(1, current);
    }

    public static Comparator<PositionWithHistory> byDistance() {
        return comparingInt(PositionWithHistory::getDistance);
    }

    public static Comparator<PositionWithHistory> byCurrentPosition() {
        return comparing(PositionWithHistory::getCurrent, Position.byY().thenComparing(Position.byX()));
    }

    public List<PositionWithHistory> neighbours() {
        return current.neighbours().stream()
                .map(position -> new PositionWithHistory(distance + 1, this, position))
                .collect(toList());
    }

    public Position originalDelta() {
        PositionWithHistory history = this;
        while (nonNull(history.previous)) {
            history = history.previous;
        }
        return history.current;
    }

    public boolean hasCurrent(final Position position) {
        return current.equals(position);
    }

}
