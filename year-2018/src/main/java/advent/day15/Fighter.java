package advent.day15;

import lombok.AllArgsConstructor;
import lombok.Data;
import util.Position;

import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;
import static java.util.Objects.nonNull;

@Data
@AllArgsConstructor
public class Fighter implements Comparable<Fighter> {

    private final Team team;
    private int health;
    private Position position;
    private final int attackPower;

    public Fighter(final Team team, final Position position) {
        this(team, 200, position, 3);
    }

    public Fighter withElfAttackPowerBoost() {
        return team == Team.ELF
                ? new Fighter(team, health, position, attackPower + 1)
                : copy();
    }

    public Fighter copy() {
        return new Fighter(team, health, position, attackPower);
    }

    public static Comparator<Fighter> byHealth() {
        return comparingInt(Fighter::getHealth);
    }

    public static Comparator<Fighter> byPosition() {
        return comparing(Fighter::getPosition, Position.byY())
                .thenComparing(Fighter::getPosition, Position.byX());
    }

    public void moveTo(final Position position) {
        this.position = position;
    }

    public void attack(final Fighter fighter) {
        fighter.health -= attackPower;
    }

    public Fighter selectTarget(final List<Fighter> fighters) {
        List<Position> range = range();
        return fighters.stream()
                .filter(this::isEnemyOf)
                .filter(Fighter::isActive)
                .filter(enemy -> range.contains(enemy.position))
                .min(byHealth().thenComparing(byPosition()))
                .orElse(null);
    }

    public List<Position> range() {
        return position.sortedNeighbours();
    }

    public boolean isEnemyOf(final Fighter fighter) {
        return nonNull(fighter) && team != fighter.team;
    }

    public boolean isFriendOf(final Fighter fighter) {
        return nonNull(fighter) && team == fighter.team;
    }

    public boolean isDefeated() {
        return health <= 0;
    }

    public boolean isActive() {
        return health > 0;
    }

    public String asAocString() {
        return team.getLetter() + "(" + health + ")";
    }

    @Override
    public int compareTo(final Fighter other) {
        return Position.byY().thenComparing(Position.byX())
                .compare(this.position, other.position);
    }
}
