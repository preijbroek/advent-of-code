package advent.day15;

import advent.InputProvider;

import java.util.List;

public class Day15Exercise {

    public static void main(final String[] args) {
        List<String> input = InputProvider.getInput(15);
        Cave cave = Cave.fromLines(input);
        part1(cave);
        part2(cave);
    }

    private static void part1(final Cave cave) {
        Cave result = cave.simulateBattle();
        System.out.println(result.outcome());
    }

    private static void part2(final Cave cave) {
        Cave intermediateStart = cave;
        Cave intermediateResult = cave.simulateBattle();
        while (intermediateResult.hasElfCasualties()) {
            intermediateStart = intermediateStart.withBoostedElves();
            intermediateResult = intermediateStart.simulateBattle();
        }
        System.out.println(intermediateResult.outcome());
    }

}
