package advent.day15;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import util.Position;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.lang.Character.isLetter;
import static java.util.Collections.emptyList;
import static java.util.Comparator.naturalOrder;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Value
@EqualsAndHashCode(exclude = "completedRounds")
@RequiredArgsConstructor
public class Cave {

    Map<Position, Character> grid;
    List<Fighter> fighters;
    @NonFinal
    int completedRounds = 0;

    public static Cave fromLines(final List<String> lines) {
        Map<Position, Character> grid = new HashMap<>();
        List<Fighter> fighters = new ArrayList<>();
        for (int y = 0; y < lines.size(); y++) {
            String line = lines.get(y);
            for (int x = 0; x < line.length(); x++) {
                char c = line.charAt(x);
                Position position = new Position(x, y);
                grid.put(position, c);
                if (isLetter(c)) {
                    Fighter fighter = new Fighter(Team.findBy(c), position);
                    fighters.add(fighter);
                }
            }
        }
        return new Cave(grid, fighters);
    }

    public Cave withBoostedElves() {
        return new Cave(new HashMap<>(grid), fighters.stream().map(Fighter::withElfAttackPowerBoost).collect(toList()));
    }

    private Cave copy() {
        return new Cave(new HashMap<>(grid), fighters.stream().map(Fighter::copy).collect(toList()));
    }

    public Cave simulateBattle() {
        Cave cave = copy();
        while (!cave.isFinished()) {
            cave.doBattle();
//            cave.draw();
        }
        return cave;
    }

    private void draw() {
        for (int y = 0; y < 32; y++) {
            for (int x = 0; x < 32; x++) {
                System.out.print(grid.getOrDefault(new Position(x, y), ' '));
            }
            System.out.println();
        }
        System.out.println(completedRounds);
    }

    private void doBattle() {
        fighters.sort(naturalOrder());
        for (Fighter fighter : fighters) {
            if (fighter.isDefeated()) continue;
            if (isFinished()) return;
            Fighter target = fighter.selectTarget(fighters);
            if (isNull(target)) {
                moveToClosestEnemy(fighter);
                target = fighter.selectTarget(fighters);
            }
            if (nonNull(target)) {
                fighter.attack(target);
                if (target.isDefeated()) {
                    grid.put(target.getPosition(), '.');
                }
            }
        }
        completedRounds++;
    }

    private boolean isFinished() {
        return fighters.stream()
                .filter(Fighter::isActive)
                .map(Fighter::getTeam)
                .distinct()
                .count() == 1L;
    }

    private void moveToClosestEnemy(final Fighter fighter) {
        Set<Position> openEnemyRange = getOpenEnemyRange(fighter);
        List<Position> pathToEnemy = findPathToEnemy(fighter, openEnemyRange);
        if (!pathToEnemy.isEmpty()) {
            Position newPosition = pathToEnemy.get(0);
            grid.put(fighter.getPosition(), '.');
            fighter.moveTo(newPosition);
            grid.put(newPosition, fighter.getTeam().getLetter());
        }
    }

    private Set<Position> getOpenEnemyRange(final Fighter fighter) {
        return fighters.stream()
                .filter(Fighter::isActive)
                .filter(fighter::isEnemyOf)
                .map(Fighter::range)
                .flatMap(List::stream)
                .filter(this::isOpen)
                .collect(toSet());
    }

    private boolean isOpen(final Position position) {
        return grid.get(position) == '.';
    }

    private List<Position> findPathToEnemy(final Fighter fighter, final Set<Position> openEnemyRange) {
        Set<Position> visited = new HashSet<>();
        visited.add(fighter.getPosition());
        Deque<List<Position>> paths = fighter.range().stream()
                .filter(this::isOpen)
                .map(Cave::mutableListOf)
                .collect(toCollection(ArrayDeque::new));
        while (!paths.isEmpty()) {
            List<Position> path = paths.removeFirst();
            Position endOfPath = path.get(path.size() - 1);
            if (openEnemyRange.contains(endOfPath)) {
                return path;
            } else if (visited.add(endOfPath)) {
                endOfPath.sortedNeighbours().stream()
                        .filter(this::isOpen)
                        .filter(position -> !visited.contains(position))
                        .forEachOrdered(position -> paths.offerLast(withElement(path, position)));
            }
        }
        return emptyList();
    }

    private static List<Position> mutableListOf(final Position... positions) {
        return new ArrayList<>(Arrays.asList(positions));
    }

    private static List<Position> withElement(final List<Position> positions, final Position position) {
        List<Position> positionList = new ArrayList<>(positions);
        positionList.add(position);
        return positionList;
    }

    public int outcome() {
        return completedRounds * remainingHealth();
    }

    private int remainingHealth() {
        return fighters.stream()
                .filter(Fighter::isActive)
                .mapToInt(Fighter::getHealth)
                .sum();
    }

    public boolean hasElfCasualties() {
        return fighters.stream()
                .filter(Fighter::isDefeated)
                .anyMatch(fighter -> fighter.getTeam() == Team.ELF);
    }

}
