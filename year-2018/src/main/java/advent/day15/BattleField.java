package advent.day15;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import util.Position;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import static java.lang.Character.isLetter;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Value
@RequiredArgsConstructor
public class BattleField {

    Set<Position> grid;
    Map<Position, Fighter> fighters;
    @NonFinal
    int completedRounds = 0;

    public static BattleField fromLines(final List<String> lines) {
        Set<Position> grid = new HashSet<>();
        Map<Position, Fighter> fighters = new HashMap<>();
        for (int y = 0; y < lines.size(); y++) {
            String line = lines.get(y);
            for (int x = 0; x < line.length(); x++) {
                char c = line.charAt(x);
                Position position = new Position(x, y);
                if (c != '#') {
                    grid.add(position);
                }
                if (isLetter(c)) {
                    Fighter fighter = new Fighter(Team.findBy(c), 200, position, 3);
                    fighters.put(position, fighter);
                }
            }
        }
        return new BattleField(grid, fighters);
    }

    public void battle() {
        while (!isFinished()) {
            battleRound();
            drawBattleField();
        }
    }

    private void drawBattleField() {
        for (int y = 0; y < 32; y++) {
            StringJoiner registeredFighters = new StringJoiner(", ", "    ", "");
            for (int x = 0; x < 32; x++) {
                Position position = new Position(x, y);
                if (grid.contains(position)) {
                    if (fighters.containsKey(position)) {
                        Fighter fighter = fighters.get(position);
                        System.out.print(fighter.getTeam().getLetter());
                        registeredFighters.add(fighter.asAocString());
                    } else {
                        System.out.print('.');
                    }
                } else {
                    System.out.print('#');
                }
            }
            System.out.println(registeredFighters);
        }
        System.out.println();
    }

    private boolean isFinished() {
        return Arrays.stream(Team.values()).anyMatch(this::hasSameTeamFighters);
    }

    private boolean hasSameTeamFighters(final Team winningTeam) {
        return fighters.values().stream()
                .map(Fighter::getTeam)
                .allMatch(team -> team == winningTeam);
    }

    private void battleRound() {
        List<Fighter> fightersInRound = new ArrayList<>(fighters.values());
        fightersInRound.sort(Fighter.byPosition());
        for (Fighter fighter : fightersInRound) {
            if (fighters.containsValue(fighter)) {
                if (isFinished()) return;
                moveToClosestEnemy(fighter);
                executeAttack(fighter);
            }
        }
        completedRounds++;
    }

    private void moveToClosestEnemy(final Fighter fighter) {
        List<Position> possibleMoves = fighter.range().stream()
                .filter(grid::contains)
                .filter(position -> !fighters.containsKey(position))
                .collect(toList());
        if (hasEnemyInRange(fighter)) return;
        Set<PositionWithHistory> totalRange = possibleMoves.stream()
                .map(PositionWithHistory::new)
                .collect(toSet());
        Set<Position> totalRangePositions = new HashSet<>(possibleMoves);
        Set<PositionWithHistory> currentRange = new HashSet<>(totalRange);
        int squaresInReach = 0;
        PositionWithHistory closestEnemyPosition;
        while (isNull(closestEnemyPosition = findClosestEnemyPosition(totalRange, fighter)) && totalRange.size() != squaresInReach) {
            squaresInReach = totalRange.size();
            Set<PositionWithHistory> currentRangeNew = new HashSet<>();
            List<PositionWithHistory> currentRangeList = new ArrayList<>(currentRange);
            currentRangeList.sort(PositionWithHistory.byCurrentPosition());
            for (PositionWithHistory pwh : currentRangeList) {
                List<PositionWithHistory> neighbours = pwh.neighbours();
                neighbours.removeIf(neighbour ->
                        !grid.contains(neighbour.getCurrent())
                                || fighter.getPosition().equals(neighbour.getCurrent())
                                || totalRangePositions.contains(neighbour.getCurrent())
                                || fighter.isFriendOf(fighters.get(neighbour.getCurrent()))
                                || currentRangeList.stream().anyMatch(current -> current.hasCurrent(neighbour.getCurrent()))
                );
                totalRange.addAll(neighbours);
                neighbours.forEach(neighbour -> totalRangePositions.add(neighbour.getCurrent()));
                currentRangeNew.addAll(neighbours);
            }
            currentRange = currentRangeNew;
        }
        if (nonNull(closestEnemyPosition)) {
            Position newPosition = closestEnemyPosition.originalDelta();
            fighters.remove(fighter.getPosition());
            fighter.setPosition(newPosition);
            fighters.put(newPosition, fighter);
        }
    }

    private boolean hasEnemyInRange(final Fighter fighter) {
        return containsEnemy(fighter.range(), fighter);
    }

    private boolean containsEnemy(final Collection<Position> range,
                                  final Fighter fighter) {
        return range.stream()
                .map(fighters::get)
                .anyMatch(fighter::isEnemyOf);
    }

    private PositionWithHistory findClosestEnemyPosition(final Set<PositionWithHistory> range, final Fighter fighter) {
        return range.stream()
                .filter(pwh -> fighter.isEnemyOf(fighters.get(pwh.getCurrent())))
                .min(PositionWithHistory.byDistance().thenComparing(PositionWithHistory.byCurrentPosition()))
                .orElse(null);
    }

    private void executeAttack(final Fighter fighter) {
        fighter.range().stream()
                .map(fighters::get)
                .filter(fighter::isEnemyOf)
                .min(Fighter.byHealth().thenComparing(Fighter.byPosition()))
                .ifPresent(enemy -> {
                    fighter.attack(enemy);
                    if (enemy.isDefeated()) {
                        fighters.remove(enemy.getPosition());
                    }
                });
    }

    public int combatOutcome() {
        return remainingHealth() * completedRounds;
    }

    private int remainingHealth() {
        return fighters.values().stream()
                .mapToInt(Fighter::getHealth)
                .sum();
    }

}
