package advent.day15;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum Team {

    ELF('E'),
    GOBLIN('G'),
    ;

    private final char letter;

    public static Team findBy(final char letter) {
        return Arrays.stream(values())
                .filter(team -> team.letter == letter)
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Unknown letter for team found: " + letter));
    }
}
