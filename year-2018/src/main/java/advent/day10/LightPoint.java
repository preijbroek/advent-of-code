package advent.day10;

import util.Position;

import java.util.Arrays;
import java.util.Objects;

public final class LightPoint {

    private final Position startPosition;
    private final Position velocity;
    private Position currentPosition;

    public LightPoint(final Position startPosition, final Position velocity) {
        this.startPosition = startPosition;
        this.velocity = velocity;
        this.currentPosition = startPosition;
    }

    public static LightPoint from(final String s) {
        Position startPosition = positionFrom(s.substring(s.indexOf('<') + 1, s.indexOf('>')));
        Position velocity = positionFrom(s.substring(s.lastIndexOf('<') + 1, s.lastIndexOf('>')));
        return new LightPoint(startPosition, velocity);
    }

    private static Position positionFrom(final String s) {
        int[] posCoordinates = Arrays.stream(s.split(", "))
                .map(String::trim)
                .mapToInt(Integer::parseInt)
                .toArray();
        return new Position(posCoordinates[0], posCoordinates[1]);
    }

    public void setNextSecond() {
        currentPosition = currentPosition.add(velocity);
    }

    public void setPreviousSecond() {
        currentPosition = currentPosition.subtract(velocity);
    }

    public Position getCurrentPosition() {
        return currentPosition;
    }

    public int getX() {
        return currentPosition.getX();
    }

    public int getY() {
        return currentPosition.getY();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LightPoint lightPoint = (LightPoint) o;
        return Objects.equals(startPosition, lightPoint.startPosition) &&
                Objects.equals(velocity, lightPoint.velocity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startPosition, velocity);
    }
}
