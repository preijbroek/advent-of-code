package advent.day10;

import advent.InputProvider;
import util.Position;

import java.util.List;
import java.util.stream.Collectors;

import static util.Comparators.maxOf;
import static util.Comparators.minOf;

public final class Day10Exercise {

    private Day10Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(10);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<LightPoint> lightPoints = input.stream()
                .map(LightPoint::from)
                .collect(Collectors.toList());
        runToMessage(lightPoints);
        print(lightPoints);
    }

    private static void part2(final List<String> input) {
        List<LightPoint> lightPoints = input.stream()
                .map(LightPoint::from)
                .collect(Collectors.toList());
        System.out.println(runToMessage(lightPoints));
    }

    private static int runToMessage(final List<LightPoint> lightPoints) {
        int minWidth = minOf(lightPoints, LightPoint::getX).getX();
        int minHeight = minOf(lightPoints, LightPoint::getY).getY();
        int maxWidth = maxOf(lightPoints, LightPoint::getX).getX();
        int maxHeight = maxOf(lightPoints, LightPoint::getY).getY();
        for (int i = 0; ; i++) {
            lightPoints.forEach(LightPoint::setNextSecond);
            int nextMinWidth = minOf(lightPoints, LightPoint::getX).getX();
            int nextMinHeight = minOf(lightPoints, LightPoint::getY).getY();
            int nextMaxWidth = maxOf(lightPoints, LightPoint::getX).getX();
            int nextMaxHeight = maxOf(lightPoints, LightPoint::getY).getY();
            if (nextMinHeight < minHeight || nextMaxHeight > maxHeight
                    || nextMinWidth < minWidth || nextMaxWidth > maxWidth) {
                lightPoints.forEach(LightPoint::setPreviousSecond);
                return i;
            }
            minWidth = nextMinWidth;
            maxWidth = nextMaxWidth;
            minHeight = nextMinHeight;
            maxHeight = nextMaxHeight;
        }
    }

    private static void print(final List<LightPoint> lightPoints) {
        List<Position> positions = lightPoints.stream()
                .map(LightPoint::getCurrentPosition)
                .collect(Collectors.toList());
        for (int y = minOf(lightPoints, LightPoint::getY).getY(); y <= maxOf(lightPoints, LightPoint::getY).getY(); y++) {
            for (int x = minOf(lightPoints, LightPoint::getX).getX(); x <= maxOf(lightPoints, LightPoint::getX).getX(); x++) {
                System.out.print(positions.contains(new Position(x, y)) ? "#" : ".");
            }
            System.out.println();
        }
    }

}
