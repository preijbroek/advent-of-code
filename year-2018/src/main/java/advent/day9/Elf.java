package advent.day9;

public final class Elf {

    private long score = 0;

    public void pocket(long marble) {
        score += marble;
    }

    public long getScore() {
        return score;
    }
}
