package advent.day9;

import advent.InputProvider;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public final class Day9Exercise {

    private Day9Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(9).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        String[] splitInput = input.split(" ");
        long players = Long.parseLong(splitInput[0]);
        long numberOfMarbles = Long.parseLong(splitInput[6]);
        Map<Long, Elf> elves = LongStream.range(0, players)
                .boxed()
                .collect(Collectors.toMap(Function.identity(), l -> new Elf()));
        playGame(numberOfMarbles, elves);
        System.out.println(elves.values().stream().mapToLong(Elf::getScore).max().orElseThrow(IllegalStateException::new));
    }

    private static void part2(final String input) {
        String[] splitInput = input.split(" ");
        long players = Long.parseLong(splitInput[0]);
        long numberOfMarbles = 100 * Long.parseLong(splitInput[6]);
        Map<Long, Elf> elves = LongStream.range(0, players)
                .boxed()
                .collect(Collectors.toMap(Function.identity(), l -> new Elf()));
        playGame(numberOfMarbles, elves);
        System.out.println(elves.values().stream().mapToLong(Elf::getScore).max().orElseThrow(IllegalStateException::new));
    }

    private static void playGame(final long numberOfMarbles, final Map<Long, Elf> elves) {
        Deque<Long> marbles = new ArrayDeque<>();
        marbles.add(0L);
        long elfTurn = 0;
        for (long l = 1; l <= numberOfMarbles; l++) {
            if (modulo(l, 23) == 0) {
                Elf luckyElf = elves.get(elfTurn);
                luckyElf.pocket(l);
                rotate(marbles, -7);
                luckyElf.pocket(marbles.pop());
            } else {
                rotate(marbles, 2);
                marbles.addLast(l);
            }
            elfTurn = modulo(elfTurn + 1, elves.size());
        }
    }

    private static long modulo(final long number, final long mod) {
        long modValue = number % mod;
        if (modValue < 0) {
            modValue += mod;
        }
        return modValue;
    }

    private static <T> void rotate(Deque<T> deque, long times) {
        if (times == 0) return;
        if (times > 0) {
            for (int i = 0; i < times; i++) {
                T t = deque.removeLast();
                deque.addFirst(t);
            }
        } else {
            for (int i = 0; i < Math.abs(times) - 1; i++) {
                T t = deque.remove();
                deque.addLast(t);
            }
        }
    }

}
