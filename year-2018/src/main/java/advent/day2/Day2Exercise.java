package advent.day2;

import advent.InputProvider;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Day2Exercise {

    private Day2Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(2);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<BoxID> boxIDs = input.stream().map(BoxID::new).collect(Collectors.toList());
        long twos = boxIDs.stream().filter(BoxID::hasExactlyTwoOfAnyLetter).count();
        long threes = boxIDs.stream().filter(BoxID::hasExactlyThreeOfAnyLetter).count();
        System.out.println(twos * threes);
    }

    private static void part2(final List<String> input) {
        List<BoxID> boxIDs = input.stream().map(BoxID::new).collect(Collectors.toList());
        List<BoxID> twoDifferentIds = boxIDs.stream().filter(boxID ->
                boxIDs.stream().anyMatch(boxID::differsByExactlyOneCharacter)
        )
                .collect(Collectors.toList());
        String commonChars = IntStream.range(0, twoDifferentIds.get(0).length())
                .filter(i -> twoDifferentIds.get(0).charAt(i) == twoDifferentIds.get(1).charAt(i))
                .mapToObj(i -> twoDifferentIds.get(0).charAt(i))
                .map(String::valueOf)
                .collect(Collectors.joining());
        System.out.println(commonChars);
    }
}
