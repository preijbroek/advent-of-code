package advent.day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;

public final class BoxID implements CharSequence {

    private final String idString;
    private final Map<String, Integer> idCharMap;

    public BoxID(final String idString) {
        this.idString = idString;
        this.idCharMap = initializeMap(idString);
    }

    private static Map<String, Integer> initializeMap(final String idString) {
        Map<String, Integer> idCharMap = new HashMap<>();
        for (char c : idString.toCharArray()) {
            idCharMap.put(String.valueOf(c), idCharMap.getOrDefault(String.valueOf(c), 0) + 1);
        }
        return idCharMap;
    }

    public boolean hasExactlyTwoOfAnyLetter() {
        return idCharMap.values().stream().anyMatch(value -> value == 2);
    }

    public boolean hasExactlyThreeOfAnyLetter() {
        return idCharMap.values().stream().anyMatch(value -> value == 3);
    }

    public boolean differsByExactlyOneCharacter(final BoxID boxID) {
        return IntStream.range(0, idString.length())
                .filter(i -> this.idString.charAt(i) != boxID.idString.charAt(i))
                .count()
                == 1L;
    }

    @Override
    public int length() {
        return idString.length();
    }

    @Override
    public char charAt(final int position) {
        return idString.charAt(position);
    }

    @Override
    public CharSequence subSequence(final int start, final int end) {
        return idString.subSequence(start, end);
    }

    @Override
    public IntStream chars() {
        return idString.chars();
    }

    @Override
    public IntStream codePoints() {
        return idString.codePoints();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoxID boxID = (BoxID) o;
        return idString.equals(boxID.idString);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idString);
    }
}
