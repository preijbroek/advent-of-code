package advent.day19;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Value
@RequiredArgsConstructor
public class Device {

    long[] registers = new long[]{0L, 0L, 0L, 0L, 0L, 0L};
    int instructionPointerRegister;
    @NonFinal
    int instructionCount = 0;
    boolean withPrinting;
    @NonFinal
    long instructionPointer = 0;

    public void setFirstRegisterValue(final int i) {
        registers[0] = i;
    }

    public void run(final List<Instruction> instructions) {
        while (instructionPointer < instructions.size() && instructionPointer >= 0) {
            Instruction instruction = instructions.get(((int) instructionPointer));
            registers[instructionPointerRegister] = instructionPointer;
            instruction.executeOn(registers);
            instructionPointer = registers[instructionPointerRegister];
            instructionPointer++;
            instructionCount++;
            if (withPrinting) {
                System.out.println(instructionPointer);
                System.out.println(Arrays.toString(registers));
            }
        }
    }

    public long runCountingInstructions(final List<Instruction> instructions, final boolean first) {
        Set<Long> equalityValues = new HashSet<>();
        long previous = 0L;
        while (instructionPointer < instructions.size() && instructionPointer >= 0) {
            Instruction instruction = instructions.get(((int) instructionPointer));
            registers[instructionPointerRegister] = instructionPointer;
            instruction.executeOn(registers);
            instructionPointer = registers[instructionPointerRegister];
            instructionPointer++;
            instructionCount++;
            if (withPrinting) {
                System.out.println(instructionPointer);
                System.out.println(Arrays.toString(registers));
            }
            if (instructionPointer == 29) {
                if (first) return registers[5];
                if (!equalityValues.add(registers[5])) {
                    return previous;
                }
                previous = registers[5];
            }
        }
        throw new IllegalStateException("This program has terminated somehow");
    }

    public long firstRegisterValue() {
        return registers[0];
    }

    public int numberOfInstructionsRun() {
        return instructionCount;
    }
}
