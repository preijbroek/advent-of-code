package advent.day19;

import advent.day16.Operation;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static advent.InputProvider.getInput;
import static java.util.stream.Collectors.toList;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Day19Exercise {

    public static void main(final String[] args) {
        List<String> input = getInput(19);
        int instructionPointerRegister = Integer.parseInt(input.get(0).split(" ")[1]);
        List<Instruction> instructions = input.subList(1, input.size()).stream()
                .map(Day19Exercise::toInstruction)
                .collect(toList());
        part1(instructionPointerRegister, instructions);
        part2(instructionPointerRegister, instructions);
    }

    private static void part1(final int instructionPointerRegister, final List<Instruction> instructions) {
        Device device = new Device(instructionPointerRegister, false);
        device.run(instructions);
        System.out.println(device.firstRegisterValue());
    }

    private static void part2(final int instructionPointerRegister, final List<Instruction> instructions) {
        // For my input, the value in register 2 is 10551354 which we should use
        int number = 10551354;
        int divisor = 1;
        int largerDivisor = number;
        Set<Integer> divisors = new HashSet<>();
        while (divisor < largerDivisor) {
            if (number % divisor == 0) {
                largerDivisor = number / divisor;
                divisors.add(divisor);
                divisors.add(largerDivisor);
            }
            divisor++;
        }
        System.out.println(divisors.stream().mapToInt(Integer::intValue).sum());
    }

    private static Instruction toInstruction(final String s) {
        String[] split = s.split(" ");
        Operation operation = Operation.valueOf(split[0].toUpperCase());
        int[] ints = Arrays.stream(split).skip(1)
                .mapToInt(Integer::parseInt)
                .toArray();
        return new Instruction(operation, ints);
    }

}
