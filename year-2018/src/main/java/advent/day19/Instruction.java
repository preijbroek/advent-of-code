package advent.day19;

import advent.day16.Operation;
import lombok.Value;

@Value
public class Instruction {

    Operation operation;
    int[] instructions;

    public Instruction(final Operation operation, final int[] instructions) {
        this.operation = operation;
        int[] ins = new int[4];
        System.arraycopy(instructions, 0, ins, 1, 3);
        this.instructions = ins;
    }

    public void executeOn(final long[] registers) {
        operation.executeOn(registers, instructions);
    }
}
