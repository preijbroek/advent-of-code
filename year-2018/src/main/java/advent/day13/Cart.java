package advent.day13;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import util.Direction;
import util.Position;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@EqualsAndHashCode
public final class Cart implements Comparable<Cart> {

    @Getter
    private Position position;
    private Direction direction;
    private CrossingDirection crossingDirection = CrossingDirection.LEFT;

    public Cart(final Position position, final char cartChar) {
        this.position = position;
        switch (cartChar) {
            case '>':
                this.direction = Direction.EAST;
                break;
            case '<':
                this.direction = Direction.WEST;
                break;
            case '^':
                this.direction = Direction.NORTH;
                break;
            case 'v':
                this.direction = Direction.SOUTH;
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(cartChar));
        }
    }

    public void move() {
        position = position.toFlipped(direction);
    }

    public void setNextDirection(final char track) {
        switch (track) {
            case '-':
            case '|':
                break;
            case '/':
                switchForwardSlash();
                break;
            case '\\':
                switchBackwardSlash();
                break;
            case '+':
                switchCrossing();
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(track));
        }
    }

    private void switchForwardSlash() {
        switch (direction) {
            case EAST:
            case WEST:
                direction = direction.left();
                break;
            case NORTH:
            case SOUTH:
                direction = direction.right();
                break;
            default:
                throw new IllegalArgumentException(direction.toString());
        }
    }

    private void switchBackwardSlash() {
        switch (direction) {
            case EAST:
            case WEST:
                direction = direction.right();
                break;
            case NORTH:
            case SOUTH:
                direction = direction.left();
                break;
            default:
                throw new IllegalArgumentException(direction.toString());
        }
    }

    private void switchCrossing() {
        switch (crossingDirection) {
            case LEFT:
                direction = direction.left();
                break;
            case RIGHT:
                direction = direction.right();
            case STRAIGHT:
                break;
            default:
                throw new IllegalArgumentException(crossingDirection.toString());
        }
        crossingDirection = crossingDirection.next();
    }

    public boolean overlapsWithAny(final List<Cart> carts) {
        return carts.stream()
                .filter(cart -> this != cart)
                .anyMatch(this::hasEqualPosition);
    }

    public List<Cart> getOverlapping(final List<Cart> carts) {
        return carts.stream()
                .filter(this::hasEqualPosition)
                .collect(Collectors.toList());
    }

    @Override
    public int compareTo(final Cart o) {
        return Comparator.comparing(Cart::getX)
                .thenComparing(Cart::getY)
                .compare(this, o);
    }

    private int getX() {
        return position.getX();
    }

    private int getY() {
        return position.getY();
    }

    private boolean hasEqualPosition(final Cart cart) {
        return Objects.equals(this.position, cart.position);
    }

    public String getCartPosition() {
        return position.getX() + "," + position.getY();
    }
}
