package advent.day13;

public enum CrossingDirection {
    LEFT,
    STRAIGHT,
    RIGHT;

    public CrossingDirection next() {
        switch (this) {
            case LEFT: return STRAIGHT;
            case STRAIGHT: return RIGHT;
            case RIGHT: return LEFT;
            default: throw new IllegalArgumentException(this.toString());
        }
    }


}
