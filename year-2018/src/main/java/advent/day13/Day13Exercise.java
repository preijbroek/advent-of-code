package advent.day13;

import advent.InputProvider;
import util.Position;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Day13Exercise {

    private Day13Exercise() {
    }

    public static void main(final String[] args) {
        List<String> input = InputProvider.getInput(13);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Map<Position, Character> rails = getRails(input);
        List<Cart> carts = getCarts(input);
        int numberOfNonCrashedCarts = carts.size();
        Cart firstCrashedCart = new Cart(new Position(Integer.MIN_VALUE, Integer.MIN_VALUE), 'v');
        while (carts.size() == numberOfNonCrashedCarts) {
            List<Cart> cartTurns = new ArrayList<>(carts);
            cartTurns.sort(Cart::compareTo);
            while (!cartTurns.isEmpty()) {
                Cart next = cartTurns.remove(0);
                next.move();
                if (next.overlapsWithAny(carts)) {
                    cartTurns.removeAll(next.getOverlapping(carts));
                    carts.removeAll(next.getOverlapping(carts));
                    firstCrashedCart = next;
                    break;
                }
                next.setNextDirection(rails.get(next.getPosition()));
            }
        }
        System.out.println(firstCrashedCart.getCartPosition());
    }

    private static void part2(final List<String> input) {
        Map<Position, Character> rails = getRails(input);
        List<Cart> carts = getCarts(input);
        while (carts.size() > 1) {
            List<Cart> cartTurns = new ArrayList<>(carts);
            cartTurns.sort(Cart::compareTo);
            while (!cartTurns.isEmpty()) {
                Cart next = cartTurns.remove(0);
                next.move();
                if (next.overlapsWithAny(carts)) {
                    cartTurns.removeAll(next.getOverlapping(carts));
                    carts.removeAll(next.getOverlapping(carts));
                }
                next.setNextDirection(rails.get(next.getPosition()));
            }
        }
        System.out.println(carts.get(0).getCartPosition());
    }

    private static Map<Position, Character> getRails(final List<String> input) {
        return IntStream.range(0, input.size()).boxed()
                .flatMap(y ->
                        IntStream.range(0, input.get(y).length()).boxed()
                                .map(x -> new Position(x, y))
                )
                .collect(Collectors.toMap(Function.identity(), p -> findRail(input, p)));
    }

    private static List<Cart> getCarts(final List<String> input) {
        return IntStream.range(0, input.size()).boxed()
                .flatMap(y ->
                        IntStream.range(0, input.get(y).length()).boxed()
                                .filter(x -> isCartPosition(input, x, y))
                                .map(x -> new Cart(new Position(x, y), input.get(y).charAt(x)))
                )
                .collect(Collectors.toList());
    }

    private static char findRail(final List<String> input, final Position p) {
        char rail = input.get(p.getY()).charAt(p.getX());
        switch (rail) {
            case '>':
            case '<':
                return '-';
            case '^':
            case 'v':
                return '|';
            default:
                return rail;
        }
    }

    private static boolean isCartPosition(final List<String> input, int x, int y) {
        return Arrays.asList('>', '<', 'v', '^').contains(input.get(y).charAt(x));
    }

}
