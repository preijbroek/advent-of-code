package advent.day7;

import advent.InputProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Day7Exercise {

    private Day7Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(7);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Map<String, Step> steps = input.stream()
                .map(s -> s.split(" "))
                .flatMap(array -> Arrays.stream(new String[]{array[1], array[7]}))
                .distinct()
                .sorted()
                .map(Step::new)
                .collect(Collectors.toMap(Step::getId, Function.identity()));
        input.stream()
                .map(s -> s.split(" "))
                .forEach(array -> steps.get(array[7]).addParent(steps.get(array[1])));
        StringBuilder result = new StringBuilder();
        while (!steps.values().stream().allMatch(Step::isFinished)) {
            Step nextStep = steps.values()
                    .stream()
                    .filter(step -> !step.isFinished())
                    .filter(Step::canBegin)
                    .min(Comparator.comparing(Step::getId))
                    .orElseThrow(IllegalStateException::new);
            nextStep.finish();
            result.append(nextStep.getId());
        }
        System.out.println(result);
    }

    private static void part2(final List<String> input) {
        Map<String, Step> stepsMap = input.stream()
                .map(s -> s.split(" "))
                .flatMap(array -> Arrays.stream(new String[]{array[1], array[7]}))
                .distinct()
                .sorted()
                .map(Step::new)
                .collect(Collectors.toMap(Step::getId, Function.identity()));
        input.stream()
                .map(s -> s.split(" "))
                .forEach(array -> stepsMap.get(array[7]).addParent(stepsMap.get(array[1])));
        Collection<Step> steps = stepsMap.values();
        List<Worker> workers = IntStream.rangeClosed(1, 5)
                .mapToObj(Worker::new)
                .collect(Collectors.toList());
        AtomicInteger time = new AtomicInteger(0);
        while (!steps.stream().allMatch(Step::isFinished)) {
            List<Step> availableSteps = steps.stream()
                    .filter(step -> !step.isFinished())
                    .filter(step -> !step.isInProgress())
                    .filter(Step::canBegin)
                    .sorted(Comparator.comparing(Step::getId))
                    .collect(Collectors.toCollection(ArrayList::new));
            while (workers.stream().anyMatch(Worker::isIdle) && !availableSteps.isEmpty()) {
                workers.stream()
                        .filter(Worker::isIdle).findAny()
                        .orElseThrow(IllegalStateException::new)
                        .assign(time.get(), availableSteps.remove(0));
            }
            workers.stream()
                    .filter(worker -> worker.willFinishTask(time.get()))
                    .forEach(Worker::finishAssignedTask);
            time.getAndIncrement();
        }
        System.out.println(time.get());
    }

}
