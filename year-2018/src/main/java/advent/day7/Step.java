package advent.day7;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public final class Step {

    private final String id;
    private final Set<Step> parents = new HashSet<>();
    private boolean finished = false;
    private boolean inProgress = false;

    public Step(final String id) {
        this.id = id;
    }

    public void addParent(final Step step) {
        parents.add(step);
    }

    public boolean canBegin() {
        return parents.stream().allMatch(Step::isFinished);
    }

    public void tryBegin() {
        if (!finished && canBegin()) {
            inProgress = true;
        }
    }

    public void finish() {
        finished = true;
        inProgress = false;
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public String getId() {
        return id;
    }

    public int timeToComplete() {
        return 60 + id.charAt(0) - 'A';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Step step = (Step) o;
        return Objects.equals(id, step.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
