package advent.day7;

import java.util.Comparator;
import java.util.Objects;

//compareTo incompatible with equals by choice!
public final class Worker implements Comparable<Worker> {

    private final int id;
    private int nextFinishTime;
    private Step currentStep;

    public Worker(final int id) {
        this.id = id;
    }

    public void assign(final int currentTime, final Step nextStep) {
        currentStep = nextStep;
        nextFinishTime = currentTime + nextStep.timeToComplete();
        nextStep.tryBegin();
    }

    public boolean isIdle() {
        return currentStep == null;
    }

    public boolean willFinishTask(final int currentTime) {
        return currentStep != null
                && currentStep.isInProgress()
                && nextFinishTime == currentTime;
    }

    public void finishAssignedTask() {
        currentStep.finish();
        currentStep = null;
    }

    private int getId() {
        return id;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Worker worker = (Worker) o;
        return id == worker.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(final Worker o) {
        return Comparator.comparing(Worker::getId)
                .compare(this, o);
    }
}
