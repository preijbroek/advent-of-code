package advent.day6;

import advent.InputProvider;
import util.Comparators;
import util.Position;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static util.Comparators.maxOf;
import static util.Comparators.minOf;

public final class Day6Exercise {

    private Day6Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(6);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<Position> positions = input.stream()
                .map(s -> s.split(", "))
                .map(array -> new Position(Integer.parseInt(array[0]), Integer.parseInt(array[1])))
                .collect(toList());
        List<Area> areas = positions.stream().map(Area::new).collect(toList());
        int minX = minOf(positions, Position::getX).getX();
        int minY = minOf(positions, Position::getY).getY();
        int maxX = maxOf(positions, Position::getX).getX();
        int maxY = maxOf(positions, Position::getY).getY();
        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                Position position = new Position(x, y);
                Map<Area, Integer> distances = areas.stream()
                        .collect(Collectors.toMap(Function.identity(), area -> area.distanceTo(position)));
                int minDistance = distances.values()
                        .stream()
                        .mapToInt(Integer::intValue)
                        .min()
                        .orElseThrow(IllegalStateException::new);
                if (distances.values().stream()
                        .filter(value -> value == minDistance)
                        .count() == 1L) {
                    distances.entrySet().stream()
                            .filter(entry -> entry.getValue() == minDistance)
                            .forEach(entry ->
                                    entry.getKey().addPosition(position)
                            );
                }
            }
        }
        System.out.println(areas.stream()
                .filter(area -> area.isFinite(minX, maxX, minY, maxY))
                .max(Comparator.comparing(Area::size))
                .map(Area::size)
                .orElseThrow(IllegalStateException::new));
    }

    private static void part2(final List<String> input) {
        List<Position> positions = input.stream()
                .map(s -> s.split(", "))
                .map(array -> new Position(Integer.parseInt(array[0]), Integer.parseInt(array[1])))
                .collect(toList());
        List<Area> areas = positions.stream().map(Area::new).collect(toList());
        int minX = Comparators.minOf(positions, Position::getX).getX();
        int minY = Comparators.minOf(positions, Position::getY).getY();
        int maxX = Comparators.maxOf(positions, Position::getX).getX();
        int maxY = Comparators.maxOf(positions, Position::getY).getY();
        int maxSafeRegionDistance = 10_000;
        List<Position> possiblePositions = IntStream.rangeClosed(minX - (maxSafeRegionDistance / areas.size()), maxX + (maxSafeRegionDistance / areas.size()))
                .boxed()
                .flatMap(x ->
                        IntStream.rangeClosed(minY - (maxSafeRegionDistance / areas.size()), maxY + (maxSafeRegionDistance / areas.size()))
                                .boxed()
                                .map(y -> new Position(x, y))
                )
                .collect(toList());
        System.out.println(possiblePositions.stream()
                .filter(position -> areas.stream()
                        .mapToInt(area -> area.distanceTo(position))
                        .sum() < maxSafeRegionDistance
                )
                .count());
    }

}
