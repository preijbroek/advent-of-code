package advent.day6;

import util.Position;

import java.util.ArrayList;
import java.util.List;

public final class Area {

    private final Position startPosition;
    private final List<Position> area = new ArrayList<>();

    public Area(final Position startPosition) {
        this.startPosition = startPosition;
    }

    public Position getStartPosition() {
        return startPosition;
    }

    public long distanceTo(final Position position) {
        return startPosition.distanceTo(position);
    }

    public boolean isFinite(final int minX, final int maxX, final int minY, final int maxY) {
        return area.stream()
                .noneMatch(position -> position.getX() == minX
                        || position.getX() == maxX
                        || position.getY() == minY
                        || position.getY() == maxY
                );
    }

    public void addPosition(final Position position) {
        area.add(position);
    }

    public int size() {
        return area.size();
    }
}
