package advent.day21;

import advent.day16.Operation;
import advent.day19.Device;
import advent.day19.Instruction;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

import static advent.InputProvider.getInput;
import static java.util.stream.Collectors.toList;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Day21Exercise {

    public static void main(final String[] args) {
        List<String> input = getInput(21);
        int instructionPointerRegister = Integer.parseInt(input.get(0).split(" ")[1]);
        List<Instruction> instructions = input.subList(1, input.size()).stream()
                .map(Day21Exercise::toInstruction)
                .collect(toList());
        part1(instructionPointerRegister, instructions);
        part2(instructionPointerRegister, instructions);
    }

    private static void part1(final int instructionPointerRegister, final List<Instruction> instructions) {
        Device device = new Device(instructionPointerRegister, false);
        System.out.println(device.runCountingInstructions(instructions, true));
    }

    private static void part2(final int instructionPointerRegister, final List<Instruction> instructions) {
        Device device = new Device(instructionPointerRegister, false);
        System.out.println(device.runCountingInstructions(instructions, false));
    }

    private static Instruction toInstruction(final String s) {
        String[] split = s.split(" ");
        Operation operation = Operation.valueOf(split[0].toUpperCase());
        int[] ints = Arrays.stream(split).skip(1)
                .mapToInt(Integer::parseInt)
                .toArray();
        return new Instruction(operation, ints);
    }


}
