package advent.day25;

import lombok.AllArgsConstructor;
import lombok.Value;
import util.Position;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.Collections.singleton;


@Value
@AllArgsConstructor
public class Constellation {

    Set<Position> fixedPoints;

    public Constellation() {
        this(new HashSet<>());
    }

    public Constellation(final Position position) {
        this(new HashSet<>(singleton(position)));
    }

    public Stream<Constellation> tryMerge(final Constellation other) {
        if (hasInRange(other)) {
            return Stream.of(this.merge(other));
        } else {
            return Stream.of(this, other).filter(Constellation::isNotEmpty);
        }
    }

    public boolean hasInRange(final Constellation other) {
        return fixedPoints.stream()
                .anyMatch(fixedPoint -> other.fixedPoints.stream()
                        .anyMatch(otherFixedPoint -> fixedPoint.distanceTo(otherFixedPoint) <= 3)
                );
    }

    private boolean isNotEmpty() {
        return !fixedPoints.isEmpty();
    }

    public Constellation merge(final Constellation constellation) {
        Set<Position> fixedPoints = new HashSet<>(this.fixedPoints);
        fixedPoints.addAll(constellation.fixedPoints);
        return new Constellation(fixedPoints);
    }

}
