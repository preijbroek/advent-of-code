package advent.day25;

import util.Position;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static advent.InputProvider.getInput;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class Day25Exercise {

    public static void main(final String[] args) {
        List<Position> positions = getInput(25).stream()
                .map(Day25Exercise::to4DPosition)
                .collect(toList());
        part1(positions);
    }

    private static void part1(final List<Position> positions) {
        Set<Constellation> unusedConstellations = positions.stream()
                .map(Constellation::new)
                .collect(toSet());
        int constellations = 0;
        do {
            Constellation nextBase = unusedConstellations.iterator().next();
            Set<Constellation> constellationsInRange;
            do {
                constellationsInRange = unusedConstellations.stream()
                        .filter(nextBase::hasInRange)
                        .collect(toSet());
                unusedConstellations.removeAll(constellationsInRange);
                nextBase = constellationsInRange.stream().reduce(nextBase, Constellation::merge);
            } while (!constellationsInRange.isEmpty());
            constellations++;
        } while (!unusedConstellations.isEmpty());
        System.out.println(constellations);
    }

    private static Position to4DPosition(final String s) {
        int[] ints = Arrays.stream(s.split(","))
                .mapToInt(Integer::parseInt)
                .toArray();
        return new Position(ints[0], ints[1], ints[2], ints[3]);
    }

}
