package advent.day14;

import advent.InputProvider;

import java.util.function.Predicate;

public final class Day14Exercise {

    private Day14Exercise() {
    }

    public static void main(String[] args) {
        int input = Integer.parseInt(InputProvider.getInput(14).get(0));
        part1(input);
        part2(input);
    }

    private static void part1(final int input) {
        StringBuilder recipes = new StringBuilder();
        Predicate<StringBuilder> condition = stringBuilder -> stringBuilder.length() < input + 10;
        runUntilConditionIsFalse(condition, recipes);
        System.out.println(recipes.subSequence(input, input + 10));
    }

    private static void part2(final int input) {
        StringBuilder recipes = new StringBuilder();
        Predicate<StringBuilder> condition =
                stringBuilder -> stringBuilder.indexOf(Integer.toString(input), Math.max(0, stringBuilder.length() - 7)) == -1;
        runUntilConditionIsFalse(condition, recipes);
        System.out.println(recipes.indexOf(Integer.toString(input)));
    }

    private static void runUntilConditionIsFalse(final Predicate<StringBuilder> condition, final StringBuilder recipes) {
        recipes.append(3);
        recipes.append(7);
        Elf firstElf = new Elf(0, 3);
        Elf secondElf = new Elf(1, 7);
        while (condition.test(recipes)) {
            addRecipes(recipes, firstElf, secondElf);
            firstElf.evaluate(recipes);
            secondElf.evaluate(recipes);
        }
    }

    private static void addRecipes(final StringBuilder recipes, final Elf firstElf, final Elf secondElf) {
        int nextRecipesScores = firstElf.getScore() + secondElf.getScore();
        if (nextRecipesScores > 9) {
            recipes.append(nextRecipesScores / 10);
            recipes.append(nextRecipesScores % 10);
        } else {
            recipes.append(nextRecipesScores);
        }
    }

}
