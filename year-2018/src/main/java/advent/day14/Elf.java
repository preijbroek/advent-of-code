package advent.day14;

import lombok.Getter;

public final class Elf {

    private int position;
    @Getter
    private int score;

    public Elf(final int position, final int score) {
        this.position = position;
        this.score = score;
    }

    public void evaluate(final CharSequence recipes) {
        setPositionBasedOn(recipes);
        setScoreBasedOn(recipes);
    }

    private void setPositionBasedOn(final CharSequence recipes) {
        this.position = (position + score + 1) % recipes.length();
    }

    private void setScoreBasedOn(final CharSequence recipes) {
        this.score = recipes.charAt(position) - '0';
    }
}
