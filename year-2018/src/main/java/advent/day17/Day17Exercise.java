package advent.day17;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import util.Position;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static advent.InputProvider.getInput;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Day17Exercise {

    public static void main(final String[] args) {
        Set<Position> clay = getInput(17).stream()
                .flatMap(Day17Exercise::toPositions)
                .collect(toSet());
        Reservoir reservoir = new Reservoir(clay);
        reservoir.flow();
        part1(reservoir);
        part2(reservoir);
    }

    private static void part1(final Reservoir reservoir) {
        System.out.println(reservoir.waterCount());
    }

    private static void part2(final Reservoir reservoir) {
        System.out.println(reservoir.stillWaterCount());
    }

    private static Stream<Position> toPositions(final String s) {
        String[] split = s.split(", ");
        int constant = Integer.parseInt(split[0].substring(2));
        List<Integer> line = Arrays.stream(split[1].substring(2).split("\\.\\."))
                .map(Integer::parseInt).collect(toList());
        if (split[0].charAt(0) == 'x') {
            return IntStream.rangeClosed(line.get(0), line.get(1))
                    .mapToObj(y -> new Position(constant, y));
        } else {
            return IntStream.rangeClosed(line.get(0), line.get(1))
                    .mapToObj(x -> new Position(x, constant));
        }
    }

}
