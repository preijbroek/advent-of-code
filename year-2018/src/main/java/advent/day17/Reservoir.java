package advent.day17;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import util.Direction;
import util.Position;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.Collections.asLifoQueue;
import static java.util.Collections.reverseOrder;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Reservoir {

    Set<Position> basins;
    Map<Position, Character> flow;
    Queue<Position> stack;
    @NonFinal
    int minX;
    @NonFinal
    int maxX;
    @NonFinal
    int minY;
    @NonFinal
    int maxY;

    public Reservoir(final Collection<Position> basins) {
        this(
                new HashSet<>(basins),
                basins.stream().collect(toMap(identity(), p -> '#')),
                asLifoQueue(new ArrayDeque<>())
        );
        stack.offer(new Position(500, 0));
        minX = minX() - 1;
        minY = 1;
        maxX = maxX() + 1;
        maxY = maxY();
    }

    public void flow() {
        if (!(flow.size() == basins.size())) return;
        while (!stack.isEmpty()) {
            Position current = stack.poll();
            Position below = current.toFlipped(Direction.SOUTH);
            flow.put(current, '|');
            if (!isInBounds(below)) continue;
            if (canDripDown(below)) {
                stack.offer(below);
            } else {
                SideFlow sideFlow = sideFlow(current);
                char flowChar = sideFlow.isBounded ? '~' : '|';
                sideFlow.line.forEach(position -> flow.put(position, flowChar));
                if (!sideFlow.isBounded) {
                    sideFlow.line.stream()
                            .filter(position -> isInBounds(position.toFlipped(Direction.SOUTH)))
                            .filter(position -> flow.getOrDefault(position.toFlipped(Direction.SOUTH), '.') == '.')
                            .sorted(reverseOrder(Position.byX()))
                            .forEach(stack::offer);
                } else if (isInBounds(current.toFlipped(Direction.NORTH))) {
                    stack.offer(current.toFlipped(Direction.NORTH));
                }
            }
        }
        minY = minY();
//        print();
    }

    private SideFlow sideFlow(final Position position) {
        SideFlow sideFlowEast = sideFlow(position, Direction.EAST);
        SideFlow sideFlowWest = sideFlow(position, Direction.WEST);
        return new SideFlow(
                sideFlowEast.isBounded && sideFlowWest.isBounded,
                Stream.of(sideFlowEast.line, sideFlowWest.line).flatMap(Set::stream).collect(toSet())
        );
    }

    private SideFlow sideFlow(final Position position, final Direction direction) {
        Set<Position> positions = new HashSet<>();
        positions.add(position);
        Position current = position;
        while (true) {
            current = current.to(direction);
            if (!isInBounds(current)) return new SideFlow(false, positions);
            if (basins.contains(current)) return new SideFlow(true, positions);
            Position below = current.toFlipped(Direction.SOUTH);
            if (isInBounds(below) && Arrays.asList('~', '#').contains(flow.getOrDefault(below, '.'))) {
                positions.add(current);
            } else if (flow.getOrDefault(below, '.') != '|') {
                positions.add(current);
                return new SideFlow(false, positions);
            } else {
//                print();
                return new SideFlow(false, positions);
            }
        }
    }

    public void print() {
        for (int y = 0; y <= maxY; y++) {
            for (int x = minX; x <= maxX; x++) {
                System.out.print(flow.getOrDefault(new Position(x, y), '.'));
            }
            System.out.println();
        }
    }

    private boolean isInBounds(final Position position) {
        return minY <= position.getY() && maxY >= position.getY();
    }

    private boolean canDripDown(final Position position) {
        return isInBounds(position) && flow.getOrDefault(position, '.') == '.';
    }

    private int minY() {
        return basins.stream()
                .min(Position.byY())
                .orElseThrow(IllegalArgumentException::new)
                .getY();
    }

    private int maxY() {
        return basins.stream()
                .max(Position.byY())
                .orElseThrow(IllegalArgumentException::new)
                .getY();
    }

    private int minX() {
        return basins.stream()
                .min(Position.byX())
                .orElseThrow(IllegalArgumentException::new)
                .getX();
    }

    private int maxX() {
        return basins.stream()
                .max(Position.byX())
                .orElseThrow(IllegalArgumentException::new)
                .getX();
    }

    public long waterCount() {
        return flow.entrySet().stream().filter(entry -> isInBounds(entry.getKey()))
                .map(Map.Entry::getValue)
                .filter(c -> c == '|' || c == '~')
                .count();
    }

    public long stillWaterCount() {
        return flow.entrySet().stream().filter(entry -> isInBounds(entry.getKey()))
                .map(Map.Entry::getValue)
                .filter(c -> c == '~')
                .count();
    }

    @Value
    private static class SideFlow {

        boolean isBounded;
        Set<Position> line;
    }
}
