package advent.day24;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import static java.util.Objects.nonNull;

@Value
@NoArgsConstructor(force = true)
@AllArgsConstructor
public class Winner {

    Army army;

    public int remainingUnits() {
        return nonNull(army) ? army.remainingUnits() : 0;
    }

    public boolean isImmuneSystem() {
        return nonNull(army) && army.isImmuneSystem();
    }
}
