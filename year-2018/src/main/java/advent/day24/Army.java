package advent.day24;

import lombok.AccessLevel;
import lombok.Value;
import lombok.With;
import util.JavaPair;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Collections.reverseOrder;
import static java.util.stream.Collectors.toList;

@Value
public class Army {

    ArmyType armyType;
    @With(AccessLevel.PRIVATE)
    List<Group> groups;

    public Army(final ArmyType armyType, final List<Group> groups) {
        this.armyType = armyType;
        this.groups = groups;
    }

    public static Army fromString(final String input) {
        final List<String> lines = Arrays.asList(input.split("\n"));
        final ArmyType armyType = ArmyType.fromDescription(lines.get(0));
        final List<Group> groups = lines.subList(1, lines.size()).stream()
                .map(Group::fromString)
                .collect(toList());
        return new Army(armyType, groups);
    }

    public Army copy() {
        List<Group> copiedGroups = groups.stream()
                .map(Group::copy)
                .collect(toList());
        return withGroups(copiedGroups);
    }

    public Army withBoost() {
        List<Group> boostedGroups = groups.stream()
                .map(Group::withBoost)
                .collect(toList());
        return withGroups(boostedGroups);
    }

    public List<JavaPair<Group>> pairWithVictims(final Army defender) {
        groups.sort(reverseOrder(Group.byTargetSelectionCriteria()));
        final Set<Group> defenders = new HashSet<>(defender.groups);
        return groups.stream()
                .map(group -> new JavaPair<>(group, group.pickTargetFrom(defenders)))
                .collect(toList());
    }

    public void removeEliminatedGroups() {
        groups.removeIf(Group::isEliminated);
    }

    public boolean isDefeated() {
        return groups.isEmpty();
    }

    public int remainingUnits() {
        return groups.stream().mapToInt(Group::getUnits).sum();
    }

    public boolean isImmuneSystem() {
        return armyType == ArmyType.IMMUNE_SYSTEM;
    }

}
