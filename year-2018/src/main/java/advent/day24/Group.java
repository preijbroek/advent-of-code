package advent.day24;

import lombok.AccessLevel;
import lombok.Value;
import lombok.With;
import lombok.experimental.NonFinal;

import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;
import static java.util.Objects.nonNull;

@Value
public class Group {

    @NonFinal
    int units;
    int hitPoints;
    @With(AccessLevel.PRIVATE)
    int attackDamage;
    Attack attackType;
    int initiative;
    Set<Attack> immunities;
    Set<Attack> weaknesses;

    public static Group fromString(final String s) {
        final String[] data = s.split("( units each with )|( hit points )|(with an attack that does )|( damage at initiative )");
        final int units = Integer.parseInt(data[0]);
        final int hitPoints = Integer.parseInt(data[1]);
        final Set<Attack> immunities = parseImmunities(data[2].trim());
        final Set<Attack> weaknesses = parseWeaknesses(data[2].trim());
        final String[] attack = data[3].split(" ");
        final int attackDamage = Integer.parseInt(attack[0]);
        final Attack attackType = Attack.valueOf(attack[1]);
        final int initiative = Integer.parseInt(data[4]);
        return new Group(units, hitPoints, attackDamage, attackType, initiative, immunities, weaknesses);
    }

    private static Set<Attack> parseImmunities(final String data) {
        return parseAttacks(data, "immune to ");
    }

    private static Set<Attack> parseWeaknesses(final String data) {
        return parseAttacks(data, "weak to ");
    }

    private static Set<Attack> parseAttacks(final String data, final String type) {
        String[] stats = data.replace("(", "")
                .replace(")", "")
                .split(";");
        return Arrays.stream(stats)
                .filter(s -> s.contains(type))
                .map(String::trim)
                .map(s -> s.replace(type, ""))
                .flatMap(s -> Arrays.stream(s.split(", ")))
                .map(Attack::valueOf)
                .collect(Collectors.toCollection(() -> EnumSet.noneOf(Attack.class)));
    }

    public Group withBoost() {
        return withAttackDamage(attackDamage + 1);
    }

    public Group copy() {
        return new Group(units, hitPoints, attackDamage, attackType, initiative, immunities, weaknesses);
    }

    public boolean isEliminated() {
        return units == 0;
    }

    public int effectivePower() {
        return units * attackDamage;
    }

    public void attack(final Group victim) {
        if (nonNull(victim)) {
            final int damage = potentialDamageTo(victim);
            victim.sufferDamage(damage);
        }
    }

    public void sufferDamage(final int damage) {
        int casualties = damage / hitPoints;
        units = Math.max(units - casualties, 0);
    }

    private int potentialDamageTo(final Group potentialVictim) {
        return effectivePower() * potentialVictim.damageFactorFor(attackType);
    }

    private int damageFactorFor(final Attack attackType) {
        if (immunities.contains(attackType)) {
            return 0;
        } else if (weaknesses.contains(attackType)) {
            return 2;
        } else {
            return 1;
        }
    }

    public Group pickTargetFrom(final Set<Group> groups) {
        Group target = groups.stream()
                .filter(this::canDamage)
                .max(byPotentialDamage().thenComparing(byTargetSelectionCriteria()))
                .orElse(null);
        groups.remove(target);
        return target;
    }

    private boolean canDamage(final Group group) {
        return group.damageFactorFor(attackType) > 0;
    }

    private Comparator<Group> byPotentialDamage() {
        return comparingInt(this::potentialDamageTo);
    }

    public static Comparator<Group> byTargetSelectionCriteria() {
        return Group.byEffectivePower().thenComparing(Group.byInitiative());
    }

    private static Comparator<Group> byEffectivePower() {
        return comparingInt(Group::effectivePower);
    }

    public static Comparator<Group> byInitiative() {
        return comparing(Group::getInitiative);
    }
}
