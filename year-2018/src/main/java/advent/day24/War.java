package advent.day24;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;
import util.JavaPair;

import java.util.List;

import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparing;
import static java.util.Objects.isNull;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class War {

    Army immuneSystem;
    Army infection;

    public static War between(final Army immuneSystem, final Army infection) {
        return new War(immuneSystem, infection);
    }

    public War withImmuneSystemBoost() {
        return between(immuneSystem.withBoost(), infection.copy());
    }

    private War copy() {
        return new War(immuneSystem.copy(), infection.copy());
    }

    public Winner fight() {
        return copy().doFight();
    }

    private Winner doFight() {
        Winner winner;
        do {
            winner = doNextRound();
        } while (isNull(winner));
        return winner;
    }

    private Winner doNextRound() {
        War stateBeforeBattle = copy();
        List<JavaPair<Group>> battlePairs = getBattlePairs();
        doBattle(battlePairs);
        return determineWinner(stateBeforeBattle);
    }

    private List<JavaPair<Group>> getBattlePairs() {
        List<JavaPair<Group>> battlePairs = immuneSystem.pairWithVictims(infection);
        battlePairs.addAll(infection.pairWithVictims(immuneSystem));
        battlePairs.sort(comparing(JavaPair::getLeft, reverseOrder(Group.byInitiative())));
        return battlePairs;
    }

    private void doBattle(final List<JavaPair<Group>> battlePairs) {
        battlePairs.forEach(pair -> pair.getLeft().attack(pair.getRight()));
        immuneSystem.removeEliminatedGroups();
        infection.removeEliminatedGroups();
    }

    private Winner determineWinner(final War lastState) {
        if (immuneSystem.isDefeated()) {
            return new Winner(infection);
        } else if (infection.isDefeated()) {
            return new Winner(immuneSystem);
        } else if (isStalemate(lastState)) {
            return new Winner();
        } else {
            return null;
        }
    }

    private boolean isStalemate(final War lastState) {
        return this.equals(lastState);
    }

}
