package advent.day24;

public enum Attack {

    bludgeoning,
    cold,
    fire,
    radiation,
    slashing,

}
