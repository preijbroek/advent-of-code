package advent.day24;

import advent.InputProvider;

import java.util.List;

public class Day24Exercise {

    public static void main(final String[] args) {
        final List<String> inputBlocks = InputProvider.getInputBlocks(24);
        final Army immuneSystem = Army.fromString(inputBlocks.get(0));
        final Army infection = Army.fromString(inputBlocks.get(1));
        War war = War.between(immuneSystem, infection);
        part1(war);
        part2(war);
    }

    private static void part1(final War war) {
        Winner winner = war.fight();
        System.out.println(winner.remainingUnits());
    }

    private static void part2(final War war) {
        Winner winner = war.fight();
        War warWithBoostedImmuneSystem = war;
        while (!winner.isImmuneSystem()) {
            warWithBoostedImmuneSystem = warWithBoostedImmuneSystem.withImmuneSystemBoost();
            winner = warWithBoostedImmuneSystem.fight();
        }
        System.out.println(winner.remainingUnits());
    }

}
