package advent.day24;

import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum ArmyType {

    IMMUNE_SYSTEM("Immune System"),
    INFECTION("Infection"),
    ;

    private final String description;

    public static ArmyType fromDescription(final String description) {
        return Arrays.stream(values())
                .filter(armyType -> description.contains(armyType.description))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Unknown description found: " + description));
    }
}
