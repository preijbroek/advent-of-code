package advent;

import kotlin.io.FilesKt;
import kotlin.text.Charsets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public final class InputProvider {

    private InputProvider() {
    }

    public static List<String> getInput(final int day) {
        try {
            return Files.readAllLines(Paths.get("year-2018/src/main/resources/day" + day + "exercise.csv"));
        } catch (IOException e) {
            throw new IllegalArgumentException(String.valueOf(day), e);
        }
    }

    public static List<String> getInputBlocks(final int day) {
        final File file = new File("year-2018/src/main/resources/day" + day + "exercise.csv");
        return Arrays.asList(FilesKt.readText(file, Charsets.UTF_8).split("\n\n+"));
    }
}
