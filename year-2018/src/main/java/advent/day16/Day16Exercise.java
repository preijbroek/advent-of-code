package advent.day16;

import advent.InputProvider;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class Day16Exercise {

    public static void main(final String[] args) {
        final List<String> inputBlocks = InputProvider.getInputBlocks(16);
        part1(inputBlocks.subList(0, inputBlocks.size() - 1));
        part2(inputBlocks.subList(0, inputBlocks.size() - 1), Arrays.asList(inputBlocks.get(inputBlocks.size() - 1).split("\n")));
    }

    private static void part1(final List<String> input) {
        final long result = input.stream()
                .map(Block::fromString)
                .mapToLong(Block::matchingOperationsCount)
                .filter(count -> count >= 3L)
                .count();
        System.out.println(result);
    }

    private static void part2(final List<String> part1Input, final List<String> input) {
        final Map<Integer, Operation> operations = getOperationsWithOpCodes(part1Input);
        final List<int[]> instructions = input.stream()
                .map(s -> Arrays.stream(s.split(" ")).mapToInt(Integer::parseInt).toArray())
                .collect(toList());
        Values values = new Values(new long[]{0, 0, 0, 0});
        for (int[] instruction : instructions) {
            values = values.apply(instruction, operations.get(instruction[0]));
        }
        System.out.println(values.firstRegister());
    }

    private static Map<Integer, Operation> getOperationsWithOpCodes(final List<String> input) {
        final List<Block> blocks = input.stream()
                .map(Block::fromString)
                .collect(toList());
        final Map<Integer, Operation> operationsWithOpCodes = new HashMap<>();
        while (operationsWithOpCodes.size() < Operation.values().length) {
            final List<SimpleEntry<Integer, List<Operation>>> entries = blocks.stream()
                    .map(block -> new SimpleEntry<>(block.getOpCode(), block.matchingOperations()))
                    .filter(entry -> !operationsWithOpCodes.containsKey(entry.getKey()))
                    .collect(toList());
            entries.forEach(entry -> entry.getValue().removeAll(operationsWithOpCodes.values()));
            entries.stream()
                    .filter(entry -> entry.getValue().size() == 1)
                    .forEachOrdered(entry -> operationsWithOpCodes.put(entry.getKey(), entry.getValue().get(0)));
        }
        return operationsWithOpCodes;
    }

}
