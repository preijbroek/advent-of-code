package advent.day16;

import lombok.Value;

import java.util.Arrays;

@Value
public class Values {

    long[] registers;

    public Values apply(final int[] instruction, final Operation operation) {
        final long[] result = operation.executeOn(registersCopy(), instruction);
        return new Values(result);
    }

    private long[] registersCopy() {
        return Arrays.copyOf(registers, registers.length);
    }

    public long firstRegister() {
        return registers[0];
    }

}
