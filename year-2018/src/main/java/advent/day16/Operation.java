package advent.day16;

import lombok.AllArgsConstructor;

import java.util.function.BiConsumer;

@AllArgsConstructor
public enum Operation {

    ADDR((values, instruction) -> values[instruction[3]] = values[instruction[1]] + values[instruction[2]]),
    ADDI((values, instruction) -> values[instruction[3]] = values[instruction[1]] + instruction[2]),
    MULR((values, instruction) -> values[instruction[3]] = values[instruction[1]] * values[instruction[2]]),
    MULI((values, instruction) -> values[instruction[3]] = values[instruction[1]] * instruction[2]),
    BANR((values, instruction) -> values[instruction[3]] = values[instruction[1]] & values[instruction[2]]),
    BANI((values, instruction) -> values[instruction[3]] = values[instruction[1]] & instruction[2]),
    BORR((values, instruction) -> values[instruction[3]] = values[instruction[1]] | values[instruction[2]]),
    BORI((values, instruction) -> values[instruction[3]] = values[instruction[1]] | instruction[2]),
    SETR((values, instruction) -> values[instruction[3]] = values[instruction[1]]),
    SETI((values, instruction) -> values[instruction[3]] = instruction[1]),
    GTIR((values, instruction) -> values[instruction[3]] = instruction[1] > values[instruction[2]] ? 1 : 0),
    GTRI((values, instruction) -> values[instruction[3]] = values[instruction[1]] > instruction[2] ? 1 : 0),
    GTRR((values, instruction) -> values[instruction[3]] = values[instruction[1]] > values[instruction[2]] ? 1 : 0),
    EQIR((values, instruction) -> values[instruction[3]] = instruction[1] == values[instruction[2]] ? 1 : 0),
    EQRI((values, instruction) -> values[instruction[3]] = values[instruction[1]] == instruction[2] ? 1 : 0),
    EQRR((values, instruction) -> values[instruction[3]] = values[instruction[1]] == values[instruction[2]] ? 1 : 0),
    ;

    private final BiConsumer<long[], int[]> calculation;

    public long[] executeOn(final long[] values, final int[] instruction) {
        calculation.accept(values, instruction);
        return values;
    }

}
