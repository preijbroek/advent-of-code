package advent.day16;

import lombok.Value;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Value
public class Block {

    Values originalValues;
    int[] instruction;
    Values expectedResult;

    public static Block fromString(final String s) {
        final String[] lines = s.split("\n");
        final Values originalValues = new Values(getValues(lines[0]));
        final int[] instruction = Arrays.stream(lines[1].split(" ")).mapToInt(Integer::parseInt).toArray();
        final Values expectedResult = new Values(getValues(lines[2]));
        return new Block(originalValues, instruction, expectedResult);
    }

    private static long[] getValues(final String s) {
        final String[] strings = s.substring(s.indexOf('[') + 1, s.indexOf(']'))
                .split(", ");
        return Arrays.stream(strings).mapToLong(Long::parseLong).toArray();
    }

    public List<Operation> matchingOperations() {
        return Arrays.stream(Operation.values())
                .filter(operation -> originalValues.apply(instruction, operation).equals(expectedResult))
                .collect(toList());
    }

    public long matchingOperationsCount() {
        return Arrays.stream(Operation.values())
                .map(operation -> originalValues.apply(instruction, operation))
                .filter(expectedResult::equals)
                .count();
    }

    public int getOpCode() {
        return instruction[0];
    }

}
