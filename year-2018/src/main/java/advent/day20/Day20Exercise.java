package advent.day20;

import util.Direction;
import util.Position;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

import static advent.InputProvider.getInput;

public class Day20Exercise {

    public static void main(final String[] args) {
        String directions = getInput(20).get(0);
        Map<Position, Integer> rooms = toRoomsMap(directions);
        part1(rooms);
        part2(rooms);
    }

    private static void part1(final Map<Position, Integer> rooms) {
        System.out.println(rooms.values().stream()
                .mapToInt(Integer::intValue)
                .max()
                .orElseThrow(IllegalStateException::new));
    }

    private static void part2(final Map<Position, Integer> rooms) {
        System.out.println(rooms.values().stream()
                .filter(distance -> distance >= 1000)
                .count());
    }

    private static Map<Position, Integer> toRoomsMap(final String directions) {
        Position current = Position.CENTRE;
        Map<Position, Integer> rooms = new HashMap<>();
        rooms.put(current, 0);
        Deque<Position> detourPositions = new ArrayDeque<>();
        for (char c : directions.toCharArray()) {
            switch (c) {
                case '(':
                    detourPositions.add(current);
                    break;
                case ')':
                    current = detourPositions.removeLast();
                    break;
                case '|':
                    current = detourPositions.getLast();
                    break;
                case 'N':
                case 'E':
                case 'S':
                case 'W':
                    Position next = current.to(fromChar(c));
                    rooms.put(next, Math.min(rooms.getOrDefault(next, Integer.MAX_VALUE), rooms.get(current) + 1));
                    current = next;
            }
        }
        return rooms;
    }

    private static Direction fromChar(final char c) {
        switch (c) {
            case 'N': return Direction.NORTH;
            case 'E': return Direction.EAST;
            case 'S': return Direction.SOUTH;
            case 'W': return Direction.WEST;
            default: throw new IllegalArgumentException(Character.toString(c));
        }
    }

}
