package advent.day11;

import util.Position;

import java.util.Objects;

public final class FuelCell {

    private final Position position;
    private final int powerLevel;

    private FuelCell(final Position position, final int powerLevel) {
        this.position = position;
        this.powerLevel = powerLevel;
    }

    public static FuelCell from(final Position position, final int gridSerialNumber) {
        int rackID = position.getX() + 10;
        int powerLevel = rackID * position.getY();
        powerLevel += gridSerialNumber;
        powerLevel *= rackID;
        powerLevel /= 100;
        powerLevel %= 10;
        powerLevel -= 5;
        return new FuelCell(position, powerLevel);
    }

    public Position getPosition() {
        return position;
    }

    public int getPowerLevel() {
        return powerLevel;
    }

    public String coordinateString() {
        return position.getX() + "," + position.getY();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FuelCell fuelCell = (FuelCell) o;
        return powerLevel == fuelCell.powerLevel &&
                Objects.equals(position, fuelCell.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, powerLevel);
    }
}
