package advent.day11;

import advent.InputProvider;
import util.Position;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Day11Exercise {

    private static final int GRID = 300;

    private Day11Exercise() {
    }

    public static void main(String[] args) {
        int input = Integer.parseInt(InputProvider.getInput(11).get(0));
        part1(input);
        part2(input);
    }

    private static void part1(final int input) {
        Map<Position, FuelCell> fuelCells = getFuelCellMap(input);
        Map<Position, Integer> fuelCellPowerMap = getFuelCellPowerMap(fuelCells);
        refillFuelCellPowerMap(fuelCellPowerMap);
        FuelCellProperties properties = getFuelCellProperties(fuelCells, fuelCellPowerMap, 3);
        System.out.println(properties.fuelCell.coordinateString());
    }

    private static void part2(final int input) {
        Map<Position, FuelCell> fuelCells = getFuelCellMap(input);
        Map<Position, Integer> fuelCellPowerMap = getFuelCellPowerMap(fuelCells);
        refillFuelCellPowerMap(fuelCellPowerMap);
        FuelCellProperties properties = new FuelCellProperties(null, 0, 0);
        for (int gridSize = 1; gridSize <= GRID; gridSize++) {
            FuelCellProperties biggerProperties = getFuelCellProperties(fuelCells, fuelCellPowerMap, gridSize);
            if (biggerProperties.powerOfGrid > properties.powerOfGrid) {
                properties = biggerProperties;
            }
        }
        System.out.println(properties.fuelCell.coordinateString() + "," + properties.gridSize);
    }

    private static Map<Position, FuelCell> getFuelCellMap(final int input) {
        return IntStream.rangeClosed(1, GRID).boxed()
                .flatMap(x ->
                        IntStream.rangeClosed(1, GRID).boxed()
                                .map(y -> FuelCell.from(new Position(x, y), input))
                )
                .sequential()
                .collect(Collectors.toMap(FuelCell::getPosition, Function.identity()));
    }

    private static Map<Position, Integer> getFuelCellPowerMap(final Map<Position, FuelCell> fuelCells) {
        return fuelCells.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getPowerLevel()));
    }

    private static void refillFuelCellPowerMap(final Map<Position, Integer> fuelCellPowerMap) {
        for (int x = 1; x <= GRID; x++) {
            for (int y = 1; y <= GRID; y++) {
                Position position = new Position(x, y);
                fuelCellPowerMap.put(position,
                        fuelCellPowerMap.getOrDefault(position, 0)
                                + fuelCellPowerMap.getOrDefault(position.west(), 0)
                                + fuelCellPowerMap.getOrDefault(position.south(), 0)
                                - fuelCellPowerMap.getOrDefault(position.south().west(), 0)
                );
            }
        }
    }

    private static FuelCellProperties getFuelCellProperties(final Map<Position, FuelCell> fuelCells, final Map<Position, Integer> fuelCellPowerMap, final int gridSize) {
        FuelCellProperties properties = new FuelCellProperties(null, 0, 0);
        for (int x = 0; x <= GRID + 1 - gridSize; x++) {
            for (int y = 0; y <= GRID + 1 - gridSize; y++) {
                int sum = fuelCellPowerMap.getOrDefault(new Position(x + gridSize, y + gridSize), Integer.MIN_VALUE >> 2)
                        - fuelCellPowerMap.getOrDefault(new Position(x + gridSize, y), Integer.MAX_VALUE >> 2)
                        - fuelCellPowerMap.getOrDefault(new Position(x, y + gridSize), Integer.MAX_VALUE >> 2)
                        + fuelCellPowerMap.getOrDefault(new Position(x, y), Integer.MIN_VALUE >> 2);
                if (properties.fuelCell == null || properties.powerOfGrid < sum) {
                    properties = new FuelCellProperties(fuelCells.get(new Position(x + 1, y + 1)),
                            sum, gridSize);
                }
            }
        }
        return properties;
    }

    private static final class FuelCellProperties {
        private final FuelCell fuelCell;
        private final int powerOfGrid;
        private final int gridSize;

        private FuelCellProperties(final FuelCell fuelCell, final int powerOfGrid, final int gridSize) {
            this.fuelCell = fuelCell;
            this.powerOfGrid = powerOfGrid;
            this.gridSize = gridSize;
        }
    }

}
