package advent.day22;

import lombok.Value;
import util.Position;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.IntStream;

import static java.util.Comparator.comparing;

@Value
public class Cave {

    int depth;
    Position target;
    Map<Position, Region> regions;

    public Cave(final int depth, final Position target) {
        this.depth = depth;
        this.target = target;
        this.regions = new HashMap<>();
        this.regions.put(Position.CENTRE, new Region(Position.CENTRE, 0, depth));
        this.regions.put(target, new Region(target, 0, depth));
    }

    public int getRiskLevel() {
        return IntStream.rangeClosed(0, target.getX())
                .boxed()
                .flatMap(x -> IntStream.rangeClosed(0, target.getY())
                        .mapToObj(y -> new Position(x, y)))
                .map(this::getRegion)
                .map(Region::getRegionType)
                .mapToInt(RegionType::getLevel)
                .sum();
    }

    public int getShortestTimeToTarget() {
        Queue<Region> regionQueue = new PriorityQueue<>(comparing(Region::getTime));
        regionQueue.offer(new Region(Position.CENTRE, getGeologicIndex(Position.CENTRE), depth));
        Map<Region, Integer> times = new HashMap<>();
        final Region targetRegion = new Region(target, 0, depth);
        while (!regionQueue.isEmpty()) {
            final Region currentRegion = regionQueue.remove();
            if (times.containsKey(currentRegion) && times.get(currentRegion) <= currentRegion.getTime()) {
                continue;
            }
            times.put(currentRegion, currentRegion.getTime());
            if (currentRegion.matches(targetRegion)) {
                return currentRegion.getTime();
            }
            regionQueue.offer(currentRegion.withOtherTool());
            currentRegion.getPosition().neighbours().stream()
                    .filter(position -> position.isInRange(0, 1000, 0, 1000))
                    .map(position -> new Region(position, getGeologicIndex(position), depth, currentRegion.getTool(), currentRegion.getTime() + 1))
                    .filter(Region::isPassable)
                    .filter(region -> !times.containsKey(region) || region.isReachedBefore(times.get(region)))
                    .forEachOrdered(regionQueue::offer);
        }
        throw new IllegalStateException("Cannot find shortest path to target");
    }

    private int getGeologicIndex(final Position position) {
        if (Position.CENTRE.equals(position) || target.equals(position)) {
            return 0;
        } else if (position.getY() == 0) {
            return position.getX() * 16807;
        } else if (position.getX() == 0) {
            return position.getY() * 48271;
        } else {
            return getRegion(position.west()).getErosionLevel() * getRegion(position.south()).getErosionLevel();
        }
    }

    private Region getRegion(final Position position) {
        if (regions.containsKey(position)) {
            return regions.get(position);
        } else {
            final Region region = new Region(position, getGeologicIndex(position), depth);
            regions.put(position, region);
            return region;
        }
    }
}
