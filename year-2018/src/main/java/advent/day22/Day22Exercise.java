package advent.day22;

import advent.InputProvider;
import util.Position;

import java.util.List;

public class Day22Exercise {

    public static void main(final String[] args) {
        final List<String> input = InputProvider.getInput(22);
        final int depth = Integer.parseInt(input.get(0).substring(input.get(0).indexOf(' ') + 1));
        final String[] targets = input.get(1).substring(input.get(1).indexOf(' ') + 1).split(",");
        final Position target = new Position(Integer.parseInt(targets[0]), Integer.parseInt(targets[1]));
        part1(depth, target);
        part2(depth, target);
    }

    private static void part1(final int depth, final Position target) {
        final Cave cave = new Cave(depth, target);
        System.out.println(cave.getRiskLevel());
    }

    private static void part2(final int depth, final Position target) {
        final Cave cave = new Cave(depth, target);
        System.out.println(cave.getShortestTimeToTarget());
    }

}
