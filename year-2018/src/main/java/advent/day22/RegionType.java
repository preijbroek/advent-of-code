package advent.day22;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;

@Getter
@AllArgsConstructor
public enum RegionType {

    ROCKY(0, Tool.TORCH, Tool.CLIMBING_GEAR),
    WET(1, Tool.CLIMBING_GEAR, Tool.NEITHER),
    NARROW(2, Tool.TORCH, Tool.NEITHER),
    ;

    private final int level;
    private final Set<Tool> tools;

    RegionType(final int level, final Tool tool1, final Tool tool2) {
        this(level, EnumSet.of(tool1, tool2));
    }

    public static RegionType findByErosionLevel(final int erosionLevel) {
        return Arrays.stream(values())
                .filter(regionType -> regionType.level == erosionLevel % 3)
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("unknown region tpye for erosion level " + erosionLevel));
    }

    public Tool getOtherTool(final Tool currentTool) {
        return tools.stream()
                .filter(tool -> tool != currentTool)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No other tool found for region type " + this));
    }

    public boolean isPassableWith(final Tool tool) {
        return tools.contains(tool);
    }

}
