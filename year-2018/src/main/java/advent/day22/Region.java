package advent.day22;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.With;
import util.Position;

@Value
@EqualsAndHashCode(exclude = {"time"})
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Region {

    Position position;
    int geologicIndex;
    int erosionLevel;
    RegionType regionType;
    @With(AccessLevel.PRIVATE)
    int time;
    @With(AccessLevel.PRIVATE)
    Tool tool;

    public Region(final Position position, final int geologicIndex, final int depth) {
        this(position, geologicIndex, depth, Tool.TORCH, 0);
    }

    public Region(final Position position,
                  final int geologicIndex,
                  final int depth,
                  final Tool tool,
                  final int time) {
        this.position = position;
        this.geologicIndex = geologicIndex;
        this.erosionLevel = (geologicIndex + depth) % 20183;
        this.regionType = RegionType.findByErosionLevel(erosionLevel);
        this.tool = tool;
        this.time = time;
    }

    public Region withOtherTool() {
        return withTool(regionType.getOtherTool(tool)).withTime(time + 7);
    }

    public boolean matches(final Region region) {
        return this.position.equals(region.position)
                && this.tool == region.tool;
    }

    public boolean isPassable() {
        return regionType.isPassableWith(tool);
    }

    public boolean isReachedBefore(final int time) {
        return this.time < time;
    }
}
