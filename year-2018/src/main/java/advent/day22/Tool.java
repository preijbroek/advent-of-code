package advent.day22;

public enum Tool {

    TORCH,
    CLIMBING_GEAR,
    NEITHER
}
