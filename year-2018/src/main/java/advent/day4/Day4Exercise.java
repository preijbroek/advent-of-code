package advent.day4;

import advent.InputProvider;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public final class Day4Exercise {

    private Day4Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(4);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<Event> events = input.stream()
                .map(Event::from)
                .sorted()
                .collect(Collectors.toList());
        Map<Integer, Guard> guards = events.stream()
                .filter(Event::isStartOfShift)
                .mapToInt(Event::guardId)
                .distinct()
                .mapToObj(Guard::new)
                .collect(toMap(Guard::getId, Function.identity()));
        Guard currentGuard = null;
        int sleepyMinute = 0;
        for (Event event : events) {
            if (event.isStartOfShift()) {
                currentGuard = guards.get(event.guardId());
            } else if (event.isStartOfSleep()) {
                sleepyMinute = event.minute();
            } else if (event.isStartOfAwake()) {
                for (int minute = sleepyMinute; minute != event.minute(); minute = (minute + 1) % 60) {
                    currentGuard.addSleepingMinute(minute);
                }
            }
        }
        Guard mostSleepyGuard = guards.values()
                .stream()
                .max(Comparator.comparing(Guard::amountOfSleep))
                .orElseThrow(IllegalStateException::new);
        System.out.println(mostSleepyGuard.sleepiestMinute() * mostSleepyGuard.getId());
    }

    private static void part2(final List<String> input) {
        List<Event> events = input.stream()
                .map(Event::from)
                .sorted()
                .collect(Collectors.toList());
        Map<Integer, Guard> guards = events.stream()
                .filter(Event::isStartOfShift)
                .mapToInt(Event::guardId)
                .distinct()
                .mapToObj(Guard::new)
                .collect(toMap(Guard::getId, Function.identity()));
        Guard currentGuard = null;
        int sleepyMinute = 0;
        for (Event event : events) {
            if (event.isStartOfShift()) {
                currentGuard = guards.get(event.guardId());
            } else if (event.isStartOfSleep()) {
                sleepyMinute = event.minute();
            } else if (event.isStartOfAwake()) {
                for (int minute = sleepyMinute; minute != event.minute(); minute = (minute + 1) % 60) {
                    currentGuard.addSleepingMinute(minute);
                }
            }
        }
        Guard target = guards.values()
                .stream()
                .max(Comparator.comparing(Guard::sleepiestMinuteTimes))
                .orElseThrow(IllegalStateException::new);
        System.out.println(target.sleepiestMinute() * target.getId());
    }

}
