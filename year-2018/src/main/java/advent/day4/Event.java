package advent.day4;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public final class Event implements Comparable<Event> {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private final LocalDateTime moment;
    private final String description;

    public Event(final LocalDateTime moment, final String description) {
        this.moment = moment;
        this.description = description;
    }

    public static Event from(final String s) {
        String description = s.substring(s.indexOf(']') + 2);
        String dateString = s.substring(1, 17);
        LocalDateTime moment = LocalDateTime.parse(dateString, FORMATTER);
        return new Event(moment, description);
    }

    public boolean isStartOfShift() {
        return description.contains("begins shift");
    }

    public boolean isStartOfSleep() {
        return "falls asleep".equals(description);
    }

    public boolean isStartOfAwake() {
        return "wakes up".equals(description);
    }

    public int guardId() {
        int guardIndex = description.indexOf('#') + 1;
        int guardEndIndex = description.indexOf(' ', guardIndex);
        return isStartOfShift() ? Integer.parseInt(description.substring(guardIndex, guardEndIndex)) : -1;
    }

    public int minute() {
        return moment.getMinute();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(moment, event.moment) &&
                Objects.equals(description, event.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(moment, description);
    }

    @Override
    public int compareTo(final Event o) {
        return this.moment.compareTo(o.moment);
    }
}
