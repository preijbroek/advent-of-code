package advent.day4;

import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toMap;

public final class Guard {

    private final int id;
    private final Map<Integer, Integer> sleepingMinutes = IntStream.range(0, 60)
            .boxed()
            .collect(toMap(Function.identity(), i -> 0));

    public Guard(final int id) {
        this.id = id;
    }

    public void addSleepingMinute(final int minute) {
        sleepingMinutes.put(minute, sleepingMinutes.get(minute) + 1);
    }

    public int amountOfSleep() {
        return sleepingMinutes.values()
                .stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public int sleepiestMinute() {
        return sleepingMinutes.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .orElseThrow(IllegalStateException::new);
    }

    public int sleepiestMinuteTimes() {
        return sleepingMinutes.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue())
                .map(Map.Entry::getValue)
                .orElseThrow(IllegalStateException::new);
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guard guard = (Guard) o;
        return id == guard.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
