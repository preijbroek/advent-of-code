package advent.day12;

import advent.InputProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static util.Comparators.maxOf;
import static util.Comparators.minOf;

public final class Day12Exercise {

    private Day12Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(12);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        System.out.println(spread(input, 20L));
    }

    private static void part2(final List<String> input) {
        System.out.println(spread(input, 50000000000L));
    }

    private static long spread(final List<String> input, final long iterations) {
        String initialState = input.get(0).split(" ")[2];
        Map<Long, Boolean> plants = IntStream.range(0, initialState.length())
                .boxed()
                .collect(toMap(i -> (long) i, i -> initialState.charAt(i) == '#'));
        Map<List<Boolean>, PotSequence> pots = IntStream.range(2, input.size())
                .mapToObj(i -> PotSequence.from(input.get(i)))
                .collect(Collectors.toMap(PotSequence::getPattern, Function.identity()));
        long sum = 0L;
        long difference = 0;
        long times = 0L;
        while (times < iterations) {
            times++;
            updateMap(plants, pots);
            long nextSum = plants.entrySet().stream()
                    .filter(Map.Entry::getValue)
                    .mapToLong(Map.Entry::getKey)
                    .sum();
            long nextDifference = nextSum - sum;
            sum = nextSum;
            if (nextDifference == difference) {
                break;
            }
            difference = nextDifference;
        }
        return sum + (iterations - times) * difference;
    }

    private static void updateMap(final Map<Long, Boolean> plants,
                                  final Map<List<Boolean>, PotSequence> pots) {
        Map<Long, Boolean> nextPhasePlants = new HashMap<>();
        long min = minOf(plants.keySet()) - 2;
        long max = maxOf(plants.keySet()) + 2;
        for (long l = min; l <= max; l++) {
            List<Boolean> pattern = LongStream.rangeClosed(l - 2, l + 2)
                    .mapToObj(plant -> plants.getOrDefault(plant, false))
                    .collect(toList());
            nextPhasePlants.put(l, pots.get(pattern).result());
        }
        plants.clear();
        plants.putAll(nextPhasePlants);
    }

}
