package advent.day12;

import java.util.Objects;

public class Pot {

    private final boolean hasPlant;

    public Pot(final boolean hasPlant) {
        this.hasPlant = hasPlant;
    }

    public boolean hasPlant() {
        return hasPlant;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pot pot = (Pot) o;
        return hasPlant == pot.hasPlant;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hasPlant);
    }
}
