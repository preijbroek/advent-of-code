package advent.day12;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class PotSequence {

    private final List<Boolean> pattern;
    private final boolean result;

    private PotSequence(final List<Boolean> pattern, final boolean result) {
        this.pattern = Collections.unmodifiableList(pattern);
        this.result = result;
    }

    public static PotSequence from(final String s) {
        String[] params = s.split(" => ");
        boolean result = "#".equals(params[1]);
        List<Boolean> pattern = params[0].chars()
                .mapToObj(i -> (char) i)
                .map(c -> c == '#')
                .collect(Collectors.toList());
        return new PotSequence(pattern, result);
    }

    public boolean result() {
        return result;
    }

    public List<Boolean> getPattern() {
        return pattern;
    }
}
