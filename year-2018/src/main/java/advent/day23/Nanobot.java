package advent.day23;

import lombok.Value;
import util.Position;

import java.util.Arrays;

@Value
public class Nanobot {

    Position position;
    int radius;

    public static Nanobot fromString(final String s) {
        final String[] data = s.split(", ");
        final int[] coordinates = Arrays.stream(data[0].substring(5, data[0].length() - 1).split(","))
                .mapToInt(Integer::parseInt)
                .toArray();
        final Position position = new Position(coordinates[0], coordinates[1], coordinates[2]);
        final int radius = Integer.parseInt(data[1].substring(2));
        return new Nanobot(position, radius);
    }

    public boolean hasInRange(final Nanobot nanobot) {
        return position.distanceTo(nanobot.position) <= radius;
    }

    public long minDistanceToCentre() {
        return Math.max(position.distanceToCentre() - radius, 0);
    }

    public long maxDistanceToCentre() {
        return position.distanceToCentre() + radius;
    }

}
