package advent.day23;

import advent.InputProvider;
import util.JavaPair;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class Day23Exercise {

    public static void main(final String[] args) {
        final List<Nanobot> input = InputProvider.getInput(23)
                .stream()
                .map(Nanobot::fromString)
                .collect(toList());
        part1(input);
        part2(input);
    }

    private static void part1(final List<Nanobot> input) {
        final Nanobot nanobotWithMaxRadius = Collections.max(input, comparing(Nanobot::getRadius));
        System.out.println(input.stream()
                .filter(nanobotWithMaxRadius::hasInRange)
                .count());
    }

    private static void part2(final List<Nanobot> input) {
        final Map<Integer, Integer> ranges = input.stream()
                .map(nanobot -> Arrays.asList(new JavaPair<>(nanobot.minDistanceToCentre(), 1), new JavaPair<>(nanobot.maxDistanceToCentre(), -1)))
                .flatMap(List::stream)
                .collect(toMap(JavaPair::getLeft, JavaPair::getRight, (a, b) -> b, TreeMap::new));
        int count = 0;
        int result = 0;
        int maxCount = 0;
        for (Map.Entry<Integer, Integer> each : ranges.entrySet()) {
            count += each.getValue();
            if (count > maxCount) {
                result = each.getKey();
                maxCount = count;
            }
        }
        System.out.println(result);
    }

}
