package advent.day1;

import advent.InputProvider;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class Day1Exercise {

    private Day1Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(1);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        System.out.println(input.stream().mapToInt(Integer::parseInt).sum());
    }

    private static void part2(final List<String> input) {
        List<Integer> frequencies = input.stream().map(Integer::parseInt).collect(Collectors.toList());
        Set<Integer> reachedFrequencies = new HashSet<>();
        int currentPosition = 0;
        int currentFrequency = frequencies.get(currentPosition);
        while (!reachedFrequencies.contains(currentFrequency)) {
            reachedFrequencies.add(currentFrequency);
            currentPosition = (currentPosition + 1) % frequencies.size();
            currentFrequency += frequencies.get(currentPosition);
        }
        System.out.println(currentFrequency);
    }

}
