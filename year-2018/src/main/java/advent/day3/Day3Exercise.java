package advent.day3;

import advent.InputProvider;
import util.Position;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class Day3Exercise {

    private Day3Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(3);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<Claim> claims = input.stream()
                .map(Claim::from)
                .collect(Collectors.toList());
        Map<Position, Integer> possibleOverlaps = new HashMap<>();
        claims.stream()
                .flatMap(claim -> claim.getPositions().stream())
                .forEach(position -> possibleOverlaps.put(position, possibleOverlaps.getOrDefault(position, 0) + 1));
        long overlaps = possibleOverlaps.values()
                .stream()
                .filter(value -> value > 1)
                .count();
        System.out.println(overlaps);
    }

    private static void part2(final List<String> input) {
        List<Claim> claims = input.stream()
                .map(Claim::from)
                .collect(Collectors.toList());
        claims.stream()
                .filter(claim -> claims.stream().noneMatch(claim::hasOverlap))
                .forEach(claim -> System.out.println(claim.getId()));
    }

}
