package advent.day3;

import util.Position;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public final class Claim {

    private final int id;
    private final Set<Position> positions;

    public Claim(final int id, final Set<Position> positions) {
        this.id = id;
        this.positions = positions;
    }

    public static Claim from(final String s) {
        String[] parameters = s.replaceAll("[@:#]", "").split("\\s+");
        int id = Integer.parseInt(parameters[0]);
        int cornerSplitIndex = parameters[1].indexOf(",");
        Position topLeftCorner =
                new Position(
                        Integer.parseInt(parameters[1].substring(0, cornerSplitIndex)),
                        Integer.parseInt(parameters[1].substring(cornerSplitIndex + 1))
                );
        int clothSplitIndex = parameters[2].indexOf("x");
        int x = Integer.parseInt(parameters[2].substring(0, clothSplitIndex));
        int y = Integer.parseInt(parameters[2].substring(clothSplitIndex + 1));
        Set<Position> positions = new HashSet<>();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                positions.add(topLeftCorner.add(new Position(i, j)));
            }
        }
        return new Claim(id, Collections.unmodifiableSet(positions));
    }

    public boolean hasOverlap(final Claim claim) {
        return !this.equals(claim) && !Collections.disjoint(this.positions, claim.positions);
    }

    public int getId() {
        return id;
    }

    public Set<Position> getPositions() {
        return positions;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Claim claim = (Claim) o;
        return id == claim.id &&
                Objects.equals(positions, claim.positions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, positions);
    }

    @Override
    public String toString() {
        return "Claim{" +
                "id=" + id +
                ", positions=" + positions +
                '}';
    }
}
