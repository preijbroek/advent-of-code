package advent.day18;

import lombok.Value;
import util.Position;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@Value
public class Grid {

    private static final char OPEN_GROUND = '.';
    private static final char TREE = '|';
    private static final char LUMBERYARD = '#';

    Map<Position, Character> state;

    public Grid nextMinute() {
        Map<Position, Character> nextState = new HashMap<>();
        for (Map.Entry<Position, Character> entry : state.entrySet()) {
            nextState.put(entry.getKey(), nextState(entry.getKey()));
        }
        return new Grid(nextState);
    }

    private char nextState(final Position position) {
        final List<Character> neighbours = neighbourStates(position);
        switch (state.get(position)) {
            case OPEN_GROUND: return nextOpenGroundState(neighbours);
            case TREE: return nextTreeState(neighbours);
            case LUMBERYARD: return nextLumberyardState(neighbours);
            default: throw new IllegalArgumentException("Unknown state found: " + state.get(position));
        }
    }

    private List<Character> neighbourStates(final Position position) {
        return position.allNeighbours().stream()
                .map(state::get)
                .filter(Objects::nonNull)
                .collect(toList());
    }

    private static char nextOpenGroundState(final List<Character> neighbours) {
        if (neighboursTypeCount(neighbours, TREE) >= 3) {
            return TREE;
        } else {
            return OPEN_GROUND;
        }
    }

    private static char nextTreeState(final List<Character> neighbours) {
        if (neighboursTypeCount(neighbours, LUMBERYARD) >= 3) {
            return LUMBERYARD;
        } else {
            return TREE;
        }
    }

    private static char nextLumberyardState(final List<Character> neighbours) {
        if (neighboursTypeCount(neighbours, LUMBERYARD) >= 1 && neighboursTypeCount(neighbours, TREE) >= 1) {
            return LUMBERYARD;
        } else {
            return OPEN_GROUND;
        }
    }

    private static long neighboursTypeCount(final List<Character> neighbours, final char type) {
        return neighbours.stream()
                .filter(c -> c == type)
                .count();
    }

    public long getResourceValue() {
        return countLumberYardAcres() * countTreeAcres();
    }

    private long countLumberYardAcres() {
        return countAcres(LUMBERYARD);
    }

    private long countTreeAcres() {
        return countAcres(TREE);
    }

    private long countAcres(final char type) {
        return state.values().stream()
                .filter(acreType -> acreType == type)
                .count();
    }

}
