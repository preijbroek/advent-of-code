package advent.day18;

import advent.InputProvider;
import util.Position;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day18Exercise {

    public static void main(final String[] args) {
        part1(getGrid());
        part2(getGrid());
    }

    private static void part1(final Grid grid) {
        Grid currentGrid = grid;
        for (int i = 0; i < 10; i++) {
           currentGrid = currentGrid.nextMinute();
        }
        System.out.println(currentGrid.getResourceValue());
    }

    private static void part2(final Grid grid) {
        int totalRounds = 1000000000;
        int minutes = 1;
        final Set<Grid> grids = new HashSet<>();
        grids.add(grid);
        Grid currentGrid = grid;
        while (grids.add(currentGrid = currentGrid.nextMinute())) {
            minutes++;
        }
        totalRounds -= minutes;
        minutes = 1;
        grids.clear();
        grids.add(currentGrid);
        while (grids.add(currentGrid = currentGrid.nextMinute())) {
            minutes++;
        }
        final int remainingRounds = totalRounds % minutes;
        for (int i = 0; i < remainingRounds; i++) {
            currentGrid = currentGrid.nextMinute();
        }
        System.out.println(currentGrid.getResourceValue());
    }

    private static Grid getGrid() {
        final List<String> input = InputProvider.getInput(18);
        final HashMap<Position, Character> acres = new HashMap<>();
        for (int y = 0; y < input.size(); y++) {
            final String line = input.get(y);
            for (int x = 0; x < line.toCharArray().length; x++) {
                acres.put(new Position(x, y), line.charAt(x));
            }
        }
        return new Grid(acres);
    }

}
