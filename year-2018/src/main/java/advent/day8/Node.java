package advent.day8;

import java.util.ArrayList;
import java.util.List;

public final class Node {

    private final List<Node> children = new ArrayList<>();
    private final List<Integer> metaData = new ArrayList<>();

    public int metaDataSumPart1() {
        return children.stream().mapToInt(Node::metaDataSumPart1).sum()
                + metaData.stream().mapToInt(Integer::intValue).sum();
    }

    public int metaDataSumPart2() {
        if (children.isEmpty()) {
            return metaData.stream().mapToInt(Integer::intValue).sum();
        } else {
            return metaData.stream()
                    .filter(i -> i <= children.size())
                    .map(i -> children.get(i - 1))
                    .mapToInt(Node::metaDataSumPart2)
                    .sum();
        }
    }

    public void addChild(final Node child) {
        children.add(child);
    }

    public void addMetaData(final Integer integer) {
        metaData.add(integer);
    }
}
