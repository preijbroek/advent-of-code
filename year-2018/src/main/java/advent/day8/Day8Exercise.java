package advent.day8;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class Day8Exercise {

    private Day8Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(8).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        Node root = new Node();
        List<Integer> data = Arrays.stream(input.split(" "))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        buildTree(0, root, data);
        System.out.println(root.metaDataSumPart1());
    }

    private static void part2(final String input) {
        Node root = new Node();
        List<Integer> data = Arrays.stream(input.split(" "))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        buildTree(0, root, data);
        System.out.println(root.metaDataSumPart2());
    }

    private static int buildTree(final int index, final Node currentNode, final List<Integer> data) {
        int nextIndex = index;
        int children = data.get(nextIndex++);
        int metaData = data.get(nextIndex++);
        for (int i = 0; i < children; i++) {
            Node child = new Node();
            currentNode.addChild(child);
            nextIndex = buildTree(nextIndex, child, data);
        }
        for (int i = 0; i < metaData; i++) {
            currentNode.addMetaData(data.get(nextIndex + i));
        }
        return nextIndex + metaData;
    }

}
