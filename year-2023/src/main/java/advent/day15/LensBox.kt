package advent.day15

data class LensBox(
    val id: Int,
) {

    private val lenses = mutableMapOf<String, Int>()

    fun run(label: String, operation: Char, focalValue: Int = 0) {
        when (operation) {
            '-' -> lenses.remove(label)
            '=' -> lenses[label] = focalValue
        }
    }

    fun focusingPower() = (id + 1) * lenses.values.mapIndexed { index, value -> (index + 1) * value }
        .sum()
}
