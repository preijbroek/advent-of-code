package advent.day15

import advent.getInput

fun main() {
    val input = getInput(15).first().split(",")
    println(part1(input))
    println(part2(input))
}

private fun part1(input: List<String>): Int {
    return input.sumOf(String::hash)
}

private fun part2(input: List<String>): Int {
    val boxes = (0..255).map(::LensBox).associateBy(LensBox::id)
    input.forEach { boxes.getValue(it.label().hash()).run(it.label(), it.instruction(), it.focalValue()) }
    return boxes.values.sumOf(LensBox::focusingPower)
}

private fun String.hash() = fold(0) { acc, c -> ((acc + c.code) * 17) and 255 }

private fun String.label() = substringBefore('=').substringBefore('-')

private fun String.instruction() = last().takeIf { it == '-' } ?: '='

private fun String.focalValue() = last().digitToIntOrNull() ?: 0
