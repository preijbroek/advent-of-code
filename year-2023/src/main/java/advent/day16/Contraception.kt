package advent.day16

import util.Movement
import util.Position

data class Contraception(private val grid: Map<Position, Char>) {

    fun energize(start: Beam): Int {
        val energizedFields = mutableSetOf(start.position)
        val seenBeams = mutableSetOf(start)
        generateSequence(listOf(start)) { beams ->
            beams.flatMap { it.next(grid).map(Beam::move) }.filter {
                it.position.takeIf { position -> position in grid }?.let(energizedFields::add)
                it !in seenBeams
            }
        }.takeWhile(List<Beam>::isNotEmpty).forEach(seenBeams::addAll)
        return energizedFields.size
    }

    fun boundary(): Set<Beam> {
        val minX = grid.keys.minOf(Position::x)
        val minY = grid.keys.minOf(Position::y)
        val maxX = grid.keys.maxOf(Position::x)
        val maxY = grid.keys.maxOf(Position::y)
        return (grid.keys.filter { it.x == minX }.map { Beam(it, Movement.RIGHT) } +
                grid.keys.filter { it.x == maxX }.map { Beam(it, Movement.LEFT) } +
                grid.keys.filter { it.y == minY }.map { Beam(it, Movement.DOWN) } +
                grid.keys.filter { it.y == maxY }.map { Beam(it, Movement.UP) }).toSet()
    }
}

data class Beam(
    val position: Position,
    val movement: Movement
)

fun Beam.move() = copy(position = position.to(movement))

fun Beam.next(grid: Map<Position, Char>): List<Beam> {
    return when (val tile = grid[position]) {
        null -> emptyList()
        '.' -> listOf(this)
        '|' ->
            if (movement == Movement.LEFT || movement == Movement.RIGHT)
                listOf(copy(movement = Movement.UP), copy(movement = Movement.DOWN))
            else
                listOf(this)
        '-' ->
            if (movement == Movement.UP || movement == Movement.DOWN)
                listOf(copy(movement = Movement.LEFT), copy(movement = Movement.RIGHT))
            else
                listOf(this)
        '/' ->
            if (movement == Movement.UP || movement == Movement.DOWN)
                listOf(copy(movement = movement.right()))
            else
                listOf(copy(movement = movement.left()))
        '\\' ->
            if (movement == Movement.UP || movement == Movement.DOWN)
                listOf(copy(movement = movement.left()))
            else
                listOf(copy(movement = movement.right()))
        else -> throw IllegalArgumentException(tile.toString())
    }
}