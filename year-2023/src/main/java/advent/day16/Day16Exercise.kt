package advent.day16

import advent.getInput
import util.Movement
import util.Position
import util.toCharMap

fun main() {
    val contraception = getInput(16).toCharMap().let(::Contraception)
    println(part1(contraception))
    println(part2(contraception))
}

private fun part1(contraception: Contraception): Int {
    return contraception.energize(start = Beam(Position.CENTRE, Movement.RIGHT))
}


private fun part2(contraception: Contraception): Int {
    return contraception.boundary().maxOf {
        contraception.energize(start = it)
    }
}
