package advent.day7

import advent.getInput

fun main() {
    val hands = getInput(7).map(Hand::parse)
    println(part1(hands))
    println(part2(hands))
}

private fun part1(hands: List<Hand>): Long {
    return hands.totalWinnings(Hand::compareRegular)
}

private fun part2(hands: List<Hand>): Long {
    return hands.totalWinnings(Hand::compareJoker)
}

private fun List<Hand>.totalWinnings(comparator: Comparator<in Hand>) =
    sortedWith(comparator).mapIndexed { index, hand -> (index + 1) * hand.bid }.sum()
