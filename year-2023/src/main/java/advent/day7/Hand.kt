package advent.day7

import util.second

data class Hand(
    private val cards: List<Card>,
    val bid: Long,
) {

    private val regularType = cards.regularType()
    private val jokerType = cards.jokerType()

    fun compareRegular(other: Hand): Int {
        return regularType.compareTo(other.regularType).takeIfNot0()
            ?: (cards zip other.cards).firstNotNullOfOrNull { (card1, card2) -> card1.compareTo(card2).takeIfNot0() }
            ?: 0
    }
    fun compareJoker(other: Hand): Int {
        return jokerType.compareTo(other.jokerType).takeIfNot0()
            ?: (cards zip other.cards).firstNotNullOfOrNull { (card1, card2) -> card1.compareToJokerLast(card2).takeIfNot0() }
            ?: 0
    }

    companion object {
        fun parse(s: String): Hand {
            val parameters = s.split(" ")
            val cards = parameters.first().map(Card::from)
            val bid = parameters.second().toLong()
            return Hand(cards, bid)
        }
    }
}

private fun Int.takeIfNot0() = takeUnless { it == 0 }

fun List<Card>.regularType(): Type {
    val groupedCards = groupingBy { it }.eachCount()
    return when (groupedCards.size) {
        1 -> Type.FIVE_OF_A_KIND
        2 ->
            if (groupedCards.values.any { it == 4 }) Type.FOUR_OF_A_KIND
            else Type.FULL_HOUSE
        3 ->
            if (groupedCards.values.any { it == 3 }) Type.THREE_OF_A_KIND
            else Type.TWO_PAIR
        4 -> Type.ONE_PAIR
        5 -> Type.HIGH_CARD
        else -> throw Exception("Wrong hand detected: $this")
    }
}

fun List<Card>.jokerType(): Type {
    val groupedCards = groupingBy { it }.eachCount().withDefault { 0 }
    val mostCommonCard = (groupedCards - Card.VJ).maxByOrNull(Map.Entry<Card, Int>::value)?.key ?: Card.VA
    return this.map { if (it == Card.VJ) mostCommonCard else it }.regularType()
}

enum class Type {
    HIGH_CARD,
    ONE_PAIR,
    TWO_PAIR,
    THREE_OF_A_KIND,
    FULL_HOUSE,
    FOUR_OF_A_KIND,
    FIVE_OF_A_KIND,
}

enum class Card {
    V2,
    V3,
    V4,
    V5,
    V6,
    V7,
    V8,
    V9,
    VT,
    VJ,
    VQ,
    VK,
    VA,
    ;

    companion object {
        fun from(c: Char) = values().first { it.name.last() == c }
    }

    fun compareToJokerLast(other: Card): Int {
        return if (this == other) 0
        else if (this == VJ) -1
        else if (other == VJ) 1
        else this.compareTo(other)
    }
}

