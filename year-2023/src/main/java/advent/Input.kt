package advent

import util.getInputBlocksForYear
import util.getInputForYear

fun getInput(day: Int) = getInputForYear(2023, day)

fun getInputBlocks(day: Int) = getInputBlocksForYear(2023, day)