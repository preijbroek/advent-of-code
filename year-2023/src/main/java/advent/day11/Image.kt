package advent.day11

import util.Position
import util.Position.Companion.byX
import util.Position.Companion.byY
import util.x

data class Image(
    val galaxies: Set<Position>,
) {

    fun expanded(expansionFactor: Int): Image {
        return galaxies.expand(Position::x, Position::plusX, expansionFactor)
            .expand(Position::y, Position::plusY, expansionFactor)
            .let(::Image)
    }

    private fun Set<Position>.expand(
        direction: Position.() -> Long,
        adjust: Position.(Int) -> Position,
        expansionFactor: Int
    ): Set<Position> {
        val presentGalaxies = map(direction).toSet()
        val absentGalaxies = (presentGalaxies.min()..presentGalaxies.max()) - presentGalaxies
        return map { galaxy ->
            galaxy.adjust(absentGalaxies.count { it < galaxy.direction() } * (expansionFactor - 1))
        }.toSet()
    }

    fun sumOfDistances(): Long {
        return (galaxies x galaxies)
            .asSequence()
            .filter { (g1, g2) -> g1 != g2 }
            .map { (g1, g2) -> minOf(g1, g2, byX().then(byY())) to maxOf(g1, g2, byX().then(byY())) }
            .toSet()
            .sumOf { (g1, g2) -> g1 distanceTo g2 }
    }
}