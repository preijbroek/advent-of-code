package advent.day11

import advent.getInput
import util.toCharMap

fun main() {
    val image = getInput(11).toCharMap().filterValues { it == '#' }.keys.let(::Image)
    println(part1(image))
    println(part2(image))
}

private fun part1(image: Image): Long {
    return image.expanded(expansionFactor = 2).sumOfDistances()
}

private fun part2(image: Image): Long {
    return image.expanded(expansionFactor = 1_000_000).sumOfDistances()
}
