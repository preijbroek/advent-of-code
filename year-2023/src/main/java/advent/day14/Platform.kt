package advent.day14

import util.Direction
import util.Position
import util.Position.Companion.byX
import util.Position.Companion.byY
import util.box

data class Platform(
    private val rocksMap: Map<Position, Rock>,
    private val maxX: Int = rocksMap.maxOf { (position, _) -> position.x },
    private val maxY: Int = rocksMap.maxOf { (position, _) -> position.y },
) {

    private val box = Position.CENTRE box Position(maxX, maxY)
    val hash = rocksMap.toString()

    fun slide(direction: Direction): Platform {
        val newRocksMap = rocksMap.toMutableMap()
        rocksMap.filterValues { it == Rock.ROUNDED }.keys.sortedWith(direction.sortingOrder())
            .forEach { position ->
                var current = position
                var next = current.toFlipped(direction)
                while (next !in newRocksMap && next in box) {
                    newRocksMap.remove(current)
                    newRocksMap[next] = Rock.ROUNDED
                    current = next
                    next = next.toFlipped(direction)
                }
            }
        return Platform(
            rocksMap = newRocksMap,
            maxX = maxX,
            maxY = maxY,
        )
    }

    private fun Direction.sortingOrder(): Comparator<in Position> = when (this) {
        Direction.NORTH -> byY().thenComparing(byX())
        Direction.WEST -> byX().thenComparing(byY())
        Direction.SOUTH -> byY().reversed().thenComparing(byX())
        Direction.EAST -> byX().reversed().thenComparing(byY())
    }

    fun totalLoad() = rocksMap.filterValues { it == Rock.ROUNDED }
        .keys.sumOf { maxY - it.y + 1 }
}

enum class Rock(val c: Char) {
    ROUNDED('O'),
    CUBED('#'),
    ;

    companion object {
        private val cachedValues = values()

        fun valueOf(c: Char) = cachedValues.first { it.c == c }
    }
}
