package advent.day14

import advent.getInput
import util.Direction
import util.toCharMap

fun main() {
    val platform = getInput(14).toCharMap().filterValues { it != '.' }
        .mapValues { Rock.valueOf(it.value) }.let(::Platform)
    println(part1(platform))
    println(part2(platform))
}

private fun part1(platform: Platform): Int {
    return platform.slide(Direction.NORTH).totalLoad()
}

private fun part2(platform: Platform): Int {
    val totalCycles = 1_000_000_000
    val seenPlatforms = mutableMapOf<String, Int>()
    var completedCycles = 0
    var lastMovedDirection = Direction.EAST
    var next = platform

    fun slide() {
        lastMovedDirection = lastMovedDirection.left()
        next = next.slide(lastMovedDirection)
        if (lastMovedDirection == Direction.EAST) {
            completedCycles++
        }
    }

    while (next.hash(lastMovedDirection) !in seenPlatforms && completedCycles < totalCycles) {
        seenPlatforms[next.hash(lastMovedDirection)] = completedCycles
        slide()
    }
    val repetitionCycle = completedCycles - seenPlatforms.getValue(next.hash(lastMovedDirection))
    val repetitionCyclesCount = (totalCycles - completedCycles) / repetitionCycle
    completedCycles += repetitionCyclesCount * repetitionCycle
    while (completedCycles < totalCycles) {
        slide()
    }
    return next.totalLoad()
}


private fun Platform.hash(direction: Direction) = "$hash$direction"
