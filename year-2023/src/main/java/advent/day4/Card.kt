package advent.day4

import util.pow
import util.second

data class Card(
    val id: Int,
    val winningNumbers: Set<Int>,
    val chosenNumbers: Set<Int>,
) {

    val winningNumbersCount = (winningNumbers intersect chosenNumbers).size

    val score = 2.pow((winningNumbersCount)) / 2

    companion object {

        fun parse(s: String): Card {
            val idAndNumbers = s.split(": +".toRegex())
            val id = idAndNumbers.first().split(" +".toRegex()).last().toInt()
            val numbers = idAndNumbers.second().split(" \\| +".toRegex())
            val winningNumbers = numbers.first().split(" +".toRegex()).map(String::toInt).toSet()
            val chosenNumbers = numbers.second().split(" +".toRegex()).map(String::toInt).toSet()
            return Card(
                id = id,
                winningNumbers = winningNumbers,
                chosenNumbers = chosenNumbers
            )
        }
    }
}

class Cards(cards: List<Card>) {

    private val cardsMap = cards.associateBy(Card::id)

    fun cardCounts(): Int {
        val cardsCount = cardsMap.values.associateWith { 1 }.toMutableMap()

        fun Card.add(n: Int) {
            cardsCount[this] = cardsCount.getValue(this) + n
        }

        cardsMap.entries.forEach { (id, card) ->
            repeat(card.winningNumbersCount) {
                cardsMap.getValue(id + it + 1).add(cardsCount.getValue(card))
            }
        }
        return cardsCount.values.sum()
    }
}
