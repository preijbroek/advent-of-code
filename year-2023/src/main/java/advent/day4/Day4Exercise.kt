package advent.day4

import advent.getInput

fun main() {
    val cards = getInput(4).map(Card::parse)
    println(part1(cards))
    println(part2(cards))
}

private fun part1(cards: List<Card>): Int {
    return cards.sumOf(Card::score)
}

private fun part2(cards: List<Card>): Int {
    return Cards(cards).cardCounts()
}