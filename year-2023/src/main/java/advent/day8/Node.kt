package advent.day8

import util.second

data class Node(
    val id: String,
    val left: () -> Node,
    val right: () -> Node,
) {

    companion object {
        fun parse(s: String, nodesMap: Map<String, Node>): Node {
            val parameters = s.split(" = ")
            val id = parameters.first()
            val (left, right) = parameters.second().drop(1).dropLast(1).split(", ")
            return Node(
                id = id,
                left = { nodesMap.getValue(left) },
                right = { nodesMap.getValue(right) },
            )
        }
    }
}
