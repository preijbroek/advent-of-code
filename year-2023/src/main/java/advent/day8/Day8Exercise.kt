package advent.day8

import advent.getInputBlocks
import util.lcm
import util.second

fun main() {
    val input = getInputBlocks(8)
    val instructions = input.first()
    val nodesMap = mutableMapOf<String, Node>()
    input.second().lines().map { Node.parse(it, nodesMap) }
        .forEach { nodesMap[it.id] = it }
    println(part1(nodesMap, instructions))
    println(part2(nodesMap, instructions))
}

private fun part1(nodes: Map<String, Node>, instructions: String): Int {
    return nodes.getValue("AAA").runUntilZ(instructions)
}

private fun part2(nodesMap: Map<String, Node>, instructions: String): Long {
    return nodesMap.filterKeys { it.endsWith('A') }.values
        .map { it.runUntilZ(instructions) }.map(Int::toLong).reduce { acc, l -> acc.lcm(l) }
}

private fun Node.runUntilZ(instructions: String): Int {
    var node = this
    var index = 0
    while (!node.id.endsWith('Z')) {
        node = node.next(instructions[(index++) % instructions.length])
    }
    return index
}

private fun Node.next(c: Char) = when (c) {
    'L' -> left()
    'R' -> right()
    else -> throw IllegalArgumentException(c.toString())
}
