package advent.day18

import util.Movement
import util.Position

data class DigPlan(
    val digLines: List<DigLine>,
) {

    fun withExpandedColours(): DigPlan = DigPlan(
        digLines.map(DigLine::extractFromColour)
    )

    fun execute(): Long {
        val polygonPositions = digLines.fold(listOf(Position.CENTRE)) { acc, line ->
            acc + acc.last().to(line.movement, line.length)
        }
        // Shoelace theorem
        val area = polygonPositions.indices.sumOf { index ->
            polygonPositions[index] determinant2d polygonPositions[(index + 1) % polygonPositions.size]
        } shr 1
        val boundary = digLines.sumOf(DigLine::length).toLong()
        // Pick's theorem
        val interior = area - (boundary shr 1) + 1
        return boundary + interior
    }
}

data class DigLine(
    val movement: Movement,
    val length: Int,
    val colour: String,
) {

    fun extractFromColour() = DigLine(
        movement = colour.drop(5).toMovement(),
        length = colour.dropLast(1).toInt(radix = 16),
        colour = colour
    )

    companion object {
        fun parse(s: String): DigLine {
            val (movement, length, colour) = s.split(" ")
                .let { (m, l, c) ->
                    Triple(m.toMovement(), l.toInt(), c.drop(2).dropLast(1))
                }
            return DigLine(
                movement,
                length,
                colour
            )
        }

        private fun String.toMovement() = when (this) {
            "D", "1" -> Movement.DOWN
            "U", "3" -> Movement.UP
            "R", "0" -> Movement.RIGHT
            "L", "2" -> Movement.LEFT
            else -> throw IllegalArgumentException(this)
        }
    }
}