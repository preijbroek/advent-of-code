package advent.day18

import advent.getInput

fun main() {
    val digLines = getInput(18).map(DigLine::parse).let(::DigPlan)
    println(part1(digLines))
    println(part2(digLines))
}

private fun part1(digPlan: DigPlan): Long {
    return digPlan.execute()
}

private fun part2(digPlan: DigPlan): Long {
    return digPlan.withExpandedColours().execute()
}

