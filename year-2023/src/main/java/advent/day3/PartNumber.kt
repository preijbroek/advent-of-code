package advent.day3

import util.Position

data class PartNumber(
    val value: Int,
    val positions: Set<Position>,
)
