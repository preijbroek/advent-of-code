package advent.day3

import advent.getInput
import util.Position
import util.second
import util.toCharMap

fun main() {
    val grid = getInput(3).toCharMap()
    println(part1(grid))
    println(part2(grid))
}

private fun part1(grid: Map<Position, Char>): Int {
    val partNumbers = grid.filterValues(Char::isDigit).parsePartNumbers()
        .flatMap { partNumber -> partNumber.positions.map { position -> position to partNumber } }
        .toMap()
    return grid.filterValues { it.isSymbol() }
        .flatMap { (position, _) -> position.neighbouringPartNumbers(partNumbers) }
        .toSet()
        .sumOf(PartNumber::value)
}

private fun part2(grid: Map<Position, Char>): Int {
    val partNumbers = grid.filterValues(Char::isDigit).parsePartNumbers()
        .flatMap { it.positions.map { position -> position to it } }
        .toMap()
    return grid.filterValues(Char::isGear)
        .map { (position, _) -> position.neighbouringPartNumbers(partNumbers) }
        .map(List<PartNumber>::toSet)
        .filter { it.size == 2 }
        .sumOf { it.first().value * it.second().value }
}

private fun Map<Position, Char>.parsePartNumbers(): List<PartNumber> {
    val partNumbers = mutableListOf<PartNumber>()
    val usedPositions = mutableSetOf<Position>()
    for (position in keys) {
        if (!usedPositions.add(position)) continue
        val partNumberPositions = mutableSetOf(position)
        var value = ""
        var nextPosition = position
        while (nextPosition in this) {
            usedPositions.add(nextPosition)
            partNumberPositions.add(nextPosition)
            value += this[nextPosition]
            nextPosition = nextPosition.east()
        }
        partNumbers.add(PartNumber(value.toInt(), partNumberPositions))
    }
    return partNumbers
}

private fun Char?.isSymbol(): Boolean {
    return this != null && !isDigit() && this != '.'
}

private fun Char.isGear() = this == '*'

private fun Position.neighbouringPartNumbers(partNumbers: Map<Position, PartNumber>) =
    allNeighbours().mapNotNull { partNumbers[it] }
