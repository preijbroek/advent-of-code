package advent.day21

import util.Position
import util.pow
import util.rem
import util.second
import util.third

data class Garden(
    private val startingPosition: Position,
    private val path: Set<Position>,
) {

    private val gardenLength = path.maxOf(Position::x) + 1

    fun infinitelySizedReachableIn(totalSteps: Long): Long {
        val gridsCount = totalSteps / gardenLength
        val remainder = totalSteps % gardenLength
        val coordinates = mutableListOf<Int>()
        var steps = 0
        val destinations = mutableMapOf(startingPosition to 0)
        var currentPositions = setOf(startingPosition)
        (0..2).forEach { n ->
            while (steps < n * gardenLength + remainder) {
                steps++
                currentPositions = currentPositions
                    .flatMap { position -> position.neighbours() }
                    .filter { (it % gardenLength) in path && it !in destinations }
                    .toSet()
                currentPositions.forEach { destinations[it] = steps }
            }
            val comparingInt = (steps and 1)
            coordinates.add(destinations.filterValues { (it and 1) == comparingInt }.size)
        }
        // For quadratic formula:
        val c = coordinates.first()
        val aPlusB = coordinates.second() - c
        val fourAPlusTwoB = coordinates.third() - c
        val twoA = fourAPlusTwoB - (aPlusB shl 1)
        val a = twoA shr 1
        val b = aPlusB - a
        return a * gridsCount.pow(2) + b * gridsCount + c
    }
}
