package advent.day21

import advent.getInput
import util.toCharMap

fun main() {
    val grid = getInput(21).toCharMap().filterValues { it != '#' }
    val startingPosition = grid.entries.first { (_, path) -> path == 'S' }.key
    val garden = Garden(startingPosition, grid.keys)
    println(part1(garden))
    println(part2(garden))
}

private fun part1(garden: Garden): Long {
    return garden.infinitelySizedReachableIn(totalSteps = 64L)
}

private fun part2(garden: Garden): Long {
    return garden.infinitelySizedReachableIn(totalSteps = 26501365L)
}