package advent.day1

import advent.getInput

fun main() {
    val input = getInput(1)
    println(part1(input))
    println(part2(input))
}

private fun part1(input: List<String>): Int {
    return input.sumOf(String::calibrationValue)
}

private fun part2(input: List<String>): Int {
    return input.sumOf { it.requiredDigits().calibrationValue() }
}

private fun String.calibrationValue() = "${firstDigit()}${lastDigit()}".toInt()

private fun String.firstDigit() = first { it.isDigit() }

private fun String.lastDigit() = last { it.isDigit() }

private fun String.requiredDigits(): String {
    val firstDigit = digitStrings.getValue(firstDigitString())
    val lastDigitIndex = lastIndexOfAny(digitStrings.keys + digitStrings.values)
    val lastDigit = digitStrings.getValue(substring(lastDigitIndex).firstDigitString())
    return "$firstDigit$lastDigit"
}

private fun String.firstDigitString() = numbersRegex.find(this)?.value
    ?: throw IllegalArgumentException(this)

private val digitStrings = mapOf(
    "one" to "1",
    "two" to "2",
    "three" to "3",
    "four" to "4",
    "five" to "5",
    "six" to "6",
    "seven" to "7",
    "eight" to "8",
    "nine" to "9",
).withDefault { it }

private val numbersRegex = (digitStrings.keys + digitStrings.values)
    .joinToString(separator = "|", prefix = "(", postfix = ")")
    .toRegex()
