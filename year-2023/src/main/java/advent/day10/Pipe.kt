package advent.day10

import util.Direction
import util.Direction.EAST
import util.Direction.NORTH
import util.Direction.SOUTH
import util.Direction.WEST
import util.isDisjoint
import java.util.EnumSet

enum class Pipe(val c: Char, private val possibleTraffic: EnumSet<Direction>) {
    START('S', EnumSet.of(NORTH, EAST, SOUTH, WEST)),
    NORTH_EAST('L', EnumSet.of(NORTH, EAST)),
    NORTH_SOUTH('|', EnumSet.of(NORTH, SOUTH)),
    NORTH_WEST('J', EnumSet.of(NORTH, WEST)),
    EAST_SOUTH('F', EnumSet.of(EAST, SOUTH)),
    EAST_WEST('-', EnumSet.of(EAST, WEST)),
    SOUTH_WEST('7', EnumSet.of(SOUTH, WEST)),
    GROUND('.', EnumSet.noneOf(Direction::class.java)),
    ;

    fun nextDirection(movement: Direction): Direction {
        val enteringDirection = movement.back()
        if (enteringDirection !in possibleTraffic) throw IllegalArgumentException("Cannot enter $this from $enteringDirection")
        return possibleTraffic.first { it != enteringDirection }
    }

    fun canConnect(direction: Direction) = direction in possibleTraffic

    fun mayGoEast() = EAST in possibleTraffic

    infix fun isDisjoint(other: Pipe) = (possibleTraffic isDisjoint other.possibleTraffic)

    companion object {
        private val cachedValues = values()

        fun parse(c: Char): Pipe =
            cachedValues.firstOrNull { it.c == c } ?: throw IllegalArgumentException(c.toString())
    }
}