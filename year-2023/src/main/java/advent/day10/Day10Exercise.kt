package advent.day10

import advent.getInput
import util.Direction
import util.Direction.EAST
import util.Direction.NORTH
import util.Direction.SOUTH
import util.Direction.WEST
import util.Position
import util.toCharMap

fun main() {
    val pipes = getInput(10).toCharMap().mapValues { Pipe.parse(it.value) }
    println(part1(pipes))
    println(part2(pipes))
}

private fun part1(pipes: Map<Position, Pipe>): Int {
    return pipes.loop().size / 2
}

private fun part2(pipes: Map<Position, Pipe>): Int {
    val loop = pipes.loop()
    val reachedPositions = mutableSetOf<Position>()
    val positionsToCheck = ArrayDeque(listOf(Position.CENTRE))
    while (positionsToCheck.isNotEmpty()) {
        val position = positionsToCheck.removeFirst()
        if (reachedPositions.add(position)) {
            positionsToCheck.addAll(
                position.neighbours().filter {
                    it !in reachedPositions && it !in positionsToCheck && it !in loop && it in pipes
                }
            )
        }
    }
    return (pipes.keys - reachedPositions.toSet() - loop)
        .count { it.isEnclosedIn(loop, pipes) }
}

private fun Map<Position, Pipe>.loop(): Set<Position> {
    var (currentPosition, currentPipe) = entries.first { (_, value) -> value == Pipe.START }
    var direction = currentPosition.firstPossibleDirection(this)
    val loop = mutableSetOf<Position>()
    do {
        currentPosition = currentPosition.toFlipped(direction)
        currentPipe = getValue(currentPosition)
        direction = currentPipe.nextDirection(direction)
        loop.add(currentPosition)
    } while (currentPipe != Pipe.START)
    return loop
}

private fun Position.firstPossibleDirection(pipes: Map<Position, Pipe>): Direction {
    return when {
        pipes[north()]?.canConnect(NORTH) == true -> SOUTH
        pipes[east()]?.canConnect(WEST) == true -> EAST
        pipes[south()]?.canConnect(SOUTH) == true -> NORTH
        pipes[west()]?.canConnect(EAST) == true -> WEST
        else -> throw IllegalStateException("Map $pipes with starting position $this is invalid")
    }
}

fun Position.isEnclosedIn(loop: Set<Position>, pipes: Map<Position, Pipe>): Boolean {
    val intersections = (0..x).map { Position(it, y) }.toSet() intersect loop
    var isEnclosed = false
    val iterator = intersections.iterator()
    while (iterator.hasNext()) {
        val rowEntry = iterator.next()
        when {
            pipes.getValue(rowEntry).mayGoEast() -> {
                var next: Position
                do {
                    next = iterator.next()
                } while (pipes.getValue(next).mayGoEast())
                if (pipes.getValue(next) isDisjoint pipes.getValue(rowEntry)) {
                    isEnclosed = !isEnclosed
                }
            }
            else -> isEnclosed = !isEnclosed
        }
    }
    return isEnclosed
}
