package advent.day12

import advent.getInput

fun main() {
    val springs = getInput(12).map(Springs::parse)
    println(part1(springs))
    println(part2(springs))
}

private fun part1(springs: List<Springs>): Long {
    return springs.sumOf(Springs::possibleConfigurations)
}

private fun part2(springs: List<Springs>): Long {
    return springs.map(Springs::unfold).sumOf(Springs::possibleConfigurations)
}
