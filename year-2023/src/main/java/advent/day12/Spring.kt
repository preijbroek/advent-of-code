package advent.day12

import util.second
import util.times
import java.util.Collections.nCopies

data class Springs(
    private val springsLine: String,
    private val damagedSpringsGroups: List<Long>,
) {

    private val knownPossibleCombinations = mutableMapOf<String, Long>()

    fun possibleConfigurations(
        remainingLine: String = springsLine,
        remainingGroups: List<Long> = damagedSpringsGroups
    ): Long {
        val hash = hash(remainingLine, remainingGroups)
        return when {
            hash in knownPossibleCombinations -> knownPossibleCombinations.getValue(hash)
            remainingLine.isEmpty() -> 1L * remainingGroups.isEmpty()
            remainingGroups.isEmpty() ->
                (1L * ('#' !in remainingLine)).also { knownPossibleCombinations[hash] = it }
            (remainingGroups.sum() + remainingGroups.lastIndex) > remainingLine.length -> 0
            else -> when (remainingLine.first()) {
                '?' ->
                    (possibleConfigurations(
                        remainingLine.replaceFirst('?', '.'),
                        remainingGroups
                    ) + possibleConfigurations(
                        remainingLine.replaceFirst('?', '#'),
                        remainingGroups
                    )).also { knownPossibleCombinations[hash] = it }
                '.' -> possibleConfigurations(remainingLine.drop(1), remainingGroups)
                    .also { knownPossibleCombinations[hash] = it }
                '#' -> when {
                    '.' in remainingLine.substring(0, remainingGroups.first().toInt()) -> 0L
                    remainingLine.length == remainingGroups.first().toInt() -> 1L * (remainingGroups.size == 1)
                    remainingLine[remainingGroups.first().toInt()] == '#' -> 0L
                    else -> possibleConfigurations(
                        remainingLine.drop(remainingGroups.first().toInt() + 1),
                        remainingGroups.drop(1)
                    ).also { knownPossibleCombinations[hash] = it }
                }
                else -> throw IllegalArgumentException(remainingLine.first().toString())
            }
        }
    }

    fun unfold() = copy(
        springsLine = nCopies(5, springsLine).joinToString(separator = "?"),
        damagedSpringsGroups = nCopies(5, damagedSpringsGroups).flatten()
    )

    private fun hash(remainingLine: String, remainingGroups: List<Long>) = "$remainingLine$remainingGroups"

    companion object {

        fun parse(s: String): Springs {
            val parameters = s.split(" ")
            val springsLine = parameters.first()
            val damagedSpringsGroups = parameters.second().split(",").map(String::toLong)
            return Springs(springsLine, damagedSpringsGroups)
        }
    }
}
