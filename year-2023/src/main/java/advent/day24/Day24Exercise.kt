package advent.day24

import advent.getInput
import util.Decimal
import util.Decimal.Companion.ZERO
import util.DecimalPosition
import util.DecimalRange
import util.distinctPairs
import util.div
import util.minus
import util.plus
import util.rangeTo
import util.times
import util.toDecimal
import util.unaryMinus

fun main() {
    val hailStones = getInput(24).map(HailStone::parse)
    println(part1(hailStones))
    println(part2(hailStones))
}

private fun part1(hailStones: List<HailStone>): Int {
    val min = 200000000000000L.toDecimal()
    val max = 400000000000000L.toDecimal()
    return hailStones.mapIndexed { index, hailStone ->
        val possibleCrossingStones = hailStones.subList(index, hailStones.size).drop(1)
        possibleCrossingStones.mapNotNull(hailStone::crossesInFutureAtIgnoringZ)
            .count { (x, y) -> x in min..max && y in min..max }
    }.sum()
}

private fun part2(hailStones: List<HailStone>): Long {
    val (h1, h2) = hailStones

    return hailStones
        .possibleVelocities()
        .mapNotNull { velocity -> velocity.deduceThrowingLocation(h1, h2)?.let { HailStone(it, velocity) } }
        .first { rock -> hailStones.all { rock.willCollideWith(it) } }
        .let { it.currentPosition.x + it.currentPosition.y + it.currentPosition.z }
        .toLong()
}

private fun List<HailStone>.possibleVelocities(): Sequence<DecimalPosition> = sequence {
    val amplitude = 250
    val velocityRange = -amplitude.toDecimal()..amplitude.toDecimal()
    val invalidXRanges = mutableSetOf<DecimalRange>()
    val invalidYRanges = mutableSetOf<DecimalRange>()
    val invalidZRanges = mutableSetOf<DecimalRange>()

    fun MutableSet<DecimalRange>.testImpossible(p1: Decimal, v1: Decimal, p2: Decimal, v2: Decimal) {
        if (p1 > p2 && v1 > v2) add(v2..v1)
        if (p2 > p1 && v2 > v1) add(v1..v2)
    }

    distinctPairs().forEach { (h1, h2) ->
        invalidXRanges.testImpossible(h1.currentPosition.x, h1.velocity.x, h2.currentPosition.x, h2.velocity.x)
        invalidYRanges.testImpossible(h1.currentPosition.y, h1.velocity.y, h2.currentPosition.y, h2.velocity.y)
        invalidZRanges.testImpossible(h1.currentPosition.z, h1.velocity.z, h2.currentPosition.z, h2.velocity.z)
    }

    val possibleX = velocityRange.filter { x -> invalidXRanges.none { x in it } }
    val possibleY = velocityRange.filter { y -> invalidYRanges.none { y in it } }
    val possibleZ = velocityRange.filter { z -> invalidZRanges.none { z in it } }

    possibleX.forEach { x ->
        possibleY.forEach { y ->
            possibleZ.forEach { z ->
                yield(DecimalPosition(x, y, z))
            }
        }
    }
}

private fun DecimalPosition.deduceThrowingLocation(h1: HailStone, h2: HailStone): DecimalPosition? {
    // Horrible naming scheme, read as: hailstone relative velocity; translated to rock's inertial frame.
    val h1relativeVelocityX = h1.velocity.x - x
    val h1relativeVelocityY = h1.velocity.y - y
    val h2relativeVelocityX = h2.velocity.x - x
    val h2relativeVelocityY = h2.velocity.y - y

    val slopeDiff = h1relativeVelocityX * h2relativeVelocityY - h1relativeVelocityY * h2relativeVelocityX
    if (slopeDiff == ZERO) return null

    val t: Decimal = (h2relativeVelocityY * (h2.currentPosition.x - h1.currentPosition.x) - h2relativeVelocityX * (h2.currentPosition.y - h1.currentPosition.y)) / slopeDiff
    if (t < ZERO) return null

    return DecimalPosition(
        x = h1.currentPosition.x + (h1.velocity.x - x) * t,
        y = h1.currentPosition.y + (h1.velocity.y - y) * t,
        z = h1.currentPosition.z + (h1.velocity.z - z) * t,
    )
}