package advent.day24

import util.Decimal
import util.Decimal.Companion.ZERO
import util.DecimalPosition
import util.div
import util.minus
import util.plus
import util.times

data class HailStone(
    val currentPosition: DecimalPosition,
    val velocity: DecimalPosition,
) {

    fun positionAfter(time: Decimal) = currentPosition + time * velocity

    fun willCollideWith(other: HailStone): Boolean {
        val time = when {
            velocity.x != other.velocity.x -> (other.currentPosition.x - currentPosition.x) / (velocity.x - other.velocity.x)
            velocity.y != other.velocity.y -> (other.currentPosition.y - currentPosition.y) / (velocity.y - other.velocity.y)
            velocity.z != other.velocity.z -> (other.currentPosition.z - currentPosition.z) / (velocity.z - other.velocity.z)
            else -> return false
        }
        return time >= ZERO && positionAfter(time) == other.positionAfter(time)
    }

    fun coordinatesSum() = (currentPosition.x + currentPosition.y + currentPosition.z).toLong()

    fun velocitySum() = (velocity.x + velocity.y + velocity.z).toLong()

    fun crossesInFutureAtIgnoringZ(other: HailStone): Pair<Decimal, Decimal>? {
        val aThis = velocity.y / velocity.x
        val bThis = currentPosition.y - (aThis * currentPosition.x)
        val aOther = other.velocity.y / other.velocity.x
        val bOther = other.currentPosition.y - aOther * other.currentPosition.x
        return if (aThis == aOther) {
            null
        } else {
            val xCrossing = (bOther - bThis) / (aThis - aOther)
            val yCrossing = xCrossing * aThis + bThis
            return (xCrossing to yCrossing).takeIf {
                xCrossing > currentPosition.x == velocity.x > ZERO && xCrossing > other.currentPosition.x == other.velocity.x > ZERO
            }
        }
    }

    companion object {
        fun parse(s: String): HailStone {
            return s.split(" @ ").let { (first, second) ->
                val currentPosition =
                    first.split(", ").let { (x, y, z) -> DecimalPosition(x.toLong(), y.toLong(), z.toLong()) }
                val velocity = second.split(", ").let { (x, y, z) -> DecimalPosition(x.toInt(), y.toInt(), z.toInt()) }
                HailStone(currentPosition, velocity)
            }
        }
    }
}
