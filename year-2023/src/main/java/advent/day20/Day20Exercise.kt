package advent.day20

import advent.getInput
import util.lcm
import util.second

fun main() {
    val stateMachine = ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>()
    val finalDestination = DestinationModule(stateMachine)
    val modules = mutableMapOf<String, CommunicationModule>(
        "rx" to finalDestination
    )
    val sources = getInput(20).map { it.split(" -> ") }.map { it.first() to it.second() }
    sources.map { CommunicationModule.parse(it, modules, stateMachine, sources) }
        .forEach { modules[it.id] = it }
    InitialModule(modules, stateMachine).also { modules[it.id] = it }
    println(part1(modules, stateMachine))
    println(part2(modules, stateMachine, finalDestination))
}

private fun part1(
    modules: MutableMap<String, CommunicationModule>,
    stateMachine: ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>
): Long {
    repeat(1000) {
        modules.getValue("first").emit(Pulse.LOW)
        while (stateMachine.isNotEmpty()) {
            val (source, destination, pulse) = stateMachine.removeFirst()
            destination.receive(pulse, source)
        }
    }
    return modules.values.fold(0L to 0L) { acc, module ->
        (acc.first + module.sentPulses(Pulse.LOW)) to (acc.second + module.sentPulses(Pulse.HIGH))
    }.let { (low, high) -> low * high }
}

private fun part2(
    modules: MutableMap<String, CommunicationModule>,
    stateMachine: ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>,
    finalDestination: DestinationModule
): Long {
    modules.values.forEach(CommunicationModule::reset)
    val nodeBeforeFinalDestination = modules.values.first { it.hasDestination(finalDestination.id) }
    val nodesToTriggerNodeBeforeFinalDestination = modules.values.filter { it.hasDestination(nodeBeforeFinalDestination.id) }
    val timesToEmitHighSignals = mutableMapOf<CommunicationModule, Long>()
    var index = 0L
    while (nodesToTriggerNodeBeforeFinalDestination.any { it !in timesToEmitHighSignals }) {
        index++
        modules.getValue("first").emit(Pulse.LOW)
        while (stateMachine.isNotEmpty()) {
            val (source, destination, pulse) = stateMachine.removeFirst()
            destination.receive(pulse, source)
            if (source in nodesToTriggerNodeBeforeFinalDestination && pulse == Pulse.HIGH) {
                timesToEmitHighSignals[source] = timesToEmitHighSignals[source] ?: index
            }
        }
    }
    return timesToEmitHighSignals.values.reduce { acc, l -> acc lcm l }
    // Solution that will take forever:
    modules.values.forEach(CommunicationModule::reset)
    // If fast solution is removed, use the following line instead.
    // var index = 0L
    index = 0L
    while (!finalDestination.hasReceivedLowPulse) {
        modules.getValue("first").emit(Pulse.LOW)
        while (stateMachine.isNotEmpty()) {
            val (source, destination, pulse) = stateMachine.removeFirst()
            destination.receive(pulse, source)
        }
        index++
    }
    return index
}
