package advent.day20

enum class Pulse {
    LOW,
    HIGH
}

sealed class CommunicationModule(
    val id: String,
    private val destinations: List<() -> CommunicationModule>,
    private val stateMachine: ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>,
) {
    private var highPulsesSent: Long = 0L
    private var lowPulsesSent: Long = 0L

    abstract fun receive(pulse: Pulse, source: CommunicationModule)

    fun hasDestination(id: String) = destinations.any { it().id  == id }

    open fun reset() {
        highPulsesSent = 0L
        lowPulsesSent = 0L
    }

    fun emit(pulse: Pulse) {
        destinations.forEach {
            stateMachine.addLast(Triple(this, it(), pulse))
            when (pulse) {
                Pulse.HIGH -> highPulsesSent++
                Pulse.LOW -> lowPulsesSent++
            }
        }
    }

    fun sentPulses(pulse: Pulse): Long {
        return when (pulse) {
            Pulse.HIGH -> highPulsesSent
            Pulse.LOW -> lowPulsesSent
        }
    }

    companion object {

        fun parse(
            s: Pair<String, String>,
            modules: MutableMap<String, CommunicationModule>,
            stateMachine: ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>,
            sources: List<Pair<String, String>>
        ): CommunicationModule {
            val (origin, destinationsString) = s
            val destinations: List<() -> CommunicationModule> = destinationsString.split(", ").map {
                { modules.getValue(it) }
            }
            return when (origin.first()) {
                '%' -> FlipFlopModule(
                    id = origin.drop(1),
                    destinations = destinations,
                    stateMachine = stateMachine
                )
                '&' -> ConjunctionModule(
                    id = origin.drop(1),
                    destinations = destinations,
                    stateMachine = stateMachine,
                    lastReceivedPulses = sources.filter { origin.drop(1) in it.second }
                        .associate { it.first.substringAfter('&').substringAfter('%') to Pulse.LOW }
                        .toMutableMap()
                )
                else -> BroadcasterModule(
                    destinations = destinations,
                    stateMachine = stateMachine,
                )
            }
        }
    }
}

class FlipFlopModule(
    id: String,
    destinations: List<() -> CommunicationModule>,
    stateMachine: ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>,
) : CommunicationModule(id, destinations, stateMachine) {

    private var isOn: Boolean = false

    override fun reset() {
        super.reset()
        isOn = false
    }

    override fun receive(pulse: Pulse, source: CommunicationModule) {
        if (pulse == Pulse.LOW) {
            isOn = !isOn
            if (isOn) {
                emit(Pulse.HIGH)
            } else {
                emit(Pulse.LOW)
            }
        }
    }
}

class ConjunctionModule(
    id: String,
    destinations: List<() -> CommunicationModule>,
    stateMachine: ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>,
    private val lastReceivedPulses: MutableMap<String, Pulse>,
) : CommunicationModule(id, destinations, stateMachine) {

    override fun receive(pulse: Pulse, source: CommunicationModule) {
        lastReceivedPulses[source.id] = pulse
        if (lastReceivedPulses.values.all { it == Pulse.HIGH }) {
            emit(Pulse.LOW)
        } else {
            emit(Pulse.HIGH)
        }
    }

    override fun reset() {
        super.reset()
        lastReceivedPulses.keys.forEach {
            lastReceivedPulses[it] = Pulse.LOW
        }
    }
}

class BroadcasterModule(
    destinations: List<() -> CommunicationModule>,
    stateMachine: ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>,
) : CommunicationModule("broadcaster", destinations, stateMachine) {

    override fun receive(pulse: Pulse, source: CommunicationModule) {
        emit(pulse)
    }
}

class InitialModule(
    modules: Map<String, CommunicationModule>,
    stateMachine: ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>,
) : CommunicationModule("first", listOf { modules.getValue("broadcaster") }, stateMachine) {

    override fun receive(pulse: Pulse, source: CommunicationModule) {
        emit(pulse)
    }
}

class DestinationModule(
    stateMachine: ArrayDeque<Triple<CommunicationModule, CommunicationModule, Pulse>>,
) : CommunicationModule("rx", emptyList(), stateMachine) {

// All this logic is not necessary for the fast solution, but I'm keeping it for my original slow one.
    var hasReceivedLowPulse = false

    override fun receive(pulse: Pulse, source: CommunicationModule) {
        hasReceivedLowPulse = hasReceivedLowPulse || pulse == Pulse.LOW
    }

    override fun reset() {
        hasReceivedLowPulse = false
    }
}