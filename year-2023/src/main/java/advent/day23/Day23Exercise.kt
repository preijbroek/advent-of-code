package advent.day23

import advent.getInput
import util.toCharMap

fun main() {
    val hikingTrails = getInput(23).toCharMap().filterValues { it != '#' }.let(::HikingTrails)
    println(part1(hikingTrails))
    println(part2(hikingTrails))
}

private fun part1(hikingTrails: HikingTrails): Int {
    return hikingTrails.longestTrailLength(ignoreSlopes = false)
}

private fun part2(hikingTrails: HikingTrails): Int {
    return hikingTrails.longestTrailLength(ignoreSlopes = true)
}
