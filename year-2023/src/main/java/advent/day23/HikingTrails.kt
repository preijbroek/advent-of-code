package advent.day23

import util.Movement
import util.Position

data class HikingTrails(
    private val trails: Map<Position, Char>,
) {
    private val start = trails.keys.minBy(Position::y)
    private val end = trails.keys.maxBy(Position::y)

    private fun slipperyCrossings() = crossings(ignoreSlopes = false)
    private fun dryCrossings() = crossings(ignoreSlopes = true)

    private fun crossings(ignoreSlopes: Boolean): Map<Position, List<Pair<Position, Int>>> {
        val crossings = trails.filterKeys { position ->
            position == start || position.neighbours().count { neighbour -> neighbour in trails } > 2
        }.keys
        return crossings.associate { position ->
            position.neighbours().filter { neighbour -> neighbour in trails }
                .mapNotNull { neighbour -> crossRoad(position, neighbour, ignoreSlopes) }
                .let { list -> position to list }
        }
    }

    private fun crossRoad(
        startingPoint: Position,
        actualStartingPoint: Position,
        ignoreSlopes: Boolean
    ): Pair<Position, Int>? {
        var previousTrails = listOf(HikingTrail(setOf(startingPoint), startingPoint))
        var lastTrails = listOf(HikingTrail(setOf(startingPoint, actualStartingPoint), actualStartingPoint))
        while (lastTrails.size == 1) {
            previousTrails = lastTrails
            lastTrails = lastTrails.flatMap { it.next(trails, ignoreSlopes = ignoreSlopes) }
        }
        val destination = previousTrails.first()
        return if (lastTrails.isEmpty() && destination.currentPosition != start && destination.currentPosition != end) {
            null
        } else {
            destination.currentPosition to destination.length()
        }
    }

    fun longestTrailLength(ignoreSlopes: Boolean): Int {
        val crossings = if (ignoreSlopes) dryCrossings() else slipperyCrossings()
        val lastCrossing = crossings.entries.first { (key, _) ->
            end in crossings.getValue(key).map(Pair<Position, Int>::first)
        }.let { (key, value) -> key to value.first { it.first == end } }
        return if (ignoreSlopes) dryCrossings().longestTrailLength(lastCrossing)
        else slipperyCrossings().longestTrailLength(lastCrossing)
    }

    private fun Map<Position, List<Pair<Position, Int>>>.longestTrailLength(
        lastCrossing: Pair<Position, Pair<Position, Int>>,
        current: Position = start,
        visited: MutableSet<Position> = mutableSetOf(),
        currentDistance: Int = 0,
    ): Int {
        return if (current == lastCrossing.first) currentDistance + lastCrossing.second.second
        else {
            visited.add(current)
            val max = getValue(current)
                .filter { (neighbour) -> neighbour !in visited }
                .maxOfOrNull { (neighbour, pointDistance) ->
                    longestTrailLength(lastCrossing, neighbour, visited, currentDistance + pointDistance)
                }
                ?: 0
            max.also { visited.remove(current) }
        }
    }
}

data class HikingTrail(
    private val visited: Set<Position>,
    val currentPosition: Position
) {

    operator fun contains(position: Position) = position in visited

    fun next(trails: Map<Position, Char>, ignoreSlopes: Boolean) =
        nextTiles(trails, ignoreSlopes).map { HikingTrail(visited + it, it) }

    private fun nextTiles(trails: Map<Position, Char>, ignoreSlopes: Boolean): List<Position> {
        return if (ignoreSlopes) {
            currentPosition.neighbours().filter { it in trails && it !in visited }
        } else {
            when (trails.getValue(currentPosition)) {
                'v' -> listOf(currentPosition.to(Movement.DOWN))
                '>' -> listOf(currentPosition.to(Movement.RIGHT))
                '^' -> listOf(currentPosition.to(Movement.UP))
                '<' -> listOf(currentPosition.to(Movement.LEFT))
                else -> currentPosition.neighbours().filter { it in trails }
            }.filter { it !in visited }
        }
    }

    fun length() = visited.size - 1
}
