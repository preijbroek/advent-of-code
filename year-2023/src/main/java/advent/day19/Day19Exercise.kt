package advent.day19

import advent.getInputBlocks
import util.Box
import util.Position
import util.asBox
import util.box

fun main() {
    val workflowsMap = mutableMapOf<String, Workflow>(
        "A" to TerminatingWorkflow("A"),
        "R" to TerminatingWorkflow("R"),
    )
    val positions = getInputBlocks(19).let { (workflows, positions) ->
        workflows.lines().map { Workflow.parse(it, workflowsMap) }
            .forEach { workflowsMap[it.id] = it }
        positions.lines().map(::parse).map(Position::asBox)
    }
    println(part1(workflowsMap, positions))
    println(part2(workflowsMap))
}

private fun parse(s: String): Position {
    return s.drop(1).dropLast(1).split(",").map { it.substringAfter('=') }
        .let { (x, m, a, s) -> Position(x = x.toInt(), y = m.toInt(), z = a.toInt(), w = s.toInt()) }
}

private fun part1(workflowsMap: MutableMap<String, Workflow>, boxes: List<Box>): Long {
    val firstWorkflow = workflowsMap.getValue("in")
    boxes.forEach(firstWorkflow::run)
    return workflowsMap.getValue("A").score()
}

private fun part2(workflowsMap: MutableMap<String, Workflow>): Long {
    workflowsMap.getValue("A").clear()
    workflowsMap.getValue("R").clear()
    val giantBox = Position(1, 1, 1, 1) box Position(4000, 4000, 4000, 4000)
    workflowsMap.getValue("in").run(giantBox)
    return workflowsMap.getValue("A").count()
}
