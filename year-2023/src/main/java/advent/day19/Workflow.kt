package advent.day19

import util.Box

sealed class Workflow(
    val id: String,
) {

    abstract fun run(box: Box, index: Int = 0)

    abstract fun score(): Long

    abstract fun count(): Long

    abstract fun clear()

    companion object {
        fun parse(s: String, map: Map<String, Workflow>): Workflow {
            val id = s.substringBefore('{')
            val instructions = s.substringAfter('{').dropLast(1).split(",").map { it.toRule(map) }
            return IntermediateWorkflow(id, instructions)
        }

        private fun String.toRule(map: Map<String, Workflow>): Pair<(Box) -> List<Box?>, () -> Workflow> {
            return when {
                ':' in this -> {
                    { box: Box -> box.split(this, includeLeft = '>' in this) } to { map.getValue(extractDestination()) }
                }
                else -> {
                    { box: Box -> listOf(box, null) } to { map.getValue(this) }
                }
            }
        }

        private fun Box.split(s: String, includeLeft: Boolean) =
            when (val field = s.substringBefore('<').substringBefore('>')) {
                "x" -> split(x = s.extractLong(), includeLeft = includeLeft)
                "m" -> split(y = s.extractLong(), includeLeft = includeLeft)
                "a" -> split(z = s.extractLong(), includeLeft = includeLeft)
                "s" -> split(w = s.extractLong(), includeLeft = includeLeft)
                else -> throw IllegalStateException(field)
            }.let { if (includeLeft) it.reversed() else it }

        private fun String.extractLong() = substringAfter('<')
            .substringAfter('>')
            .substringBefore(':')
            .toLong()

        private fun String.extractDestination() = substringAfter(':')
    }
}

class IntermediateWorkflow(
    id: String,
    private val rules: List<Pair<(Box) -> List<Box?>, () -> Workflow>>,
) : Workflow(id) {

    override fun run(box: Box, index: Int) {
        val (splitter, travel) = rules[index]
        val (traveller, keeper) = splitter(box)
        traveller?.run { travel().run(box = this) }
        keeper?.run { run(box = this, index = index + 1) }
    }

    override fun score(): Long = 0L

    override fun count(): Long = 0L

    override fun clear() {}
}

class TerminatingWorkflow(
    id: String,
) : Workflow(id) {

    private val store = mutableListOf<Box>()

    override fun run(box: Box, index: Int) {
        store.add(box)
    }

    override fun score(): Long = store.sumOf { it.distanceToCentre() }

    override fun count(): Long = store.sumOf(Box::size)

    override fun clear() = store.clear()
}