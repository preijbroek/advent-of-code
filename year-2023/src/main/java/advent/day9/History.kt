package advent.day9

data class History(
    val values: List<Int>,
) {

    fun predictedNextValue(): Int {
        return if (all0) return 0
        else differences().predictedNextValue() + values.last()
    }

    fun predictedPreviousValue(): Int {
        return if (all0) return 0
        else values.first() - differences().predictedPreviousValue()
    }

    private val all0 = values.all { it == 0 }

    private fun differences() = values.windowed(size = 2).map { (first, second) -> second - first }.let(::History)
}
