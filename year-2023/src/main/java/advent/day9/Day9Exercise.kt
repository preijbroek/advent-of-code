package advent.day9

import advent.getInput

fun main() {
    val histories = getInput(9).map { it.split(" ").map(String::toInt).let(::History) }
    println(part1(histories))
    println(part2(histories))
}

private fun part1(histories: List<History>): Int {
    return histories.sumOf(History::predictedNextValue)
}

private fun part2(histories: List<History>): Int {
    return histories.sumOf(History::predictedPreviousValue)
}
