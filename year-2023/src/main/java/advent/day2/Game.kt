package advent.day2

import util.second

data class Game(
    val id: Int,
    val rounds: List<Round>,
) {

    companion object {
        fun parse(s: String): Game {
            val idAndRounds = s.split(": ")
            val id = idAndRounds.first().substringAfter("Game ").toInt()
            val rounds = idAndRounds.second().split("; ")
                .map { it.split(", ") }
                .map {
                    Round(
                        red = it.cubes("red"),
                        green = it.cubes("green"),
                        blue = it.cubes("blue"),
                    )
                }
            return Game(id, rounds)
        }

        private fun List<String>.cubes(colour: String) = find { it.endsWith(colour) }
            ?.substringBefore(" ")?.toInt() ?: 0
    }
}

fun Game.minimumRound() = Round(
    red = rounds.maxOf(Round::red),
    green = rounds.maxOf(Round::green),
    blue = rounds.maxOf(Round::blue),
)

data class Round(
    val red: Int,
    val green: Int,
    val blue: Int,
)

operator fun Round.contains(round: Round) = red >= round.red && green >= round.green && blue >= round.blue

fun Round.power() = red * green * blue

