package advent.day2

import advent.getInput

fun main() {
    val games = getInput(2).map(Game::parse)
    println(part1(games))
    println(part2(games))
}

private fun part1(games: List<Game>): Int {
    val minimumRound = Round(red = 12, green = 13, blue = 14)
    return games.filter { it.minimumRound() in minimumRound }.sumOf(Game::id)
}

private fun part2(games: List<Game>): Int {
    return games.map(Game::minimumRound).sumOf(Round::power)
}
