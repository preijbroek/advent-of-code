package advent.day5

import advent.getInputBlocks
import util.second

fun main() {
    val inputBlocks = getInputBlocks(5)
    val seeds = inputBlocks.first().substringAfter("seeds: ").split(" ").map(String::toLong)
    val resourceMaps = inputBlocks.drop(1).map {
        it.split("\n").drop(1).associate { rangeString ->
            rangeString.split(" ").map(String::toLong).let { (destinationStart, sourceStart, length) ->
                (sourceStart ..< sourceStart + length) to (destinationStart ..< destinationStart + length)
            }
        }
    }
    println(part1(seeds, resourceMaps))
    println(part2(seeds, resourceMaps))
}

private fun part1(seeds: List<Long>, resourceMaps: List<Map<LongRange, LongRange>>): Long {
    return seeds.minOf { seed ->
        resourceMaps.fold(seed) { acc, map ->
            map.entries.firstOrNull { acc in it.key }
                ?.let { (sourceRange, destinationRange) -> destinationRange.first + (acc - sourceRange.first) }
                ?: acc
        }
    }
}

private fun part2(seeds: List<Long>, resourceMaps: List<Map<LongRange, LongRange>>): Long {
    return seeds.chunked(size = 2).map {
        it.first() ..< it.first() + it.second()
    }.flatMap { seedsRange ->
        resourceMaps.fold(listOf(seedsRange)) { acc, map ->
            acc.flatMap { range -> range.destinationRanges(map) }
        }
    }.minOf(LongRange::first)
}

private fun LongRange.destinationRanges(map: Map<LongRange, LongRange>): List<LongRange> {
    val mappedRanges = mutableListOf<LongRange>()
    val mappedOutputRanges = map.entries.mapNotNull { (sourceRange, destinationRange) ->
        val start = maxOf(first, sourceRange.first)
        val end = minOf(last, sourceRange.last)
        (start..end).takeUnless(LongRange::isEmpty)
            ?.let {
                mappedRanges += it
                val mappingConstant = destinationRange.first - sourceRange.first
                (start + mappingConstant)..(end + mappingConstant)
            }
    }
    val unmappedOutputRanges = (listOf(first) +
            mappedRanges.sortedBy(LongRange::first).flatMap { listOf(it.first, it.last) } +
            listOf(last))
        .chunked(size = 2)
        .mapNotNull { (first, second) ->
            if (first < second)
                if (second == last) first..second
                else first..<second
            else null
        }
    return mappedOutputRanges + unmappedOutputRanges
}
