package advent.day22

import advent.getInput
import util.Position
import util.plus

fun main() {
    val bricks = getInput(22).map { it.split("~") }
        .map { (start, end) -> Brick(start.toPosition(), end.toPosition()) }
    println(part1(bricks))
    println(part2(bricks))
}

private fun part1(bricks: List<Brick>): Int {
    val (stableBricks, unsafeToDisintegrateBricks) = bricks.stableAndSupportingBricks()
    return stableBricks.size - unsafeToDisintegrateBricks.size
}

private fun part2(bricks: List<Brick>): Int {
    val (stableBricks, unsafeToDisintegrateBricks) = bricks.stableAndSupportingBricks()
    return unsafeToDisintegrateBricks.sumOf { supportingBrick ->
        val nextBricks = stableBricks - supportingBrick
        nextBricks.stableAndSupportingBricks().third
    }
}

private fun String.toPosition() = split(",").let { (x, y, z) -> Position(x.toInt(), y.toInt(), z.toInt()) }

private fun Collection<Brick>.stableAndSupportingBricks(): Triple<Set<Brick>, Set<Brick>, Int> {
    val stableBricks = filter(Brick::isOnGround).toMutableSet()
    val floatingBricks = ArrayDeque((this - stableBricks).sortedBy(Brick::lowestPoint))
    val stablePositions = stableBricks.flatMap(Brick::range).toMutableSet()
    var droppedBricks = 0
    while (floatingBricks.isNotEmpty()) {
        val droppingBrick = floatingBricks.removeFirst()
        val stableBrick = droppingBrick.drop(stablePositions)
        droppedBricks += (droppingBrick != stableBrick)
        stablePositions.addAll(stableBrick.range)
        stableBricks.add(stableBrick)
    }
    val bricksMap = stableBricks.flatMap { it.range.map { position -> position to it } }.toMap()
    val supportingBricksSets = stableBricks.map { it.findSupportedByIn(bricksMap) }
    return Triple(stableBricks, supportingBricksSets.filter { it.size == 1 }.flatten().toSet(), droppedBricks)
}

