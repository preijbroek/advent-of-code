package advent.day22

import util.Position

data class Brick(
    private val start: Position,
    private val end: Position,
) {

    val range = start..end

    val lowestPoint = minOf(start.z, end.z)

    val isOnGround = lowestPoint == 1

    fun drop(stablePositions: Set<Position>): Brick {
        var newBrick = this
        while (!newBrick.isOnGround && newBrick.dropOne().range.none { it in stablePositions })  {
            newBrick = newBrick.dropOne()
        }
        return newBrick
    }

    fun findSupportedByIn(bricksMap: Map<Position, Brick>): Set<Brick> {
        return dropOne().range.mapNotNull(bricksMap::get).filter { it !== this }.toSet()
    }

    private fun dropOne() = Brick(start.minZ(), end.minZ())
}
