package advent.day6

import advent.getInput
import util.productOf
import util.second
import util.toDecimal

fun main() {
    val input = getInput(6).map { it.split(" +".toRegex()).drop(1) }
    val races = (input.first() zip input.second()).map { (time, distance) -> Race(time.toDecimal(), distance.toDecimal()) }
    println(part1(races))
    println(part2(races))
}

private fun part1(races: List<Race>): Long {
    return races.productOf(Race::waysToBeatRecord)
}

private fun part2(races: List<Race>): Long {
    return races.reduce(Race::combine).waysToBeatRecord()
}
