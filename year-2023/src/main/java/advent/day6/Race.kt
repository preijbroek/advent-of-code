package advent.day6

import util.Decimal
import util.Decimal.Companion.ONE
import util.Decimal.Companion.TWO
import util.ceilUp
import util.div
import util.minus
import util.plus
import util.pow
import util.sqrt
import util.times
import util.toDecimal

data class Race(
    val time: Decimal,
    val distance: Decimal,
) {

    fun waysToBeatRecord(): Long {
        val firstWin = ((time - abcFactor()) / TWO).ceilUp()
        val lastWin = time - firstWin
        return (lastWin - firstWin + ONE).toLong()
    }

    private fun abcFactor(): Decimal = (time.pow(2) - (4 * distance)).sqrt()

    fun combine(other: Race): Race {
        val newTime = (time.integerString() + other.time.integerString()).toDecimal()
        val newDistance = (distance.integerString() + other.distance.integerString()).toDecimal()
        return Race(newTime, newDistance)
    }
}