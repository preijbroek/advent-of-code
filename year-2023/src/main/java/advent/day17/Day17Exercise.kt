package advent.day17

import advent.getInput
import util.toCharMap

fun main() {
    val city = getInput(17).toCharMap().mapValues { it.value.digitToInt() }.let(::City)
    println(part1(city))
    println(part2(city))
}

private fun part1(city: City): Int {
    return city.travel(useUltraCrucibles = false)
}

private fun part2(city: City): Int {
    return city.travel(useUltraCrucibles = true)
}
