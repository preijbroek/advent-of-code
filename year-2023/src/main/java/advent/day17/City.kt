package advent.day17

import util.Movement
import util.Position
import util.Position.Companion.byX
import util.Position.Companion.byY

data class City(
    private val blocks: Map<Position, Int>,
) {

    private val start = Position.CENTRE
    private val end = blocks.keys.maxWith(byX().thenComparing(byY()))

    fun travel(useUltraCrucibles: Boolean): Int {
        val seenTravels = mutableMapOf(
            BlockTravel(start, Movement.RIGHT, 0) to 0,
            BlockTravel(start, Movement.DOWN, 0) to 0,
        )
        var currentTravels = seenTravels.toMutableMap()
        while (currentTravels.isNotEmpty()) {
            currentTravels = currentTravels.flatMap { (blockTravel, heat) ->
                blockTravel.next(useUltraCrucibles)
                    .filter { it.position in blocks }
                    .map { it to heat + blocks.getValue(it.position) }
                    .filter { (travel, nextHeat) -> travel !in seenTravels || seenTravels.getValue(travel) > nextHeat }
            }
                .sortedByDescending { (_, nextHeat) -> nextHeat }
                .toMap().toMutableMap()
            seenTravels.putAll(currentTravels)
        }
        return seenTravels.filterKeys { it.position == end }
            .filter { (travel, _) -> !useUltraCrucibles || travel.straightLine >= 4 }
            .minOf { it.value }
    }
}

data class BlockTravel(
    val position: Position,
    val movement: Movement,
    val straightLine: Int,
) {

    fun next(useUltraCrucibles: Boolean) = if (useUltraCrucibles) {
        listOfNotNull(
            copy(position = position.to(movement), straightLine = straightLine + 1).takeIf { it.straightLine < 11 },
            copy(position = position.to(movement.left()), straightLine = 1, movement = movement.left()).takeIf { straightLine >= 4 },
            copy(position = position.to(movement.right()), straightLine = 1, movement = movement.right()).takeIf { straightLine >= 4 },
        )
    } else {
        listOfNotNull(
            copy(position = position.to(movement), straightLine = straightLine + 1).takeIf { it.straightLine < 4 },
            copy(position = position.to(movement.left()), straightLine = 1, movement = movement.left()),
            copy(position = position.to(movement.right()), straightLine = 1, movement = movement.right()),
        )
    }
}
