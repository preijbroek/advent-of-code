package advent.day25

import util.triple
import kotlin.random.Random

typealias WireConnection = Pair<String, String>

class WireGraph(
    private val verticesCount: Int,
    private val edgeCount: Int,
    private val connections: Array<WireConnection>,
) {

    val size: Int = verticesCount

    fun cut(): Triple<Int, Int, Int> {
        val subGraphs = connections.flatMap(WireConnection::toList)
            .associateWith { SubGraph(it, rank = 0) }
            .toMutableMap()
        var currentVertices = verticesCount
        while (currentVertices > 2) {
            val i = Random.nextInt(from = 0, until = edgeCount)
            val subGraph1 = subGraphs.find(connections[i].first)
            val subGraph2 = subGraphs.find(connections[i].second)
            if (subGraph1 != subGraph2) {
                currentVertices--
                subGraphs.contract(subGraph1, subGraph2)
            }
        }
        val cuts = (0..<edgeCount).count {
            connections[it].let { (source, destination) -> subGraphs.find(source) != subGraphs.find(destination) }
        }
        val (subGraph1Size, subGraph2Size) = subGraphs.values.groupBy { subGraphs.find(it.parent) }
            .values.map(List<SubGraph>::size)
        return cuts triple (subGraph1Size to subGraph2Size)
    }

    private fun MutableMap<String, SubGraph>.find(vertex: String): String {
        if (getValue(vertex).parent != vertex) {
            getValue(vertex).parent = find(getValue(vertex).parent)
        }
        return getValue(vertex).parent
    }

    private fun MutableMap<String, SubGraph>.contract(subGraph1: String, subGraph2: String) {
        val xRoot = find(subGraph1)
        val yRoot = find(subGraph2)
        if (getValue(xRoot).rank < getValue(yRoot).rank) {
            getValue(xRoot).parent = yRoot
        } else if (getValue(xRoot).rank > getValue(yRoot).rank) {
            getValue(yRoot).parent = xRoot
        } else {
            getValue(yRoot).parent = xRoot
            getValue(xRoot).rank++
        }
    }
}

private class SubGraph(
    var parent: String,
    var rank: Int,
)
