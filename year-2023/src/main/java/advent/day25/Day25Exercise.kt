package advent.day25

import advent.getInput

fun main() {
    val verticesCount = getInput(25).flatMap { it.split(":? ".toRegex()) }.toSet().size
    val edges = getInput(25).map { it.split(":? ".toRegex()) }.flatMap { line ->
        line.drop(1).map { WireConnection(line.first(), it) }
    }.toTypedArray()
    val edgesCount = edges.size
    val graph = WireGraph(verticesCount, edgesCount, edges)
    println(part1(graph))
}

private fun part1(graph: WireGraph): Int {
    while (true) {
        val (cuts, subGraph1Size, subGraph2Size) = graph.cut()
        if (cuts == 3) return subGraph1Size * subGraph2Size
    }
}
