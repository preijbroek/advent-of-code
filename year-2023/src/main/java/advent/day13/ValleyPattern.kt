package advent.day13

data class ValleyPattern(private val s: String) {

    private val rows = s.lines()
    private val columns = rows.first().indices.map { index ->
        rows.joinToString(separator = "") { it[index].toString() }
    }

    fun reflectionScore(withSmudges: Boolean): Int {
        return columns.reflectionScore(withSmudges) + 100 * rows.reflectionScore(withSmudges)
    }

    private fun List<String>.reflectionScore(withSmudges: Boolean): Int {
        return indices.firstOrNull {
            foldAt(it).isMirror(withSmudges)
        } ?: 0
    }

    private fun List<String>.foldAt(index: Int): Pair<List<String>, List<String>> {
        val left = take(index)
        val right = drop(index)
        return when {
            left.size == right.size -> left to right
            left.size < right.size -> left to right.take(left.size)
            else -> left.takeLast(right.size) to right
        }
    }

    private fun Pair<List<String>, List<String>>.isMirror(withSmudges: Boolean): Boolean {
        val (left, right) = let { (left, right) -> left to right.reversed() }
        return left.isNotEmpty() && right.isNotEmpty() && if (withSmudges) {
            left.indices.sumOf {
                smudges(left[it], right[it])
            } == 1
        } else {
            left == right
        }
    }

    private fun smudges(s1: String, s2: String): Int {
        return s1.indices.count { s1[it] != s2[it] }
    }
}
