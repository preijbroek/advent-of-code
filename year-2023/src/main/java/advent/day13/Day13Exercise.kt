package advent.day13

import advent.getInputBlocks

fun main() {
    val valleyPatterns = getInputBlocks(13).map(::ValleyPattern)
    println(part1(valleyPatterns))
    println(part2(valleyPatterns))
}

private fun part1(valleyPatterns: List<ValleyPattern>): Int {
    return valleyPatterns.sumOf { it.reflectionScore(withSmudges = false) }
}

private fun part2(valleyPatterns: List<ValleyPattern>): Int {
    return valleyPatterns.sumOf { it.reflectionScore(withSmudges = true) }
}
