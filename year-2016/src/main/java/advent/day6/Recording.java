package advent.day6;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static util.Comparators.maxOf;
import static util.Comparators.minOf;

public final class Recording {

    private final List<String> input;

    public Recording(final List<String> input) {
        this.input = Collections.unmodifiableList(input);
    }

    public String decodeMostCommon() {
        char[] correctedMessageArray = new char[input.get(0).length()];
        for (int i = 0; i < input.get(0).length(); i++) {
            correctedMessageArray[i] = requiredCharacterAtPosition(i, false);
        }
        return new String(correctedMessageArray);
    }

    public String decodeLeastCommon() {
        char[] correctedMessageArray = new char[input.get(0).length()];
        for (int i = 0; i < input.get(0).length(); i++) {
            correctedMessageArray[i] = requiredCharacterAtPosition(i, true);
        }
        return new String(correctedMessageArray);
    }

    private char requiredCharacterAtPosition(final int position, final boolean leastFound) {
        Map<Character, Integer> charMap = new HashMap<>();
        input.forEach(value -> charMap.put(value.charAt(position), charMap.getOrDefault(value.charAt(position), 0) + 1));
        return leastFound
                ? minOf(charMap.keySet(), charMap::get)
                : maxOf(charMap.keySet(), charMap::get);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recording recording = (Recording) o;
        return Objects.equals(input, recording.input);
    }

    @Override
    public int hashCode() {
        return Objects.hash(input);
    }
}
