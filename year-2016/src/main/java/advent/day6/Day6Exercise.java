package advent.day6;

import advent.InputProvider;

import java.util.List;

public final class Day6Exercise {

    private Day6Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(6);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Recording recording = new Recording(input);
        System.out.println(recording.decodeMostCommon());
    }

    private static void part2(final List<String> input) {
        Recording recording = new Recording(input);
        System.out.println(recording.decodeLeastCommon());
    }

}
