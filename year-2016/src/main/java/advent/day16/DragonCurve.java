package advent.day16;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class DragonCurve {

    private DragonCurve() {
    }

    public static String of(final String s) {
        return s + '0' + inverseComplementOf(s);

    }

    private static String inverseComplementOf(final String s) {
        return IntStream.rangeClosed(1, s.length())
                .mapToObj(i -> s.charAt(s.length() - i))
                .map(DragonCurve::invert)
                .map(String::valueOf)
                .collect(Collectors.joining());
    }

    private static Character invert(final Character character) {
        if (character == '0') {
            return '1';
        } else if (character == '1') {
            return '0';
        } else {
            throw new IllegalArgumentException(character.toString());
        }
    }

}
