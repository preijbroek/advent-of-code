package advent.day16;

import advent.InputProvider;

public final class Day16Exercise {

    private Day16Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(16).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        Disk disk = new Disk(272);
        System.out.println(disk.checkSum(input));
    }

    private static void part2(final String input) {
        Disk disk = new Disk(35651584);
        System.out.println(disk.checkSum(input));
    }

}
