package advent.day16;

import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;

public final class Disk {

    private final int memory;

    public Disk(final int memory) {
        this.memory = memory;
    }

    public String checkSum(final String s) {
        String data = s;
        while (data.length() < memory) {
            data = DragonCurve.of(data);
        }
        return checksumOf(data);
    }

    private String checksumOf(final String data) {
        String checkSum = data.substring(0, memory);
        while ((checkSum.length() & 1) == 0) {
            checkSum = calculate(checkSum);
        }
        return checkSum;
    }

    private String calculate(final String checkSum) {
        return IntStream.range(0, checkSum.length())
                .filter(i -> (i & 1) == 0)
                .mapToObj(i -> binaryIsSame(checkSum.charAt(i), checkSum.charAt(i + 1)))
                .map(String::valueOf)
                .collect(joining());
    }

    private Character binaryIsSame(final char c1, final char c2) {
        return c1 == c2 ? '1' : '0';
    }


}
