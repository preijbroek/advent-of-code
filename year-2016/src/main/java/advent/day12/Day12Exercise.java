package advent.day12;

import advent.AssemblyComputer;
import advent.InputProvider;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Day12Exercise {

    private Day12Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(12);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Map<String, Register> registers = Stream.of("a", "b", "c", "d")
                .collect(Collectors.toMap(Function.identity(), Register::new));
        AssemblyComputer computer = new AssemblyComputer(registers);
        computer.run(input);
        System.out.println(computer.get("a").getValue());
    }

    private static void part2(final List<String> input) {
        Map<String, Register> registers = Stream.of("a", "b", "c", "d")
                .collect(Collectors.toMap(Function.identity(), Register::new));
        AssemblyComputer computer = new AssemblyComputer(registers);
        computer.set("c", 1);
        computer.run(input);
        System.out.println(computer.get("a").getValue());
    }

}
