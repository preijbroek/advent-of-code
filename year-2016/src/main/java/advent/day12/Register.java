package advent.day12;

import java.util.Objects;

public final class Register {

    private final String id;
    private int value = 0;

    public Register(final String id) {
        this.id = id;
    }

    public void copy(final Register register) {
        this.value = register.value;
    }

    public void copy(final int value) {
        this.value = value;
    }

    public void increase() {
        value++;
    }

    public void decrease() {
        value--;
    }

    public int getValue() {
        return value;
    }

    public void setValue(final int value) {
        this.value = value;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Register register = (Register) o;
        return Objects.equals(id, register.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
