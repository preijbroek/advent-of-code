package advent.day9;

import advent.InputProvider;

public final class Day9Exercise {

    private Day9Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(9).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        System.out.println(Decompressor.decompressedLength(input));
    }

    private static void part2(final String input) {
        System.out.println(Decompressor.iterativelyDecompressedLength(input));
    }

}
