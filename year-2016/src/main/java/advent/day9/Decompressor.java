package advent.day9;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public final class Decompressor {

    private Decompressor() {
    }

    public static int decompressedLength(final String s) {
        String stripped = s.replaceAll(" ", "");
        int length = 0;
        int index = 0;
        while (index < stripped.length()) {
            if (stripped.charAt(index) == '(') {
                String[] parameters = stripped.substring(index + 1, stripped.indexOf(')', index)).split("x");
                length += Arrays.stream(parameters)
                        .mapToInt(Integer::parseInt).reduce(1, (n, m) -> n * m);
                index = stripped.indexOf(')', index) + 1 + Integer.parseInt(parameters[0]);
            } else {
                index++;
                length++;
            }
        }
        return length;
    }

    public static long iterativelyDecompressedLength(final String s) {
        String stripped = s.replaceAll(" ", "");
        List<String> chars = Arrays.stream(stripped.split("")).collect(toList());
        List<Integer> charWeights = chars.stream().map(str -> 1).collect(toList());
        long weight = 0;
        int index = 0;
        while (index < chars.size()) {
            if ("(".equals(chars.get(index))) {
                int xIndex = index + 1;
                while (!"x".equals(chars.get(xIndex))) {
                    xIndex++;
                }
                int positions = Integer.parseInt(stripped.substring(index + 1, xIndex));
                int timesIndex = xIndex + 1;
                while (!")".equals(chars.get(timesIndex))) {
                    timesIndex++;
                }
                int times = Integer.parseInt(stripped.substring(xIndex + 1, timesIndex));
                for (int i = timesIndex + 1; i <= timesIndex + positions; i++) {
                    charWeights.set(i, charWeights.get(i) * times);
                }
                index = timesIndex + 1;
            } else {
                weight += charWeights.get(index);
                index++;
            }
        }
        return weight;
    }

}
