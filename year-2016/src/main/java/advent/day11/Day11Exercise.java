package advent.day11;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;

public final class Day11Exercise {

    private Day11Exercise() {
    }

    /**
     * With this exercise, to get one block and chip at the top and one block and chip one level lower takes
     * ten moves. After that, getting a different set from the bottom to the top takes another twelve moves.
     * Finally, when there is no block left at the bottom, you require one more move.
     * With different inputs, you need to find the first moves to get to the state where twelve moves are required.
     * If you have a pair at a higher level, with another pair at the second highest level, it takes four moves less.
     * */
    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(11);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        long elements = input.stream()
                .mapToLong(s -> Arrays.stream(s.split(" "))
                        .filter(split -> split.contains("microchip")).count())
                .sum();
        System.out.println(10 + (elements - 2) * 12 + 1);
    }

    private static void part2(final List<String> input) {
        long elements = input.stream()
                .mapToLong(s -> Arrays.stream(s.split(" "))
                        .filter(split -> split.contains("microchip")).count())
                .sum();
        System.out.println(10 + (elements - 2 + 2) * 12 + 1);
    }

}
