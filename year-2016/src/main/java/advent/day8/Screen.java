package advent.day8;

import java.util.Arrays;

public final class Screen {

    private final boolean[][] pixels = new boolean[6][50];

    public void applyInstruction(final String instruction) {
        if (instruction.startsWith("rect")) {
            turnOn(instruction.split(" ")[1]);
        } else if (instruction.startsWith("rotate row")) {
            rotateRow(instruction.replace("rotate row y=", ""));
        } else if (instruction.startsWith("rotate column")) {
            rotateColumn(instruction.replace("rotate column x=", ""));
        } else {
            throw new IllegalArgumentException(instruction);
        }
    }

    public int lightsOn() {
        int lightsOn = 0;
        for (boolean[] pixelRow : pixels) {
            for (boolean pixel : pixelRow) {
                if (pixel) {
                    lightsOn++;
                }
            }
        }
        return lightsOn;
    }

    private void turnOn(final String rectangle) {
        int[] pixelPositions = Arrays.stream(rectangle.split("x"))
                .mapToInt(Integer::parseInt)
                .toArray();
        for (int i = 0; i < pixelPositions[1]; i++) {
            for (int j = 0; j < pixelPositions[0]; j++) {
                pixels[i][j] = true;
            }
        }
    }

    private void rotateRow(final String rowInstruction) {
        String[] instructions = rowInstruction.split(" by ");
        int row = Integer.parseInt(instructions[0]);
        int rotation = Integer.parseInt(instructions[1]);
        int length = pixels[row].length;
        boolean[] newRow = new boolean[length];
        for (int i = 0; i < length; i++) {
            int position = (i + rotation) % length;
            newRow[position] = pixels[row][i];
        }
        pixels[row] = newRow;
    }

    private void rotateColumn(final String columnInstruction) {
        String[] instructions = columnInstruction.split(" by ");
        int column = Integer.parseInt(instructions[0]);
        int rotation = Integer.parseInt(instructions[1]);
        int length = pixels.length;
        boolean[] newColumn = new boolean[length];
        for (int i = 0; i < length; i++) {
            int position = (i + rotation) % length;
            newColumn[position] = pixels[i][column];
        }
        for (int i = 0; i < length; i++) {
            pixels[i][column] = newColumn[i];
        }
    }

    public void draw() {
        for (boolean[] pixelRow : pixels) {
            for (boolean pixel : pixelRow) {
                System.out.print(pixel ? '#' : '.');
            }
            System.out.println();
        }
        System.out.println();
    }

}
