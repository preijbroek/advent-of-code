package advent.day8;

import advent.InputProvider;

import java.util.List;

public final class Day8Exercise {

    private Day8Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(8);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Screen screen = new Screen();
        input.forEach(screen::applyInstruction);
        System.out.println(screen.lightsOn());
    }

    private static void part2(final List<String> input) {
        Screen screen = new Screen();
        input.forEach(screen::applyInstruction);
        screen.draw();
    }
}
