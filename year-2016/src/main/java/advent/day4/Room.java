package advent.day4;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.joining;

public final class Room {

    private static final int ALPHABET_LENGTH = 26;

    private final String data;

    public Room(final String data) {
        this.data = data;
    }

    public boolean isReal() {
        return dataMap().entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .sorted(Map.Entry.comparingByValue(reverseOrder()))
                .limit(5)
                .map(Map.Entry::getKey)
                .map(String::valueOf)
                .allMatch(checkSum()::contains);
    }

    public int sectorId() {
        String sectorIdString = data.substring(data.lastIndexOf('-') + 1, data.indexOf('['));
        return Integer.parseInt(sectorIdString);
    }

    private String checkSum() {
        return data.substring(data.indexOf('[') + 1, data.length() - 1);
    }

    private Map<Character, Integer> dataMap() {
        Map<Character, Integer> stringMap = new HashMap<>();
        data.substring(0, data.lastIndexOf('-'))
                .chars()
                .filter(value -> value != '-')
                .forEach(value -> stringMap.put((char) value, stringMap.getOrDefault((char) value, 0) + 1));
        return stringMap;
    }

    public String decrypt() {
        return name().chars()
                .mapToObj(c -> rotate(sectorId(), (char) c))
                .map(String::valueOf)
                .collect(joining());
    }

    private String name() {
        return data.substring(0, data.lastIndexOf('-'));
    }

    private static char rotate(final int times, final char c) {
        return c == '-' ? ' ' : ((char) ((c - 'a' + times) % ALPHABET_LENGTH + 'a'));
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return Objects.equals(data, room.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }
}
