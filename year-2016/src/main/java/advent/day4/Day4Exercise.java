package advent.day4;

import advent.InputProvider;

import java.util.List;

public final class Day4Exercise {

    private Day4Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(4);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        System.out.println(input.stream()
                .map(Room::new)
                .filter(Room::isReal)
                .mapToInt(Room::sectorId)
                .sum());
    }

    private static void part2(final List<String> input) {
        input.stream()
                .map(Room::new)
                .filter(Room::isReal)
                .filter(room -> room.decrypt().startsWith("northpole object storage"))
                .map(Room::sectorId)
                .forEach(System.out::println);
    }

}
