package advent.day19;

import advent.InputProvider;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.stream.IntStream;

public final class Day19Exercise {

    private Day19Exercise() {
    }

    public static void main(String[] args) {
        int input = Integer.parseInt(InputProvider.getInput(19).get(0));
        part1(input);
        part2(input);
    }

    private static void part1(final int input) {
        System.out.println(2 * (input - Integer.highestOneBit(input)) + 1);
    }

    private static void part2(final int input) {
        Deque<Integer> left = new ArrayDeque<>();
        Deque<Integer> right = new ArrayDeque<>();
        IntStream.rangeClosed(1, input)
                .forEach(i -> {
                    if ((i < (input >> 1) + 1)) {
                        left.add(i);
                    } else {
                        right.addFirst(i);
                    }
                });
        while (!left.isEmpty() && !right.isEmpty()) {
            if (left.size() > right.size()) {
                left.pollLast();
            } else {
                right.pollLast();
            }
            right.addFirst(left.poll());
            left.add(right.pollLast());
        }
        left.addAll(right);
        left.forEach(System.out::println);
    }

}
