package advent.day18;

import java.util.stream.IntStream;

public final class TrapCalculator {

    private TrapCalculator() {
    }

    public static int countSafeTiles(final String firstRow, final int rows) {
        return count(firstRow, rows, false);
    }

    public static int countTraps(final String firstRow, final int rows) {
        return count(firstRow, rows, true);
    }

    private static int count(final String firstRow, final int rows, final boolean countTraps) {
        int tiles = 0;
        boolean[] currentRow = fromString(firstRow);
        for (int i = 0; i < rows; i++) {
            tiles += count(currentRow, countTraps);
            currentRow = nextRow(currentRow);
        }
        return tiles;
    }

    private static boolean[] fromString(final String firstRow) {
        boolean[] row = new boolean[firstRow.length()];
        IntStream.range(0, firstRow.length())
                .forEach(i -> row[i] = (firstRow.charAt(i) == '^'));
        return row;
    }

    private static long count(final boolean[] row, final boolean wantTrap) {
        return IntStream.range(0, row.length)
                .filter(i -> row[i] == wantTrap)
                .count();
    }

    private static boolean[] nextRow(final boolean[] currentRow) {
        boolean[] nextRow = new boolean[currentRow.length];
        for (int i = 0; i < currentRow.length; i++) {
            if (i == 0) {
                nextRow[i] = currentRow[i + 1];
            } else if (i == currentRow.length - 1) {
                nextRow[i] = currentRow[i - 1];
            } else {
                nextRow[i] = currentRow[i - 1] ^ currentRow[i + 1];
            }
        }
        return nextRow;
    }

}
