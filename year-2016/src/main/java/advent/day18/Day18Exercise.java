package advent.day18;

import advent.InputProvider;

public final class Day18Exercise {

    private Day18Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(18).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        System.out.println(TrapCalculator.countSafeTiles(input, 40));
    }

    private static void part2(final String input) {
        System.out.println(TrapCalculator.countSafeTiles(input, 400_000));
    }
}
