package advent.day14;

import advent.InputProvider;
import advent.MD5Hasher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Day14Exercise {

    private Day14Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(14).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        runHashes(input, 1);
    }

    private static void part2(final String input) {
        runHashes(input, 2017);
    }

    private static void runHashes(final String input, final int hashIterations) {
        MD5Hasher hasher = new MD5Hasher(input);
        List<String> calculatedHashes = IntStream.range(0, 1000)
                .mapToObj(index -> hasher.hash(index, hashIterations))
                .collect(Collectors.toCollection(ArrayList::new));
        Map<Integer, Character> threesInARow = IntStream.range(0, calculatedHashes.size())
                .filter(i -> threeInARowOf(calculatedHashes.get(i)) != null)
                .boxed()
                .collect(Collectors.toMap(Function.identity(), i -> threeInARowOf(calculatedHashes.get(i))));
        Map<Integer, String> fivesInARow = IntStream.range(0, calculatedHashes.size())
                .filter(i -> hasFiveInARow(calculatedHashes.get(i)))
                .boxed()
                .collect(Collectors.toMap(Function.identity(), calculatedHashes::get));
        int lastKeyIndex = 0;
        int keysFound = 0;
        int index = 0;
        while (keysFound < 64) {
            String hash = hasher.hash(index + 1000, hashIterations);
            calculatedHashes.add(hash);
            Character c = threeInARowOf(hash);
            if (c != null) {
                threesInARow.put(index + 1000, c);
                if (hasFiveInARow(hash)) {
                    fivesInARow.put(index + 1000, hash);
                }
            }
            Character currentHashChar = threesInARow.get(index);
            if (currentHashChar != null) {
                if (IntStream.rangeClosed(index + 1, index + 1000)
                        .filter(fivesInARow::containsKey)
                        .anyMatch(i -> hasFiveInARowOf(currentHashChar, calculatedHashes.get(i)))) {
                    lastKeyIndex = index;
                    keysFound++;
                }
            }
            index++;
        }
        System.out.println(lastKeyIndex);
    }

    private static boolean hasFiveInARow(final String hash) {
        return IntStream.range(0, hash.length() - 4)
                .anyMatch(i -> areAll(hash.charAt(i), hash, i, 5));
    }

    private static Character threeInARowOf(final String hash) {
        return IntStream.range(0, hash.length() - 2)
                .filter(i -> areAll(hash.charAt(i), hash, i, 3))
                .mapToObj(hash::charAt)
                .findFirst()
                .orElse(null);
    }

    private static boolean hasFiveInARowOf(final Character c, final String hash) {
        return IntStream.range(0, hash.length() - 4)
                .anyMatch(i -> areAll(c, hash, i, 5));
    }

    private static boolean areAll(final Character c, final String hash, final int i, final int number) {
        return IntStream.range(i, i + number)
                .mapToObj(hash::charAt)
                .allMatch(hashChar -> hashChar == c);
    }

}
