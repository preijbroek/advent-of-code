package advent.day5;

import advent.InputProvider;
import jakarta.xml.bind.DatatypeConverter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class Day5Exercise {

    private Day5Exercise() {
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        String input = InputProvider.getInput(5).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) throws NoSuchAlgorithmException {
        StringBuilder result = new StringBuilder();
        int index = 0;
        for (int i = 0; i < 8; i++) {
            String hash = "";
            while(!hash.startsWith("00000")) {
                hash = md5Hash(input, index);
                index++;
            }
            result.append(hash.charAt(5));
            index++;
        }
        System.out.println(result);
    }

    private static void part2(final String input) throws NoSuchAlgorithmException {
        char[] result = new char[8];
        boolean[] filled = new boolean[8];
        int index = 0;
        while (IntStream.range(0, filled.length).mapToObj(i -> filled[i]).anyMatch(bool -> !bool)) {
            String hash = "";
            while(!hash.startsWith("00000")) {
                hash = md5Hash(input, index);
                index++;
            }
            String finalVarForHash = hash;
            if (Stream.of('0', '1', '2', '3', '4', '5', '6', '7').anyMatch(value -> value == finalVarForHash.charAt(5))) {
                int position = Integer.parseInt(hash.substring(5, 6));
                if (!filled[position]) {
                    result[position] = hash.charAt(6);
                }
                filled[position] = true;
                index++;
            }
        }
        System.out.println(result);
    }

    private static String md5Hash(final String code, final int index) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        String input = code + index;
        md5.update(input.getBytes());
        return DatatypeConverter.printHexBinary(md5.digest());
    }

}
