package advent.day23;

import advent.InputProvider;

import java.util.List;

public final class Day23Exercise {

    private Day23Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(23);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        long firstFactor = Long.parseLong(input.get(19).split(" ")[1]);
        long secondFactor = Long.parseLong(input.get(20).split(" ")[1]);
        System.out.println(factorial(7) + firstFactor * secondFactor);
    }

    private static void part2(final List<String> input) {
        long firstFactor = Long.parseLong(input.get(19).split(" ")[1]);
        long secondFactor = Long.parseLong(input.get(20).split(" ")[1]);
        long bigLoop = factorial(12L);
        System.out.println(bigLoop + firstFactor * secondFactor);
    }

    private static long factorial(final long l) {
        if (l == 1L) {
            return l;
        }
        return l * factorial(l - 1);
    }

}
