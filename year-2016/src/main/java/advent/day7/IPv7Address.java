package advent.day7;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public final class IPv7Address {

    private final String address;
    private final List<String> hypernetSequences = new ArrayList<>();
    private final List<String> supernetSequences = new ArrayList<>();

    public IPv7Address(final String address) {
        this.address = address;
        int index = 0;
        while (index < address.length()) {
            if (address.charAt(index) == '[') {
                int closingIndex = address.indexOf(']', index);
                hypernetSequences.add(address.substring(index + 1, closingIndex));
                index = closingIndex + 1;

            } else {
                int nextHypernetIndex = address.indexOf('[', index);
                if (nextHypernetIndex >= 0) {
                    supernetSequences.add(address.substring(index, nextHypernetIndex));
                    index = nextHypernetIndex;
                } else {
                    supernetSequences.add(address.substring(index));
                    index = address.length();
                }
            }
        }
    }

    public boolean supportsTLS() {
        return containsABBA(address) && hypernetSequences.stream().noneMatch(IPv7Address::containsABBA);
    }

    private static boolean containsABBA(final String s) {
        return IntStream.rangeClosed(0, s.length() - 4)
                .anyMatch(
                        n -> s.charAt(n) != s.charAt(n + 1)
                                && s.charAt(n) == s.charAt(n + 3)
                                && s.charAt(n + 1) == s.charAt(n + 2)
                );
    }

    public boolean supportsSSL() {
        List<String> neededBABs = neededBABs();
        return hypernetSequences.stream()
                .anyMatch(s -> neededBABs.stream().anyMatch(s::contains));
    }

    private List<String> neededBABs() {
        return supernetSequences.stream()
                .flatMap(s ->
                        IntStream.rangeClosed(0, s.length() - 3)
                                .mapToObj(n -> s.substring(n, n + 3))
                )
                .filter(IPv7Address::isABA)
                .map(IPv7Address::toBAB)
                .collect(toList());
    }

    private static boolean isABA(final String s) {
        return s.charAt(0) != s.charAt(1) && s.charAt(0) == s.charAt(2);
    }

    private static String toBAB(final String aba) {
        return String.valueOf(aba.charAt(1)) +
                aba.charAt(0) +
                aba.charAt(1);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IPv7Address that = (IPv7Address) o;
        return Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address);
    }
}
