package advent.day7;

import advent.InputProvider;

import java.util.List;

public final class Day7Exercise {

    private Day7Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(7);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        System.out.println(input.stream()
                .map(IPv7Address::new)
                .filter(IPv7Address::supportsTLS)
                .count());
    }

    private static void part2(final List<String> input) {
        System.out.println(input.stream()
                .map(IPv7Address::new)
                .filter(IPv7Address::supportsSSL)
                .count());
    }

}
