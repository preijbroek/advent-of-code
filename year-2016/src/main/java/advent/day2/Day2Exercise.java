package advent.day2;

import advent.InputProvider;

import java.util.List;

public final class Day2Exercise {

    private Day2Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(2);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Pad pad = new Pad();
        System.out.println(pad.codeWithSimplePad(input));
    }

    private static void part2(final List<String> input) {
        Pad pad = new Pad();
        System.out.println(pad.codeWithDifficultPad(input));
    }
}
