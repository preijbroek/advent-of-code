package advent.day2;

import util.Position;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.joining;

public final class Pad {

    private static final Map<Position, String> SIMPLE_PAD_POSITIONS = new HashMap<>();
    private static final Map<Position, String> DIFFICULT_PAD_POSITIONS = new HashMap<>();

    static {
        SIMPLE_PAD_POSITIONS.put(new Position(1, 1), "1");
        SIMPLE_PAD_POSITIONS.put(new Position(2, 1), "2");
        SIMPLE_PAD_POSITIONS.put(new Position(3, 1), "3");
        SIMPLE_PAD_POSITIONS.put(new Position(1, 2), "4");
        SIMPLE_PAD_POSITIONS.put(new Position(2, 2), "5");
        SIMPLE_PAD_POSITIONS.put(new Position(3, 2), "6");
        SIMPLE_PAD_POSITIONS.put(new Position(1, 3), "7");
        SIMPLE_PAD_POSITIONS.put(new Position(2, 3), "8");
        SIMPLE_PAD_POSITIONS.put(new Position(3, 3), "9");

        DIFFICULT_PAD_POSITIONS.put(new Position(3, 1), "1");
        DIFFICULT_PAD_POSITIONS.put(new Position(2, 2), "2");
        DIFFICULT_PAD_POSITIONS.put(new Position(3, 2), "3");
        DIFFICULT_PAD_POSITIONS.put(new Position(4, 2), "4");
        DIFFICULT_PAD_POSITIONS.put(new Position(1, 3), "5");
        DIFFICULT_PAD_POSITIONS.put(new Position(2, 3), "6");
        DIFFICULT_PAD_POSITIONS.put(new Position(3, 3), "7");
        DIFFICULT_PAD_POSITIONS.put(new Position(4, 3), "8");
        DIFFICULT_PAD_POSITIONS.put(new Position(5, 3), "9");
        DIFFICULT_PAD_POSITIONS.put(new Position(2, 4), "A");
        DIFFICULT_PAD_POSITIONS.put(new Position(3, 4), "B");
        DIFFICULT_PAD_POSITIONS.put(new Position(4, 4), "C");
        DIFFICULT_PAD_POSITIONS.put(new Position(3, 5), "D");
    }


    public String codeWithSimplePad(final List<String> input) {
        return findCodePositions(input, new Position(2, 2), SIMPLE_PAD_POSITIONS.keySet())
                .stream().map(SIMPLE_PAD_POSITIONS::get).collect(joining());
    }

    public String codeWithDifficultPad(final List<String> input) {
        return findCodePositions(input, new Position(1, 3), DIFFICULT_PAD_POSITIONS.keySet())
                .stream().map(DIFFICULT_PAD_POSITIONS::get).collect(joining());
    }

    private List<Position> findCodePositions(final List<String> input,
                                             final Position startPosition,
                                             final Set<Position> pad) {
        Position currentPosition = startPosition;
        List<Position> code = new ArrayList<>(5);
        for (String s : input) {
            for (int i = 0; i < s.length(); i++) {
                currentPosition = findNextPosition(s.charAt(i), currentPosition, pad);

            }
            code.add(currentPosition);
        }
        return code;
    }

    private static Position findNextPosition(final char c,
                                             final Position currentPosition,
                                             final Set<Position> pad) {
        Position newPosition;
        switch (c) {
            case 'U':
                newPosition = currentPosition.south();
                break;
            case 'R':
                newPosition = currentPosition.east();
                break;
            case 'D':
                newPosition = currentPosition.north();
                break;
            case 'L':
                newPosition = currentPosition.west();
                break;
            default:
                throw new IllegalArgumentException(String.valueOf(c));
        }
        if (pad.contains(newPosition)) {
            return newPosition;
        } else {
            return currentPosition;
        }
    }

}
