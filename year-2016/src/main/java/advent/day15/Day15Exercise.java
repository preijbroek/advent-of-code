package advent.day15;

import advent.InputProvider;

import java.util.List;

public final class Day15Exercise {

    private Day15Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(15);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Disc periodDisc = input.stream()
                .map(Disc::from)
                .reduce((Disc::merge))
                .orElseThrow(IllegalStateException::new);
        System.out.println(periodDisc.getMovesUntilZero());

    }

    private static void part2(final List<String> input) {
        Disc periodDisc = input.stream()
                .map(Disc::from)
                .reduce((Disc::merge))
                .orElseThrow(IllegalStateException::new);
        periodDisc = periodDisc.merge(new Disc(7, 11, 0));
        System.out.println(periodDisc.getMovesUntilZero());
    }

}
