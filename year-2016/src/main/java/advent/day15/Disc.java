package advent.day15;

import java.math.BigInteger;
import java.util.Objects;

public final class Disc {

    private final BigInteger positions;
    private final BigInteger startPosition;

    public Disc(final int floor, final int positions, final int startPosition) {
        this(positions, floor + startPosition);
    }

    private Disc(final int positions, final int startPosition) {
        this(BigInteger.valueOf(positions), BigInteger.valueOf(startPosition));
    }

    private Disc(final BigInteger positions, final BigInteger startPosition) {
        this.positions = positions;
//        this.movesUntilZero = positions.subtract(startPosition).mod(positions);
        this.startPosition = startPosition;
    }

    public static Disc from(final String line) {
        String[] initialValues = line.split(" ");
        int floor = Integer.parseInt(initialValues[1].split("")[1]);
        int positions = Integer.parseInt(initialValues[3]);
        int startPosition = Integer.parseInt(initialValues[11].replaceAll("\\.", ""));
        return new Disc(floor, positions, startPosition);
    }

    public Disc merge(final Disc disc) {
        BigInteger p = this.positions;
        BigInteger a = this.startPosition;
        BigInteger q = disc.positions;
        BigInteger b = disc.startPosition;
        BigInteger positions = p.multiply(q);
        BigInteger movesUntilZero = b.multiply(p.modInverse(q).multiply(p))
                .add(a.multiply(q.modInverse(p).multiply(q))).mod(positions);
        return new Disc(positions, movesUntilZero);
    }

    public int getPositions() {
        return positions.intValue();
    }

    public int getStartPosition() {
        return startPosition.intValue();
    }

    public int getMovesUntilZero() {
        return positions.subtract(startPosition).intValue();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disc disc = (Disc) o;
        return Objects.equals(positions, disc.positions) &&
                Objects.equals(startPosition, disc.startPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(positions, startPosition);
    }
}
