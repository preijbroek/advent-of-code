package advent.day15;

import java.util.Arrays;
import java.util.List;

public class Main {

    private static class Disc {

        private final int positions;
        private final int pos;

        public Disc(final int positions, final int pos) {
            this.positions = positions;
            this.pos = pos;
        }

        public int getPositions() {
            return positions;
        }

        public int getPos() {
            return pos;
        }
    }

    private static final List<Disc> discs = Arrays.asList(
            new Disc(13, 1),
            new Disc(19, 10),
            new Disc(3, 2),
            new Disc(7, 1),
            new Disc(5, 3),
            new Disc(17, 5)
//            new Disc(11, 0) // Remove this disc for part 1
    );

    public static void main(String[] args) {
        main:
        for (int i = 0; ; i++) {
            for (int j = 0; j < discs.size(); j++) {
                Disc d = discs.get(j);
                if ((d.getPos() + (i + j + 1)) % d.getPositions() != 0) {
                    continue main;
                }
            }
            System.out.println(i);
            break;
        }
    }
}
