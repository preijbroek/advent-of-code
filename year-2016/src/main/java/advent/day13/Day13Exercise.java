package advent.day13;

import advent.InputProvider;
import util.Position;

public final class Day13Exercise {

    private Day13Exercise() {
    }

    public static void main(String[] args) {
        int input = Integer.parseInt(InputProvider.getInput(13).get(0));
        part1(input);
        part2(input);
    }

    private static void part1(final int input) {
        Maze maze = new Maze(input);
        System.out.println(maze.distanceTo(new Position(31, 39)));
    }

    private static void part2(final int input) {
        Maze maze = new Maze(input);
        System.out.println(maze.findSpread(50));
    }

}
