package advent.day13;

import util.Position;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public final class Maze {

    private static final Position startPosition = new Position(1, 1);

    private final Map<Position, Integer> distances = new HashMap<>();
    private final int number;

    public Maze(final int number) {
        this.number = number;
    }

    public int findSpread(final int moves) {
        distances.clear();
        int distance = 0;
        final Set<Position> currentPositions = new HashSet<>();
        currentPositions.add(startPosition);
        distances.put(startPosition, distance);
        for (int i = 0; i < moves; i++) {
            distance = moveToNextPositions(distance, currentPositions);
        }
        return distances.size();
    }

    public int distanceTo(final Position position) {
        distances.clear();
        int distance = 0;
        final Set<Position> currentPositions = new HashSet<>();
        currentPositions.add(startPosition);
        distances.put(startPosition, distance);
        while (!distances.containsKey(position)) {
            distance = moveToNextPositions(distance, currentPositions);
        }
        return distances.get(position);
    }

    private int moveToNextPositions(int distance, final Set<Position> currentPositions) {
        distance++;
        int nextDistance = distance;
        Set<Position> newCurrents = new HashSet<>();
        currentPositions.forEach(p -> p.neighbours().stream()
                .filter(neighbour -> !distances.containsKey(neighbour))
                .filter(Maze::isValid)
                .filter(this::isOpen)
                .forEachOrdered(pos -> {
                    newCurrents.add(pos);
                    distances.put(pos, nextDistance);
                }));
        currentPositions.clear();
        currentPositions.addAll(newCurrents);
        return distance;
    }

    private boolean isOpen(final Position position) {
        final int x = position.getX();
        final int y = position.getY();
        return (Integer.bitCount((x * x) + (3 * x) + (2 * x * y) + y + (y * y) + number)) % 2 == 0;
    }

    private static boolean isValid(final Position position) {
        return position.getX() >= 0 && position.getY() >= 0;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Maze maze = (Maze) o;
        return number == maze.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
