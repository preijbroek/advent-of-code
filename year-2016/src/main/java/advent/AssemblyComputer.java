package advent;

import advent.day12.Register;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class AssemblyComputer {

    private final Map<String, Register> registers;

    public AssemblyComputer(final Map<String, Register> registers) {
        this.registers = registers;
    }

    public Register get(final String id) {
        return registers.get(id);
    }

    public void set(final String id, final int value) {
        registers.get(id).setValue(value);
    }

    public void run(final List<String> input) {
        List<String> inputCopy = new ArrayList<>(input);
        int position = 0;
        while (position < inputCopy.size()) {
            String[] instruction = inputCopy.get(position).split(" ");
            if ("cpy".equals(instruction[0])) {
                runCopy(instruction);
                position++;
            } else if ("dec".equals(instruction[0])) {
                if (registers.containsKey(instruction[1])) {
                    registers.get(instruction[1]).decrease();
                }
                position++;
            } else if ("inc".equals(instruction[0])) {
                if (registers.containsKey(instruction[1])) {
                    registers.get(instruction[1]).increase();
                }
                position++;
            } else if ("jnz".equals(instruction[0])) {
                position = setPosition(position, instruction);
            } else if ("tgl".equals(instruction[0])) {
                int positionToModify;
                if (Character.isDigit(instruction[0].charAt(0))) {
                    positionToModify = position + Integer.parseInt(instruction[1]);
                } else {
                    positionToModify = position + registers.get(instruction[1]).getValue();
                }
                if (positionToModify >= 0 && positionToModify < inputCopy.size()) {
                    toggle(positionToModify, inputCopy);
                }
                position++;
            } else {
                throw new IllegalArgumentException(inputCopy.get(position));
            }
        }
    }

    private void toggle(final int positionToModify, final List<String> input) {
        String instruction = input.get(positionToModify);
        String[] splits = instruction.split(" ");
        if (splits.length == 2) {
            if (instruction.startsWith("inc")) {
                input.set(positionToModify, input.get(positionToModify).replaceAll("inc", "dec"));
            } else {
                input.set(positionToModify, "inc" + input.get(positionToModify).substring(3));
            }
        } else if (splits.length == 3) {
            if (instruction.startsWith("jnz")) {
                input.set(positionToModify, input.get(positionToModify).replaceAll("jnz", "cpy"));
            } else {
                input.set(positionToModify, "jnz" + input.get(positionToModify).substring(3));
            }
        }
    }

    private int setPosition(int position, final String[] instruction) {
        int relative = Character.isDigit(instruction[2].charAt(0)) || instruction[2].charAt(0) == '-'
                ? Integer.parseInt(instruction[2])
                : registers.get(instruction[2]).getValue();
        if (Character.isDigit(instruction[1].charAt(0)) && Integer.parseInt(instruction[1]) != 0) {
            position += relative;
        } else if (!Character.isDigit(instruction[1].charAt(0)) && registers.get(instruction[1]).getValue() != 0) {
            position += relative;
        } else {
            position++;
        }
        return position;
    }

    private void runCopy(final String[] instruction) {
        if (registers.containsKey(instruction[2])) {
            if (Character.isDigit(instruction[1].charAt(0)) || instruction[1].charAt(0) == '-') {
                registers.get(instruction[2]).copy(Integer.parseInt(instruction[1]));
            } else {
                registers.get(instruction[2]).copy(registers.get(instruction[1]));
            }
        }
    }
}
