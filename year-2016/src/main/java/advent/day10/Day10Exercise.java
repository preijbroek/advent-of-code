package advent.day10;

import advent.InputProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Day10Exercise {

    private Day10Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(10);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Map<Integer, Robot> robotMap = initializeRobotMap(input);
        while (true) {
            AtomicBoolean hasMovedSomething = new AtomicBoolean(false);
            robotMap.values().stream()
                    .filter(Robot::canDistribute)
                    .forEach(robot -> {
                        hasMovedSomething.set(true);
                        int low = robot.lowOutput();
                        int high = robot.highOutput();
                        if (low == 17 && high == 61) {
                            System.out.println(robot.getIdentifier());
                        }
                        robotMap.get(robot.getLowerRobotId()).addChip(low);
                        robotMap.get(robot.getHigherRobotId()).addChip(high);
                        robot.clear();
                    });
            if (!hasMovedSomething.get()) {
                break;
            }
        }
    }

    private static void part2(final List<String> input) {
        Map<Integer, Robot> robotMap = initializeRobotMap(input);
        while (true) {
            AtomicBoolean hasMovedSomething = new AtomicBoolean(false);
            robotMap.values().stream()
                    .filter(Robot::canDistribute)
                    .forEach(robot -> {
                        hasMovedSomething.set(true);
                        int low = robot.lowOutput();
                        int high = robot.highOutput();
                        robotMap.get(robot.getLowerRobotId()).addChip(low);
                        robotMap.get(robot.getHigherRobotId()).addChip(high);
                        robot.clear();
                    });
            if (!hasMovedSomething.get()) {
                break;
            }
        }
        System.out.println(IntStream.rangeClosed(-3, -1)
                .flatMap(i -> robotMap.get(i).valueStream())
                .reduce(1, (m, n) -> m * n));
    }

    private static Map<Integer, Robot> initializeRobotMap(final List<String> input) {
        Map<Integer, Robot> robotMap = new HashMap<>();
        List<String[]> splitInputs = input.stream()
                .filter(s -> !s.contains("value"))
                .map(s -> s.split(" "))
                .collect(Collectors.toList());
        for (String[] splitInput : splitInputs) {
            int id = Integer.parseInt(splitInput[1]);
            Robot robot = robotMap.computeIfAbsent(id, Robot::new);
            robot.setHigherRobotId(splitInput[10], Integer.parseInt(splitInput[11]));
            robot.setLowerRobotId(splitInput[5], Integer.parseInt(splitInput[6]));
            robotMap.computeIfAbsent(robot.getHigherRobotId(), Robot::new);
            robotMap.computeIfAbsent(robot.getLowerRobotId(), Robot::new);
        }
        input.stream()
                .filter(s -> s.contains("value"))
                .map(s -> s.split(" "))
                .forEach(array -> {
                    int value = Integer.parseInt(array[1]);
                    int robotId = Integer.parseInt(array[5]);
                    robotMap.get(robotId).addChip(value);
                });
        return robotMap;
    }

}
