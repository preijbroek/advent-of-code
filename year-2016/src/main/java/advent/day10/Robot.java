package advent.day10;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import static util.Comparators.maxOf;
import static util.Comparators.minOf;

public final class Robot {

    private final List<Integer> chips = new ArrayList<>(2);
    private final int identifier;
    private int lowerRobotId;
    private int higherRobotId;

    public Robot(final int identifier) {
        this.identifier = identifier;
    }

    public boolean canDistribute() {
        return chips.size() >= 2 && identifier >= 0;
    }

    public IntStream valueStream() {
        return chips.stream().mapToInt(n -> n);
    }

    public void addChip(final int value) {
        chips.add(value);
    }

    public void clear() {
        chips.clear();
    }

    public int lowOutput() {
        return minOf(chips);
    }

    public int highOutput() {
        return maxOf(chips);
    }

    public int getIdentifier() {
        return identifier;
    }

    public int getLowerRobotId() {
        return lowerRobotId;
    }

    public int getHigherRobotId() {
        return higherRobotId;
    }

    public void setLowerRobotId(final String type, final int lowerRobotId) {
        if ("output".equals(type)) {
            this.lowerRobotId = -lowerRobotId - 1;
        } else {
            this.lowerRobotId = lowerRobotId;
        }
    }

    public void setHigherRobotId(final String type, final int higherRobotId) {
        if ("output".equals(type)) {
            this.higherRobotId = -higherRobotId - 1;
        } else {
            this.higherRobotId = higherRobotId;
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Robot robot = (Robot) o;
        return identifier == robot.identifier;
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }
}
