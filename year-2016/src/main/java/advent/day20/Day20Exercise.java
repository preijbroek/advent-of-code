package advent.day20;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public final class Day20Exercise {

    private Day20Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(20);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Map<Long, Long> ranges = input.stream()
                .map(s -> Arrays.stream(s.split("-")).mapToLong(Long::parseLong).toArray())
                .collect(Collectors.toMap(longs -> longs[0], longs -> longs[1]));
        long value = 0;
        Optional<Long> nextValue = nextRange(value, ranges);
        while (nextValue.isPresent()) {
            value = nextValue.get() + 1;
            nextValue = nextRange(value, ranges);
        }
        System.out.println(value);
    }

    private static void part2(final List<String> input) {
        Map<Long, Long> ranges = input.stream()
                .map(s -> Arrays.stream(s.split("-")).mapToLong(Long::parseLong).toArray())
                .collect(Collectors.toMap(longs -> longs[0], longs -> longs[1]));
        final long max = 4294967295L;
        long allowedValues = 0;
        long value = 0;
        while (value <= max) {
            Optional<Long> nextValue = nextRange(value, ranges);
            while (nextValue.isPresent()) {
                value = nextValue.get() + 1;
                nextValue = nextRange(value, ranges);
            }
            if (value <= max) {
                allowedValues++;
            }
            value++;
        }
        System.out.println(allowedValues);
    }

    private static Optional<Long> nextRange(final long value, final Map<Long, Long> ranges) {
        return ranges.entrySet()
                .stream()
                .filter(entry -> entry.getKey() <= value)
                .filter(entry -> entry.getValue() >= value)
                .map(Map.Entry::getValue)
                .max(Long::compare);
    }

}
