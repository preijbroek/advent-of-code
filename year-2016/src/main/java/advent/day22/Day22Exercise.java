package advent.day22;

import advent.InputProvider;
import util.JavaPair;
import util.Position;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class Day22Exercise {

    private Day22Exercise() {
    }

    public static void main(final String[] args) {
        List<String> input = InputProvider.getInput(22);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        List<Node> nodes = input.stream()
                .filter(s -> s.startsWith("/dev"))
                .map(Node::from)
                .collect(Collectors.toList());
        System.out.println(nodes.stream()
                .flatMap(node ->
                        nodes.stream().filter(node::isViableWith).map(pairedNode -> new JavaPair<>(node, pairedNode))
                ).distinct()
                .count()
                );
    }

    private static void part2(final List<String> input) {
        List<Node> nodes = input.stream()
                .filter(s -> s.startsWith("/dev"))
                .map(Node::from)
                .collect(Collectors.toList());
        final Node initialEmptyNode = nodes.stream()
                .filter(node -> node.getUsed() == 0)
                .findFirst()
                .orElseThrow(IllegalStateException::new);
        final int maxX = nodes.stream()
                .max(Comparator.comparing(node -> node.getPosition().getX()))
                .orElseThrow(IllegalStateException::new)
                .getPosition()
                .getX();
        final Node targetNode = nodes.stream()
                .filter(node -> new Position(maxX, 0).equals(node.getPosition()))
                .findFirst()
                .orElseThrow(IllegalStateException::new);
        int stepsToTargetNode = 0;
        Set<Node> currentNodes = new HashSet<>();
        currentNodes.add(initialEmptyNode);
        Set<Node> visitedNodes = new HashSet<>();
        visitedNodes.add(initialEmptyNode);
        while (!visitedNodes.contains(targetNode)) {
            Set<Node> newCurrentNodes = new HashSet<>();
            for (Node currentNode : currentNodes) {
                currentNode.neighbours(nodes)
                        .stream()
                        .filter(currentNode::mayContain)
                        .filter(node -> !visitedNodes.contains(node))
                        .forEachOrdered(node -> {
                            newCurrentNodes.add(node);
                            visitedNodes.add(node);
                        });
            }
            stepsToTargetNode++;
            currentNodes.clear();
            currentNodes.addAll(newCurrentNodes);
        }

        int result = stepsToTargetNode + (maxX - 1) * 5;
        System.out.println(result);
    }

}
