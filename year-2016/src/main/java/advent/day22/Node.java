package advent.day22;

import util.Position;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

public final class Node {

    private final Position position;
    private final int size, used, available, usedPercentage;

    public Node(final Position position, final int size, final int used, final int available, final int usedPercentage) {
        this.position = position;
        this.size = size;
        this.used = used;
        this.available = available;
        this.usedPercentage = usedPercentage;
    }

    public static Node from(final String s) {
        String[] properties = s.split(" +");
        String[] positions = properties[0].split("-");
        Position position = new Position(Integer.parseInt(positions[1].substring(1)),
                Integer.parseInt(positions[2].substring(1)));
        properties = Arrays.stream(properties)
                .map(str -> str.replaceAll("T", "").replaceAll("%", ""))
                .toArray(String[]::new);
        int size = Integer.parseInt(properties[1]);
        int used = Integer.parseInt(properties[2]);
        int available = Integer.parseInt(properties[3]);
        int usedPercentage = Integer.parseInt(properties[4]);
        return new Node(position, size, used, available, usedPercentage);
    }

    public boolean isViableWith(final Node node) {
        return !this.equals(node) && this.used > 0 && this.used <= node.available;
    }

    public boolean mayContain(final Node node) {
        return this.size >= node.used;
    }

    public List<Node> neighbours(final List<Node> nodes) {
        return nodes.stream()
                .filter(node -> position.neighbours().contains(node.position))
                .collect(toList());
    }

    public Position getPosition() {
        return position;
    }

    public int getSize() {
        return size;
    }

    public int getUsed() {
        return used;
    }

    public int getAvailable() {
        return available;
    }

    public int getUsedPercentage() {
        return usedPercentage;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return size == node.size &&
                Objects.equals(position, node.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, size);
    }

    @Override
    public String toString() {
        return "Node{" +
                "position=" + position +
                ", size=" + size +
                ", used=" + used +
                ", available=" + available +
                ", usedPercentage=" + usedPercentage +
                '}';
    }
}
