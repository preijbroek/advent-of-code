package advent;

import jakarta.xml.bind.DatatypeConverter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public final class MD5Hasher {

    private final MessageDigest md5;
    private final String code;

    public MD5Hasher(final String code) {
        this.code = code;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }

    public String hash(final String path) {
        String result = code + path;
        md5.update(result.getBytes());
        return DatatypeConverter.printHexBinary(md5.digest()).toLowerCase();
    }

    public String hash(final int index) {
        return hash(index, 1);
    }

    public String hash(final int index, final int times) {
        String result = code + index;
        for (int i = 0; i < times; i++) {
            md5.update(result.getBytes());
            result = DatatypeConverter.printHexBinary(md5.digest()).toLowerCase();
        }
        return result;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MD5Hasher md5Hasher = (MD5Hasher) o;
        return Objects.equals(code, md5Hasher.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
