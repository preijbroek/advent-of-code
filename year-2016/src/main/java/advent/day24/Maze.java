package advent.day24;

import util.JavaPair;
import util.Position;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toSet;

public final class Maze {

    private final Map<Position, Character> mazeMap;

    public Maze(final List<String> lines) {
        mazeMap = Collections.unmodifiableMap(initializeMap(lines));
    }

    private static Map<Position, Character> initializeMap(final List<String> lines) {
        Map<Position, Character> mazeMap = new HashMap<>();
        for (int y = 0; y < lines.size(); y++) {
            for (int x = 0; x < lines.get(0).length(); x++) {
                if (lines.get(y).charAt(x) != '#') {
                    mazeMap.put(new Position(x, y), lines.get(y).charAt(x));
                }
            }
        }
        return mazeMap;
    }

    public int shortestRouteToAllWires() {
        Set<Position> wirePositions = mazeMap.entrySet()
                .stream()
                .filter(entry -> Character.isDigit(entry.getValue()))
                .map(Map.Entry::getKey)
                .collect(toSet());
        Map<JavaPair<Position>, Integer> distances = new HashMap<>();
        wirePositions.forEach(position ->
                wirePositions.forEach(
                        pos -> {
                            if (!pos.equals(position)) {
                                distances.put(new JavaPair<>(position, pos), distanceBetween(position, pos));
                            }
                        }
                )
        );
        Position initialPosition = mazeMap.entrySet()
                .stream()
                .filter(entry -> entry.getValue() == '0')
                .map(Map.Entry::getKey)
                .findAny()
                .orElseThrow(IllegalStateException::new);
        List<Position> unreachedWires = new ArrayList<>(wirePositions);
        unreachedWires.remove(initialPosition);
        List<List<Position>> permutations = permutations(unreachedWires);
        int minDistance = Integer.MAX_VALUE;
        for (List<Position> permutation : permutations) {
            int distance = distanceBetween(initialPosition, permutation.get(0));
            distance += IntStream.range(1, permutation.size())
                    .map(i -> distances.get(new JavaPair<>(permutation.get(i), permutation.get(i - 1))))
                    .sum();
            if (minDistance > distance) {
                minDistance = distance;
            }
        }
        return minDistance;
    }

    public int shortestRouteToAllWiresWithReturn() {
        Set<Position> wirePositions = mazeMap.entrySet()
                .stream()
                .filter(entry -> Character.isDigit(entry.getValue()))
                .map(Map.Entry::getKey)
                .collect(toSet());
        Map<JavaPair<Position>, Integer> distances = new HashMap<>();
        wirePositions.forEach(position ->
                wirePositions.forEach(
                        pos -> {
                            if (!pos.equals(position)) {
                                distances.put(new JavaPair<>(position, pos), distanceBetween(position, pos));
                            }
                        }
                )
        );
        Position initialPosition = mazeMap.entrySet()
                .stream()
                .filter(entry -> entry.getValue() == '0')
                .map(Map.Entry::getKey)
                .findAny()
                .orElseThrow(IllegalStateException::new);
        List<Position> unreachedWires = new ArrayList<>(wirePositions);
        unreachedWires.remove(initialPosition);
        List<List<Position>> permutations = permutations(unreachedWires);
        int minDistance = Integer.MAX_VALUE;
        for (List<Position> permutation : permutations) {
            int distance = distanceBetween(initialPosition, permutation.get(0));
            distance += IntStream.range(1, permutation.size())
                    .map(i -> distances.get(new JavaPair<>(permutation.get(i), permutation.get(i - 1))))
                    .sum();
            distance += distanceBetween(permutation.get(permutation.size() - 1), initialPosition);
            if (minDistance > distance) {
                minDistance = distance;
            }
        }
        return minDistance;
    }

    private Position getPositionOf(final Character character) {
        return mazeMap.entrySet().stream()
                .filter(entry -> entry.getValue().equals(character))
                .map(Map.Entry::getKey)
                .findAny()
                .orElseThrow(IllegalStateException::new);
    }

    private static <T> List<List<T>> permutations(final Collection<T> items) {
        return IntStream.range(0, factorial(items.size()))
                .mapToObj(i -> permutation(i, items)).collect(Collectors.toList());
    }

    private static int factorial(final int num) {
        return IntStream.rangeClosed(2, num).reduce(1, (x, y) -> x * y);
    }

    private static <T> List<T> permutation(final int count, final List<T> input, final List<T> output) {
        if (input.isEmpty()) {
            return output;
        }

        final int factorial = factorial(input.size() - 1);
        output.add(input.remove(count / factorial));
        return permutation(count % factorial, input, output);
    }

    private static <T> List<T> permutation(final int count, final Collection<T> items) {
        return permutation(count, new LinkedList<>(items), new ArrayList<>());
    }

    private int distanceBetween(final Position position1, final Position position2) {
        Set<Position> currentPositions = new HashSet<>();
        currentPositions.add(position1);
        Set<Position> visitedPositions = new HashSet<>();
        visitedPositions.add(position1);
        int distance = 0;
        while (!currentPositions.contains(position2)) {
            Set<Position> newCurrentPositions = new HashSet<>();
            for (Position currentPosition : currentPositions) {
                currentPosition.neighbours()
                        .stream()
                        .filter(mazeMap::containsKey)
                        .filter(neighbour -> !visitedPositions.contains(neighbour))
                        .forEachOrdered(neighbour -> {
                            newCurrentPositions.add(neighbour);
                            visitedPositions.add(neighbour);
                        });
            }
            currentPositions.clear();
            currentPositions.addAll(newCurrentPositions);
            distance++;
        }
        return distance;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Maze maze = (Maze) o;
        return Objects.equals(mazeMap, maze.mazeMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mazeMap);
    }
}
