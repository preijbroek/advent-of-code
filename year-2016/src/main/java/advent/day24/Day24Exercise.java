package advent.day24;

import advent.InputProvider;

import java.util.List;

public final class Day24Exercise {

    private Day24Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(24);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Maze maze = new Maze(input);
        System.out.println(maze.shortestRouteToAllWires());
    }

    private static void part2(final List<String> input) {
        Maze maze = new Maze(input);
        System.out.println(maze.shortestRouteToAllWiresWithReturn());
    }

}
