package advent.day21;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Collections.rotate;
import static java.util.stream.Collectors.joining;

public final class Password {

    private final String password;

    public Password(final String password) {
        this.password = password;
    }

    public String scramble(final List<String> instructions) {
        List<String> instructionsCopy = new ArrayList<>(instructions);
        final List<Character> scrambled = password.chars()
                .mapToObj(n -> (char) n)
                .collect(Collectors.toCollection(ArrayList::new));
        for (String instruction : instructionsCopy) {
            String[] positions = instruction.split(" ");
            if (instruction.startsWith("rotate based")) {
                rotateOnPosition(scrambled, positions[6].charAt(0));
            } else if (instruction.startsWith("rotate right")) {
                rotateRight(scrambled, Integer.parseInt(positions[2]));
            } else if (instruction.startsWith("rotate left")) {
                rotateLeft(scrambled, Integer.parseInt(positions[2]));
            } else if (instruction.startsWith("reverse")) {
                reverse(scrambled, Integer.parseInt(positions[2]), Integer.parseInt(positions[4]));
            } else if (instruction.startsWith("swap letter")) {
                swapPosition(scrambled, positions[2].charAt(0), positions[5].charAt(0));
            } else if (instruction.startsWith("swap position")) {
                swapPosition(scrambled, Integer.parseInt(positions[2]), Integer.parseInt(positions[5]));
            } else if (instruction.startsWith("move")) {
                move(scrambled, Integer.parseInt(positions[2]), Integer.parseInt(positions[5]));
            } else {
                throw new IllegalArgumentException(instruction);
            }
        }
        return scrambled.stream().map(String::valueOf).collect(joining());
    }

    public String unscramble(final List<String> instructions) {
        System.out.println();
        List<String> instructionsCopy = new ArrayList<>(instructions);
        Collections.reverse(instructionsCopy);
        final List<Character> unscrambled = password.chars()
                .mapToObj(n -> (char) n)
                .collect(Collectors.toCollection(ArrayList::new));
        System.out.println(password);
        int index = 100;
        for (String instruction : instructionsCopy) {
            String[] positions = instruction.split(" ");
            if (instruction.startsWith("rotate based")) {
                rotateBackOnPosition(unscrambled, positions[6].charAt(0));
            } else if (instruction.startsWith("rotate right")) {
                rotateLeft(unscrambled, Integer.parseInt(positions[2]));
            } else if (instruction.startsWith("rotate left")) {
                rotateRight(unscrambled, Integer.parseInt(positions[2]));
            } else if (instruction.startsWith("reverse")) {
                reverse(unscrambled, Integer.parseInt(positions[2]), Integer.parseInt(positions[4]));
            } else if (instruction.startsWith("swap letter")) {
                swapPosition(unscrambled, positions[2].charAt(0), positions[5].charAt(0));
            } else if (instruction.startsWith("swap position")) {
                swapPosition(unscrambled, Integer.parseInt(positions[2]), Integer.parseInt(positions[5]));
            } else if (instruction.startsWith("move")) {
                move(unscrambled, Integer.parseInt(positions[5]), Integer.parseInt(positions[2]));
            } else {
                throw new IllegalArgumentException(instruction);
            }
            System.out.println(index-- + ": " + unscrambled.stream().map(String::valueOf).collect(joining()));
        }
        return unscrambled.stream().map(String::valueOf).collect(joining());
    }

    private static void rotateOnPosition(final List<Character> scrambled, final Character character) {
        int index = scrambled.indexOf(character);
        int toIndex = index < 4 ? (index << 1) + 1 : ((index - 3) << 1) % 8;
        rotate(scrambled, toIndex - index);
    }

    private static void rotateBackOnPosition(final List<Character> scrambled, final Character character) {
        int index = scrambled.indexOf(character);
        int toIndex = index == 0 ? 7 : ((index & 1) == 0 ? (index >> 1) + 3 : (index - 1) >> 1);
        rotate(scrambled, toIndex - index);
    }

    private static void rotateRight(final List<Character> scrambled, final int i) {
        rotate(scrambled, i);
    }

    private static void rotateLeft(final List<Character> scrambled, final int i) {
        rotate(scrambled, -i);
    }

    private static void reverse(final List<Character> scrambled, final int x, final int y) {
        Collections.reverse(scrambled.subList(x, y + 1));
    }

    private static void swapPosition(final List<Character> scrambled, final char x, final char y) {
        int indexOfX = scrambled.indexOf(x);
        int indexOfY = scrambled.indexOf(y);
        scrambled.set(indexOfX, y);
        scrambled.set(indexOfY, x);
    }

    private static void swapPosition(final List<Character> scrambled, final int x, final int y) {
        Character charAtX = scrambled.get(x);
        Character charAtY = scrambled.get(y);
        scrambled.set(x, charAtY);
        scrambled.set(y, charAtX);
    }

    private static void move(final List<Character> scrambled, final int x, final int y) {
        scrambled.add(y, scrambled.remove(x));
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Password password1 = (Password) o;
        return Objects.equals(password, password1.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(password);
    }
}
