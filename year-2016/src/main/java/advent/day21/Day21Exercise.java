package advent.day21;

import advent.InputProvider;

import java.util.List;

public final class Day21Exercise {

    private Day21Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(21);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        Password password = new Password("abcdefgh");
        System.out.println(password.scramble(input));
    }

    private static void part2(final List<String> input) {
        Password password = new Password("fbgdceah");
        System.out.println(password.unscramble(input));
    }
}
