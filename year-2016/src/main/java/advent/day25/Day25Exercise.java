package advent.day25;

import advent.InputProvider;

import java.util.List;

public final class Day25Exercise {

    private Day25Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(25);
        part1(input);
    }

    private static void part1(final List<String> input) {
        int firstFactor = Integer.parseInt(input.get(1).split(" ")[1]);
        int secondFactor = Integer.parseInt(input.get(2).split(" ")[1]);
        int calculatorFactor = firstFactor * secondFactor;
        int totalFactor = 0b10;
        while (totalFactor - calculatorFactor < 0) {
            totalFactor <<= 2;
            totalFactor += 0b10;
        }
        System.out.println(totalFactor - calculatorFactor);
    }

}
