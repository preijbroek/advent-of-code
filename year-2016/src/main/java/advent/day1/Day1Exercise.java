package advent.day1;

import advent.InputProvider;

import java.util.Arrays;
import java.util.List;

public final class Day1Exercise {

    private Day1Exercise() {
    }

    public static void main(String[] args) {
        List<String> code = Arrays.asList(InputProvider.getInput(1).get(0).split(", "));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        Walk walk = new Walk();
        walk.runThrough(code);
        System.out.println(walk.distanceToStartPosition());
    }

    private static void part2(final List<String> code) {
        Walk walk = new Walk();
        walk.runUntilCrossing(code);
        System.out.println(walk.distanceToStartPosition());
    }
}
