package advent.day1;

import util.Direction;
import util.Position;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Walk {

    private static final Position startPosition = new Position(0, 0);

    private final Set<Position> visited = new HashSet<>();
    private Position currentPosition = startPosition;
    private Direction currentDirection = Direction.NORTH;

    public Walk() {
    }

    public void runThrough(final List<String> instructions) {
        for (String s : instructions) {
            String dir = s.substring(0, 1);
            setDirection(dir);
            move(Integer.parseInt(s.substring(1)), false);
        }
    }

    public void runUntilCrossing(final List<String> instructions) {
        visited.add(startPosition);
        for (String s : instructions) {
            String dir = s.substring(0, 1);
            setDirection(dir);
            if (move(Integer.parseInt(s.substring(1)), true)) {
                break;
            }
        }
    }

    private void setDirection(final String instruction) {
        if ("R".equals(instruction)) {
            currentDirection = currentDirection.right();
        } else if ("L".equals(instruction)) {
            currentDirection = currentDirection.left();
        } else {
            throw new IllegalArgumentException(instruction);
        }
    }

    private boolean move(final int blocks, boolean includeCrossings) {
        int moved = 0;
        while (moved++ < blocks) {
            switch (currentDirection) {
                case EAST:
                    currentPosition = currentPosition.east();
                    break;
                case SOUTH:
                    currentPosition = currentPosition.south();
                    break;
                case WEST:
                    currentPosition = currentPosition.west();
                    break;
                case NORTH:
                    currentPosition = currentPosition.north();
                    break;
                default:
                    throw new IllegalArgumentException(currentDirection.toString());
            }
            if (includeCrossings && visited.contains(currentPosition)) {
                return true;
            }
            visited.add(currentPosition);
        }
        return false;
    }

    public long distanceToStartPosition() {
        return currentPosition.distanceTo(startPosition);
    }
}
