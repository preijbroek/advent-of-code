package advent.day17;

import advent.InputProvider;
import advent.MD5Hasher;
import util.Position;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class Day17Exercise {

    private Day17Exercise() {
    }

    public static void main(String[] args) {
        String input = InputProvider.getInput(17).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        Map<String, Position> paths = new HashMap<>();
        paths.put("", Position.CENTRE);
        MD5Hasher hasher = new MD5Hasher(input);
        final Set<String> currentPaths = new HashSet<>(paths.keySet());
        while (!paths.containsValue(new Position(3, 3))) {
            Set<String> newCurrentPaths = new HashSet<>();
            moveToNextRooms(paths, hasher, currentPaths, newCurrentPaths);
            currentPaths.clear();
            currentPaths.addAll(newCurrentPaths);
        }
        System.out.println(paths.entrySet().stream()
                .filter(entry -> entry.getValue().equals(new Position(3, 3)))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElseThrow(IllegalStateException::new)
        );
    }

    private static void part2(final String input) {
        Map<String, Position> paths = new HashMap<>();
        paths.put("", Position.CENTRE);
        MD5Hasher hasher = new MD5Hasher(input);
        final Set<String> currentPaths = new HashSet<>(paths.keySet());
        while (!currentPaths.isEmpty()) {
            Set<String> newCurrentPaths = new HashSet<>();
            moveToNextRooms(paths, hasher, currentPaths, newCurrentPaths);
            newCurrentPaths.removeIf(path -> new Position(3, 3).equals(paths.get(path)));
            currentPaths.clear();
            currentPaths.addAll(newCurrentPaths);
        }
        System.out.println(paths.entrySet().stream()
                .filter(entry -> entry.getValue().equals(new Position(3, 3)))
                .map(Map.Entry::getKey)
                .mapToInt(String::length)
                .max()
                .orElseThrow(IllegalStateException::new)
        );
    }

    private static void moveToNextRooms(final Map<String, Position> paths, final MD5Hasher hasher, final Set<String> currentPaths, final Set<String> newCurrentPaths) {
        for (String path : currentPaths) {
            String hash = hasher.hash(path);
            Position up = up(paths.get(path));
            Position down = down(paths.get(path));
            Position left = left(paths.get(path));
            Position right = right(paths.get(path));
            if (isOpen(hash.charAt(0)) && isInGrid(up)) {
                paths.put(path + "U", up);
                newCurrentPaths.add(path + "U");
            }
            if (isOpen(hash.charAt(1)) && isInGrid(down)) {
                paths.put(path + "D", down);
                newCurrentPaths.add(path + "D");
            }
            if (isOpen(hash.charAt(2)) && isInGrid(left)) {
                paths.put(path + "L", left);
                newCurrentPaths.add(path + "L");
            }
            if (isOpen(hash.charAt(3)) && isInGrid(right)) {
                paths.put(path + "R", right);
                newCurrentPaths.add(path + "R");
            }
        }
    }

    private static boolean isOpen(final char c) {
        return Arrays.asList('b', 'c', 'd', 'e', 'f').contains(c);
    }

    private static Position up(final Position position) {
        return position.south();
    }

    private static Position down(final Position position) {
        return position.north();
    }

    private static Position left(final Position position) {
        return position.west();
    }

    private static Position right(final Position position) {
        return position.east();
    }

    private static boolean isInGrid(final Position position) {
        return position.getX() >= 0 && position.getX() <= 3
                && position.getY() >= 0 && position.getY() <= 3;
    }

}
