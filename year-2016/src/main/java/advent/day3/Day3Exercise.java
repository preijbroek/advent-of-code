package advent.day3;

import advent.InputProvider;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public final class Day3Exercise {

    private Day3Exercise() {
    }

    public static void main(String[] args) {
        List<String> input = InputProvider.getInput(3);
        part1(input);
        part2(input);
    }

    private static void part1(final List<String> input) {
        System.out.println(input.stream()
                .map(Triangle::new)
                .filter(Triangle::isPossible)
                .count());
    }

    private static void part2(final List<String> input) {
        List<String> newInput = input.stream().map(String::trim).collect(toList());
        List<Triangle> triangles = new ArrayList<>(newInput.size());
        for (int i = 0; i < newInput.size() - 2; i += 3) {
            String[] line1 = newInput.get(i).split(" +");
            String[] line2 = newInput.get(i + 1).split(" +");
            String[] line3 = newInput.get(i + 2).split(" +");
            for (int j = 0; j < 3; j++) {
                triangles.add(
                        new Triangle(
                                Integer.parseInt(line1[j]),
                                Integer.parseInt(line2[j]),
                                Integer.parseInt(line3[j])
                        )
                );
            }
        }
        System.out.println(triangles.stream().filter(Triangle::isPossible).count());
    }

}
