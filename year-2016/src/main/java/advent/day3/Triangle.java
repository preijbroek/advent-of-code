package advent.day3;

import java.util.Objects;
import java.util.stream.IntStream;

public final class Triangle {

    private final int side1;
    private final int side2;
    private final int side3;

    public Triangle(final int side1, final int side2, final int side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public Triangle(final String s) {
        String[] sides = s.trim().split(" +");
        this.side1 = Integer.parseInt(sides[0]);
        this.side2 = Integer.parseInt(sides[1]);
        this.side3 = Integer.parseInt(sides[2]);
    }

    public boolean isPossible() {
        return 2 * IntStream.of(side1, side2, side3).max().orElseThrow(IllegalStateException::new) -
                IntStream.of(side1, side2, side3).sum() < 0;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangle triangle = (Triangle) o;
        return side1 == triangle.side1 &&
                side2 == triangle.side2 &&
                side3 == triangle.side3;
    }

    @Override
    public int hashCode() {
        return Objects.hash(side1, side2, side3);
    }
}
