package advent.day3

import advent.getInputBlocks
import util.second
import util.third

fun main() {
    val input = getInputBlocks(3).first()
    println(part1(input))
    println(part2(input))
}

private fun part1(input: String): Int {
    return input.mul()
}

private fun part2(input: String): Int {
    return "do()$input".split("don't()").sumOf {
        it.substringAfter("do()", missingDelimiterValue = "").mul()
    }
}

private val mulPattern = "mul\\((\\d+),(\\d+)\\)".toRegex()

private fun String.mul() = mulPattern.findAll(this).sumOf {
    it.groupValues.second().toInt() * it.groupValues.third().toInt()
}
