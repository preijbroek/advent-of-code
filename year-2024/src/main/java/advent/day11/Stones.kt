package advent.day11

import util.isEven

data class Stones(
    private val pebbles: List<Long>,
) {

    fun change(times: Int): Long {
        return generateSequence(pebbles.associateWith { 1L }) { next ->
            next.entries.flatMap { (pebble, occurrence) ->
                pebble.change().map { occurrence to it }
            }
                .groupBy { (_, pebble) -> pebble }
                .mapValues { it.value.sumOf { (occurrences, _) -> occurrences } }
        }.take(times + 1).last().values.sum()
    }

    private fun Long.change(): List<Long> = when {
        this == 0L -> listOf(1L)
        this.toString().length.isEven() -> this.toString().let {
            listOf(
                it.substring(0, it.length shr 1).toLong(),
                it.substring(it.length shr 1, it.length).toLong()
            )
        }

        else -> listOf(this * 2024L)
    }
}
