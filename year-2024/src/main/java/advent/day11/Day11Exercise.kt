package advent.day11

import advent.getInput

fun main() {
    val pebbles = getInput(11).first().split(" ").map(String::toLong).let(::Stones)
    println(part1(pebbles))
    println(part2(pebbles))
}

private fun part1(stones: Stones): Long {
    return stones.change(25)
}

private fun part2(stones: Stones): Long {
    return stones.change(75)
}
