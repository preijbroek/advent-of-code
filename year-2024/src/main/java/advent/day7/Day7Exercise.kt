package advent.day7

import advent.getInput
import util.second

fun main() {
    val equations = getInput(7).map { it.split(": ") }
        .map { it.first().toLong() to it.second().split(" ").map(String::toLong) }
    println(part1(equations))
    println(part2(equations))
}

private fun part1(equations: List<Pair<Long, List<Long>>>): Long {
    return equations.filter { it.isSolvable(allowConcatenation = false) }
        .sumOf { (result, _) -> result }
}

private fun part2(equations: List<Pair<Long, List<Long>>>): Long {
    return equations.filter { it.isSolvable(allowConcatenation = true) }
        .sumOf { (result, _) -> result }
}

private fun Pair<Long, List<Long>>.isSolvable(allowConcatenation: Boolean): Boolean {
    val (result, arguments) = this
    return arguments.possibleResults(result, allowConcatenation).any { it == result }
}

private fun List<Long>.possibleResults(result: Long, allowConcatenation: Boolean): List<Long> {
    return if (size == 2) {
        listOfNotNull(
            first() + second(),
            first() * second(),
            if (allowConcatenation) first() concatenate second() else null,
        )
    } else {
        val last = last()
        val remainingResults = dropLast(1).possibleResults(result, allowConcatenation)
        return listOfNotNull(
            remainingResults.map { it + last },
            remainingResults.map { it * last },
            remainingResults.takeIf { allowConcatenation }?.map { it concatenate last }
        ).flatten().filter { it <= result }
    }
}

private infix fun Long.concatenate(other: Long) = """$this$other""".toLong()
