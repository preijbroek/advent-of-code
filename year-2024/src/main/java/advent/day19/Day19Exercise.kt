package advent.day19

import advent.getInputBlocks

fun main() {
    val (availableTowels, towelsToDisplay) = getInputBlocks(19).let { (first, second) ->
        first.split(", ").toSet() to second.split("\n")
    }
    println(part1(availableTowels, towelsToDisplay))
    println(part2(availableTowels, towelsToDisplay))
}

private fun part1(availableTowels: Set<String>, towelsToDisplay: List<String>): Int {
    val towelsRegex = availableTowels.joinToString(separator = "|", prefix = "(", postfix = ")*").let(::Regex)
    return towelsToDisplay.count(towelsRegex::matches)
}

private fun part2(availableTowels: Set<String>, towelsToDisplay: List<String>): Long {
    val knownDesigns = mutableMapOf<String, Long>()
    return towelsToDisplay.sumOf { availableTowels.findMatches(it, knownDesigns) }
}

private fun Set<String>.findMatches(towelToDisplay: String, knownDesigns: MutableMap<String, Long>): Long {
    return when {
        towelToDisplay.isEmpty() -> 1L
        towelToDisplay in knownDesigns -> knownDesigns.getValue(towelToDisplay)
        else -> filter { towelToDisplay.startsWith(it) }.sumOf {
            findMatches(towelToDisplay.removePrefix(it), knownDesigns)
        }.also { count ->
            knownDesigns[towelToDisplay] = count
        }
    }
}


