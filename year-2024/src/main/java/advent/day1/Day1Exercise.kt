package advent.day1

import advent.getInput
import util.abs
import util.second

fun main() {
    val input = getInput(1).map { it.split("   ").map(String::toInt) }
    println(part1(input))
    println(part2(input))
}

private fun part1(input: List<List<Int>>): Int {
    val firstSortedList = input.map { it.first() }.sorted()
    val secondSortedList = input.map { it.second() }.sorted()
    return (firstSortedList zip secondSortedList).sumOf { (it.first - it.second).abs() }
}

private fun part2(input: List<List<Int>>): Int {
    val firstSortedList = input.map { it.first() }
    val secondNumbersCount = input.map { it.second() }
        .groupingBy { it }
        .eachCount()
        .withDefault { 0 }
    return firstSortedList.sumOf { it * secondNumbersCount.getValue(it) }
}
