package advent.day16

import advent.getInput
import util.Position

fun main() {
    val reindeerMaze = getInput(16).flatMapIndexed { y, s ->
        s.mapIndexed { x, c -> Position(x, y) to c }
    }.toMap().let(::ReindeerMaze)
    println(part1(reindeerMaze))
    println(part2(reindeerMaze))
}

private fun part1(reindeerMaze: ReindeerMaze): Int {
    return reindeerMaze.minScore()
}

private fun part2(reindeerMaze: ReindeerMaze): Int {
    return reindeerMaze.bestPathTiles()
}
