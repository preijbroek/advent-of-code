package advent.day16

import util.Direction
import util.Position
import util.times

data class ReindeerMaze(
    private val grid: Map<Position, Char>,
) {

    private val start = grid.entries.first { (_, value) -> value == 'S' }.key
    private val end = grid.entries.first { (_, value) -> value == 'E' }.key

    fun minScore(): Int {
        return scores().filter { (key, _) -> key.first == end }.minOf { it.value }
    }

    fun bestPathTiles(): Int {
        val scores = scores()
        val endScore = scores.filter { (key, _) -> key.first == end }.values.min()
        val visited = scores.filter { (key, value) -> key.first == end && value == endScore }.keys.toMutableSet()
        val currents = ArrayDeque(scores.filter { (key, value) -> key.first == end && value == endScore }.map { it.key to it.value })
        while (currents.isNotEmpty()) {
            val (pair, score) = currents.removeFirst()
            val (position, direction) = pair
            val neighbours = position.neighbours()
            scores.filter { it.key.first in neighbours && it.key !in visited }
                .filter { (key, value) -> value == score - 1 - (1000 * (key.second != direction)) }
                .also {
                    visited.addAll(it.map { (key, _) -> key.first to key.second })
                    currents.addAll(it.map { (key, value) -> key to value })
                }
        }
        return visited.map { it.first }.distinct().size
    }

    private fun scores(): Map<Pair<Position, Direction>, Int> {
        val current = mutableSetOf(start to Direction.EAST)
        val scores = mutableMapOf((start to Direction.EAST) to 0)
        while (current.isNotEmpty()) {
            val next = current.flatMap { (position, direction) ->
                val next = generateSequence(direction) { it.right() }.take(4).toList()
                    .associateBy { position.toFlipped(it) }
                    .filter { (key, _) -> grid[key] != '#' }
                val newScores = next
                    .map { (key, value) -> (key to value) to scores.getValue(position to direction) + (1 + 1000 * (direction != value)) }
                    .filter { (key, value) -> scores[key] == null || scores.getValue(key) > value }
                    .toMap()
                scores.putAll(newScores)
                next.filter { (key, value) -> key to value in newScores }.map { it.key to it.value }
            }
            current.clear()
            current.addAll(next)
        }
        return scores
    }
}
