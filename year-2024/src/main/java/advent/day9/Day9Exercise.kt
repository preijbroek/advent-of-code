package advent.day9

import advent.getInput

fun main() {
    val amphipod = getInput(9).first()
        .mapIndexed { index, c -> index.toLong() to c.digitToInt().toLong() }
        .toMap()
        .let(::Amphipod)
    println(part1(amphipod))
    println(part2(amphipod))
}

private fun part1(amphipod: Amphipod): Long {
    return amphipod.compactChecksum()
}

private fun part2(amphipod: Amphipod): Long {
    return amphipod.fileIntegrityKeepingChecksum()
}
