package advent.day9

import util.isEven

data class Amphipod(
    private val line: Map<Long, Long>,
) {

    fun compactChecksum(): Long {
        var currentPosition = 0L
        val id = line.entries.flatMap { (position, value) ->
            val valueToAdd = position.takeIf { it.isFreeSpace() }?.idValue()
            (0L..<value).map {
                (currentPosition + it) to valueToAdd
            }.also {
                currentPosition += value
            }
        }.toMap().toMutableMap()
        var forward = 0L
        var backward = id.keys.max()
        while (backward > forward) {
            if (id[forward] != null) {
                forward++
            } else {
                id[forward] = id[backward]
                id.remove(backward)
                backward--
            }
        }
        return id.entries.sumOf { it.key * (it.value ?: 0L) }
    }

    fun fileIntegrityKeepingChecksum(): Long {
        data class FreeSpace(val position: Long, val size: Long)
        data class FileSpace(val position: Long, val size: Long, val id: Long)
        val freeSpaces = mutableListOf<FreeSpace>()
        val files = mutableListOf<FileSpace>()
        var currentPosition = 0L
        val result = line.entries.flatMap { (position, value) ->
            val entries = if (position.isFreeSpace()) {
                val id = position.idValue()
                files.add(FileSpace(currentPosition, value, id))
                (0L..<value).map {
                    (currentPosition + it) to id
                }
            } else {
                freeSpaces.add(FreeSpace(currentPosition, value))
                (0L..<value).map {
                    (currentPosition + it) to null
                }
            }
            entries.also { currentPosition += value }
        }.toMap().toMutableMap()
        files.reversed().forEach { (filePosition, fileSize, id) ->
            freeSpaces.withIndex().find { (_, freeSpace) ->
                freeSpace.position < filePosition && fileSize <= freeSpace.size
            }?.let { (index, freeSpace) ->
                (0L..<fileSize).forEach {
                    result[filePosition + it] = null
                    result[freeSpace.position + it] = id
                }
                val nextFreeSpace = FreeSpace(freeSpace.position + fileSize, freeSpace.size - fileSize)
                    .takeIf { it.size > 0 }
                if (nextFreeSpace != null) {
                    freeSpaces[index] = FreeSpace(freeSpace.position + fileSize, freeSpace.size - fileSize)
                } else {
                    freeSpaces.removeAt(index)
                }
            }
        }
        return result.entries.sumOf { it.key * (it.value ?: 0L) }
    }

    private fun Long.isFreeSpace() = isEven()

    private fun Long.idValue() = this shr 1
}
