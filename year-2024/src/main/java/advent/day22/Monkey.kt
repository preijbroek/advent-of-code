package advent.day22

import util.modulo

data class Monkey(
    private val initialSecret: Long,
) {

    private val allSecrets = generateSequence(initialSecret) {
        it.regenerate()
    }.take(2001).toList()

    private val prices = allSecrets.asSequence().map { it % 10 }
        .windowed(size = 5)
        .map { (first, second, third, fourth, fifth) ->
            listOf(second - first, third - second, fourth - third, fifth - fourth)
        }.mapIndexed { index, changes -> changes to allSecrets[index + 4] % 10L }
        .distinctBy { it.first }.toList().toMap()

    private fun Long.regenerate(): Long {
        val step1 = (this shl 6).mix(this).prune()
        val step2 = (step1 ushr 5).mix(step1).prune()
        return (step2 shl 11).mix(step2).prune()
    }

    private fun Long.mix(secret: Long) = this xor secret

    private fun Long.prune() = this modulo 16777216L

    fun secretAfter2000Steps(): Long {
        return allSecrets.last()
    }

    fun combine(knownSequences: MutableMap<List<Long>, Long>): MutableMap<List<Long>, Long> {
        prices.forEach { (list, value) ->
            knownSequences[list] = knownSequences.getValue(list) + value
        }
        return knownSequences
    }
}
