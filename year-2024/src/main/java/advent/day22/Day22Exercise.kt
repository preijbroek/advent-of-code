package advent.day22

import advent.getInput

fun main() {
    val monkeys = getInput(22).map(String::toLong).map(::Monkey)
    println(part1(monkeys))
    println(part2(monkeys))
}

private fun part1(monkeys: List<Monkey>): Long {
    return monkeys.sumOf(Monkey::secretAfter2000Steps)
}

private fun part2(monkeys: List<Monkey>): Long {
    val sequenceMaps = monkeys.fold(mutableMapOf<List<Long>, Long>().withDefault { 0L }) { acc, monkey ->
        monkey.combine(acc)
    }
    return sequenceMaps.values.max()
}
