package advent.day14

import advent.getInput
import util.product

fun main() {
    val robots = getInput(14).map(Robot::fromString)
    println(part1(robots))
    println(part2(robots))
}

private fun part1(robots: List<Robot>): Int {
    val quadrants = robots.mapNotNull { it.quadrant(100L) }
    return quadrants
        .groupingBy { it }.eachCount().values.product()
}

private fun part2(robots: List<Robot>): Long {
    return (0..(Robot.edge.x * Robot.edge.y)).first { time ->
        robots.map { it.position(time) }.distinct().size == robots.size
    }
}
