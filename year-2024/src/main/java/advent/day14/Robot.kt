package advent.day14

import util.Box
import util.Position
import util.div
import util.minus
import util.plus
import util.rem
import util.times

data class Robot(
    private val start: Position,
    private val velocity: Position,
) {

    fun position(time: Long): Position = (start + time * velocity) % edge

    fun quadrant(time: Long): Box? = position(time).let { current -> quadrants.firstOrNull { current in it } }

    companion object {
        val edge = Position(101, 103)
        val quadrants = listOf(
            Box(Position.CENTRE, edge / 2 - Position(1, 1)),
            Box(Position(edge.x / 2 + 1, 0), Position(edge.x - 1, edge.y / 2 - 1)),
            Box(Position(0, edge.y / 2 + 1), Position(edge.x / 2 - 1, edge.y - 1)),
            Box(edge / 2 + Position(1, 1), edge - Position(1, 1)),
        )

        fun fromString(s: String): Robot {
            return s.split(" ").map {
                it.drop(2).split(",").let { (x, y) -> Position(x.toLong(), y.toLong()) }
            }.let { (start, velocity) -> Robot(start, velocity) }
        }
    }
}
