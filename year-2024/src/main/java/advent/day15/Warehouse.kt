package advent.day15

import util.Movement
import util.Position
import util.second

data class Warehouse(
    private val grid: Map<Position, Char>,
    private val moves: List<Movement>,
    private val wall: Set<Position> = grid.keys.filter { grid[it] == '#' }.toSet(),
    private val robot: Position = grid.keys.first { grid[it] == '@' },
    private val boxes: Set<Position> = grid.keys.filter { grid[it] == 'O' }.toSet(),
) {

    fun gpsCoordinatesSum() = grid.filter { (_, value) -> value == 'O' }.keys.sumOf { 100 * it.y + it.x }

    fun moveRobot(): Warehouse {
        val newGrid = grid.toMutableMap()
        var newRobot = robot
        val newBoxes = boxes.toMutableSet()
        for (move in moves) {
            val nextRobot = newRobot.to(move)
            val boxesToMove = generateSequence(nextRobot) { it.to(move) }.takeWhile { it in newBoxes }.toSet()
            val possibleBlocker = (boxesToMove.lastOrNull() ?: newRobot).to(move)
            if (newGrid[possibleBlocker] != '#') {
                newGrid[newRobot] = '.'
                newRobot = nextRobot
                newGrid[newRobot] = '@'
                boxesToMove.lastOrNull()?.also {
                    val next = it.to(move)
                    newGrid[next] = 'O'
                    newBoxes.add(next)
                }
                boxesToMove.firstOrNull()?.also {
                    newBoxes.remove(it)
                }
            }
        }
        return Warehouse(
            grid = newGrid,
            moves = moves,
            wall = wall,
            robot = newRobot,
            boxes = newBoxes,
        )
    }

    fun expand(): ExpandedWareHouse {
        val expandedGrid = grid.entries.flatMap { (position, value) ->
            listOf(
                Position(2 * position.x, position.y) to value.leftExpanded(),
                Position(2 * position.x + 1, position.y) to value.rightExpanded(),
            )
        }.toMap()
        return ExpandedWareHouse(expandedGrid, moves)
    }

    private fun Char.leftExpanded(): Char = when (this) {
        'O' -> '['
        else -> this
    }

    private fun Char.rightExpanded(): Char = when (this) {
        'O' -> ']'
        '@' -> '.'
        else -> this
    }
}

data class ExpandedWareHouse(
    private val grid: Map<Position, Char>,
    private val moves: List<Movement>,
    private val wall: Set<Position> = grid.keys.filter { grid[it] == '#' }.toSet(),
    private val robot: Position = grid.keys.first { grid[it] == '@' },
) {

    fun gpsCoordinatesSum() = grid.filter { (_, value) -> value == '[' }.keys.sumOf { 100 * it.y + it.x }

    fun moveRobot(): ExpandedWareHouse {
        val newGrid = grid.toMutableMap()
        var newRobot = robot

        fun List<Position>.isBox() = size == 2 && newGrid[first()] == '[' && newGrid[second()] == ']'
        fun Position.verticalBox(move: Movement): List<Position> = listOfNotNull(
            to(move),
            to(move).to(Movement.LEFT).takeIf { newGrid[it] == '[' },
            to(move).to(Movement.RIGHT).takeIf { newGrid[it] == ']' },
        ).sortedWith(Position.byX())
        fun boxesToMove(move: Movement): Set<List<Position>> {
            return when (move) {
                Movement.LEFT, Movement.RIGHT ->
                    generateSequence(
                        listOf(
                            newRobot.to(move),
                            newRobot.to(move, 2)
                        ).sortedWith(Position.byX())
                    ) { box -> box.map { it.to(move, 2) } }.takeWhile { it.isBox() }.toSet()

                Movement.UP, Movement.DOWN ->
                    generateSequence(
                        listOf(
                            newRobot.verticalBox(move)
                        ).filter { it.isBox() }.toSet()
                    ) { boxes: Set<List<Position>> ->
                        boxes.flatMap { box -> box.map { it.verticalBox(move) } }.filter { it.isBox() }.toSet()
                    }.takeWhile { it.isNotEmpty() }.flatten().toSet()
            }
        }

        fun List<Position>.isBlocked(move: Movement) = any { newGrid[it.to(move)] == '#' }
        for (move in moves) {
            val boxesToMove = boxesToMove(move)
            if (boxesToMove.none { it.isBlocked(move) } && newGrid[newRobot.to(move)] != '#') {
                boxesToMove.forEach { box ->
                    newGrid.putAll(box.associateWith { '.' })
                }
                boxesToMove.forEach { box ->
                    newGrid[box.first().to(move)] = '['
                    newGrid[box.second().to(move)] = ']'
                }
                newRobot = newRobot.to(move)
            }
        }
        return ExpandedWareHouse(
            grid = newGrid,
            moves = moves,
            wall = wall,
            robot = newRobot,
        )
    }
}
