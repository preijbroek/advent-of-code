package advent.day15

import advent.getInputBlocks
import util.Movement
import util.Position

fun main() {
    val (gridString, movesString) = getInputBlocks(15)
    val grid = gridString.split("\n").flatMapIndexed { y, s ->
        s.mapIndexed { x, c -> Position(x, y) to c }
    }.toMap()
    val moves = movesString.mapNotNull { it.toMovement() }
    val warehouse = Warehouse(grid, moves)
    println(part1(warehouse))
    println(part2(warehouse))
}

private fun part1(warehouse: Warehouse): Long {
    return warehouse.moveRobot().gpsCoordinatesSum()
}

private fun part2(warehouse: Warehouse): Long {
    return warehouse.expand().moveRobot().gpsCoordinatesSum()
}

private fun Char.toMovement() = when (this) {
    '^' -> Movement.UP
    '>' -> Movement.RIGHT
    'v' -> Movement.DOWN
    '<' -> Movement.LEFT
    else -> null
}