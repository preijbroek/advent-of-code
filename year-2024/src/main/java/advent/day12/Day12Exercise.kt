package advent.day12

import advent.getInput
import util.Position

fun main() {
    val farm = getInput(12).flatMapIndexed { y: Int, s: String ->
        s.mapIndexed { x, c -> Position(x, y) to c }
    }.toMap().let(::Farm)
    println(part1(farm))
    println(part2(farm))
}

private fun part1(farm: Farm): Int {
    return farm.price()
}

private fun part2(farm: Farm): Int {
    return farm.discountedPrice()
}
