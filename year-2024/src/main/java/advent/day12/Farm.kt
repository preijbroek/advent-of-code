package advent.day12

import util.Movement
import util.Position

data class Farm(
    private val grid: Map<Position, Char>,
) {

    private val flowerBeds = grid.entries.groupBy({ it.value }, { it.key })
        .mapValues { it.value.separateFlowerBeds() }

    private fun List<Position>.separateFlowerBeds(): List<List<Position>> {
        val visited = mutableSetOf<Position>()
        val current = mutableSetOf<Position>()
        val flowerBeds = mutableListOf<List<Position>>()
        fun Position.visit() {
            current.add(this)
            visited.add(this)
            neighbours().filter { it !in visited && it in this@separateFlowerBeds }.forEach { it.visit() }
        }
        for (position in this) {
            if (position !in visited) {
                position.visit()
                flowerBeds.add(current.toList())
                current.clear()
            }
        }
        return flowerBeds
    }

    fun price() =
        flowerBeds.values.sumOf { it.sumOf { flowerBed -> flowerBed.price() } }

    fun discountedPrice() = flowerBeds.values.sumOf { it.sumOf { flowerBed -> flowerBed.discountedPrice() } }

    private fun List<Position>.price() = area() * perimeter()

    private fun List<Position>.discountedPrice() = area() * sides()

    private fun List<Position>.area() = size

    private fun List<Position>.perimeter() = sumOf { it.neighbours().count { neighbour -> neighbour !in this } }

    private fun List<Position>.sides(): Int {
        val set = toSet()
        fun borders(movement: Movement) = movement to filter { it.to(movement) !in set }
        return listOf(
            borders(Movement.UP).let { (movement, borders) ->
                movement to borders.sortedWith(Position.byY().thenComparing(Position.byX())).toSet()
            },
            borders(Movement.RIGHT).let { (movement, borders) ->
                movement to borders.sortedWith(Position.byX().thenComparing(Position.byY())).toSet()
            },
            borders(Movement.DOWN).let { (movement, borders) ->
                movement to borders.sortedWith(Position.byY().thenComparing(Position.byX().reversed())).toSet()
            },
            borders(Movement.LEFT).let { (movement, borders) ->
                movement to borders.sortedWith(Position.byX().thenComparing(Position.byY().reversed())).toSet()
            },
        ).sumOf { it.sides() }

    }

    private fun Pair<Movement, Set<Position>>.sides(): Int {
        val (movement, positions) = this
        val movementToCheck = movement.right()
        val visited = mutableSetOf<Position>()
        val current = mutableSetOf<Position>()
        var sides = 0
        fun Position.visit() {
            current.add(this)
            visited.add(this)
            to(movementToCheck).takeIf { it in positions }?.visit()
        }
        for (position in positions) {
            if (position !in visited) {
                position.visit()
                sides++
                current.clear()
            }
        }
        return sides
    }
}
