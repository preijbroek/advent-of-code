package advent.day23

import advent.getInput

fun main() {
    val connections = getInput(23).map { it.split("-") }
    val computers = connections.flatten().distinct().associateWith(::Computer)
    connections.forEach { (id1, id2) ->
        val computer1 = computers.getValue(id1)
        val computer2 = computers.getValue(id2)
        computer1.addLink(computer2)
        computer2.addLink(computer1)
    }
    println(part1(computers.values.toSet()))
    println(part2(computers.values.toSet()))
}

private fun part1(computers: Set<Computer>): Int {
    return computers.filter { it.id.startsWith('t') }.flatMap { it.interlinkedComputersGroupsOfThree() }
        .distinct().size
}

private fun part2(computers: Set<Computer>): String {
    return computers.map { it.fullyInterlinkedComputers() }.maxBy { it.size }
        .joinToString(separator = ",", transform = Computer::id)
}
