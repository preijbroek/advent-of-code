package advent.day23

data class Computer(
    val id: String,
) {

    private val linkedComputers = mutableSetOf<Computer>()

    fun addLink(other: Computer) = linkedComputers.add(other)

    fun interlinkedComputersGroupsOfThree(): Set<List<Computer>> {
        return linkedComputers.associateWith { this intersect it }
            .flatMap { (key, value) -> value.map { listOf(this, key, it).sortedBy(Computer::id) } }
            .toSet()
    }

    fun fullyInterlinkedComputers(): List<Computer> {
        var links = listOf(listOf(this))
        var previous = emptyList<List<Computer>>()
        while (links.isNotEmpty()) {
            previous = links
            links = links.flatMap { it.interlinked() }.distinct()
        }
        return previous.first()
    }

    private fun List<Computer>.interlinked(): List<List<Computer>> {
        return map { it.linkedComputers }
            .reduce { acc: Set<Computer>, computers: Set<Computer> -> acc intersect computers }
            .map { (this + it).sortedBy(Computer::id) }
    }

    private infix fun intersect(other: Computer) = linkedComputers intersect other.linkedComputers
}
