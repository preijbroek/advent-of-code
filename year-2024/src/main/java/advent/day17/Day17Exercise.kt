package advent.day17

import advent.getInputBlocks

fun main() {
    val computer = getInputBlocks(17).let(Computer::fromStrings)
    println(part1(computer))
    println(part2(computer))
}

private fun part1(computer: Computer): String {
    return computer.execute().joinToString(separator = ",")
}

private fun part2(computer: Computer): Long {
    return computer.lowestAValue()
}
