package advent.day17

import util.second
import util.third

data class Computer(
    private val registers: List<Long>,
    private val instructions: List<Long>,
) {
    fun lowestAValue(): Long {
        return instructions.reversed().fold(listOf(0L)) { acc, instruction ->
            acc.flatMap { a ->
                (0b000..0b111).mapNotNull { candidate ->
                    ((a shl 3) + candidate).takeIf {
                        execute(initialA = it).firstOrNull() == instruction
                    }
                }
            }
        }.min()
    }

    fun execute(initialA: Long = registers.first()): List<Long> {
        var registerA = initialA
        var registerB = registers.second()
        var registerC = registers.third()
        var instructionPointer = 0
        fun Long.comboOperand() = when (this) {
            in 0L..3L -> this
            4L -> registerA
            5L -> registerB
            6L -> registerC
            7L -> TODO("7 is not valid combo operator")
            else -> TODO("Unknown combo operand provided: $this")
        }

        fun literalOperand() = instructions[instructionPointer + 1]
        fun comboOperand() = literalOperand().comboOperand()

        val output = mutableListOf<Long>()
        while (instructionPointer < instructions.size) {
            when (val opCode = instructions[instructionPointer]) {
                0L -> {
                    registerA = registerA ushr comboOperand().toInt()
                    instructionPointer += 2
                }

                1L -> {
                    registerB = registerB xor literalOperand()
                    instructionPointer += 2
                }

                2L -> {
                    registerB = comboOperand() and 7L
                    instructionPointer += 2
                }

                3L -> {
                    if (registerA != 0L) {
                        instructionPointer = literalOperand().toInt()
                    } else {
                        instructionPointer += 2
                    }
                }

                4L -> {
                    registerB = registerB xor registerC
                    instructionPointer += 2
                }

                5L -> {
                    output.add(comboOperand() and 7)
                    instructionPointer += 2
                }

                6L -> {
                    registerB = registerA ushr comboOperand().toInt()
                    instructionPointer += 2
                }

                7L -> {
                    registerC = registerA ushr comboOperand().toInt()
                    instructionPointer += 2
                }

                else -> TODO("Unknown opcode provided: $opCode")
            }
        }
        return output
    }

    companion object {

        fun fromStrings(strings: List<String>): Computer {
            val (registersString, instructionsString) = strings
            val registers = registersString.split("\n").map { it.split(" ").last().toLong() }
            val instructions = instructionsString.split(" ").last().split(",").map(String::toLong)
            return Computer(registers, instructions)
        }
    }
}
