package advent.day13

import advent.getInputBlocks

fun main() {
    val clawMachines = getInputBlocks(13).map(ClawMachine::fromString)
    println(part1(clawMachines))
    println(part2(clawMachines))
}

private fun part1(clawMachines: List<ClawMachine>): Long {
    return clawMachines.mapNotNull(ClawMachine::cost).sum()
}


private fun part2(clawMachines: List<ClawMachine>): Long {
    return clawMachines.map(ClawMachine::corrected).mapNotNull(ClawMachine::cost).sum()
}
