package advent.day13

import util.Position
import util.fourth
import util.plus
import util.second
import util.third
import util.times

data class ClawMachine(
    private val buttonA: Position,
    private val buttonB: Position,
    private val prize: Position,
) {

    fun cost(): Long? {
        val divisor = buttonA determinant2d buttonB
        val aPresses = (prize determinant2d buttonB) / divisor
        val bPresses = (buttonA determinant2d prize) / divisor
        return (3L * aPresses + bPresses).takeIf { aPresses * buttonA + bPresses * buttonB == prize }
    }

    fun corrected() = copy(prize = prize + Position(10000000000000L, 10000000000000L))

    companion object {
        fun fromString(s: String): ClawMachine {
            val (buttonA, buttonB, prize) = s.split("\n")
                .map { it.split(",? ".toRegex()).map { value -> value.drop(2) } }
           return ClawMachine(
               buttonA = buttonA.let { Position(it.third().toInt(), it.fourth().toInt()) },
               buttonB = buttonB.let { Position(it.third().toInt(), it.fourth().toInt()) },
               prize = prize.let { Position(it.second().toInt(), it.third().toInt()) },
           )
        }
    }
}
