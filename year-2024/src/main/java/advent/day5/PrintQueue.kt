package advent.day5

import util.middle
import util.second

typealias Rule = List<Int>
typealias Update = List<Int>

data class PrintQueue(
    private val rules: List<Rule>,
    private val updates: List<Update>,
) {

    fun correctUpdatesSum() = correctUpdates().sumOf { it.middle() }

    fun incorrectFixedUpdatesSum() = incorrectUpdates().map { it.fixed() }.sumOf { it.middle() }

    private val beforeRules = rules.groupBy { it.second() }
        .mapValues { it.value.map(Update::first).toSet() }.withDefault { emptySet() }

    private fun correctUpdates() = updates.filter { it.isCorrect() }

    private fun incorrectUpdates() = updates.filterNot { it.isCorrect() }

    private fun Update.isCorrect(): Boolean {
        return mapIndexed { index, value ->
            val beforeValues = beforeRules.getValue(value)
                (subList(index + 1, size).toSet() intersect beforeValues).isEmpty()
        }.all { it }
    }

    private fun Update.fixed(): Update {
        return mapIndexed { index, value ->
            value to ((subList(0, index) + subList(index + 1, size)).toSet() intersect beforeRules.getValue(value))
        }.sortedBy { (_, update) -> update.size }
            .map { (value, _) -> value }
    }
}
