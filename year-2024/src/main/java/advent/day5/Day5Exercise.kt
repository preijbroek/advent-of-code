package advent.day5

import advent.getInputBlocks

fun main() {
    val (rulesString, updatesString) = getInputBlocks(5)
    val rules = rulesString.split("\n")
        .map { it.split("|").map(String::toInt) }
    val updates = updatesString.split("\n")
        .map { it.split(",").map(String::toInt) }
    val printQueue = PrintQueue(rules, updates)
    println(part1(printQueue))
    println(part2(printQueue))
}

private fun part1(printQueue: PrintQueue): Int {
    return printQueue.correctUpdatesSum()
}

private fun part2(printQueue: PrintQueue): Int {
    return printQueue.incorrectFixedUpdatesSum()
}