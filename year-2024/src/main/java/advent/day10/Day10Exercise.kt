package advent.day10

import advent.getInput
import util.Position

fun main() {
    val hikingTrails = getInput(10).flatMapIndexed { y: Int, s: String ->
        s.mapIndexed { x, c -> Position(x, y) to c.digitToInt() }
    }.toMap().let(::HikingTrails)
    println(part1(hikingTrails))
    println(part2(hikingTrails))
}

private fun part1(hikingTrails: HikingTrails): Int {
    return hikingTrails.score()
}

private fun part2(hikingTrails: HikingTrails): Int {
    return hikingTrails.rating()
}
