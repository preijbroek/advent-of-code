package advent.day10

import util.Position

data class HikingTrails(
    private val area: Map<Position, Int>,
) {

    fun score(): Int {
        return area.keys.sumOf { it.hikingTrails().score(rating = false) }
    }

    fun rating(): Int {
        return area.keys.sumOf { it.hikingTrails().score(rating = true) }
    }

    private fun Position.hikingTrails(current: Int = 0): List<Position> {
        return if (area[this] != current) {
            emptyList()
        } else if (area[this] == 9) {
            listOf(this)
        } else {
            neighbours().flatMap { it.hikingTrails(current = current + 1) }
        }
    }

    private fun List<Position>.score(rating: Boolean) =
        if (rating) {
            size
        } else {
            distinct().size
        }
}
