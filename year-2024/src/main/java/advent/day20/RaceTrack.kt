package advent.day20

import util.Position

data class RaceTrack(
    private val track: Map<Position, Char>
) {

    private val start = track.entries.first { (_, value) -> value == 'S' }.key
    private val end = track.entries.first { (_, value) -> value == 'E' }.key

    private val regularRace: List<Position> = regularRace()

    private fun regularRace(): List<Position> {
        val visited = mutableListOf(start)
        var current = start
        while (end !in visited) {
            val next = current.neighbours().first { it !in visited && track[it] != '#' }
            visited.add(next)
            current = next
        }
        return visited
    }

    fun findSavings(cheats: Int): Int {
        return (0..<regularRace.lastIndex).sumOf { current ->
            (current + 1..<regularRace.size).count { next ->
                val distance = regularRace[current] distanceTo regularRace[next]
                distance <= cheats && next - current - distance >= 100
            }
        }
    }
}
