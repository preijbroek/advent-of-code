package advent.day20

import advent.getInput
import util.Position

fun main() {
    val raceTrack = getInput(20).flatMapIndexed { y, s ->
        s.mapIndexed { x, c -> Position(x, y) to c }
    }.toMap().let(::RaceTrack)
    println(part1(raceTrack))
    println(part2(raceTrack))
}

private fun part1(raceTrack: RaceTrack): Int {
    return raceTrack.findSavings(cheats = 2)
}

private fun part2(raceTrack: RaceTrack): Int {
    return raceTrack.findSavings(cheats = 20)
}
