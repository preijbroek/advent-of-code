package advent.day24

import util.toLong

data class Wire(
    val id: String,
    private val condition: () -> Boolean
) {

    val evaluated by lazy { condition() }

    fun value() = evaluated.toLong()
}
