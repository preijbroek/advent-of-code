package advent.day24

import advent.getInputBlocks
import util.toBoolean

fun main() {
    val wires = mutableMapOf<String, Wire>()
    getInputBlocks(24).let { (firstString, secondString) ->
        firstString.split("\n").forEach {
            it.split(": ").also { (id, value) ->
                wires[id] = Wire(id) { value.toLong().toBoolean() }
            }
        }
        secondString.split("\n").forEach {
            it.split(" ").also { (wire1, operation, wire2, _, destination) ->
                when (operation) {
                    "AND" -> wires[destination] =
                        Wire(destination) { wires.getValue(wire1).evaluated && wires.getValue(wire2).evaluated }

                    "OR" -> wires[destination] =
                        Wire(destination) { wires.getValue(wire1).evaluated || wires.getValue(wire2).evaluated }

                    "XOR" -> wires[destination] =
                        Wire(destination) { wires.getValue(wire1).evaluated xor wires.getValue(wire2).evaluated }
                }
            }
        }
    }
    println(part1(wires))
    println(part2(wires))
}

private fun part1(wires: Map<String, Wire>): Long {
    return wires.filter { (id, _) -> id.startsWith("z") }.values
        .sortedByDescending { it.id }
        .map { it.value() }
        .joinToString(separator = "")
        .toLong(radix = 2)
}

private fun part2(wires: MutableMap<String, Wire>): String {
    // wires += wires.filter { (key, _) -> key.startsWith("x") || key.startsWith("y") }
    //    .map { (key, _) -> key to Wire(key) { true } }.toMap()
    // val xValues = wires.withPrefix("x").binaryValue().also(::println)
    // val yValues = wires.withPrefix("y").binaryValue().also(::println)
    // val expectedSum = (xValues.toLong(radix = 2) + yValues.toLong(radix = 2)).toString(radix = 2).also(::println)
    // val actualSum = wires.withPrefix("z").binaryValue().also(::println)
    // use above to do manual checks on the input, after commenting out part1. Note that this output is right
    // but only for my input.
    return listOf("djg", "z12", "sbg", "z19", "mcq", "hjm", "dsd", "z37").sorted().joinToString(separator = ",")
}

private fun Map<String, Wire>.withPrefix(prefix: String) = filter { (id, _) -> id.startsWith(prefix) }.values
    .sortedByDescending { it.id }

private fun List<Wire>.binaryValue() = map { it.value() }
    .joinToString(separator = "")
