package advent.day4

import advent.getInput
import util.Position

fun main() {
    val input = getInput(4).flatMapIndexed { y, line ->
        line.mapIndexed { x, c ->
            Position(x, y) to c
        }
    }.toMap().let(::XmasSearch)
    println(part1(input))
    println(part2(input))
}

private fun part1(input: XmasSearch): Int {
    return input.xmasCount()
}

private fun part2(input: XmasSearch): Int {
    return input.xMasCount()
}