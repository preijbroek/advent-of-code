package advent.day4

import util.Position
import util.plus

class XmasSearch(
    private val grid: Map<Position, Char>,
) {

    fun xmasCount(): Int = grid.keys.sumOf { it.xmasCount() }

    private fun Position.xmasCount() = 0 + isXmas(Position::north) +
        isXmas { north().east() } +
        isXmas(Position::east) +
        isXmas { east().south() } +
        isXmas(Position::south) +
        isXmas { south().west() } +
        isXmas(Position::west) +
        isXmas { west().north() }

    private fun Position.isXmas(step: Position.() -> Position): Boolean =
        grid[this] == 'X' &&
            grid[step()] == 'M' &&
            grid[step().step()] == 'A' &&
            grid[step().step().step()] == 'S'

    fun xMasCount() = grid.keys.count { it.isXMas() }

    private fun Position.isXMas(): Boolean =
        grid[this] == 'A' && isXmasUp() && isXmasDown()

    private fun Position.isXmasUp(): Boolean =
        (grid[north().east()] == 'M' && grid[south().west()] == 'S') ||
            (grid[north().east()] == 'S' && grid[south().west()] == 'M')

    private fun Position.isXmasDown(): Boolean =
        (grid[north().west()] == 'M' && grid[south().east()] == 'S') ||
            (grid[north().west()] == 'S' && grid[south().east()] == 'M')
}