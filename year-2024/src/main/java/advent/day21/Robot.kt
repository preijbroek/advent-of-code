package advent.day21

data class Robot(
    private val code: String,
) {

    private val numericCodePart = code.dropLast(1).toLong()

    fun complexity(numberOfRobots: Int) = shortestPath(numberOfRobots) * numericCodePart

    private fun shortestPath(numberOfRobots: Int): Long {
        val memo = mutableMapOf<Triple<Char, Char, Int>, Long>()
        return "A${numPadSequence()}".windowed(size = 2).sumOf { countPaths(memo, it[0], it[1], numberOfRobots) }
    }

    private fun numPadSequence() = "A$code".windowed(size = 2)
        .joinToString(separator = "") { numPadPaths.getValue(it[0] to it[1]) }

    private fun countPaths(
        memo: MutableMap<Triple<Char, Char, Int>, Long>,
        current: Char,
        next: Char,
        numberOfRobots: Int,
    ): Long {
        return memo.getOrPut(Triple(current, next, numberOfRobots)) {
            val nextSequence = dirPadPaths.getValue(current to next)
            if (numberOfRobots == 1) {
                nextSequence.length.toLong()
            } else {
                "A$nextSequence".windowed(size = 2).sumOf { countPaths(memo, it[0], it[1], numberOfRobots - 1) }
            }
        }
    }

    companion object {

        private val numPadPaths = mapOf(
            ('A' to 'A') to "A",
            ('A' to '0') to "<A",
            ('A' to '1') to "^<<A",
            ('A' to '2') to "<^A",
            ('A' to '3') to "^A",
            ('A' to '4') to "^^<<A",
            ('A' to '5') to "<^^A",
            ('A' to '6') to "^^A",
            ('A' to '7') to "^^^<<A",
            ('A' to '8') to "<^^^A",
            ('A' to '9') to "^^^A",
            ('0' to 'A') to ">A",
            ('0' to '0') to "A",
            ('0' to '1') to "^<A",
            ('0' to '2') to "^A",
            ('0' to '3') to "^>A",
            ('0' to '4') to "^^<A",
            ('0' to '5') to "^^A",
            ('0' to '6') to "^^>A",
            ('0' to '7') to "^^^<A",
            ('0' to '8') to "^^^A",
            ('0' to '9') to "^^^>A",
            ('1' to 'A') to ">>vA",
            ('1' to '0') to ">vA",
            ('1' to '1') to "A",
            ('1' to '2') to ">A",
            ('1' to '3') to ">>A",
            ('1' to '4') to "^A",
            ('1' to '5') to "^>A",
            ('1' to '6') to "^>>A",
            ('1' to '7') to "^^A",
            ('1' to '8') to "^^>A",
            ('1' to '9') to "^^>>A",
            ('2' to 'A') to "v>A",
            ('2' to '0') to "vA",
            ('2' to '1') to "<A",
            ('2' to '2') to "A",
            ('2' to '3') to ">A",
            ('2' to '4') to "<^A",
            ('2' to '5') to "^A",
            ('2' to '6') to "^>A",
            ('2' to '7') to "<^^A",
            ('2' to '8') to "^^A",
            ('2' to '9') to "^^>A",
            ('3' to 'A') to "vA",
            ('3' to '0') to "<vA",
            ('3' to '1') to "<<A",
            ('3' to '2') to "<A",
            ('3' to '3') to "A",
            ('3' to '4') to "<<^A",
            ('3' to '5') to "<^A",
            ('3' to '6') to "^A",
            ('3' to '7') to "<<^^A",
            ('3' to '8') to "<^^A",
            ('3' to '9') to "^^A",
            ('4' to 'A') to ">>vvA",
            ('4' to '0') to ">vvA",
            ('4' to '1') to "vA",
            ('4' to '2') to "v>A",
            ('4' to '3') to "v>>A",
            ('4' to '4') to "A",
            ('4' to '5') to ">A",
            ('4' to '6') to ">>A",
            ('4' to '7') to "^A",
            ('4' to '8') to "^>A",
            ('4' to '9') to "^>>A",
            ('5' to 'A') to "vv>A",
            ('5' to '0') to "vvA",
            ('5' to '1') to "<vA",
            ('5' to '2') to "vA",
            ('5' to '3') to "v>A",
            ('5' to '4') to "<A",
            ('5' to '5') to "A",
            ('5' to '6') to ">A",
            ('5' to '7') to "<^A",
            ('5' to '8') to "^A",
            ('5' to '9') to "^>A",
            ('6' to 'A') to "vvA",
            ('6' to '0') to "<vvA",
            ('6' to '1') to "<<vA",
            ('6' to '2') to "<vA",
            ('6' to '3') to "vA",
            ('6' to '4') to "<<A",
            ('6' to '5') to "<A",
            ('6' to '6') to "A",
            ('6' to '7') to "<<^A",
            ('6' to '8') to "<^A",
            ('6' to '9') to "^A",
            ('7' to 'A') to ">>vvvA",
            ('7' to '0') to ">vvvA",
            ('7' to '1') to "vvA",
            ('7' to '2') to "vv>A",
            ('7' to '3') to "vv>>A",
            ('7' to '4') to "vA",
            ('7' to '5') to "v>A",
            ('7' to '6') to "v>>A",
            ('7' to '7') to "A",
            ('7' to '8') to ">A",
            ('7' to '9') to ">>A",
            ('8' to 'A') to "vvv>A",
            ('8' to '0') to "vvvA",
            ('8' to '1') to "<vvA",
            ('8' to '2') to "vvA",
            ('8' to '3') to "vv>A",
            ('8' to '4') to "<vA",
            ('8' to '5') to "vA",
            ('8' to '6') to "v>A",
            ('8' to '7') to "<A",
            ('8' to '8') to "A",
            ('8' to '9') to ">A",
            ('9' to 'A') to "vvvA",
            ('9' to '0') to "<vvvA",
            ('9' to '1') to "<<vvA",
            ('9' to '2') to "<vvA",
            ('9' to '3') to "vvA",
            ('9' to '4') to "<<vA",
            ('9' to '5') to "<vA",
            ('9' to '6') to "vA",
            ('9' to '7') to "<<A",
            ('9' to '8') to "<A",
            ('9' to '9') to "A",
        )

        private val dirPadPaths = mapOf(
            ('A' to 'A') to "A",
            ('A' to '^') to "<A",
            ('A' to '<') to "v<<A",
            ('A' to 'v') to "<vA",
            ('A' to '>') to "vA",
            ('^' to 'A') to ">A",
            ('^' to '^') to "A",
            ('^' to '<') to "v<A",
            ('^' to 'v') to "vA",
            ('^' to '>') to "v>A",
            ('<' to 'A') to ">>^A",
            ('<' to '^') to ">^A",
            ('<' to '<') to "A",
            ('<' to 'v') to ">A",
            ('<' to '>') to ">>A",
            ('v' to 'A') to "^>A",
            ('v' to '^') to "^A",
            ('v' to '<') to "<A",
            ('v' to 'v') to "A",
            ('v' to '>') to ">A",
            ('>' to 'A') to "^A",
            ('>' to '^') to "<^A",
            ('>' to '<') to "<<A",
            ('>' to 'v') to "<A",
            ('>' to '>') to "A",
        )
    }
}
