package advent.day21

import advent.getInput

fun main() {
    val robots = getInput(21).map(::Robot)
    println(part1(robots))
    println(part2(robots))
}

private fun part1(robots: List<Robot>): Long {
    return robots.sumOf { it.complexity(numberOfRobots = 2) }
}

private fun part2(robots: List<Robot>): Long {
    return robots.sumOf { it.complexity(numberOfRobots = 25) }
}
