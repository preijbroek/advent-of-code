package advent.day18

import util.Box
import util.Position

data class CorruptedComputer(
    private val fallingBytes: Set<Position>,
) {

    fun shortestPathOut(n: Int): Int? {
        val fallingBytes = fallingBytes.take(n).toSet()
        val visited = mutableSetOf(start)
        val current = visited.toMutableSet()
        var steps = 0
        while (end !in visited && current.isNotEmpty()) {
            val next = current.flatMap { it.neighbours() }
                .filter { it !in visited && it in grid && it !in fallingBytes }
            visited.addAll(next)
            current.clear()
            current.addAll(next)
            steps++
        }
        return steps.takeIf { end in visited }
    }

    fun firstBlockingByte(): Position {
        var max = fallingBytes.size
        var min = 1024
        while (max - min > 1) {
            val index = (max + min) ushr 1
            val path = shortestPathOut(index)
            if (path == null) {
                max = index
            } else {
                min = index
            }
        }
        return fallingBytes.toList()[min]
    }

    companion object {
        private val start = Position.CENTRE
        private val end = Position(70, 70)
        private val grid = Box(start, end)
    }
}