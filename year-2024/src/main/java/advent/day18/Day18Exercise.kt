package advent.day18

import advent.getInput
import util.Position

fun main() {
    val corruptedComputer = getInput(18)
        .map { it.split(",").let { (x, y) -> Position(x.toLong(), y.toLong()) } }
        .toSet()
        .let(::CorruptedComputer)
    println(part1(corruptedComputer))
    println(part2(corruptedComputer))
}

private fun part1(corruptedComputer: CorruptedComputer): Int? {
    return corruptedComputer.shortestPathOut(n = 1024)
}

private fun part2(corruptedComputer: CorruptedComputer): String {
    return corruptedComputer.firstBlockingByte().let { "${it.x},${it.y}" }
}
