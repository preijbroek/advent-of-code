package advent.day8

import advent.getInput
import util.Position

fun main() {
    val antennaMap = getInput(8).flatMapIndexed { y, s ->
        s.mapIndexed { x, c -> Position(x, y) to c }
    }.toMap().let(::AntennaMap)
    println(part1(antennaMap))
    println(part2(antennaMap))
}

private fun part1(antennaMap: AntennaMap): Int {
    return antennaMap.antiNodesCount(useHarmonicEffect = false)
}

private fun part2(antennaMap: AntennaMap): Int {
    return antennaMap.antiNodesCount(useHarmonicEffect = true)
}