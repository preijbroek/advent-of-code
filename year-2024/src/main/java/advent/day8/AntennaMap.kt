package advent.day8

import util.Position
import util.minus
import util.plus
import util.second

data class AntennaMap(
    private val grid: Map<Position, Char>,
) {

    private val antennaGroups = grid.entries.filter { (_, value) -> value != '.' }.groupBy({ it.value }, { it.key })

    fun antiNodesCount(useHarmonicEffect: Boolean): Int {
        return antennaGroups.flatMap { (_, positions) -> positions.antiNodes(useHarmonicEffect) }
            .filter { it in grid }
            .distinct()
            .size
    }

    private fun List<Position>.antiNodes(useHarmonicEffect: Boolean): Set<Position> {
        return if (size < 2) {
            emptySet()
        } else if (size == 2) {
            val first = first()
            val second = second()
            val difference = first - second
            if (useHarmonicEffect) {
                val firsts = generateSequence(first) { next -> next + difference }
                    .takeWhile { it in grid }
                    .toList()
                val seconds = generateSequence(second) { next -> next - difference }
                    .takeWhile { it in grid }
                    .toList()
                listOf(firsts, seconds).flatten().toSet()
            } else {
                setOf(first + difference, second - difference)
            }
        } else {
            val first = first()
            val remaining = drop(1)
            return remaining.antiNodes(useHarmonicEffect) + remaining.flatMap { listOf(first, it).antiNodes(useHarmonicEffect) }
        }
    }
}
