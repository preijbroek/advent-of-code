package advent.day6

import util.Movement
import util.Position

data class Lab(
    private val area: Map<Position, Char>,
    private val guardStartingPoint: Position = area.entries.first { (_, value) -> value == '^' }.key,
) {

    fun moveGuardOut(): Int {
        return moveGuard().visited.size
    }

    fun countAllLoops(): Int {
        val initiallyVisited = moveGuard().visited
        return area.entries.filter { (position, _) -> position in initiallyVisited }
            .filter { (_, value) -> value != '^' }
            .count { (position, _) -> Lab(area + (position to '#'), guardStartingPoint).moveGuard().isInLoop() }
    }

    private fun moveGuard(): Guard {
        val guard = Guard(guardStartingPoint)
        while (!guard.isOutOfLab() && !guard.isInLoop()) {
            guard.move(area[guard.currentPosition.to(guard.currentMovement)])
        }
        return guard
    }

    private fun Guard.isOutOfLab() = currentPosition !in area
}

data class Guard(
    private val startingPosition: Position
) {

    private var _currentPosition: Position = startingPosition
    private var _currentMovement: Movement = Movement.UP
    private val _visited = mutableSetOf<Position>()
    val visited: Set<Position> get() = _visited

    val currentPosition: Position get() = _currentPosition
    val currentMovement: Movement get() = _currentMovement

    private val path = mutableSetOf<Pair<Position, Movement>>()

    fun isInLoop() = currentPosition to currentMovement in path

    fun move(nextTile: Char?) {
        _visited.add(_currentPosition)
        path.add(_currentPosition to _currentMovement)
        if (nextTile == '#') {
            _currentMovement = _currentMovement.right()
        } else {
            _currentPosition = _currentPosition.to(_currentMovement)
        }
    }
}
