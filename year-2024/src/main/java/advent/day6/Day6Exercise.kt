package advent.day6

import advent.getInput
import util.Position

fun main() {
    val lab = getInput(6).flatMapIndexed { y: Int, s: String ->
        s.mapIndexed { x, c -> Position(x, y) to c }
    }.toMap().let(::Lab)
    println(part1(lab))
    println(part2(lab))
}

private fun part1(lab: Lab): Int {
    return lab.moveGuardOut()
}

private fun part2(lab: Lab): Int {
    return lab.countAllLoops()
}
