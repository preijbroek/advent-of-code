package advent.day25

import advent.getInputBlocks

fun main() {
    val pinTypes = getInputBlocks(25).map(PinType::invoke)
    println(part1(pinTypes))
}

fun part1(pinTypes: List<PinType>): Int {
    return pinTypes.withIndex().sumOf { (index, pinType) ->
        pinTypes.subList(index, pinTypes.size).count(pinType::mayMatch)
    }
}
