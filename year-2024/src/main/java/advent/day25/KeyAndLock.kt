package advent.day25

sealed class PinType(
    shape: String,
) {
    private val combination: List<Int> = shape.split("\n").drop(1).dropLast(1).let { lines ->
        lines.indices.map { index -> lines.count { it[index] == '#' } }
    }

    fun mayMatch(other: PinType): Boolean {
        return when (other) {
            is Key -> this is Lock
            is Lock -> this is Key
        } && mayCombine(other)
    }

    private fun mayCombine(other: PinType): Boolean {
        return (combination zip other.combination).all { (pin1, pin2) -> pin1 + pin2 <= 5 }
    }

    companion object {
        operator fun invoke(shape: String): PinType {
            return if (shape.first() == '.') {
                Key(shape)
            } else {
                Lock(shape)
            }
        }
    }
}

class Key(shape: String) : PinType(shape)

class Lock(shape: String) : PinType(shape)