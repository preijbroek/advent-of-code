package advent.day2

import advent.getInput

fun main() {
    val input = getInput(2).map { it.split(" ").map(String::toInt) }
    println(part1(input))
    println(part2(input))
}

private fun part1(input: List<List<Int>>): Int {
    return input.filter { it.isSafe(withProblemDampener = false) }.size
}

private fun part2(input: List<List<Int>>): Int {
    return input.filter { it.isSafe(withProblemDampener = true) }.size
}

private fun List<Int>.isSafe(withProblemDampener: Boolean): Boolean =
    isGraduallyIncreasing()
        || isGraduallyDecreasing()
        || (withProblemDampener && indices.asSequence().map { this.toMutableList().apply { removeAt(it) } }.any { it.isSafe(withProblemDampener = false) } )

private fun List<Int>.isGraduallyIncreasing() =
    windowed(size = 2).all { (low, high) -> low < high && high - low <= 3 }

private fun List<Int>.isGraduallyDecreasing() =
    windowed(size = 2).all { (high, low) -> low < high && high - low <= 3 }
