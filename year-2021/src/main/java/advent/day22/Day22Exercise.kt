package advent.day22

import advent.getInput
import util.productOf
import util.second
import util.third

fun main() {
    val cubes = getInput(22).map(String::toCube)
    println(part1(cubes))
    println(part2(cubes))
}

private fun part1(cubes: List<Cube>): Long {
    val initializingCubes = cubes.filter(Cube::isPartOfInitializationProcedure)
    return switchAll(initializingCubes)
}

private fun part2(cubes: List<Cube>): Long {
    return switchAll(cubes)
}

private fun switchAll(cubes: List<Cube>): Long {
    var onCubes = mutableSetOf(cubes.first { it.switch == Switch.on })
    cubes.forEach { cube ->
        onCubes = onCubes.flatMap { it.splitWith(cube) }.toMutableSet()
    }
    return onCubes.sumOf(Cube::size)
}

private fun String.toCube(): Cube {
    val info = split("[ ,][xyz]=".toRegex())
    val switch = Switch.valueOf(info.first())
    val ranges = info.drop(1).map { it.split("..").map(String::toInt) }
        .map { it.first()..it.second() + 1 }
    return Cube(
        switch = switch,
        xRange = ranges.first(),
        yRange = ranges.second(),
        zRange = ranges.third()
    )
}

private data class Cube(
    val switch: Switch,
    val xRange: IntRange,
    val yRange: IntRange,
    val zRange: IntRange
) {

    fun size() = ranges().map { maxOf(0, it.count() - 1) }.productOf(Int::toLong)

    fun isPartOfInitializationProcedure() = ranges().all { it in -50..51 }

    private fun ranges() = listOf(xRange, yRange, zRange)

    fun splitWith(cube: Cube): Set<Cube> {
        return if (this isDisjointFrom cube)
            mutableSetOf(this)
        else {
            val xRanges = blocks(xRange, cube.xRange)
            val yRanges = blocks(yRange, cube.yRange)
            val zRanges = blocks(zRange, cube.zRange)
            xRanges.flatMap { xRange ->
                yRanges.flatMap { yRange ->
                    zRanges.map { zRange ->
                        Cube(switch, xRange, yRange, zRange)
                    }
                }
            }.filter { it in this && it !in cube }
                .toMutableSet()
        }.also { if (cube.switch == Switch.on) it.add(cube) }
    }

    private infix fun isDisjointFrom(cube: Cube) = !intersects(cube)

    private infix fun intersects(cube: Cube) = xRange intersects cube.xRange
            && yRange intersects cube.yRange
            && zRange intersects cube.zRange

    private operator fun contains(cube: Cube): Boolean {
        return cube.xRange in xRange
                && cube.yRange in yRange
                && cube.zRange in zRange
    }

}

private infix fun IntRange.intersects(other: IntRange) =
    first < other.last && other.first < last

private infix operator fun IntRange.contains(other: IntRange): Boolean {
    return first <= other.first && last >= other.last
}

private fun blocks(first: IntRange, second: IntRange): List<IntRange> {
    return listOf(first.first, first.last, second.first, second.last)
        .distinct()
        .sorted()
        .windowed(2)
        .map { it.first()..it.second() }
}

private enum class Switch {
    on,
    off
}