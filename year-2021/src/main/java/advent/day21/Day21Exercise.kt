package advent.day21

import advent.getInput
import util.nonZeroMod
import util.second

fun main() {
    val players = getInput(21).map { Player(it.last().digitToInt()) }
    println(part1(players))
    println(part2(players))
}

private fun part1(players: List<Player>): Int {
    val die = Die()
    var gameWithPlayers = listOf(players.first().withTurn(), players.second())
    while (gameWithPlayers.none(Player::hasWon)) {
        gameWithPlayers = gameWithPlayers.map { it.afterRoll(die).withTurnSwitched() }
    }
    return gameWithPlayers.first { !it.hasWon() }.score * die.numberOfRolls
}

private fun part2(players: List<Player>): Long {
    val diracDice = listOf(1, 2, 3)
    val steps = diracDice.flatMap { roll1 ->
        diracDice.flatMap { roll2 ->
            diracDice.map { roll3 ->
                roll1 + roll2 + roll3
            }
        }
    }
        .groupingBy { it }.eachCount().mapValues { it.value.toLong() }
    return quantumGame(players.first(), players.second(), steps).maxOrNull()!!
}

private fun quantumGame(playerWithTurn: Player, playerWithoutTurn: Player, steps: Map<Int, Long>): List<Long> {
    return if (playerWithoutTurn.hasWonQuantumGame()) {
        listOf(0, 1)
    } else {
        var playerWithTurnWins = 0L
        var playerWithoutTurnWins = 0L
        steps.forEach { (step, times) ->
            val nextPosition = (playerWithTurn.currentPosition + step).nonZeroMod(10)
            val nextScore = playerWithTurn.score + nextPosition
            val scores =
                quantumGame(playerWithoutTurn, Player(currentPosition = nextPosition, score = nextScore), steps)
            playerWithTurnWins += times * scores.second()
            playerWithoutTurnWins += times * scores.first()
        }
        listOf(playerWithTurnWins, playerWithoutTurnWins)
    }
}

private data class Player(
    var currentPosition: Int,
    var score: Int = 0,
    var hasTurn: Boolean = false,
) {

    fun afterRoll(die: Die): Player {
        return if (hasTurn) {
            var nextPosition = currentPosition
            repeat(3) {
                nextPosition = (nextPosition + die.roll()).nonZeroMod(10)
            }
            val nextScore = score + nextPosition
            return copy(currentPosition = nextPosition, score = nextScore)
        } else {
            this
        }
    }

    fun hasWon() = score >= 1000

    fun hasWonQuantumGame() = score >= 21

    fun withTurn() = copy(hasTurn = true)

    fun withTurnSwitched() = copy(hasTurn = !hasTurn)
}

private data class Die(var numberOfRolls: Int = 0, var lastRoll: Int = 100) {

    fun roll(): Int {
        lastRoll = (lastRoll + 1).nonZeroMod(100)
        numberOfRolls++
        return lastRoll
    }
}