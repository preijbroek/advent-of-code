package advent.day19

import advent.getInputBlocks
import util.Position
import util.minus
import util.plus
import util.second
import util.third

fun main() {
    val scanners = getInputBlocks(19).map(String::toScanner)
    scanners.first { it.id == 0 }.apply {
        correctOrientation = possibleOrientations.first()
        position = Position.CENTRE
    }
    val continuous3dRegion = Continuous3dRegion(scanners)
    println(part1(continuous3dRegion))
    println(part2(continuous3dRegion))
}

private fun part1(continuous3dRegion: Continuous3dRegion): Int {
    return continuous3dRegion.beacons.size
}

private fun part2(continuous3dRegion: Continuous3dRegion): Long {
    val scannerPositions = continuous3dRegion.scanners.mapNotNull(Scanner::position)
    return scannerPositions.flatMap { scannerPositions.map { position -> position to it } }
        .maxOf { (pos1, pos2) -> pos1.distanceTo(pos2) }
}

private fun String.toScanner(): Scanner {
    val lines = split("\n")
    val id = lines.first().split(" ").first { it.any { c -> c.isDigit() } }.toInt()
    val positions = lines.drop(1).map { it.split(",").map(String::toInt) }
        .map { Position(it.first(), it.second(), it.third()) }
    return Scanner(id, positions).withPossibleOrientations()
}

private data class Continuous3dRegion(val scanners: List<Scanner>) {

    val beacons = mutableSetOf<Position>()

    init {
        val determinedScanners = ArrayDeque<Scanner>()
        determinedScanners.addAll(scanners.filter(Scanner::isDetermined))
        while (determinedScanners.isNotEmpty()) {
            val orientingScanner = determinedScanners.removeFirst()
            scanners.asSequence()
                .filterNot(Scanner::isDetermined)
                .map { it to orientingScanner.correctOrientation?.orientationAndRelativePositionOf(it) }
                .filter { it.second != null }
                .forEach {
                    val scanner = it.first
                    val (orientation, position) = it.second!!
                    scanner.apply {
                        this.correctOrientation = orientation
                        this.position = orientingScanner.position!! + position
                    }
                    determinedScanners.add(scanner)
                }
        }
        scanners.mapNotNull(Scanner::absolutePositionsOfBeacons)
            .flatten()
            .forEach(beacons::add)
    }
}

private data class Scanner(
    val id: Int,
    val beacons: List<Position>
) {

    var correctOrientation: Scanner? = null
    var position: Position? = null

    val possibleOrientations = mutableListOf<Scanner>()


    fun isDetermined() = correctOrientation != null && position != null

    fun withPossibleOrientations(): Scanner {
        (0..3).flatMap { rotation ->
            (0..5).map { spin ->
                Scanner(id, beacons.map { it.spin(spin).rotate(rotation) })
            }
        }.forEach(possibleOrientations::add)
        return this
    }

    fun orientationAndRelativePositionOf(other: Scanner): Pair<Scanner, Position>? {
        return other.possibleOrientations.asSequence()
            .map { it to relativePosition(it) }
            .filter { (_, position) -> position != null }
            .map { it.first to it.second!! }
            .firstOrNull()
    }

    fun relativePosition(other: Scanner): Position? {
        return beacons.flatMap { other.beacons.map { otherBeacon -> otherBeacon - it } }
            .firstOrNull { relativePosition ->
                beacons.count { b -> other.beacons.any { b + relativePosition == it } } >= 12
            }
    }

    fun absolutePositionsOfBeacons() = correctOrientation?.takeIf { position != null }
        ?.beacons?.map { it - position!! }
}

private fun Position.rotate(times: Int = 1): Position =
    when (times) {
        0 -> this
        1 -> Position(-y, x, z)
        else -> rotate().rotate(times - 1)
    }

private fun Position.spin(times: Int = 1): Position =
    when (times) {
        0 -> this
        1 -> Position(x, -y, -z)
        2 -> Position(x, -z, y)
        3 -> Position(-y, -z, x)
        4 -> Position(-x, -z, -y)
        5 -> Position(y, -z, -x)
        else -> throw Exception()
    }