package advent.day2

import advent.getInput
import util.Position
import util.second

fun main() {
    val input = getInput(2).map {
        val instruction = it.split(" ")
        instruction.first() to instruction.second().toInt()
    }
    val subMarine = SubMarine()
    subMarine.execute(input)
    println(part1(subMarine))
    println(part2(subMarine))
}

private fun part1(subMarine: SubMarine): Int {
    return subMarine.productPosition1()
}

private fun part2(subMarine: SubMarine): Int {
    return subMarine.productPosition2()
}

private data class SubMarine(
    var positionRun1: Position = Position.CENTRE,
    var positionRun2: Position = Position.CENTRE,
    var aim: Int = 0
) {

    fun execute(instructions: List<Pair<String, Int>>) {
        instructions.forEach { (move, delta) ->
            when(move) {
                "forward" -> moveForward(delta)
                "down" -> moveDown(delta)
                "up" -> moveUp(delta)
            }
        }
    }

    fun moveForward(delta: Int) {
        positionRun1 = positionRun1.east(delta)
        positionRun2 = positionRun2.east(delta).north(delta * aim)
    }

    fun moveDown(delta: Int) {
        positionRun1 = positionRun1.north(delta)
        aim += delta
    }

    fun moveUp(delta: Int) {
        positionRun1 = positionRun1.south(delta)
        aim -= delta
    }

    fun productPosition1() = positionRun1.product()

    fun productPosition2() = positionRun2.product()

    fun Position.product() = x * y
}