package advent.day16

import advent.getInput

fun main() {
    val packet = getInput(16).first().map { it.digitToInt(16) }
        .map { it.toString(2) }
        .joinToString("") { it.padStart(4, '0') }
        .toPacket()
    println(part1(packet))
    println(part2(packet))
}

private fun part1(packet: Packet): Int {
    return packet.versionSum()
}

private fun part2(packet: Packet): Long {
    return packet.value()
}