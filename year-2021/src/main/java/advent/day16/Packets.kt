package advent.day16

import util.productOf
import util.second
import util.takeWhileInclusive
import util.times

sealed interface Packet {

    fun versionSum(): Int

    fun value(): Long
}

class Literal(
    private val version: Int,
    private val value: Long,
) : Packet {

    override fun versionSum() = version

    override fun value() = value
}

class Operator(
    private val version: Int,
    private val typeId: Int,
    private val subPackets: List<Packet>,
) : Packet {

    override fun versionSum() = version + subPackets.sumOf(Packet::versionSum)

    override fun value() = when (typeId) {
        0 -> subPackets.sumOf(Packet::value)
        1 -> subPackets.productOf(Packet::value)
        2 -> subPackets.minOf(Packet::value)
        3 -> subPackets.maxOf(Packet::value)
        5 -> 1L * (subPackets.first().value() > subPackets.second().value())
        6 -> 1L * (subPackets.first().value() < subPackets.second().value())
        7 -> 1L * (subPackets.first().value() == subPackets.second().value())
        else -> throw IllegalStateException(typeId.toString())
    }
}

fun String.toPacket() = toSubPacket().second

fun String.toSubPacket(): Pair<Int, Packet> {
    val version = substring(0..2).toInt(2)
    return when (val typeId = substring(3..5).toInt(2)) {
        4 -> {
            val (index, value) = substring(6).toLiteral()
            index + 6 to Literal(version, value)
        }
        else -> {
            val (index, subPackets) = substring(6).toOperator()
            index + 6 to Operator(version, typeId, subPackets)
        }
    }
}

fun String.toLiteral(): Pair<Int, Long> {
    return generateSequence(0) { it + 5 }
        .takeWhileInclusive { this[it] != '0' }
        .map { it to substring(it + 1, it + 5) }
        .reduce { acc, pair -> maxOf(acc.first, pair.first) to (acc.second + pair.second) }
        .let { (index, binaryString) -> index + 5 to binaryString.toLong(2) }
}

fun String.toOperator(): Pair<Int, List<Packet>> {
    val lengthTypeId = first().digitToInt(2)
    val subPackets = mutableListOf<Packet>()
    return when (lengthTypeId) {
        0 -> {
            var index = 16
            val totalBitLength = index + substring(1..15).toInt(2)
            while (index < totalBitLength) {
                val (subLength, packet) = substring(index).toSubPacket()
                subPackets.add(packet)
                index += subLength
            }
            index to subPackets
        }
        1 -> {
            var index = 12
            val subPacketsCount = substring(1..11).toInt(2)
            repeat(subPacketsCount) {
                val (subLength, packet) = substring(index).toSubPacket()
                subPackets.add(packet)
                index += subLength
            }
            index to subPackets
        }
        else -> throw IllegalStateException(lengthTypeId.toString())
    }
}