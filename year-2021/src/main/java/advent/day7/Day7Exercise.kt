package advent.day7

import advent.getInput
import util.abs

fun main() {
    val crabs = getInput(7).first().split(",").map(String::toInt).sorted()
    println(part1(crabs))
    println(part2(crabs))
}

private fun part1(crabs: List<Int>): Int {
    val maxCrab = crabs.first()
    val minCrab = crabs.last()
    return (minCrab..maxCrab).minOf { crabs.fuelCostsPart1(it) }
}

private fun part2(crabs: List<Int>): Int {
    val maxCrab = crabs.first()
    val minCrab = crabs.last()
    return (minCrab..maxCrab).minOf { crabs.fuelCostsPart2(it) }
}

private fun List<Int>.fuelCostsPart1(position: Int) = sumOf { (it - position).abs() }

private fun List<Int>.fuelCostsPart2(position: Int) = sumOf { (it - position).abs() * ((it - position).abs() + 1) / 2 }