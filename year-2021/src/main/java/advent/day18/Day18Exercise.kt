package advent.day18

import advent.getInput

fun main() {
    val snailFishNumbers = getInput(18).map(String::toSnailNumber)
    println(part1(snailFishNumbers))
    println(part2(snailFishNumbers))
}

private fun part1(snailFishNumbers: List<SnailFishNumber>): Int {
    return snailFishNumbers.reduce(SnailFishNumber::plus).magnitude()
}

private fun part2(snailFishNumbers: List<SnailFishNumber>): Int {
    return snailFishNumbers.flatMap { sfn1 -> snailFishNumbers.map { sfn2 -> sfn1 to sfn2 } }
        .filter { (sfn1, sfn2) -> sfn1 != sfn2 }
        .flatMap { (sfn1, sfn2) -> listOf(sfn1 + sfn2, sfn2 + sfn1) }
        .maxOf(SnailFishNumber::magnitude)
}

private fun String.toSnailNumber(): SnailFishNumber {
    var index = 0
    val values = mutableListOf<Pair<Int, Int>>()
    for (c in this) {
        when {
            c == '[' -> index++
            c == ']' -> index--
            c.isDigit() -> values.add(index to c.digitToInt())
        }
    }
    return SnailFishNumber(values)
}

private data class SnailFishNumber(val values: List<Pair<Int, Int>>) {

    operator fun plus(other: SnailFishNumber): SnailFishNumber {
        val resultValues = (values + other.values)
            .map { (index, value) -> index + 1 to value }
            .toMutableList()
        var previous: List<Pair<Int, Int>>
        do {
            previous = resultValues.toList()
            resultValues.explodeFirstDeeplyNestedPair()
            if (resultValues == previous) {
                resultValues.splitFirstLargeNumber()
            }
        } while (resultValues != previous)
        return SnailFishNumber(resultValues)
    }

    private fun MutableList<Pair<Int, Int>>.explodeFirstDeeplyNestedPair() {
        indexOfFirst { (index, _) -> index == 5 }
            .takeIf { it >= 0 }
            ?.also {
                val (leftIndex, leftValue) = this[it]
                val (_, rightValue) = this[it + 1]
                if (it != 0) {
                    val (previousIndex, previousValue) = this[it - 1]
                    this[it - 1] = previousIndex to previousValue + leftValue
                }
                if (it + 1 != this.lastIndex) {
                    val (nextIndex, nextValue) = this[it + 2]
                    this[it + 2] = nextIndex to nextValue + rightValue
                }
                this[it] = leftIndex - 1 to 0
                this.removeAt(it + 1)
            }
    }

    private fun MutableList<Pair<Int, Int>>.splitFirstLargeNumber() {
        indexOfFirst { (_, value) -> value >= 10 }
            .takeIf { it >= 0 }
            ?.also {
                val (index, value) = this[it]
                val leftValue = value / 2
                val rightValue = (value + 1) / 2
                this[it] = index + 1 to leftValue
                this.add(it + 1, index + 1 to rightValue)
            }
    }

    fun magnitude(): Int {
        var magnitudes: MutableList<Pair<Int, Int>?> = values.toMutableList()
        while (magnitudes.size > 1) {
            val deepestIndex = magnitudes.maxOf { it?.first ?: Int.MIN_VALUE }
            magnitudes.forEachIndexed { listIndex, pair ->
                if (pair != null && pair.first == deepestIndex) {
                    val (index, value) = pair
                    val (_, nextValue) = magnitudes[listIndex + 1]!!
                    magnitudes[listIndex] = index - 1 to 3 * value + 2 * nextValue
                    magnitudes[listIndex + 1] = null
                }
            }
            magnitudes = magnitudes.filterNotNull().toMutableList()
        }
        return magnitudes.first()!!.second
    }
}