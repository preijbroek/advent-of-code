package advent.day18

import advent.getInput

fun main() {
    val input = getInput(18).map(String::toSnailFishNumber)
    println(part1(input.map(FishNumber::clone)))
    println(part2(input.map(FishNumber::clone)))
}

private fun part1(snailFishNumbers: List<FishNumber>): Int {
    return snailFishNumbers
        .reduce { acc, fishNumber -> acc + fishNumber }
        .magnitude()
}

private fun part2(snailFishNumbers: List<FishNumber>): Int {
    return snailFishNumbers.flatMap { sn1 ->
        snailFishNumbers.map { sn2 ->
            sn1.clone() to sn2.clone()
        }
    }.filter { (sn1, sn2) -> sn1 != sn2 }
        .maxOf { (sn1, sn2) -> (sn1 + sn2).magnitude() }
}

private fun String.toSnailFishNumber(depth: Int = 1): FishNumber {
    return if (startsWith('[')) {
        var brackets = 0
        var middle = 0
        withIndex().forEach { (index, char) ->
            brackets += char.bracketNumber()
            index.takeIf { middle == 0 && char.isComma() && brackets == 1 }
                ?.also { middle = index }
        }
        val left = take(middle).drop(1)
        val right = drop(middle + 1).dropLast(1)
        CompositeFishNumber(left.toSnailFishNumber(depth + 1), right.toSnailFishNumber(depth + 1), depth)
    } else {
        PrimeFishNumber(toInt(), depth)
    }
}

private fun Char.bracketNumber() =
    when (this) {
        '[' -> 1
        ']' -> -1
        else -> 0
    }

private fun Char.isComma() = this == ','

private sealed class FishNumber(
    var parent: CompositeFishNumber? = null,
) {

    abstract var depth: Int

    abstract fun magnitude(): Int

    abstract fun exploding(): CompositeFishNumber?

    abstract fun splitting(): PrimeFishNumber?

    operator fun plus(fishNumber: FishNumber): FishNumber {
        val number: FishNumber = CompositeFishNumber(this.addDepth(), fishNumber.addDepth())
        var hasChanged: Boolean
        do {
            hasChanged = number.exploding()?.explode() ?: number.splitting()?.split() ?: false
        } while (hasChanged)
        return number
    }

    abstract fun addDepth(): FishNumber

    abstract fun rightMost(): PrimeFishNumber

    abstract fun leftMost(): PrimeFishNumber

    abstract fun clone(): FishNumber
}

private data class CompositeFishNumber(var left: FishNumber, var right: FishNumber, override var depth: Int = 1): FishNumber() {
    init {
        left.parent = this
        right.parent = this
    }

    override fun magnitude() = 3 * left.magnitude() + 2 * right.magnitude()

    override fun exploding(): CompositeFishNumber? {
        return if (depth == 5) {
            this
        } else {
            left.exploding() ?: right.exploding()
        }
    }

    override fun splitting(): PrimeFishNumber? = left.splitting() ?: right.splitting()

    override fun clone(): FishNumber = copy(left = left.clone(), right = right.clone())

    fun explode(): Boolean {
        val leftValue = (left as PrimeFishNumber).value
        val rightValue = (right as PrimeFishNumber).value
        deviatingParent(CompositeFishNumber::left)?.left?.rightMost()
            ?.apply { this.value += leftValue }
        deviatingParent(CompositeFishNumber::right)?.right?.leftMost()
            ?.apply { this.value += rightValue }
        parent?.replace(this, PrimeFishNumber(0, depth))
        return true
    }

    fun deviatingParent(side: CompositeFishNumber.() -> FishNumber): CompositeFishNumber? {
        var current = this
        while (current.parent != null) {
            val parent = current.parent
            val sideNumber = parent?.side()
            if (sideNumber !== current) {
                return parent
            } else {
                current = parent
            }
        }
        return current.parent
    }

    fun replace(previous: FishNumber, new: FishNumber) {
        when(previous) {
            left -> left = new
            right -> right = new
            else -> throw IllegalStateException(previous.toString())
        }
        new.parent = this
    }

    override fun addDepth(): FishNumber {
        depth++
        left.addDepth()
        right.addDepth()
        return this
    }

    override fun leftMost() = left.leftMost()

    override fun rightMost() = right.rightMost()

    override fun toString() = "[$left,$right]"

}

private data class PrimeFishNumber(var value: Int, override var depth: Int): FishNumber() {

    override fun magnitude() = value

    override fun exploding(): CompositeFishNumber? = null

    override fun splitting() = this.takeIf { value >= 10 }

    override fun clone(): FishNumber = copy()

    fun split(): Boolean {
        val leftValue = value / 2
        val rightValue = (value + 1) / 2
        val split = CompositeFishNumber(PrimeFishNumber(leftValue, depth + 1), PrimeFishNumber(rightValue, depth + 1), depth)
        parent?.replace(this, split)
        return true
    }

    override fun addDepth(): FishNumber {
        depth++
        return this
    }

    override fun leftMost() = this

    override fun rightMost() = this

    override fun toString() = value.toString()
}
