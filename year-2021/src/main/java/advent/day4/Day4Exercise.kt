package advent.day4

import advent.getInputBlocks

fun main() {
    val inputBlocks = getInputBlocks(4)
    val calledNumbers = inputBlocks.first().split(",")
        .map { it.trim().toInt() }.map { Cell(it) }
    val cardBlocks = inputBlocks.subList(1, inputBlocks.lastIndex)
    println(part1(calledNumbers.map { it.copy() }, cardBlocks))
    println(part2(calledNumbers.map { it.copy() }, cardBlocks))
}

private fun part1(calledNumbers: List<Cell>, cardBlocks: List<String>): Int {
    val bingo = bingo(calledNumbers, cardBlocks)
    var lastDrawn: Int
    var winner: BingoCard?
    do {
        lastDrawn = bingo.draw()
        winner = bingo.winner()
    } while (winner == null)
    return winner.remainingSum() * lastDrawn
}

private fun part2(calledNumbers: List<Cell>, cardBlocks: List<String>): Int {
    val bingo = bingo(calledNumbers, cardBlocks)
    var lastDrawn: Int
    var cardsStillPlaying: List<BingoCard>
    var loser: BingoCard? = null
    do {
        lastDrawn = bingo.draw()
        cardsStillPlaying = bingo.cardsStillPlaying()
        loser = cardsStillPlaying.takeIf { it.size == 1 }?.first() ?: loser
    } while (cardsStillPlaying.isNotEmpty())
    return loser!!.remainingSum() * lastDrawn
}

private fun bingo(calledNumbers: List<Cell>, cardBlocks: List<String>): Bingo {
    val calledNumbersMap = calledNumbers.associateBy { it.number }
    return cardBlocks.map {
        it.split("\n")
            .map { line ->
                line.trim().split(" +".toRegex())
                    .map { number -> calledNumbersMap.getValue(number.toInt()) }
            }
            .let(::BingoCard)
    }.let { Bingo(it, calledNumbers) }
}

private data class Bingo(val bingoCards: List<BingoCard>, val calledNumbers: List<Cell>) {

    private var index = 0

    fun draw(): Int {
        val cell = calledNumbers[index++]
        cell.call()
        return cell.number
    }

    fun winner() = bingoCards.firstOrNull { it.hasBingo() }

    fun cardsStillPlaying() = bingoCards.filterNot { it.hasBingo() }
}

private data class BingoCard(val numbers: List<List<Cell>>) {

    fun hasBingo() = numbers.indices.any {
        numbers[it].all(Cell::called) || numbers.all { line -> line[it].called }
    }

    fun remainingSum() = numbers.flatten().filterNot { it.called }.sumOf { it.number }

}

private data class Cell(val number: Int) {

    var called: Boolean = false

    fun call() {
        called = true
    }
}