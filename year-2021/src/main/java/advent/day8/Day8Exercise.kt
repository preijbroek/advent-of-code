package advent.day8

import advent.getInput
import util.second

fun main() {
    val input = getInput(8).map { it.split(" | ") }
    println(part1(input.map { it.second() }))
    println(part2(input))
}

private fun part1(numbers: List<String>): Int {
    return numbers.sumOf {
        it.split(" ").filter { s -> s.length in setOf(2, 3, 4, 7) }.size
    }
}

private fun part2(codes: List<List<String>>): Int {
    return codes.map { Display(it.first(), it.second()) }
        .sumOf { it.toIntCode() }
}

private data class Display(val input: String, val code: String) {

    fun toIntCode(): Int {
        val numbersMap = toNumbersMap()
        return code.split(" ").mapNotNull { numbersMap[it.toSet()] }
            .joinToString("")
            .toInt()
    }

    private fun toNumbersMap(): Map<Set<Char>, Int> {
        val split = input.split(" ").toSet()
        val numbers = split.toMutableSet()
        val one = numbers.first { it.length == 2 }
        val seven = numbers.first { it.length == 3 }
        val four = numbers.first { it.length == 4 }
        val eight = numbers.first { it.length == 7 }
        val nine = numbers.first { it.length == 6 && it.containsAll(four) }
        numbers.removeAll(one, seven, four, eight, nine)
        val three = numbers.first { it.length == 5 && it.containsAll(one) }
        numbers.remove(three)
        val bottomLeftLine = eight.filterNot(nine::contains).first()
        val five = numbers.first { it.length == 5 && !it.contains(bottomLeftLine) }
        numbers.remove(five)
        val two = numbers.first { it.length == 5 }
        numbers.remove(two)
        val six = numbers.first { !it.containsAll(one) }
        numbers.remove(six)
        val zero = numbers.first()
        return mapOf(
            zero to 0,
            one to 1,
            two to 2,
            three to 3,
            four to 4,
            five to 5,
            six to 6,
            seven to 7,
            eight to 8,
            nine to 9,
        ).mapKeys { it.key.toSet() }
    }
}

private fun <T> MutableSet<T>.removeAll(vararg ts: T) {
    removeAll(setOf(*ts))
}

private fun String.containsAll(other: String) =
    toSet().containsAll(other.toSet())