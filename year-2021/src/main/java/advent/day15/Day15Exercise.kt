package advent.day15

import advent.PrioritySetQueue
import advent.getInput
import util.Position
import util.toCharMap
import java.util.Comparator.comparing

fun main() {
    val cave = getInput(15).toCharMap().mapValues { it.value.digitToInt() }
    println(part1(cave))
    println(part2(cave))
}

private fun part1(cave: Map<Position, Int>): Int {
    return findShortestPath(cave)
}

private fun part2(cave: Map<Position, Int>): Int {
    val maxX = cave.keys.maxOf(Position::x) + 1
    val maxY = cave.keys.maxOf(Position::y) + 1
    val expandedCave = mutableMapOf<Position, Int>()
    for (x in 0..4) {
        for (y in 0..4) {
            cave.forEach { (key, value) ->
                expandedCave[key.north(maxY * y).east(maxX * x)] = (value + x + y).toRisk()
            }
        }
    }
    return findShortestPath(expandedCave)
}

private fun Int.toRisk() = if (this <= 9) this else this - 9

private fun findShortestPath(cave: Map<Position, Int>): Int {
    val destination = cave.keys.maxOfWith(Position.byX().thenComparing(Position.byY())) { it }
    val distances = cave.keys
        .associateWith { Int.MAX_VALUE }
        .toMutableMap()
    distances[Position.CENTRE] = 0
    val next = PrioritySetQueue<Position>(comparator = comparing(distances::getValue))
    next.offer(Position.CENTRE)
    val visited = mutableSetOf<Position>()
    while (destination !in visited) {
        val current = next.poll()
        val neighbours = current.neighbours().filter(cave::containsKey)
            .filter { it !in visited }
        val currentRisk = distances.getValue(current)
        neighbours.forEach {
            val risk = cave.getValue(it)
            if (currentRisk + risk < distances.getValue(it)) {
                distances[it] = currentRisk + risk
            }
        }
        visited.add(current)
        next.addAll(neighbours)
    }
    return distances.getValue(destination)
}