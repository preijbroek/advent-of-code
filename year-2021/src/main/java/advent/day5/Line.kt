package advent.day5

import util.Position
import util.minus
import util.normalize
import util.plus
import util.takeWhileInclusive
import util.times
import util.toSignedInt

data class Line(val end1: Position, val end2: Position) {

    val allPoints: Set<Position>
        get() {
            val delta = (end2 - end1).normalize() * Position((end2.x >= end1.x).toSignedInt(), (end2.y >= end1.y).toSignedInt())
            return generateSequence(end1) { it + delta }
                .takeWhileInclusive { it != end2 }
                .toSet()
        }

    fun isDiagonal() = end1.x != end2.x && end1.y != end2.y
}