package advent.day5

import advent.getInput
import util.Position
import util.map
import util.second

fun main() {
    val input = getInput(5).asSequence()
        .map { it.split(" -> ") }
        .map { it.first() to it.second() }
        .map { it.map { s -> s.split(",").map(String::toInt) } }
        .map { it.map { pos -> Position(pos.first(), pos.second()) } }
        .map { Line(it.first, it.second) }
        .toList()
    println(part1(input))
    println(part2(input))
}

private fun part1(input: List<Line>): Int {
    val diagram = Diagram()
    input.filterNot { it.isDiagonal() }
        .forEach { diagram.add(it) }
    return diagram.crossings().size
}

private fun part2(input: List<Line>): Int {
    val diagram = Diagram()
    input.forEach { diagram.add(it) }
    return diagram.crossings().size
}