package advent.day5

import util.Position

data class Diagram(private val lines: MutableMap<Position, Int> = mutableMapOf<Position, Int>().withDefault { 0 }) {

    fun add(line: Line) {
        line.allPoints.forEach { add(it) }
    }

    private fun add(position: Position) {
        lines[position] = lines.getValue(position) + 1
    }

    fun crossings(): Set<Position> {
        return lines.filterValues { it > 1 }.keys
    }
}