package advent.day3

import advent.getInput
import util.toDigitChar

fun main() {
    val input = getInput(3)
    println(part1(input))
    println(part2(input))
}

private fun part1(input: List<String>): Int {
    return input.gammaRate() * input.epsilonRate()
}

private fun part2(input: List<String>): Int {
    return input.oxygenGeneratorRate() * input.carbonDioxideScrubberRate()
}

private fun List<String>.gammaRate(): Int {
    return greekLetterRate(mostCommon = true)
}

private fun List<String>.epsilonRate(): Int {
    return greekLetterRate(mostCommon = false)
}

private fun List<String>.greekLetterRate(mostCommon: Boolean): Int {
    return first().indices.map { commonDigitAt(it, mostCommon) }
        .joinToString(separator = "")
        .toInt(2)
}

private fun List<String>.oxygenGeneratorRate() = elementRate(mostCommon = true)

private fun List<String>.carbonDioxideScrubberRate() = elementRate(mostCommon = false)

private fun List<String>.elementRate(mostCommon: Boolean): Int {
    val possibilities = this.toMutableList()
    var index = 0
    while (possibilities.size != 1) {
        possibilities.removeIf { it[index] != possibilities.commonDigitAt(index, mostCommon) }
        index++
    }
    return possibilities.first().toInt(2)
}

private fun List<String>.commonDigitAt(index: Int, mostCommon: Boolean): Char {
    val ones = count { it[index] == '1' }
    return (!((ones >= size - ones) xor mostCommon)).toDigitChar()
}