package advent.day20

import advent.getInputBlocks
import util.Position
import util.minus
import util.plus
import util.second
import util.toCharMap
import kotlin.math.pow

fun main() {
    val inputBlocks = getInputBlocks(20)
    val imageEnhancementAlgorithm =
        inputBlocks.first().mapIndexed { index, c -> index to c.toBinaryInt() }.toMap()
    val image = inputBlocks.second().split("\n").toCharMap()
        .mapValues { it.value.toBinaryInt() }
        .withDefault { 0 }
    val oceanTrench = OceanTrench(imageEnhancementAlgorithm, image)
    println(part1(oceanTrench))
    println(part2(oceanTrench))
}

private fun part1(oceanTrench: OceanTrench): Int {
    var next = oceanTrench
    repeat(2) {
        next = next.enhanced()
    }
    return next.litPixelCount()
}

private fun part2(oceanTrench: OceanTrench): Int {
    var next = oceanTrench
    repeat(50) {
        next = next.enhanced()
    }
    return next.litPixelCount()
}

private data class OceanTrench(
    val imageEnhancementAlgorithm: Map<Int, Int>,
    val image: Map<Position, Int>,
    val default: Int = 0,
    val topLeft: Position = Position.CENTRE,
    val bottomRight: Position = image.keys.maxWithOrNull(Position.byX().thenComparing(Position.byY()))!!
) {

    fun enhanced(): OceanTrench {
        val enhancedDefault = enhancedDefault()
        mutableMapOf<Position, Int>().withDefault { enhancedDefault }
        val nextImage = ((topLeft.x - 1)..(bottomRight.x + 1)).flatMap { x ->
            ((topLeft.y - 1)..(bottomRight.y + 1)).map { y ->
                val position = Position(x, y)
                val enhancementIndex = position.withAllNeighbours()
                    .asSequence()
                    .map(image::getValue)
                    .mapIndexed { index, value -> value * (2.toDouble().pow(8 - index).toInt()) }
                    .sum()
                position to imageEnhancementAlgorithm.getValue(enhancementIndex)
            }
        }.toMap().withDefault { enhancedDefault }
        return OceanTrench(
            imageEnhancementAlgorithm,
            nextImage,
            enhancedDefault,
            topLeft - Position(1, 1),
            bottomRight + Position(1, 1),
        )
    }

    fun enhancedDefault() = imageEnhancementAlgorithm.getValue(511 * default)

    fun litPixelCount() = image.values.sum()
}

private fun Char.toBinaryInt() =
    when (this) {
        '.' -> 0
        '#' -> 1
        else -> throw IllegalArgumentException(toString())
    }