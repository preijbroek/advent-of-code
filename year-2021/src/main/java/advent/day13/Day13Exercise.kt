package advent.day13

import advent.getInputBlocks
import util.Position
import util.second

fun main() {
    val inputBlocks = getInputBlocks(13)
    val dots = inputBlocks.first().split("\n")
        .map { it.split(",") }
        .map { Position(it.first().toInt(), it.second().toInt()) }
        .toSet()
    val folds = inputBlocks.second().split("\n")
        .map { it.split(" along ").second() }
        .map { it.split("=") }
        .map { it.first().first() to it.second().toInt() }
    println(part1(Manual(dots.toMutableSet()), folds))
    println(part2(Manual(dots.toMutableSet()), folds))
}

private fun part1(manual: Manual, folds: List<Pair<Char, Int>>): Int {
    val (axis, line) = folds.first()
    manual.fold(axis, line)
    return manual.dotsCount()
}

private fun part2(manual: Manual, folds: List<Pair<Char, Int>>): String {
    folds.forEach { (axis, line) -> manual.fold(axis, line) }
    return manual.picture()
}

private data class Manual(val dots: MutableSet<Position>) {

    fun fold(axis: Char, line: Int) {
        when (axis) {
            'x' -> foldX(line)
            'y' -> foldY(line)
        }
    }

    private fun foldX(line: Int) {
        val dotsToMove = dots.filter { it.x >= line }.toSet()
        dots.removeAll(dotsToMove)
        val movedDots = dotsToMove.map { Position(2 * line - it.x, it.y) }.toSet()
        dots.addAll(movedDots)
    }

    private fun foldY(line: Int) {
        val dotsToMove = dots.filter { it.y >= line }.toSet()
        dots.removeAll(dotsToMove)
        val movedDots = dotsToMove.map { Position(it.x, 2 * line - it.y) }.toSet()
        dots.addAll(movedDots)
    }

    fun dotsCount() = dots.size

    fun picture(): String {
        val maxX = dots.maxOf { it.x }
        val maxY = dots.maxOf { it.y }
        val builder = StringBuilder()
        for (y in 0..maxY) {
            for (x in (0..maxX)) {
                if (dots.contains(Position(x, y))) {
                    builder.append('#')
                } else {
                    builder.append(' ')
                }
            }
            builder.append('\n')
        }
        return builder.toString()
    }
}