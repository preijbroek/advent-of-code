package advent.day10

import advent.getInput
import util.middle

fun main() {
    val navigationSubSystem = getInput(10).map(::NavigationSubSystemLine)
    println(part1(navigationSubSystem))
    println(part2(navigationSubSystem))
}

private fun part1(navigationSubSystem: List<NavigationSubSystemLine>): Int {
    return navigationSubSystem.mapNotNull { it.firstIllegalCharacterScore() }.sum()
}


private fun part2(navigationSubSystem: List<NavigationSubSystemLine>): Long {
    return navigationSubSystem.filter { it.firstIllegalCharacterScore() == null }
        .map(NavigationSubSystemLine::completionScore)
        .sorted()
        .middle()
}

private data class NavigationSubSystemLine(val line: String) {

    private val completionList: List<Char>
    private val firstIllegalCharacter: Char?

    init {
        var ex: IllegalInstructionException? = null
        completionList = try {
            completionList()
        } catch (e: IllegalInstructionException) {
            ex = e
            emptyList()
        }
        firstIllegalCharacter = ex?.c
    }

    fun completionList(): List<Char> {
        val expectedClosingChars = mutableListOf<Char>()
        line.forEach {
            when {
                it.isOpening() -> expectedClosingChars.add(it.toComplement())
                it != expectedClosingChars.lastOrNull() -> throw IllegalInstructionException(it)
                else -> expectedClosingChars.removeLast()
            }
        }
        return expectedClosingChars.reversed()
    }

    fun Char.isOpening() = this in setOf('(', '[', '{', '<')

    fun Char.toComplement() = when (this) {
        '(' -> ')'
        '[' -> ']'
        '{' -> '}'
        '<' -> '>'
        else -> throw IllegalArgumentException("Invalid character supplied: $this")
    }

    fun firstIllegalCharacterScore() = firstIllegalCharacter?.score()

    fun Char.score() = when (this) {
        ')' -> 3
        ']' -> 57
        '}' -> 1197
        '>' -> 25137
        else -> null
    }

    fun completionScore(): Long {
        return completionList.fold(0L) { acc, c -> 5 * acc + c.completionScore() }
    }

    fun Char.completionScore() = when (this) {
        ')' -> 1L
        ']' -> 2L
        '}' -> 3L
        '>' -> 4L
        else -> throw IllegalArgumentException("Invalid character supplied: $this")
    }

}

private class IllegalInstructionException(val c: Char) : Exception()