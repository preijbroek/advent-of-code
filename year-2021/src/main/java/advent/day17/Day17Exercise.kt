package advent.day17

import advent.getInput
import util.Position
import util.abs
import util.plus
import util.second
import util.takeWhileInclusive

fun main() {
    val coordinates = getInput(17).first().substringAfter(": ")
        .split(", ")
        .map { it.substring(2) }
        .map { it.split("..").map(String::toInt) }
    val xCoordinates = coordinates.first()
    val yCoordinates = coordinates.second()
    println(part1(yCoordinates))
    println(part2(xCoordinates, yCoordinates))
}

private fun part1(yCoordinates: List<Int>): Int {
    val maxYVelocity = yCoordinates.minOrNull()!!.abs()
    return maxYVelocity * (maxYVelocity - 1) / 2
}

private fun part2(xCoordinates: List<Int>, yCoordinates: List<Int>): Int {
    val bottomLeft = Position(xCoordinates.minOrNull()!!, yCoordinates.minOrNull()!!)
    val topRight = Position(xCoordinates.maxOrNull()!!, yCoordinates.maxOrNull()!!)
    val target = Target(bottomLeft, topRight)
    val xRange = xCoordinates.toXRange()
    val yRange = yCoordinates.toYRange()
    return yRange.flatMap { y -> xRange.map { x -> Position(x, y) } }
        .count(target::willBeHit)
}

private fun List<Int>.toXRange(): IntRange {
    val minX: Int = minOrNull()!!
    var x = 0
    var step = 0
    while (x < minX) {
        step += 1
        x += step
    }
    return step..maxOrNull()!!
}

private fun List<Int>.toYRange(): IntRange {
    val maxY = minOrNull()!!.abs() - 1
    val minY = minOrNull()!!
    return minY..maxY
}

private data class Target(val bottomLeft: Position, val topRight: Position) {

    fun contains(position: Position) =
        bottomLeft.x <= position.x
                && position.x <= topRight.x
                && bottomLeft.y <= position.y
                && position.y <= topRight.y

    fun cannotBeReached(position: Position): Boolean {
        return position.x > topRight.x || position.y < bottomLeft.y
    }

    fun willBeHit(initialVelocity: Position): Boolean {
        var velocity = initialVelocity
        val last = generateSequence(Position.CENTRE) {
            val nextPosition = it + velocity
            velocity = velocity.newVelocity()
            nextPosition
        }.takeWhileInclusive {
            !contains(it) && !cannotBeReached(it)
        }.last()
        return contains(last)
    }

    fun Position.newVelocity() = Position(maxOf(x - 1, 0), y - 1)
}