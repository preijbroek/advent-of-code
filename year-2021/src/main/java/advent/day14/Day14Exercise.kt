package advent.day14

import advent.getInputBlocks
import util.second
import kotlin.collections.Map.*

fun main() {
    val inputBlocks = getInputBlocks(14)
    val insertionRules = inputBlocks.second().split("\n")
        .map { it.split(" -> ") }
        .associate { it.first() to it.second() }
    println(part1(inputBlocks.first(), insertionRules))
    println(part2(inputBlocks.first(), insertionRules))
}

private fun part1(template: String, insertionRules: Map<String, String>): Long {
    val occurrences = PolymerTemplate(template).insert(insertionRules, 10)
    return occurrences.maxOf(Entry<Char, Long>::value) - occurrences.minOf(Entry<Char, Long>::value)
}

private fun part2(template: String, insertionRules: Map<String, String>): Long {
    val occurrences = PolymerTemplate(template).insert(insertionRules, 40)
    return occurrences.maxOf(Entry<Char, Long>::value) - occurrences.minOf(Entry<Char, Long>::value)
}

private data class PolymerTemplate(val template: String) {

    private val initialPairs = template.windowed(2).groupingBy { it }
        .eachCount().mapValues { it.value.toLong() }

    fun insert(instructions: Map<String, String>, times: Int): Map<Char, Long> {
        var pairs = initialPairs
        val occurrences = template.groupingBy { it }.eachCount().mapValues { it.value.toLong() }.toMutableMap().withDefault { 0L }
        repeat(times) {
            val newPairs = mutableMapOf<String, Long>().withDefault { 0L }
            pairs.forEach { (pair, count) ->
                val addition = instructions.getValue(pair)
                occurrences[addition.first()] = occurrences.getValue(addition.first()) + count
                newPairs["${pair.first()}$addition"] = newPairs.getValue("${pair.first()}$addition") + count
                newPairs["$addition${pair.second()}"] = newPairs.getValue("$addition${pair.second()}") + count
            }
            pairs = newPairs
        }
        return occurrences
    }
}
