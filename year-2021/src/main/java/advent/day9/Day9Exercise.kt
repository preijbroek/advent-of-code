package advent.day9

import advent.getInput
import util.Position
import util.product
import util.toCharMap

fun main() {
    val floor = getInput(9).toCharMap().mapValues { it.value.digitToInt() }
    println(part1(floor))
    println(part2(floor))
}

private fun part1(floor: Map<Position, Int>): Int {
    return floor.lowPoints()
        .mapNotNull(floor::get)
        .sumOf { it + 1 }
}

private fun part2(floor: Map<Position, Int>): Int {
    return floor.lowPoints().map { it.basinSize(floor) }
        .sortedDescending()
        .take(3)
        .product()
}

private fun Map<Position, Int>.lowPoints() = keys.filter {
    it.neighbours().mapNotNull(this::get)
        .all { neighbourValue -> neighbourValue > getValue(it) }
}

private fun Position.basinSize(floor: Map<Position, Int>): Int {
    val basin = mutableSetOf(this)
    var lastAdded = setOf(this)
    while (lastAdded.isNotEmpty()) {
        val newlyAdded = lastAdded.flatMap(Position::neighbours).filterNot(basin::contains)
            .filter { floor[it] != null && floor[it] != 9 }.toSet()
        basin.addAll(newlyAdded)
        lastAdded = newlyAdded
    }
    return basin.size
}
