package advent.day6

import advent.getInput

fun main() {
    val input = getInput(6).first().split(",")
        .map(String::toInt)
        .groupingBy { it }
        .eachCount()
        .mapValues { it.value.toLong() }
        .withDefault { 0L }
    println(part1(input))
    println(part2(input))
}

private fun part1(input: Map<Int, Long>): Long {
    return input.spawn(80)
}

private fun part2(input: Map<Int, Long>): Long {
    return input.spawn(256)
}

private fun Map<Int, Long>.spawn(times: Int): Long {
    var fish = this
    repeat(times) {
        fish = fish.spawn()
    }
    return fish.values.sum()
}

private fun Map<Int, Long>.spawn(): Map<Int, Long> {
    val spawningFish = getValue(0)
    val nextCycle = (1..8).associate { it - 1 to getValue(it) }.toMutableMap()
    nextCycle[8] = spawningFish
    nextCycle[6] = nextCycle.getValue(6) + spawningFish
    return nextCycle
}
