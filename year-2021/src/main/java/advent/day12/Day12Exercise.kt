package advent.day12

import advent.getInput
import util.allDistinct
import util.second

fun main() {
    val input = getInput(12)
    val caves = input.flatMap { it.split("-") }
        .distinct()
        .map(::Cave)
    input.map { it.split("-") }
        .map { it.first() to it.second() }
        .map { (cave1, cave2) -> caves.first { it.name == cave1 } to caves.first { it.name == cave2 }  }
        .forEach { (cave1, cave2) ->
            cave1.addAdjacent(cave2)
            cave2.addAdjacent(cave1)
        }
    println(part1(caves))
    println(part2(caves))
}

private fun part1(caves: List<Cave>): Int {
    val start = caves.first(Cave::isStart)
    var paths = listOf(CavePath(listOf(start)))
    var pathCount = 0
    while (paths.isNotEmpty()) {
        paths = paths.flatMap(CavePath::next)
        pathCount += paths.count(CavePath::hasEnd)
    }
    return pathCount
}

private fun part2(caves: List<Cave>): Int {
    val start = caves.first(Cave::isStart)
    var paths = listOf(CavePath(listOf(start)))
    var pathCount = 0
    while (paths.isNotEmpty()) {
        paths = paths.flatMap(CavePath::nextLenient)
        pathCount += paths.count(CavePath::hasEnd)
    }
    return pathCount
}

private data class Cave(val name: String) {

    val adjacentCaves: MutableList<Cave> = mutableListOf()

    fun addAdjacent(cave: Cave) {
        adjacentCaves.add(cave)
    }

    fun isMiddleSmall() = !isStart() && !isEnd() && isSmall()

    fun isSmall() = name.all(Char::isLowerCase)

    fun isStart() = name == "start"

    fun isEnd() = name == "end"
}

private data class CavePath(val caves: List<Cave>) {

    fun next(): List<CavePath> {
        val last = caves.last()
        if (last.isEnd()) return emptyList()
        return last.adjacentCaves.filterNot { it.isSmall() && caves.contains(it) }
            .map { CavePath(caves + it) }
    }

    fun nextLenient(): List<CavePath> {
        val last = caves.last()
        if (last.isEnd()) return emptyList()
        val visitedSmallCaves = caves.filter(Cave::isMiddleSmall)
        return if (visitedSmallCaves.allDistinct()) {
            last.adjacentCaves.filterNot { it.isStart() }
                .map { CavePath(caves + it) }
        } else {
            last.adjacentCaves.filterNot { it.isSmall() && caves.contains(it) }
                .map { CavePath(caves + it) }
        }
    }



    fun hasEnd() = caves.last().isEnd()
}