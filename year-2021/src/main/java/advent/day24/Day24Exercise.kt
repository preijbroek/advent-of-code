package advent.day24

import advent.getInput
import util.third

fun main() {
    val lines = getInput(24)
    val equations = equations(lines)
    println(part1(equations))
    println(part2(equations))
}

private fun part1(equations: List<Triple<Int, Int, Int>>): Long {
    return equations.flatMap { (lowIndex, highIndex, difference) ->
        listOf(
            lowIndex to if (difference > 0) 9 - difference else 9,
            highIndex to if (difference > 0) 9 else 9 + difference
        )
    }.sortedBy { (index, _) -> index }
        .joinToString("") { (_, value) -> value.toString() }
        .toLong()
}

private fun part2(equations: List<Triple<Int, Int, Int>>): Long {
    return equations.flatMap { (lowIndex, highIndex, difference) ->
        listOf(
            lowIndex to if (difference > 0) 1 else 1 - difference,
            highIndex to if (difference > 0) 1 + difference else 1
        )
    }.sortedBy { (index, _) -> index }
        .joinToString("") { (_, value) -> value.toString() }
        .toLong()
}

private fun equations(lines: List<String>): List<Triple<Int, Int, Int>> {
    val yValues = mutableListOf<Pair<Int, Int>>()
    val equations = mutableListOf<Triple<Int, Int, Int>>()
    for (i in 0..13) {
        val zDivisor = lines.getValue(i, 4)
        if (zDivisor == 1) {
            val additionalYValue = lines.getValue(i, 15)
            yValues.add(Pair(i, additionalYValue))
        } else {
            val (oldIndex, value) = yValues.removeLast()
            val additionalXValue = lines.getValue(i, 5)
            equations.add(Triple(oldIndex, i, value + additionalXValue)) // oldIndex + values = newIndex
        }
    }
    return equations
}

private fun List<String>.getValue(iteration: Int, place: Int) =
    this[18 * iteration + place].split(" ").third().toInt()
