package advent.day25

import advent.getInput
import util.Direction
import util.Position
import util.toCharMap

fun main() {
    val input = getInput(25).toCharMap().mapValues { it.value.toDirection() }
    val (edgeX, edgeY, _) = input.maxOfWith(
        Position.byX().thenComparing(Position.byY()),
        Map.Entry<Position, Direction?>::key
    )
    val seaCucumbers = input.mapNotNull { (key, value) -> value?.let { key to it } }.toMap()
    val seaFloor = SeaFloor(seaCucumbers, edgeX + 1, edgeY + 1)
    println(part1(seaFloor))
}

private fun part1(seaFloor: SeaFloor): Int {
    return seaFloor.moveUntilStuck()
}

private fun Char.toDirection() = when (this) {
    '>' -> Direction.EAST
    'v' -> Direction.SOUTH
    else -> null
}

private data class SeaFloor(
    private val floorToEast: MutableSet<Position>,
    private val floorToSouth: MutableSet<Position>,
    private val xEdge: Int,
    private val yEdge: Int
) {

    constructor(seaCucumbers: Map<Position, Direction>, xEdge: Int, yEdge: Int) :
            this(
                seaCucumbers.filter { it.value == Direction.EAST }.map(Map.Entry<Position, Direction>::key)
                    .toMutableSet(),
                seaCucumbers.filter { it.value == Direction.SOUTH }.map(Map.Entry<Position, Direction>::key)
                    .toMutableSet(),
                xEdge, yEdge
            )

    private var rounds: Int = 0

    fun moveUntilStuck(): Int {
        do {
            rounds++
        } while (move() != 0)
        return rounds
    }

    private fun move(): Int {
        return moveEast() + moveSouth()
    }

    private fun moveEast(): Int {
        val movingEast = floorToEast.filter { it.canMoveEast() }.toSet()
        floorToEast.removeAll(movingEast)
        floorToEast.addAll(movingEast.map { it.toNextEastOnFloor() })
        return movingEast.size
    }

    private fun moveSouth(): Int {
        val movingSouth = floorToSouth.filter { it.canMoveSouth() }.toSet()
        floorToSouth.removeAll(movingSouth)
        floorToSouth.addAll(movingSouth.map { it.toNextSouthOnFloor() })
        return movingSouth.size
    }

    private fun Position.canMoveEast(): Boolean {
        val next = toNextEastOnFloor()
        return next !in floorToEast && next !in floorToSouth
    }

    private fun Position.toNextEastOnFloor(): Position {
        return copy(x = (x + 1).mod(xEdge))
    }

    private fun Position.canMoveSouth(): Boolean {
        val next = toNextSouthOnFloor()
        return next !in floorToEast && next !in floorToSouth
    }

    private fun Position.toNextSouthOnFloor(): Position {
        return copy(y = (y + 1).mod(yEdge))
    }
}

