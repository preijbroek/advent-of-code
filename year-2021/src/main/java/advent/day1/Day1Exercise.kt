package advent.day1

import advent.getInput

fun main() {
    val depths = getInput(1).map(String::toInt)
    println(part1(depths))
    println(part2(depths))
}

private fun part1(depths: List<Int>): Int {
    return depths.differenceCount(difference = 1)
}

private fun part2(depths: List<Int>): Int {
    return depths.differenceCount(difference = 3)
}

private fun List<Int>.differenceCount(difference: Int) =
    (difference..lastIndex).count { this[it - difference] < this[it] }


