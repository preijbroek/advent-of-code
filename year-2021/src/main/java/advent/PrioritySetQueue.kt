package advent

import java.util.PriorityQueue
import java.util.function.Predicate

class PrioritySetQueue<E>(initialCapacity: Int = 11, comparator: Comparator<E>? = null) :
    PriorityQueue<E>(initialCapacity, comparator) {

    private val elements: MutableSet<E> = HashSet()

    override fun add(element: E): Boolean {
        return !contains(element) && offer(element)
    }

    override fun addAll(elements: Collection<E>): Boolean {
        return super.addAll(elements)
    }

    override fun clear() {
        this.elements.clear()
        super.clear()
    }

    override fun remove(element: E): Boolean {
        this.elements.remove(element)
        return super.remove(element)
    }

    override fun remove(): E {
        val element = super.remove()
        this.elements.remove(element)
        return element
    }

    override fun removeAll(elements: Collection<E>): Boolean {
        return super.removeAll(elements.toSet())
    }

    override fun retainAll(elements: Collection<E>): Boolean {
        super.retainAll(elements.toSet())
        return this.elements.retainAll(elements.toSet())
    }

    override fun contains(element: E): Boolean {
        return this.elements.contains(element)
    }

    override fun containsAll(elements: Collection<E>): Boolean {
        return this.elements.containsAll(elements)
    }

    override fun removeIf(filter: Predicate<in E>): Boolean {
        this.elements.removeIf(filter)
        return super.removeIf(filter)
    }

    override fun offer(e: E): Boolean {
        return !contains(e) && this.elements.add(e) && super.offer(e)
    }

    override fun poll(): E {
        val element = super.poll()
        this.elements.remove(element)
        return element
    }
}