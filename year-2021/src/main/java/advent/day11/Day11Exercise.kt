package advent.day11

import advent.getInput
import util.Position
import util.toCharMap

fun main() {
    val octopuses = getInput(11).toCharMap()
        .map { (position, c) -> Octopus(Position(position.x, position.y), c.digitToInt()) }
        .associateBy(Octopus::position)
    println(part1(OctopusGrid(octopuses.mapValues { it.value.copy() })))
    println(part2(OctopusGrid(octopuses.mapValues { it.value.copy() })))
}

private fun part1(octopuses: OctopusGrid): Int {
    var flashCount = 0
    repeat(100) {
        flashCount += octopuses.flashRound()
    }
    return flashCount
}

private fun part2(octopuses: OctopusGrid): Int {
    var roundCount = 0
    while (!octopuses.haveAllFlashed()) {
        octopuses.flashRound()
        roundCount++
    }
    return roundCount
}

private data class OctopusGrid(val octopuses: Map<Position, Octopus>) {

    fun flashRound(): Int {
        octopuses.values.forEach(Octopus::gainEnergy)
        val flashingOctopuses = mutableSetOf<Position>()
        var lastReachedOctopuses = octopuses.entries.filter { it.value.isFullyCharged() }
            .map(Map.Entry<Position, Octopus>::key).toSet()
        while (lastReachedOctopuses.isNotEmpty()) {
            flashingOctopuses.addAll(lastReachedOctopuses)
            lastReachedOctopuses = lastReachedOctopuses.flatMap(Position::allNeighbours)
                .asSequence()
                .mapNotNull(octopuses::get)
                .map { octopus -> octopus.gainEnergy().let { octopus } }
                .filter(Octopus::isFullyCharged)
                .map(Octopus::position)
                .filterNot(flashingOctopuses::contains)
                .toSet()
        }
        flashingOctopuses.mapNotNull(octopuses::get).forEach(Octopus::flash)
        return flashingOctopuses.size
    }

    fun haveAllFlashed() = octopuses.values.all(Octopus::hasJustFlashed)
}

private data class Octopus(val position: Position, var energyLevel: Int) {

    fun flash() {
        energyLevel = 0
    }

    fun gainEnergy() {
        energyLevel++
    }

    fun isFullyCharged() = energyLevel > 9

    fun hasJustFlashed() = energyLevel == 0
}