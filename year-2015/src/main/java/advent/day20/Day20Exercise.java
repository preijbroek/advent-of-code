package advent.day20;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.IntStream;

public final class Day20Exercise {

    private Day20Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        int code = Integer.parseInt(Files.readAllLines(Paths.get("year-2015/src/main/resources/day20exercise.csv")).get(0));
        part1(code);
        part2(code);
    }

    private static void part1(final int code) {
        int maxElf = code / 10;
        int[] houses = IntStream.range(0, code / 10).map(v -> 0).toArray();
        for (int elf = 1; elf < maxElf; elf++) {
            for (int visited = elf; visited < maxElf; visited += elf) {
                houses[visited] += 10 * elf;
            }
        }
        IntStream.range(0, houses.length)
                .filter(i -> houses[i] >= code)
                .findFirst()
                .ifPresent(System.out::println);
    }

    private static void part2(final int code) {
        int maxElf = code / 10;
        int[] houses = IntStream.range(0, code / 10).map(v -> 0).toArray();
        for (int elf = 1; elf < maxElf; elf++) {
            int visitedHouses = 0;
            for (int visited = elf; visited < maxElf; visited += elf) {
                houses[visited] += 11 * elf;
                visitedHouses++;
                if (visitedHouses >= 50) {
                    break;
                }
            }
        }
        IntStream.range(0, houses.length)
                .filter(i -> houses[i] >= code)
                .findFirst()
                .ifPresent(System.out::println);
    }

}
