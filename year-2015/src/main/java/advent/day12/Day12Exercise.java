package advent.day12;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Day12Exercise {

    private Day12Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        String code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day12exercise.csv")).get(0);
        part1(code);
        part2(code);
    }

    private static void part1(final String code) {
        System.out.println(countNumbers(code));
    }

    private static void part2(final String code) {
        Pattern number = Pattern.compile("((-\\d+)|(\\d+))");
        Pattern combo = Pattern.compile("(\\{[-\\w,:\"]*})|(\\[[-\\w,\"]*])");
        String result = code;
        Matcher k;
        Matcher m = combo.matcher(result);
        while (m.find()) {
            int subsum = 0;
            if (!(m.group(0).matches("\\{[-\\w\\d,:\"]*}") && m.group(1).matches(".*:\"red\".*")))    //ignore {.:"red".}
            {
                k = number.matcher(m.group(0));
                while (k.find())                                                                            //sum up array or object
                    subsum += Integer.parseInt(k.group(0));
            }
            result = result.substring(0, m.start()) + subsum + result.substring(m.end());
            m = combo.matcher(result);
        }
        System.out.println(result);
    }

    private static int countNumbers(final String str) {
        return Arrays.stream(str.split("([^\\d-])"))
                .filter(s -> !s.isEmpty())
                .mapToInt(Integer::parseInt)
                .sum();
    }

}
