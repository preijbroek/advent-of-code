package advent.day22;

import java.util.List;

import static advent.day22.Spell.DRAIN;
import static advent.day22.Spell.MAGIC_MISSILE;
import static advent.day22.Spell.POISON;
import static advent.day22.Spell.RECHARGE;
import static advent.day22.Spell.SHIELD;

public final class Player {

    private final int defaultDamage;
    private final Player opponent;
    private int mana;
    private int hitPoints;

    private int manaSpent = 0;
    private int shieldPeriodsLeft = 0;
    private int poisonPeriodsLeft = 0;
    private int rechargePeriodsLeft = 0;
    private int rechargeEffect = 0;

    public Player(final int defaultDamage, final int hitPoints, final int mana, final List<String> code) {
        this(defaultDamage, hitPoints, mana, from(code));
    }

    private Player(final int defaultDamage, final int hitPoints) {
        this(defaultDamage, hitPoints, 0, (Player) null);
    }

    private Player(final int defaultDamage, final int hitPoints, final int mana, final Player opponent) {
        this.mana = mana;
        this.hitPoints = hitPoints;
        this.defaultDamage = defaultDamage;
        this.opponent = opponent;
    }

    private static Player from(final List<String> code) {
        int hitPoints = Integer.parseInt(code.get(0).split(" ")[2]);
        int defaultDamage = Integer.parseInt(code.get(1).split(" ")[1]);
        return new Player(defaultDamage, hitPoints);
    }

    public Player copy() {
        Player copy = new Player(defaultDamage, hitPoints, mana, copyOpponent());
        copy.rechargePeriodsLeft = rechargePeriodsLeft;
        copy.rechargeEffect = rechargeEffect;
        copy.poisonPeriodsLeft = poisonPeriodsLeft;
        copy.manaSpent = manaSpent;
        copy.shieldPeriodsLeft = shieldPeriodsLeft;
        return copy;
    }

    private Player copyOpponent() {
        return new Player(opponent.defaultDamage, opponent.hitPoints);
    }

    public void loseHitPoint() {
        hitPoints--;
    }

    public void runEffects() {
        if (poisonPeriodsLeft != 0) {
            opponent.hitPoints -= POISON.getStrength();
        }
        if (rechargePeriodsLeft != 0) {
            this.mana += rechargeEffect;
        }
        decrementPeriods();
    }

    private void decrementPeriods() {
        shieldPeriodsLeft = Math.max(shieldPeriodsLeft - 1, 0);
        poisonPeriodsLeft = Math.max(poisonPeriodsLeft - 1, 0);
        rechargePeriodsLeft = Math.max(rechargePeriodsLeft - 1, 0);
    }

    public boolean canCast(final Spell spell) {
        boolean canCast = this.mana >= spell.getCost();
        if (canCast && spell == SHIELD) {
            canCast = shieldPeriodsLeft < 1;
        } else if (canCast && spell == POISON) {
            canCast = poisonPeriodsLeft < 1;
        } else if (canCast && spell == RECHARGE) {
            canCast = rechargePeriodsLeft < 1;
        }
        return canCast;
    }

    public void cast(final Spell spell) {
        this.mana -= spell.getCost();
        this.manaSpent += spell.getCost();
        switch (spell) {
            case MAGIC_MISSILE:
                opponent.hitPoints -= MAGIC_MISSILE.getStrength();
                break;
            case DRAIN:
                opponent.hitPoints -= DRAIN.getStrength();
                this.hitPoints += DRAIN.getStrength();
                break;
            case SHIELD:
                this.shieldPeriodsLeft = SHIELD.getPeriods();
                break;
            case POISON:
                this.poisonPeriodsLeft = POISON.getPeriods();
                break;
            case RECHARGE:
                this.rechargeEffect = RECHARGE.getStrength();
                this.rechargePeriodsLeft = RECHARGE.getPeriods();
                break;
            default:
                throw new IllegalArgumentException(spell.toString());
        }
    }

    public void getHit() {
        int shieldEffect = shieldPeriodsLeft > 0 ? SHIELD.getStrength() : 0;
        this.hitPoints -= Math.max(1, opponent.defaultDamage - shieldEffect);
    }

    public boolean isAlive() {
        return hitPoints > 0;
    }

    public int manaSpent() {
        return manaSpent;
    }

    public int manaLeft() {
        return mana;
    }

    public Player getOpponent() {
        return opponent;
    }
}
