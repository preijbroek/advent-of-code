package advent.day22;

public enum Spell {

    MAGIC_MISSILE(53, 4, 1),
    DRAIN(73, 2, 1),
    SHIELD(113, 7, 6),
    POISON(173, 3, 6),
    RECHARGE(229, 101, 5);

    private final int cost;
    private final int strength;
    private final int periods;

    Spell(final int cost, final int strength, final int periods) {
        this.cost = cost;
        this.strength = strength;
        this.periods = periods;
    }

    public int getCost() {
        return cost;
    }

    public int getStrength() {
        return strength;
    }

    public int getPeriods() {
        return periods;
    }
}
