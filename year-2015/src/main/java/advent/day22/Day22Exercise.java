package advent.day22;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Comparator.comparing;

public final class Day22Exercise {

    private Day22Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day22exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        Player me = new Player(0, 50, 500, code);
        Queue<Player> states = new PriorityQueue<>(comparing(Player::manaSpent));
        states.add(me);
        AtomicInteger minMana = new AtomicInteger(Integer.MAX_VALUE);
        while (!states.isEmpty()) {
            Player current = states.poll();
            current.runEffects();
            for (Spell spell : Spell.values()) {
                if (current.canCast(spell)) {
                    Player next = current.copy();
                    next.cast(spell);
                    next.runEffects();
                    if (next.getOpponent().isAlive()) {
                        next.getHit();
                        if (next.isAlive() && next.manaLeft() > 0 && next.manaSpent() <= minMana.get()) {
                            states.add(next);
                        }
                    } else {
                        minMana.set(Math.min(minMana.get(), next.manaSpent()));
                        states.removeIf(player -> player.manaSpent() > minMana.get());
                    }
                }
            }
        }
        System.out.println(minMana.get());
    }

    private static void part2(final List<String> code) {
        Player me = new Player(0, 50, 500, code);
        Queue<Player> states = new PriorityQueue<>(comparing(Player::manaSpent));
        states.add(me);
        AtomicInteger minMana = new AtomicInteger(Integer.MAX_VALUE);
        while (!states.isEmpty()) {
            Player current = states.poll();
            current.loseHitPoint();
            if (!current.isAlive()) {
                continue;
            }
            current.runEffects();
            for (Spell spell : Spell.values()) {
                if (current.canCast(spell)) {
                    Player next = current.copy();
                    next.cast(spell);
                    next.runEffects();
                    if (next.getOpponent().isAlive()) {
                        next.getHit();
                        if (next.isAlive() && next.manaLeft() > 0 && next.manaSpent() <= minMana.get()) {
                            states.add(next);
                        }
                    } else {
                        minMana.set(Math.min(minMana.get(), next.manaSpent()));
                        states.removeIf(player -> player.manaSpent() > minMana.get());
                    }
                }
            }
        }
        System.out.println(minMana.get());
    }

}
