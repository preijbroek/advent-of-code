package advent.day13;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public final class Day13Exercise {

    private Day13Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day13exercise.csv"))
                .stream().map(s -> s.replaceAll("\\.", "")).collect(toList());
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        List<Integer> scores = new ArrayList<>();
        Set<Person> persons = code.stream()
                .map(s -> s.split(" ")[0])
                .map(Person::new)
                .collect(toSet());
        Set<Couple> couples = code.stream()
                .map(s -> findMatch(s, code))
                .map(array -> Couple.from(array[0], array[1]))
                .collect(toSet());
        fillScores(scores, new ArrayList<>(), persons, couples);
        System.out.println(scores.stream().max(Integer::compareTo).orElseThrow(IllegalStateException::new));
    }

    private static void part2(final List<String> code) {
        List<Integer> scores = new ArrayList<>();
        Set<Person> persons = code.stream()
                .map(s -> s.split(" ")[0])
                .map(Person::new)
                .collect(Collectors.toCollection(HashSet::new));
        Person paul = new Person("Paul");
        persons.add(paul);
        Set<Couple> couples = code.stream()
                .map(s -> findMatch(s, code))
                .map(array -> Couple.from(array[0], array[1]))
                .collect(Collectors.toCollection(HashSet::new));
        persons.forEach(person -> couples.add(new Couple(paul, person, 0)));
        fillScores(scores, new ArrayList<>(), persons, couples);
        System.out.println(scores.stream().max(Integer::compareTo).orElseThrow(IllegalStateException::new));
    }

    private static void fillScores(final List<Integer> scores, final List<Person> seatedPersons, final Set<Person> persons, final Set<Couple> couples) {
        if (seatedPersons.size() == persons.size()) {
            addScore(seatedPersons, couples, scores);
        } else {
            for (Person person : persons) {
                if (!seatedPersons.contains(person)) {
                    List<Person> newSeatedPersons = new ArrayList<>(seatedPersons);
                    newSeatedPersons.add(person);
                    fillScores(scores, newSeatedPersons, persons, couples);
                }
            }
        }
    }

    private static void addScore(final List<Person> seatedPersons, final Set<Couple> couples, final List<Integer> scores) {
        int score = 0;
        for (int i = 0; i < seatedPersons.size(); i++) {
            int index = i;
            int next = (i + 1) % seatedPersons.size();
            score += couples.stream()
                    .filter(couple -> couple.containsAll(Arrays.asList(seatedPersons.get(index), seatedPersons.get(next))))
                    .findAny()
                    .orElseThrow(IllegalStateException::new)
                    .happinessScore();
        }
        scores.add(score);
    }

    private static String[] findMatch(final String s, final List<String> code) {
        return code.stream()
                .filter(line -> subject(s).equals(object(line)) && object(s).equals(subject(line)))
                .map(line -> new String[]{s, line})
                .findAny()
                .orElseThrow(IllegalStateException::new);
    }

    private static String subject(final String line) {
        return line.split(" ")[0];
    }

    private static String object(final String line) {
        String[] stringArray = line.split(" ");
        return stringArray[stringArray.length - 1];
    }


}
