package advent.day13;

import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Stream;

public final class Couple implements Comparable<Couple> {

    private final Person person1;
    private final Person person2;
    private final int happinessScore;

    public Couple(final Person person1, final Person person2, final int happinessScore) {
        this.person1 = person1;
        this.person2 = person2;
        this.happinessScore = happinessScore;
    }

    public static Couple from(final String line1, final String line2) {
        Person person1 = new Person(line1.split(" ")[0]);
        Person person2 = new Person(line2.split(" ")[0]);
        int score = addScore(line1) + addScore(line2);
        return new Couple(person1, person2, score);
    }

    private static int addScore(final String line) {
        if (line.contains("gain")) {
            return Integer.parseInt(line.replaceAll("\\D", ""));
        } else {
            return -Integer.parseInt(line.replaceAll("\\D", ""));
        }
    }

    public int happinessScore() {
        return happinessScore;
    }

    public boolean containsAll(final Collection<Person> persons) {
        return Stream.of(person1, person2).allMatch(persons::contains);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Couple couple = (Couple) o;
        return happinessScore == couple.happinessScore &&
                hasSamePersons(couple);
    }

    private boolean hasSamePersons(final Couple couple) {
        return (Objects.equals(person1, couple.person1) && Objects.equals(person2, couple.person2))
                || (Objects.equals(person1, couple.person2) && Objects.equals(person2, couple.person1));
    }

    @Override
    public int hashCode() {
        return Objects.hash(person1, person2, happinessScore) + Objects.hash(person2, person1, happinessScore);
    }

    @Override
    public int compareTo(final Couple o) {
        return Comparator.comparing(Couple::happinessScore).compare(this, o);
    }
}
