package advent.day10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Day10Exercise {

    private Day10Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        String code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day10exercise.csv")).get(0);
        part1(code);
        part2(code);
    }

    private static void part1(final String code) {
        String result = code;
        for (int i = 0; i < 40; i++) {
            result = LookAndSay.apply(result);
        }
        System.out.println(result.length());
    }

    private static void part2(final String code) {
        String result = code;
        for (int i = 0; i < 50; i++) {
            result = LookAndSay.apply(result);
        }
        System.out.println(result.length());
    }

}
