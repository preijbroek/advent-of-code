package advent.day10;

public final class LookAndSay {

    private LookAndSay() {
    }

    public static String apply(final String string) {
        validate(string);
        int times = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            if (i == 0 || string.charAt(i) == string.charAt(i - 1)) {
                times++;
            } else {
                sb.append(times).append(string.charAt(i - 1));
                times = 1;
            }
            if (i == string.length() - 1) {
                sb.append(times).append(string.charAt(i));
            }
        }
        return sb.toString();
    }

    private static void validate(final String string) {
        if (!string.chars()
                .mapToObj(value -> (char) value)
                .allMatch(Character::isDigit)) {
            throw new IllegalArgumentException("Invalid string to play: " + string);
        }
    }

}
