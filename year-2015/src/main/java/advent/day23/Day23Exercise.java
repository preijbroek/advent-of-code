package advent.day23;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class Day23Exercise {

    private Day23Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day23exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        Computer computer = new Computer(code);
        computer.run();
        System.out.println(computer.getValueB());
    }

    private static void part2(final List<String> code) {
        Computer computer = new Computer(code);
        computer.setInitialValueForA(1);
        computer.run();
        System.out.println(computer.getValueB());
    }

}
