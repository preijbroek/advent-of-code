package advent.day23;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public final class Computer {

    private final List<String> code;
    private final Map<String, Register> registers = new HashMap<>();
    private int instruction = 0;

    public Computer(final List<String> code) {
        this.code = code;
        registers.put("a", new Register("a", 0));
        registers.put("b", new Register("b", 0));
    }

    public void run() {
        while (instruction < code.size()) {
            String[] instructions = code.get(instruction).split(" ");
            if (instructions[0].startsWith("hlf")) {
                registers.get(instructions[1]).half();
                instruction++;
            } else if (instructions[0].startsWith("tpl")) {
                registers.get(instructions[1]).triple();
                instruction++;
            } else if (instructions[0].startsWith("inc")) {
                registers.get(instructions[1]).increment();
                instruction++;
            } else if (instructions[0].startsWith("jmp")) {
                instruction += Integer.parseInt(instructions[1]);
            } else if (instructions[0].startsWith("jie")) {
                String register = instructions[1].substring(0, 1);
                if ((registers.get(register).value & 1) == 0) {
                    instruction += Integer.parseInt(instructions[2]);
                } else {
                    instruction++;
                }
            } else if (instructions[0].startsWith("jio")) {
                String register = instructions[1].substring(0, 1);
                if (registers.get(register).value == 1) {
                    instruction += Integer.parseInt(instructions[2]);
                } else {
                    instruction++;
                }
            } else {
                throw new IllegalArgumentException(code.get(instruction));
            }
        }
    }

    public void setInitialValueForA(int valueA) {
        registers.get("a").value = valueA;
    }

    public int getValueB() {
        return registers.get("b").value;
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Computer computer = (Computer) o;
        return Objects.equals(code, computer.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    private static final class Register {

        private final String name;
        private int value;

        private Register(final String name, final int initialValue) {
            this.name = name;
            this.value = initialValue;
        }

        private void increment() {
            value++;
        }

        private void triple() {
            value *= 3;
        }

        private void half() {
            value >>= 1;
        }
    }
}
