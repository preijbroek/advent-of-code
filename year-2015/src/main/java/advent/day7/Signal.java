package advent.day7;

import java.util.Objects;

public final class Signal {

    private final String wire;
    private final int bits;
    private final boolean assigned;

    private Signal(final String wire, final int bits, final boolean assigned) {
        this.wire = wire;
        this.bits = bits;
        this.assigned = assigned;
    }

    public String getWire() {
        return wire;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public int getBits() {
        return bits;
    }

    public static Signal empty(final String wire) {
        return new Signal(wire, (int) 0, false);
    }

    public static Signal provide(final String wire, final int bits) {
        return new Signal(wire, bits, true);
    }

    public static Signal provide(final String wire, final Signal signal) {
        return new Signal(wire, signal.bits, true);
    }

    public static Signal and(final String wire, final Signal first, final Signal second) {
        return new Signal(wire, (int) (first.bits & second.bits), true);
    }

    public static Signal and(final String wire, final int bit, final Signal signal) {
        return new Signal(wire, (int) (bit & signal.bits), true);
    }

    public static Signal or(final String wire, final Signal first, final Signal second) {
        return new Signal(wire, (int) (first.bits | second.bits), true);
    }

    public static Signal or(final String wire, final int bit, final Signal signal) {
        return new Signal(wire, (int) (bit | signal.bits), true);
    }

    public static Signal lShift(final String wire, final Signal signal, final int amount) {
        return new Signal(wire, (int) (signal.bits << amount), true);
    }

    public static Signal rShift(final String wire, final Signal signal, final int amount) {
        return new Signal(wire, (int) (signal.bits >> amount), true);
    }

    public static Signal not(final String wire, final Signal signal) {
        return new Signal(wire, (int) ~signal.bits, true);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Signal signal = (Signal) o;
        return Objects.equals(wire, signal.wire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wire);
    }
}
