package advent.day7

import advent.getInput
import util.second

fun main() {
    val part1Result = part1()
    println(part1Result)
    println(part2(part1Result))
}

private fun part1(): Int {
    val wires = wires()
    return wires.getValue("a").value()
}

fun part2(part1Result: Int): Int {
    val wires = wires(part1Result)
    return wires.getValue("a").value()
}

private fun wires(valueWireB: Int? = null): Map<String, CompositeWire> {
    val instructions = getInput(7).map { it.split(" -> ") }
    val wires = instructions.map { CompositeWire(it.second(), it.first().toOperation()) }
        .associateBy(CompositeWire::id).toMutableMap()
    val wireB = wires.getValue("b")
    wires["b"] = valueWireB?.let { wireB.copy(operation = { _: List<Wire> -> it }) } ?: wireB
    instructions.forEach {
        val wire = wires.getValue(it.second())
        it.first().split(" ").filterNot(String::isOperationKeyword)
            .map { wireId -> wires.getOrDefault(wireId, PrimeWire(wireId)) }
            .forEach(wire::addDependency)
    }
    return wires
}

private fun String.isOperationKeyword(): Boolean {
    return setOf("RSHIFT", "LSHIFT", "OR", "AND", "NOT").contains(this)
}

private fun String.toOperation(): (List<Wire>) -> Int {
    val instructions = split(" ")
    return when {
        instructions.contains("RSHIFT") -> { wires -> wires.first().value() shr wires.second().value() }
        instructions.contains("LSHIFT") -> { wires -> wires.first().value() shl wires.second().value() }
        instructions.contains("OR") -> { wires -> wires.first().value() or wires.second().value() }
        instructions.contains("AND") -> { wires -> wires.first().value() and wires.second().value() }
        instructions.first() == "NOT" -> { wires -> wires.first().value().inv() }
        else -> { wires -> wires.first().value() }
    }
}

private sealed interface Wire {

    val id: String
    val operation: (List<Wire>) -> Int

    fun value(): Int
}

private data class PrimeWire(
    override val id: String,
): Wire {

    override val operation = { _: List<Wire> -> value() }

    override fun value() = id.toInt()
}

private data class CompositeWire(
    override val id: String,
    override val operation: (List<Wire>) -> Int
): Wire {

    private var value: Int? = null
    private val dependencies = mutableListOf<Wire>()

    override fun value() = value ?: operation(dependencies).apply { value = this }

    fun addDependency(wire: Wire) = dependencies.add(wire)
}

