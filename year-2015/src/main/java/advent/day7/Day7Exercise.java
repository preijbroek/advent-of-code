package advent.day7;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class Day7Exercise {

    private Day7Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day7exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        int requiredNumberOfWires = code.stream()
                .map(instruction -> instruction.split(" -> ")[1])
                .map(Signal::empty)
                .collect(Collectors.toSet())
                .size();
        Set<Signal> signals = new HashSet<>();
        while (signals.size() < requiredNumberOfWires) {
            code.forEach(instruction -> apply(instruction, signals));
        }
        System.out.println(
                signals.stream()
                        .filter(signal -> "a".equals(signal.getWire()))
                        .findAny()
                        .orElseThrow(IllegalStateException::new)
                        .getBits()
        );
    }

    private static void part2(final List<String> code) {
        int requiredNumberOfWires = code.stream()
                .map(instruction -> instruction.split(" -> ")[1])
                .map(Signal::empty)
                .collect(Collectors.toSet())
                .size();
        Set<Signal> signals = new HashSet<>();
        while (signals.size() < requiredNumberOfWires) {
            code.forEach(instruction -> apply(instruction, signals));
        }
        Signal a = signals.stream()
                .filter(signal -> "a".equals(signal.getWire()))
                .findAny()
                .orElseThrow(IllegalStateException::new);
        signals.clear();
        signals.add(Signal.provide("b", a));
        while (signals.size() < requiredNumberOfWires) {
            code.stream()
                    .filter(instruction -> !"b".equals(instruction.split(" -> ")[1]))
                    .forEach(instruction -> apply(instruction, signals));
        }
        System.out.println(
                signals.stream()
                        .filter(signal -> "a".equals(signal.getWire()))
                        .findAny()
                        .orElseThrow(IllegalStateException::new)
                        .getBits()
        );
    }

    private static void apply(final String instruction, final Set<Signal> signals) {
        String[] split = instruction.split(" -> ");
        String target = split[1];
        List<String> app = Arrays.asList(split[0].split(" "));
        if (app.contains("RSHIFT")) {
            if (signals.contains(Signal.empty(app.get(0)))) {
                Signal sender = getSender(app.get(0), signals);
                signals.add(Signal.rShift(target, sender, Integer.parseInt(app.get(2))));
            }
        } else if (app.contains("LSHIFT")) {
            if (signals.contains(Signal.empty(app.get(0)))) {
                Signal sender = getSender(app.get(0), signals);
                signals.add(Signal.lShift(target, sender, Integer.parseInt(app.get(2))));
            }
        } else if (app.contains("NOT")) {
            if (signals.contains(Signal.empty(app.get(1)))) {
                Signal sender = getSender(app.get(1), signals);
                signals.add(Signal.not(target, sender));
            }
        } else if (app.contains("OR")) {
            if (isAllDigits(app.get(0))) {
                int value = Integer.parseInt(app.get(0));
                if (signals.contains(Signal.empty(app.get(2)))) {
                    Signal sender = getSender(app.get(2), signals);
                    signals.add(Signal.or(target, value, sender));
                }
            } else {
                if (signals.contains(Signal.empty(app.get(0))) && signals.contains(Signal.empty(app.get(2)))) {
                    Signal first = getSender(app.get(0), signals);
                    Signal second = getSender(app.get(2), signals);
                    signals.add(Signal.or(target, first, second));
                }
            }
        } else if (app.contains("AND")) {
            if (isAllDigits(app.get(0))) {
                int value = Integer.parseInt(app.get(0));
                if (signals.contains(Signal.empty(app.get(2)))) {
                    Signal sender = getSender(app.get(2), signals);
                    signals.add(Signal.and(target, value, sender));
                }
            } else {
                if (signals.contains(Signal.empty(app.get(0))) && signals.contains(Signal.empty(app.get(2)))) {
                    Signal first = getSender(app.get(0), signals);
                    Signal second = getSender(app.get(2), signals);
                    signals.add(Signal.and(target, first, second));
                }
            }
        } else {
            if (isAllDigits(app.get(0))) {
                signals.add(Signal.provide(target, Integer.parseInt(app.get(0))));
            } else {
                if (signals.contains(Signal.empty(app.get(0)))) {
                    Signal sender = getSender(app.get(0), signals);
                    signals.add(Signal.provide(target, sender));
                }
            }
        }

    }

    private static Signal getSender(final String wire, final Set<Signal> signals) {
        return signals.stream().filter(signal -> wire.equals(signal.getWire())).findAny().orElseThrow();
    }

    private static boolean isAllDigits(final String input) {
        return input.chars()
                .mapToObj(value -> (char) value)
                .allMatch(Character::isDigit);
    }

}
