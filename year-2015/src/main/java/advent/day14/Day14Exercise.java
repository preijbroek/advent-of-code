package advent.day14;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public final class Day14Exercise {

    private Day14Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day14exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        Integer result = code.stream()
                .map(Reindeer::from)
                .map(reindeer -> reindeer.calculateDistance(2503))
                .max(Integer::compareTo)
                .orElse(0);
        System.out.println(result);
    }

    private static void part2(final List<String> code) {
        List<Reindeer> reindeers = code.stream().map(Reindeer::from).collect(toList());
        for (int i = 1; i <= 2503; i++) {
            getBest(i, reindeers).forEach(Reindeer::addPoint);
        }
        System.out.println(reindeers.stream().mapToInt(Reindeer::getPoints).max().orElseThrow(IllegalStateException::new));
    }

    private static Set<Reindeer> getBest(final int time, final List<Reindeer> reindeers) {
        return reindeers.stream()
                .collect(groupingBy(reindeer -> reindeer.calculateDistance(time), TreeMap::new, toSet()))
                .lastEntry()
                .getValue();
    }
}
