package advent.day14;

import java.util.Objects;

public final class Reindeer {

    private final String name;
    private final int speed;
    private final int runTime;
    private final int restTime;
    private int points = 0;


    private Reindeer(final String name,
                     final int speed,
                     final int runTime,
                     final int restTime) {
        this.name = name;
        this.speed = speed;
        this.runTime = runTime;
        this.restTime = restTime;
    }

    public static Reindeer from(String line) {
        String[] words = line.split(" ");
        return new Reindeer(
                words[0],
                Integer.parseInt(words[3]),
                Integer.parseInt(words[6]),
                Integer.parseInt(words[13])
        );
    }

    public String name() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public void addPoint() {
        points++;
    }

    public int calculateDistance(final int time) {
        int distanceTravelled = 0;
        int cycleTime = runTime + restTime;
        int running = 0;
        while (running < time) {
            if (running % cycleTime < runTime) {
                distanceTravelled += speed;
            }
            running++;
        }
        return distanceTravelled;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reindeer reindeer = (Reindeer) o;
        return speed == reindeer.speed &&
                runTime == reindeer.runTime &&
                restTime == reindeer.restTime &&
                Objects.equals(name, reindeer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, speed, runTime, restTime);
    }
}
