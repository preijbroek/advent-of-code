package advent.day2;

import java.util.Objects;
import java.util.stream.IntStream;

import static java.lang.Integer.parseInt;

public class Box {

    private final int height;
    private final int length;
    private final int width;

    public Box(final int height, final int length, final int width) {
        this.height = height;
        this.length = length;
        this.width = width;
    }

    public Box(final String height, final String length, final String width) {
        this(parseInt(height), parseInt(length), parseInt(width));
    }

    public Box(final String[] values) {
        this(values[0], values[1], values[2]);
    }

    public int surface() {
        return 2 * ((height * length) + (height * width) + (length * width));
    }

    public int extra() {
        return IntStream.of(height * length, height * width, length * width)
                .min()
                .orElseThrow(IllegalStateException::new);
    }

    public int bow() {
        return length * height * width;
    }

    public int ribbon() {
        return 2 * IntStream.of(height + length, height + width, length + width)
                .min()
                .orElseThrow(IllegalStateException::new);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Box box = (Box) o;
        return height == box.height &&
                length == box.length &&
                width == box.width;
    }

    @Override
    public int hashCode() {
        return Objects.hash(height, length, width);
    }
}
