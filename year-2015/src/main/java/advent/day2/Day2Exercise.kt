package advent.day2

import advent.getInput
import util.product

fun main() {
    val boxes = getInput(2).map {
        it.split("x").map(String::toInt).sorted()
    }
    println(part1(boxes))
    println(part2(boxes))
}

private fun part1(boxes: List<List<Int>>): Int {
    return boxes.sumOf {
        3 * it.dropLast(1).product() + 2 * (it.drop(1).product()) + 2 * (it.first() * it.last())
    }
}

private fun part2(boxes: List<List<Int>>): Int {
    return boxes.sumOf { 2 * it.dropLast(1).sum() + it.product() }
}
