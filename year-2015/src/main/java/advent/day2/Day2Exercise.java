package advent.day2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class Day2Exercise {

    private Day2Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day2exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        int requiredSurface = code.stream()
                .map(value -> value.split("x"))
                .map(Box::new)
                .mapToInt(box -> box.surface() + box.extra())
                .sum();
        System.out.println(requiredSurface);
    }

    private static void part2(final List<String> code) {
        int requiredRibbons = code.stream()
                .map(value -> value.split("x"))
                .map(Box::new)
                .mapToInt(box -> box.bow() + box.ribbon())
                .sum();
        System.out.println(requiredRibbons);
    }


}
