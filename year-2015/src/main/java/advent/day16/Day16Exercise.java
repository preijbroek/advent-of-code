package advent.day16;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public final class Day16Exercise {

    private Day16Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day16exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        List<Sue> aunts = code.stream().map(Sue::from).collect(Collectors.toList());
        Map<String, Integer> sueProperties = sueProperties();
        Set<Sue> possibleSues = aunts.stream()
                .filter(sue -> sueProperties.entrySet().containsAll(sue.getKnownProperties().entrySet()))
                .collect(Collectors.toSet());
        if (possibleSues.size() != 1) {
            throw new IllegalStateException(possibleSues.toString());
        } else {
            System.out.println(possibleSues.iterator().next().getId());
        }
    }

    private static void part2(final List<String> code) {
        List<Sue> aunts = code.stream().map(Sue::from).collect(Collectors.toList());
        Map<String, Integer> sueProperties = sueProperties();
        Set<Sue> possibleSues = aunts.stream()
                .filter(sue -> {
                    Map<String, Integer> knownProperties = sue.getKnownProperties();
                    boolean canBeSue = true;
                    for (final Map.Entry<String, Integer> entry : knownProperties.entrySet()) {
                        String s = entry.getKey();
                        if (Arrays.asList("cats", "trees").contains(s)) {
                            canBeSue = canBeSue && entry.getValue() > sueProperties.get(s);
                        } else if (Arrays.asList("pomeranians", "goldfish").contains(s)) {
                            canBeSue = canBeSue && entry.getValue() < sueProperties.get(s);
                        } else {
                            canBeSue = canBeSue && entry.getValue().equals(sueProperties.get(s));
                        }
                    }
                    return canBeSue;
                })
                .collect(Collectors.toSet());
        if (possibleSues.size() != 1) {
            throw new IllegalStateException(possibleSues.toString());
        } else {
            System.out.println(possibleSues.iterator().next().getId());
        }
    }

    private static Map<String, Integer> sueProperties() {
        Map<String, Integer> sueProperties = new HashMap<>();
        sueProperties.put("children", 3);
        sueProperties.put("cats", 7);
        sueProperties.put("samoyeds", 2);
        sueProperties.put("pomeranians", 3);
        sueProperties.put("akitas", 0);
        sueProperties.put("vizslas", 0);
        sueProperties.put("goldfish", 5);
        sueProperties.put("trees", 3);
        sueProperties.put("cars", 2);
        sueProperties.put("perfumes", 1);
        return sueProperties;
    }

}
