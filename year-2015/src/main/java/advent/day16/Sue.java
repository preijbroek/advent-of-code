package advent.day16;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class Sue {

    private final int id;
    private final Map<String, Integer> knownProperties;

    public Sue(final int id, final Map<String, Integer> knownProperties) {
        this.id = id;
        this.knownProperties = Collections.unmodifiableMap(knownProperties);
    }

    public static Sue from(final String s) {
        String filtered = s.replaceAll(":", "").replaceAll(",", "");
        String[] properties = filtered.split(" ");
        int id = Integer.parseInt(properties[1]);
        Map<String, Integer> knownProperties = new HashMap<>();
        for (int i = 2; i < properties.length; i += 2) {
            knownProperties.put(properties[i], Integer.parseInt(properties[i + 1]));
        }
        return new Sue(id, knownProperties);
    }

    public int getId() {
        return id;
    }

    public Map<String, Integer> getKnownProperties() {
        return knownProperties;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sue sue = (Sue) o;
        return id == sue.id &&
                Objects.equals(knownProperties, sue.knownProperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, knownProperties);
    }
}
