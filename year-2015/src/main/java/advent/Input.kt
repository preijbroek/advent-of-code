package advent

import util.getInputBlocksForYear
import util.getInputForYear

fun getInput(day: Int) = getInputForYear(2015, day)

fun getInputBlocks(day: Int) = getInputBlocksForYear(2015, day)