package advent.day18;

import advent.day6.LightField;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class Day18Exercise {

    private Day18Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day18exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        LightField lightField = new LightField(code);
        for (int i = 0; i < 100; i++) {
            lightField.toggleLights();
        }
        System.out.println(lightField.litLights());
    }

    private static void part2(final List<String> code) {
        LightField lightField = new LightField(code);
        for (int i = 0; i < 100; i++) {
            lightField.toggleLightsWithPermanentlyLitCorners();
        }
        System.out.println(lightField.litLights());
    }

}
