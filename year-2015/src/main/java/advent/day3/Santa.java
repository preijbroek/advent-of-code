package advent.day3;

import util.Position;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Santa {

    private final List<Character> instructions;

    public Santa(final List<Character> instructions) {
        this.instructions = instructions;
    }

    public Set<Position> visitedHouses() {
        Set<Position> visitedHouses = new HashSet<>();
        Position current = Position.CENTRE;
        visitedHouses.add(current);
        for (int i = 0; i < instructions.size(); i++) {
            current = nextPosition(current, i);
            visitedHouses.add(current);
        }
        return visitedHouses;
    }

    private Position nextPosition(final Position current, final int i) {
        switch (instructions.get(i)) {
            case '>': return current.east();
            case 'v': return current.south();
            case '<': return current.west();
            case '^': return current.north();
            default: throw new IllegalStateException("Wrong direction encountered: " + instructions.get(i));
        }
    }
}
