package advent.day3;

import util.Position;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class Day3Exercise {

    private Day3Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        String code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day3exercise.csv")).get(0);
        part1(code);
        part2(code);
    }

    private static void part1(final String code) {
        List<Character> directions = code.chars().mapToObj(value -> (char) value)
                .collect(Collectors.toList());
        Santa santa = new Santa(directions);
        System.out.println(santa.visitedHouses().size());
    }

    private static void part2(final String code) {
        List<Character> santaInstructions = new LinkedList<>();
        List<Character> robotInstructions = new LinkedList<>();
        for (int i = 0; i < code.length(); i++) {
            if ((i & 1) == 1) {
                robotInstructions.add(code.charAt(i));
            } else {
                santaInstructions.add(code.charAt(i));
            }
        }
        Santa santa = new Santa(santaInstructions);
        Santa robot = new Santa(robotInstructions);
        Set<Position> visitedHouses = santa.visitedHouses();
        visitedHouses.addAll(robot.visitedHouses());
        System.out.println(visitedHouses.size());
    }


}
