package advent.day3

import advent.getInput
import util.Position

fun main() {
    val route = getInput(3).first()
    println(part1(route))
    println(part2(route))
}

private fun part1(route: String): Int {
    return route.visitedHouses().size
}

private fun part2(route: String): Int {
    return route.indices
        .groupBy({ it % 2 }, { route[it] })
        .values
        .map { it.joinToString("") }
        .map(String::visitedHouses)
        .flatten()
        .toSet()
        .size
}

private fun String.visitedHouses() =
    generateSequence(0 to Position.CENTRE) { (index, position) ->
        index + 1 to position.to(this[index])
    }
        .takeWhile { (index, _) -> index < length }
        .map(Pair<Int, Position>::second)
        .toSet()


private fun Position.to(c: Char) = when (c) {
    '^' -> north()
    '>' -> east()
    'v' -> south()
    '<' -> west()
    else -> throw IllegalArgumentException(c.toString())
}

