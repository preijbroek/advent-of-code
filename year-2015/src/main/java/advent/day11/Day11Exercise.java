package advent.day11;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Day11Exercise {

    private Day11Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        String code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day11exercise.csv")).get(0);
        part1(code);
        part2(code);
    }

    private static void part1(final String code) {
        System.out.println(new Password(code).nextValid());
    }

    private static void part2(final String code) {
        System.out.println(new Password(code).nextValid().nextValid());
    }

}
