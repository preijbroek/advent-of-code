package advent.day11;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class Password {

    private final String code;

    public Password(final String code) {
        this.code = code;
    }

    public Password nextValid() {
        Password password = increment();
        while (!password.isValid()) {
            password = password.increment();
        }
        return password;
    }

    private Password increment() {
        if ("zzzzzzzz".equals(code)) {
            return new Password("aaaaaaaa");
        }
        return new Password(convert(code, 7));
    }

    private String convert(final String string, final int i) {
        String result = string.substring(0, i) + next(string.charAt(i));
        if (i < string.length() - 1) {
            result += string.substring(i + 1);
        }
        if (string.charAt(i) == 'z') {
            return convert(result, i - 1);
        } else {
            return result;
        }
    }

    private char next(final char c) {
        return c == 'z' ? 'a' : (char) (c + 1);
    }

    private boolean isValid() {
        return containsNoInvalidCharacters()
                && containsRaisingSequence()
                && containsDifferentPair();
    }

    private boolean containsNoInvalidCharacters() {
        return Stream.of("i", "l", "o").noneMatch(code::contains);
    }

    private boolean containsRaisingSequence() {
        List<Integer> chars = code.chars().boxed().collect(Collectors.toList());
        return IntStream.range(0, chars.size() - 2)
                .anyMatch(i -> chars.get(i) == chars.get(i + 1) - 1 && chars.get(i) == chars.get(i + 2) - 2);
    }

    private boolean containsDifferentPair() {
        List<Integer> chars = code.chars().boxed().collect(Collectors.toList());
        return IntStream.range(0, chars.size() - 1)
                .filter(i -> chars.get(i).equals(chars.get(i + 1)))
                .mapToObj(chars::get)
                .distinct()
                .count() > 1;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Password password = (Password) o;
        return Objects.equals(code, password.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public String toString() {
        return code;
    }
}
