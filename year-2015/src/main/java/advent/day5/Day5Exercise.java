package advent.day5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class Day5Exercise {

    private Day5Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day5exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        System.out.println(code.stream().filter(Day5Exercise::isNiceForPart1).count());
    }

    private static void part2(final List<String> code) {
        System.out.println(code.stream().filter(Day5Exercise::isNiceForPart2).count());
    }

    private static boolean isNiceForPart1(final String string) {
        return string.chars().filter(Day5Exercise::isVowel).count() >= 3
                && hasDuplicate(string)
                && containsNoForbidden(string);
    }

    private static boolean isVowel(final int letter) {
        return IntStream.of('a', 'e', 'i', 'o', 'u').anyMatch(vowel -> vowel == letter);
    }

    private static boolean hasDuplicate(final String string) {
        return IntStream.range(1, string.length())
                .anyMatch(i -> string.charAt(i) == string.charAt(i - 1));
    }

    private static boolean containsNoForbidden(final String string) {
        return Stream.of("ab", "cd", "pq", "xy").noneMatch(string::contains);
    }

    private static boolean isNiceForPart2(final String string) {
        return hasDoubleNonOverlapRepeat(string) && hasRepeat(string);
    }

    private static boolean hasDoubleNonOverlapRepeat(final String string) {
        for (int i = 0; i < string.length() - 2; i++) {
            String rest = string.substring(i + 2);
            if (rest.contains(string.substring(i, i + 2))) {
                return true;
            }
        }
        return false;
    }

    private static boolean hasRepeat(final String string) {
        return IntStream.range(2, string.length())
                .anyMatch(i -> string.charAt(i) == string.charAt(i - 2));
    }
}
