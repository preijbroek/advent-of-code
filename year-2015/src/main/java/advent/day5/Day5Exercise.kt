package advent.day5

import advent.getInput

fun main() {
    val input = getInput(5)
    println(part1(input))
    println(part2(input))
}

private fun part1(input: List<String>): Int {
    return input.count(String::isNice1)
}

private fun part2(input: List<String>): Int {
    return input.count(String::isNice2)
}

private fun String.isNice1() = hasEnoughVowels()
        && hasSuccessiveDuplicate()
        && hasNoForbiddenPhrase()

private fun String.hasEnoughVowels() =
    "(.*[aeiou]){3}.*".toRegex().matches(this)

private fun String.hasSuccessiveDuplicate() = ".*(.)\\1.*".toRegex().matches(this)

private fun String.hasNoForbiddenPhrase() =
    !".*((ab)|(cd)|(pq)|(xy)).*".toRegex().matches(this)

private fun String.isNice2() = hasDoubleDuplicate()
        && hasSplitDuplicate()

private fun String.hasDoubleDuplicate() = ".*(..).*\\1.*".toRegex().matches(this)

private fun String.hasSplitDuplicate() = ".*(.).\\1.*".toRegex().matches(this)


