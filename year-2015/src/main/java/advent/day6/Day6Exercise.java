package advent.day6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class Day6Exercise {

    private Day6Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day6exercise.csv"));
        LightField field = new LightField(1000);
        field.turn(code);
        part1(field);
        part2(field);
    }

    private static void part1(final LightField field) {
        System.out.println(field.litLights());
    }

    private static void part2(final LightField field) {
        System.out.println(field.brightness());
    }
}
