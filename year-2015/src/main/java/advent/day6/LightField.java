package advent.day6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public final class LightField {

    private static final String TOGGLE = "toggle ";
    private static final String TURN_OFF = "turn off ";
    private static final String TURN_ON = "turn on ";
    private static final String THROUGH = " through ";
    private final Light[][] lights;

    public LightField(int size) {
        this.lights = new Light[size][size];
        initializeField();
    }

    public LightField(List<String> input) {
        this.lights = new Light[input.size()][input.get(0).length()];
        initializeField(input);
    }

    private void initializeField() {
        for (int i = 0; i < lights.length; i++) {
            for (int j = 0; j < lights[i].length; j++) {
                lights[j][i] = new Light(i, j);
            }
        }
    }

    private void initializeField(final List<String> input) {
        for (int i = 0; i < lights.length; i++) {
            for (int j = 0; j < lights[i].length; j++) {
                lights[j][i] = new Light(i, j, input.get(i).charAt(j) == '#');
            }
        }
    }

    public void toggleLights() {
        List<Light> lightsToToggle = new ArrayList<>();
        for (int i = 0; i < lights.length; i++) {
            for (int j = 0; j < lights[i].length; j++) {
                int y = i;
                long litNeighbours = lights[j][i].neighbours()
                        .stream()
                        .filter(light -> light.isInRange(lights.length, lights[y].length))
                        .map(light -> lights[light.getY()][light.getX()])
                        .filter(Light::isLit)
                        .count();
                if (lights[j][i].isLit() && litNeighbours != 2L && litNeighbours != 3L) {
                    lightsToToggle.add(lights[j][i]);
                } else if (!lights[j][i].isLit() && litNeighbours == 3L) {
                    lightsToToggle.add(lights[j][i]);
                }
            }
        }
        lightsToToggle.forEach(Light::toggle);
    }

    public void toggleLightsWithPermanentlyLitCorners() {
        toggleLights();
        turnOnCorners();
    }

    private void turnOnCorners() {
        lights[0][0].turnOn();
        lights[0][lights[0].length - 1].turnOn();
        lights[lights.length - 1][0].turnOn();
        lights[lights.length - 1][lights[0].length - 1].turnOn();
    }

    public void turn(final List<String> code) {
        code.forEach(this::apply);
    }

    private void apply(final String instruction) {
        int[] coordinates = getCoordinates(instruction);
        if (instruction.startsWith(TOGGLE)) {
            IntStream.rangeClosed(coordinates[0], coordinates[2]).forEach(x ->
                    IntStream.rangeClosed(coordinates[1], coordinates[3]).forEach(y ->
                            lights[y][x].toggle()
                    )
            );
        } else if (instruction.startsWith(TURN_OFF)) {
            IntStream.rangeClosed(coordinates[0], coordinates[2]).forEach(x ->
                    IntStream.rangeClosed(coordinates[1], coordinates[3]).forEach(y ->
                            lights[y][x].turnOff()
                    )
            );
        } else if (instruction.startsWith(TURN_ON)) {
            IntStream.rangeClosed(coordinates[0], coordinates[2]).forEach(x ->
                    IntStream.rangeClosed(coordinates[1], coordinates[3]).forEach(y ->
                            lights[y][x].turnOn()
                    )
            );
        } else {
            throw new IllegalStateException("Wrong instruction found: " + instruction);
        }
    }

    private int[] getCoordinates(final String instruction) {
        String coordinates = instruction;
        coordinates = coordinates.replace(TOGGLE, "");
        coordinates = coordinates.replace(TURN_OFF, "");
        coordinates = coordinates.replace(TURN_ON, "");
        String[] split = coordinates.split(THROUGH);
        return Arrays.stream(split)
                .flatMap(vars -> Arrays.stream(vars.split(",")))
                .mapToInt(Integer::parseInt)
                .toArray();
    }

    public long litLights() {
        return Arrays.stream(lights).flatMap(Arrays::stream).filter(Light::isLit).count();
    }

    public int brightness() {
        return Arrays.stream(lights).flatMap(Arrays::stream).mapToInt(Light::brightness).sum();
    }

    public void draw() {
        for (int i = 0; i < lights.length; i++) {
            for (int j = 0; j < lights[i].length; j++) {
                System.out.print(lights[j][i].isLit() ? '#' : '.');
            }
            System.out.println();
        }
        System.out.println();
    }
}
