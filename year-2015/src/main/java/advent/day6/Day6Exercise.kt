package advent.day6

import advent.getInput
import util.second
import util.third
import kotlin.math.max

fun main() {
    val instructions = getInput(6)
    val lightGrid = (0..999).map {
        (0..999).map {
            ChristmasLight()
        }
    }.let(::LightGrid)
    lightGrid.execute(instructions)
    println(part1(lightGrid))
    println(part2(lightGrid))
}

private fun part1(lightGrid: LightGrid): Int {
    return lightGrid.lightsCount()
}

private fun part2(lightGrid: LightGrid): Int {
    return lightGrid.brightness()
}

private data class LightGrid(private val lights: List<List<ChristmasLight>>) {

    fun execute(instructions: List<String>) {
        instructions.forEach { s ->
            val executeInstruction = s.toInstruction()
            s.replace("(turn off |turn on |toggle )".toRegex(), "")
                .split(" through ").flatMap { it.split(",") }
                .map(String::toInt)
                .run {
                    (first()..third()).forEach { x ->
                        (second()..last()).forEach { y ->
                            executeInstruction(x, y)
                        }
                    }
                }
        }
    }

    private fun String.toInstruction(): (Int, Int) -> Unit =
        when {
            startsWith("toggle") -> { x, y -> lights[y][x].toggle() }
            startsWith("turn off") -> { x, y -> lights[y][x].turnOff() }
            startsWith("turn on") -> { x, y -> lights[y][x].turnOn() }
            else -> throw IllegalArgumentException(this)
        }

    fun lightsCount() = lights.sumOf { it.count(ChristmasLight::on) }

    fun brightness() = lights.sumOf{ it.sumOf(ChristmasLight::brightness) }
}

private data class ChristmasLight(var on: Boolean = false, var brightness: Int = 0) {

    fun turnOn() {
        on = true
        brightness++
    }

    fun turnOff() {
        on = false
        brightness = max(0, --brightness)
    }

    fun toggle() {
        on = !on
        brightness += 2
    }
}

