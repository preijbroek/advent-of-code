package advent.day6;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class Light {

    private final int x;
    private final int y;
    private boolean isLit;
    private int brightness = 0;

    public Light(final int x, final int y) {
        this(x, y, false);
    }

    public Light(final int x, final int y, final boolean isLit) {
        this.x = x;
        this.y = y;
        this.isLit = isLit;
    }

    public List<Light> neighbours() {
        return Arrays.asList(north(), north().east(), east(), south().east(),
                south(), south().west(), west(), north().west());
    }

    private Light north() {
        return new Light(x, y - 1);
    }

    private Light south() {
        return new Light(x, y + 1);
    }

    private Light east() {
        return new Light(x + 1, y);
    }

    private Light west() {
        return new Light(x - 1, y);
    }

    public boolean isInRange(int x, int y) {
        return this.x >= 0 && this.x < x
                && this.y >= 0 && this.y < y;
    }

    public void turnOff() {
        isLit = false;
        brightness = Math.max(brightness - 1, 0);
    }

    public void turnOn() {
        isLit = true;
        brightness++;
    }

    public void toggle() {
        isLit = !isLit;
        brightness += 2;
    }

    public boolean isLit() {
        return isLit;
    }

    public int brightness() {
        return brightness;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Light light = (Light) o;
        return x == light.x &&
                y == light.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
