package advent.day15;

public final class Ingredient {

    private final String name;
    private final int capacity;
    private final int durability;
    private final int flavour;
    private final int texture;
    private final int calories;

    private Ingredient(final IngredientBuilder builder) {
        this.name = builder.name;
        this.capacity = builder.capacity;
        this.durability = builder.durability;
        this.flavour = builder.flavour;
        this.texture = builder.texture;
        this.calories = builder.calories;
    }

    public static Ingredient from(final String s) {
        String[] list = s.split(": ");
        String[] ingredients = list[1].split(", ");
        return builder().withName(list[0])
                .withCapacity(Integer.parseInt(ingredients[0].split(" ")[1]))
                .withDurability(Integer.parseInt(ingredients[1].split(" ")[1]))
                .withFlavour(Integer.parseInt(ingredients[2].split(" ")[1]))
                .withTexture(Integer.parseInt(ingredients[3].split(" ")[1]))
                .withCalories(Integer.parseInt(ingredients[4].split(" ")[1]))
                .build();

    }

    public static IngredientBuilder builder() {
        return new IngredientBuilder();
    }

    public int getCapacity() {
        return capacity;
    }

    public int getDurability() {
        return durability;
    }

    public int getFlavour() {
        return flavour;
    }

    public int getTexture() {
        return texture;
    }

    public int getCalories() {
        return calories;
    }

    private static final class IngredientBuilder {
        private String name;
        private int capacity;
        private int durability;
        private int flavour;
        private int texture;
        private int calories;

        public IngredientBuilder() {
        }

        public IngredientBuilder withName(final String name) {
            this.name = name;
            return this;
        }

        public IngredientBuilder withCapacity(final int capacity) {
            this.capacity = capacity;
            return this;
        }

        public IngredientBuilder withDurability(final int durability) {
            this.durability = durability;
            return this;
        }

        public IngredientBuilder withFlavour(final int flavour) {
            this.flavour = flavour;
            return this;
        }

        public IngredientBuilder withTexture(final int texture) {
            this.texture = texture;
            return this;
        }

        public IngredientBuilder withCalories(final int calories) {
            this.calories = calories;
            return this;
        }

        public Ingredient build() {
            return new Ingredient(this);
        }
    }
}
