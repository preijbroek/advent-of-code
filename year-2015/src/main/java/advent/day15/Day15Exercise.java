package advent.day15;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public final class Day15Exercise {

    private Day15Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day15exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        List<Ingredient> ingredients = code.stream().map(Ingredient::from).collect(toList());
        List<Integer> scores = new ArrayList<>();
        IntStream.rangeClosed(0, 100).forEach(i ->
                IntStream.rangeClosed(0, 100 - i).forEach(j ->
                        IntStream.rangeClosed(0, 100 - i - j).forEach(k -> {
                            int m = 100 - i - j - k;
                            int capacity = Math.max(0, (i * ingredients.get(0).getCapacity()) + (j * ingredients.get(1).getCapacity())
                                    + (k * ingredients.get(2).getCapacity()) + (m * ingredients.get(3).getCapacity()));
                            int durability = Math.max(0, (i * ingredients.get(0).getDurability()) + (j * ingredients.get(1).getDurability())
                                    + (k * ingredients.get(2).getDurability()) + (m * ingredients.get(3).getDurability()));
                            int flavour = Math.max(0, (i * ingredients.get(0).getFlavour()) + (j * ingredients.get(1).getFlavour())
                                    + (k * ingredients.get(2).getFlavour()) + (m * ingredients.get(3).getFlavour()));
                            int texture = Math.max(0, (i * ingredients.get(0).getTexture()) + (j * ingredients.get(1).getTexture())
                                    + (k * ingredients.get(2).getTexture()) + (m * ingredients.get(3).getTexture()));
                            scores.add(capacity * durability * flavour * texture);
                        })
                )
        );
        System.out.println(scores.stream().mapToInt(Integer::intValue).max().orElseThrow(IllegalStateException::new));
    }

    private static void part2(final List<String> code) {
        List<Ingredient> ingredients = code.stream().map(Ingredient::from).collect(toList());
        List<Integer> scores = new ArrayList<>();
        IntStream.rangeClosed(0, 100).forEach(i ->
                IntStream.rangeClosed(0, 100 - i).forEach(j ->
                        IntStream.rangeClosed(0, 100 - i - j).forEach(k -> {
                            int m = 100 - i - j - k;
                            int capacity = Math.max(0, (i * ingredients.get(0).getCapacity()) + (j * ingredients.get(1).getCapacity())
                                    + (k * ingredients.get(2).getCapacity()) + (m * ingredients.get(3).getCapacity()));
                            int durability = Math.max(0, (i * ingredients.get(0).getDurability()) + (j * ingredients.get(1).getDurability())
                                    + (k * ingredients.get(2).getDurability()) + (m * ingredients.get(3).getDurability()));
                            int flavour = Math.max(0, (i * ingredients.get(0).getFlavour()) + (j * ingredients.get(1).getFlavour())
                                    + (k * ingredients.get(2).getFlavour()) + (m * ingredients.get(3).getFlavour()));
                            int texture = Math.max(0, (i * ingredients.get(0).getTexture()) + (j * ingredients.get(1).getTexture())
                                    + (k * ingredients.get(2).getTexture()) + (m * ingredients.get(3).getTexture()));
                            int calories = Math.max(0, (i * ingredients.get(0).getCalories()) + (j * ingredients.get(1).getCalories())
                                    + (k * ingredients.get(2).getCalories()) + (m * ingredients.get(3).getCalories()));
                            if (calories != 500) {
                                return;
                            }
                            scores.add(capacity * durability * flavour * texture);
                        })
                )
        );
        System.out.println(scores.stream().mapToInt(Integer::intValue).max().orElseThrow(IllegalStateException::new));
    }

}
