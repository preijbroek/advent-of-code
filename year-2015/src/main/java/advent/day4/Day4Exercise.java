package advent.day4;

import jakarta.xml.bind.DatatypeConverter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;

public final class Day4Exercise {

    private Day4Exercise() {
    }

    public static void main(final String[] args) throws IOException, NoSuchAlgorithmException {
        String code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day4exercise.csv")).get(0);
        part1(code);
        part2(code);
    }

    private static void part1(final String code) throws NoSuchAlgorithmException {
        System.out.println(startWithNumberOfZeroes(code, 5));
    }

    private static void part2(final String code) throws NoSuchAlgorithmException {
        System.out.println(startWithNumberOfZeroes(code, 6));
    }

    private static int startWithNumberOfZeroes(final String code,
                                               final int numberOfZeroes) throws NoSuchAlgorithmException {
        String start = IntStream.range(0, numberOfZeroes)
                .mapToObj(i -> String.valueOf(0))
                .collect(joining());
        int index = 0;
        String input;
        String result = "";
        while (!result.startsWith(start)) {
            index++;
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            input = code + index;
            md5.update(input.getBytes());
            result = DatatypeConverter.printHexBinary(md5.digest());
        }
        return index;
    }
}
