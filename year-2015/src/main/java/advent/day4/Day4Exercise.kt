package advent.day4

import advent.getInput
import jakarta.xml.bind.DatatypeConverter
import java.security.MessageDigest

fun main() {
    val input = getInput(4).first()
    println(part1(input))
    println(part2(input))
}

private fun part1(input: String): Int {
    return input.lowestHashWithLeadingZeroesIndex(leadingZeroes = 5)
}

private fun part2(input: String): Int {
    return input.lowestHashWithLeadingZeroesIndex(leadingZeroes = 6)
}

private fun String.lowestHashWithLeadingZeroesIndex(leadingZeroes: Int): Int {
    val start = "".padStart(leadingZeroes, '0')
    var addition = 0
    var result = ""
    val digest = MessageDigest.getInstance("MD5")
    while (!result.startsWith(start)) {
        addition++
        result = digest.toHash("$this$addition")
    }
    return addition
}

private fun MessageDigest.toHash(s: String): String {
    update(s.encodeToByteArray())
    return DatatypeConverter.printHexBinary(digest())
}
