package advent.day9;

import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Distance implements Comparable<Distance> {

    private final Star location1;
    private final Star location2;
    private final int length;

    public Distance(final Star location1, final Star location2, final int length) {
        this.location1 = location1;
        this.location2 = location2;
        this.length = length;
    }

    public static Distance from(final String s) {
        String[] split = s.split(" = ");
        int length = Integer.parseInt(split[1]);
        String[] locations = split[0].split(" to ");
        return new Distance(new Star(locations[0]), new Star(locations[1]), length);
    }

    public boolean containsAny(final Collection<Star> stars) {
        return Stream.of(location1, location2).anyMatch(stars::contains);
    }

    public Set<Star> locations() {
        return Stream.of(location1, location2).collect(Collectors.toSet());
    }

    public int length() {
        return length;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Distance distance = (Distance) o;
        return length == distance.length &&
                hasSameStars(distance);
    }

    private boolean hasSameStars(final Distance distance) {
        return (Objects.equals(location1, distance.location1) && Objects.equals(location2, distance.location2))
                || (Objects.equals(location1, distance.location2) && Objects.equals(location2, distance.location1));
    }

    @Override
    public int hashCode() {
        return Objects.hash(location1, location2, length)
                + Objects.hash(location2, location1, length);
    }

    @Override
    public int compareTo(final Distance o) {
        return Comparator.comparing(Distance::length).compare(this, o);
    }
}
