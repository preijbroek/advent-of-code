package advent.day9;

import util.Comparators;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;

public final class Day9Exercise {

    private Day9Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day9exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        Set<Star> stars = code.stream()
                .map(line -> line.split(" = ")[0])
                .flatMap(line -> Arrays.stream(line.split(" to ")))
                .map(Star::new)
                .collect(toCollection(HashSet::new));
        Set<Distance> distances = code.stream()
                .map(Distance::from)
                .collect(Collectors.toSet());
        Distance smallest = Comparators.minOf(distances);
        Set<Star> endpoints = new HashSet<>(smallest.locations());
        int length = smallest.length();
        stars.removeAll(endpoints);
        while (!stars.isEmpty()) {
            smallest = distances.stream()
                    .filter(distance -> distance.containsAny(endpoints))
                    .filter(distance -> distance.containsAny(stars))
                    .min(Distance::compareTo)
                    .orElseThrow(IllegalStateException::new);
            length += smallest.length();
            Set<Star> newEndpoints = smallest.locations();
            stars.removeAll(newEndpoints);
            newEndpoints.forEach(endpoint -> {
                if (endpoints.contains(endpoint)) {
                    endpoints.remove(endpoint);
                } else {
                    endpoints.add(endpoint);
                }
            });
        }
        System.out.println(length);
    }

    private static void part2(final List<String> code) {
        Set<Star> stars = code.stream()
                .map(line -> line.split(" = ")[0])
                .flatMap(line -> Arrays.stream(line.split(" to ")))
                .map(Star::new)
                .collect(toCollection(HashSet::new));
        Set<Distance> distances = code.stream()
                .map(Distance::from)
                .collect(Collectors.toSet());
        Distance largest = distances.stream().max(Distance::compareTo).orElseThrow(IllegalStateException::new);
        Set<Star> endpoints = new HashSet<>(largest.locations());
        int length = largest.length();
        stars.removeAll(endpoints);
        while (!stars.isEmpty()) {
            largest = distances.stream()
                    .filter(distance -> distance.containsAny(endpoints))
                    .filter(distance -> distance.containsAny(stars))
                    .max(Distance::compareTo)
                    .orElseThrow(IllegalStateException::new);
            length += largest.length();
            Set<Star> newEndpoints = largest.locations();
            stars.removeAll(newEndpoints);
            newEndpoints.forEach(endpoint -> {
                if (endpoints.contains(endpoint)) {
                    endpoints.remove(endpoint);
                } else {
                    endpoints.add(endpoint);
                }
            });
        }
        System.out.println(length);
    }


}
