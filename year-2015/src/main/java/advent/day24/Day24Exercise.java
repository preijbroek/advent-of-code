package advent.day24;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalLong;

import static advent.day24.CollectionUtils.sum;
import static java.util.stream.Collectors.toList;

public final class Day24Exercise {

    private Day24Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<Long> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day24exercise.csv"))
                .stream().map(Long::parseLong).collect(toList());
        part1(code);
        part2(code);
    }

    private static void part1(final List<Long> code) {
        divideWeight(code, 3);
    }

    private static void part2(final List<Long> code) {
        divideWeight(code, 4);
    }

    private static void divideWeight(final List<Long> code, final int trunks) {
        int weight = getWeight(code, trunks);
        OptionalLong result = OptionalLong.empty();
        for (int i = 1; i < code.size(); i++) {
            result = combination(code, i).stream()
                    .filter(list -> sum(list) == weight)
                    .mapToLong(CollectionUtils::quantumEntanglement)
                    .min();
            if (result.isPresent()) {
                break;
            }
        }
        System.out.println(result.orElseThrow(IllegalStateException::new));
    }

    private static int getWeight(final List<Long> code, final int partitions) {
        return code.stream().mapToInt(Long::intValue).sum() / partitions;
    }

    private static List<List<Long>> combination(final List<Long> fullList, final int k) {
        List<List<Long>> result = new ArrayList<>();
        backtrack(fullList, k, 0, result, new ArrayList<>());
        return result;
    }

    private static void backtrack(final List<Long> fullList,
                                  final int k,
                                  final int startIndex,
                                  final List<List<Long>> result,
                                  final List<Long> partialList) {
        if (k == partialList.size()) {
            result.add(new ArrayList<>(partialList));
            return;
        }
        for (int i = startIndex; i < fullList.size(); i++) {
            partialList.add(fullList.get(i));
            backtrack(fullList, k, i + 1, result, partialList);
            partialList.remove(partialList.size() - 1);
        }
    }

}
