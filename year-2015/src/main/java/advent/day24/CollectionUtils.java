package advent.day24;

import java.util.Collection;

public final class CollectionUtils {

    private CollectionUtils() {
    }

    public static long sum(Collection<Long> integers) {
        return integers.stream().mapToLong(Long::longValue).sum();
    }

    public static long quantumEntanglement(Collection<Long> integers) {
        return integers.stream().mapToLong(Long::longValue).reduce((l1, l2) -> l1 * l2).orElse(0L);
    }

}
