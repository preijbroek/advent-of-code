package advent.day21;

public final class BossArmour implements Weaponry {

    private final int strength;

    public BossArmour(final String s) {
        this.strength = Integer.parseInt(s.split(" ")[1]);
    }

    @Override
    public int strength() {
        return strength;
    }

    @Override
    public int cost() {
        return 0;
    }
}
