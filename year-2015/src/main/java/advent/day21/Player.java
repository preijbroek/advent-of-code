package advent.day21;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

public final class Player {

    private final Weaponry weapon;
    private final Weaponry armour;
    private final Set<Ring> rings;
    private int hitPoints;

    public Player(final Weaponry weapon, final Weaponry armour, final Collection<Ring> rings, final int hitPoints) {
        this.weapon = weapon;
        this.armour = armour;
        this.rings = rings.isEmpty() ? EnumSet.noneOf(Ring.class) : EnumSet.copyOf(rings);
        this.hitPoints = hitPoints;
    }

    public boolean defeats(final Player opponent) {
        Player me = this.copy();
        Player them = opponent.copy();
        while (me.isAlive()) {
            me.hit(them);
            if (them.isAlive()) {
                them.hit(me);
            } else {
                break;
            }
        }
        return me.isAlive();
    }

    public int cost() {
        return weapon.cost() + armour.cost() + rings.stream().mapToInt(Ring::cost).sum();
    }

    private Player copy() {
        return new Player(weapon, armour, rings, hitPoints);
    }

    private void hit(final Player player) {
        int damage = this.weapon.strength() + this.rings.stream().mapToInt(Ring::attack).sum();
        int defence = player.armour.strength() + player.rings.stream().mapToInt(Ring::defence).sum();
        player.hitPoints -= Math.max((damage - defence), 1);
    }

    private boolean isAlive() {
        return hitPoints > 0;
    }

    public Player withRing(final Ring ring) {
        EnumSet<Ring> rings = EnumSet.copyOf(this.rings);
        rings.add(ring);
        return new Player(weapon, armour, rings, hitPoints);
    }

    public int rings() {
        return rings.size();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(weapon, player.weapon) &&
                Objects.equals(armour, player.armour) &&
                Objects.equals(rings, player.rings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weapon, armour, rings);
    }

    @Override
    public String toString() {
        return "Player{" +
                "weapon=" + weapon +
                ", armour=" + armour +
                ", rings=" + rings +
                '}';
    }
}
