package advent.day21;

public enum Armour implements Weaponry {

    NONE(0, 0),
    LEATHER(1, 13),
    CHAINMAIL(2, 31),
    SPLINTMAIL(3, 53),
    BANDEDMAIL(4, 75),
    PLATEMAIL(5, 102);

    private final int strength;
    private final int cost;

    Armour(final int strength, final int cost) {
        this.strength = strength;
        this.cost = cost;
    }

    @Override
    public int strength() {
        return strength;
    }

    @Override
    public int cost() {
        return cost;
    }
}
