package advent.day21;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toSet;

public final class Day21Exercise {

    private Day21Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day21exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        Player opponent = new Player(new BossWeapon(code.get(1)), new BossArmour(code.get(2)), emptyList(), Integer.parseInt(code.get(0).split(" ")[2]));
        Set<Player> allPlayerCombinations = new HashSet<>();
        Arrays.stream(Weapon.values()).forEach(weapon ->
                Arrays.stream(Armour.values()).forEach(armour ->
                        allPlayerCombinations.add(new Player(weapon, armour, emptyList(), 100))
                )
        );
        Arrays.stream(Ring.values()).map(ring -> allPlayerCombinations.stream()
                .map(player -> player.withRing(ring))
                .filter(player -> player.rings() <= 2)
                .collect(toSet()))
                .forEach(allPlayerCombinations::addAll);
        Arrays.stream(Ring.values()).map(ring -> allPlayerCombinations.stream()
                .map(player -> player.withRing(ring))
                .filter(player -> player.rings() <= 2)
                .collect(toSet()))
                .forEachOrdered(allPlayerCombinations::addAll);
        System.out.println(allPlayerCombinations.stream()
                .filter(player -> player.defeats(opponent))
                .mapToInt(Player::cost)
                .min()
                .orElseThrow(IllegalStateException::new));
    }

    private static void part2(final List<String> code) {
        Player opponent = new Player(new BossWeapon(code.get(1)), new BossArmour(code.get(2)), emptyList(), Integer.parseInt(code.get(0).split(" ")[2]));
        Set<Player> allPlayerCombinations = new HashSet<>();
        Arrays.stream(Weapon.values()).forEach(weapon ->
                Arrays.stream(Armour.values()).forEach(armour ->
                        allPlayerCombinations.add(new Player(weapon, armour, emptyList(), 100))
                )
        );
        Arrays.stream(Ring.values()).map(ring -> allPlayerCombinations.stream()
                .map(player -> player.withRing(ring))
                .filter(player -> player.rings() <= 2)
                .collect(toSet()))
                .forEach(allPlayerCombinations::addAll);
        Arrays.stream(Ring.values()).map(ring -> allPlayerCombinations.stream()
                .map(player -> player.withRing(ring))
                .filter(player -> player.rings() <= 2)
                .collect(toSet()))
                .forEachOrdered(allPlayerCombinations::addAll);
        System.out.println(allPlayerCombinations.stream()
                .filter(opponent::defeats)
                .mapToInt(Player::cost)
                .max()
                .orElseThrow(IllegalStateException::new));
    }

}
