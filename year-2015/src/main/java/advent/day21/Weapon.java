package advent.day21;

public enum Weapon implements Weaponry {

    DAGGER(4, 8),
    SHORTSWORD(5, 10),
    WARHAMMER(6, 25),
    LONGSWORD(7, 40),
    GREATAXE(8, 74);

    private final int strength;
    private final int cost;

    Weapon(final int strength, final int cost) {
        this.strength = strength;
        this.cost = cost;
    }

    @Override
    public int strength() {
        return strength;
    }

    @Override
    public int cost() {
        return cost;
    }
}
