package advent.day21;

public enum Ring {

    SMALL_ATTACK(1, 0, 25),
    MEDIUM_ATTACK(2, 0, 50),
    LARGE_ATTACK(3, 0, 100),
    SMALL_DEFENCE(0, 1, 20),
    MEDIUM_DEFENCE(0, 2, 40),
    LARGE_DEFENCE(0, 3, 80);

    private final int attack;
    private final int defence;
    private final int cost;

    Ring(final int attack, final int defence, final int cost) {
        this.attack = attack;
        this.defence = defence;
        this.cost = cost;
    }

    public int attack() {
        return attack;
    }

    public int defence() {
        return defence;
    }

    public int cost() {
        return cost;
    }
}
