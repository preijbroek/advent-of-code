package advent.day21;

public interface Weaponry {

    int strength();

    int cost();
}
