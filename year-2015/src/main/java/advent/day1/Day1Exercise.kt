package advent.day1

import advent.getInput
import util.takeWhileInclusive

fun main() {
    val line = getInput(1).first()
    println(part1(line))
    println(part2(line))
}

private fun part1(line: String): Int {
    return line.iterator().asSequence()
        .map(Char::toFloorDelta)
        .sum()
}

private fun part2(line: String): Int {
    val iterator = line.iterator()
    return generateSequence(0 to 0) { (index, value) ->
        index + 1 to value + iterator.nextChar().toFloorDelta()
    }
        .takeWhileInclusive { (_, value) -> value >= 0 }
        .last().first
}

private fun Char.toFloorDelta() =
    when (this) {
        '(' -> 1
        ')' -> -1
        else -> throw IllegalArgumentException(toString())
    }


