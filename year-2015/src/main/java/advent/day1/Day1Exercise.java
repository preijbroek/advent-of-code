package advent.day1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Day1Exercise {

    private Day1Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        String input = Files.readAllLines(Paths.get("year-2015/src/main/resources/day1exercise.csv")).get(0);
        part1(input);
        part2(input);
    }

    private static void part1(final String input) {
        int open = '(';
        int closed = ')';
        long up = input.chars().filter(c -> c == open).count();
        long down = input.chars().filter(c -> c == closed).count();
        System.out.println(up - down);
    }

    private static void part2(final String input) {
        int floor = 0;
        int index = 0;
        while (floor != -1) {
            floor += relativeFloor(input.charAt(index));
            index++;
        }
        System.out.println(index);
    }

    private static int relativeFloor(final char c) {
        switch (c) {
            case '(': return 1;
            case ')': return -1;
            default: throw new IllegalStateException("Encountered weird char " + c);
        }
    }


}
