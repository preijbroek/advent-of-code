package advent.day19;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Day19Exercise {

    private Day19Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day19exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        Set<String> elements = new HashSet<>();
        List<Reaction> reactions = IntStream.range(0, (code.size() - 2))
                .mapToObj(i -> Reaction.from(code.get(i)))
                .collect(Collectors.toList());
        final String element = code.get(code.size() - 1);
        for (Reaction reaction : reactions) {
            int index = 0;
            while (index < element.length() && element.substring(index).contains(reaction.getInput())) {
                int replaceIndex = element.indexOf(reaction.getInput(), index);
                String newElement = element.substring(0, replaceIndex) + reaction.getOutput()
                        + element.substring(replaceIndex + reaction.getInput().length());
                elements.add(newElement);
                index++;
            }
        }
        System.out.println(elements.size());
    }

    private static void part2(final List<String> code) {
        final String element = code.get(code.size() - 1);
        long brackets = IntStream.range(0, element.length()).filter(v -> element.substring(v).startsWith("Ar") || element.substring(v).startsWith("Rn")).count();
        long commas = IntStream.range(0, element.length()).filter(v -> element.charAt(v) == 'Y').count();
        String reactions = element.chars()
                .mapToObj(v -> (char) v)
                .filter(Character::isUpperCase)
                .map(String::valueOf)
                .collect(Collectors.joining());
        System.out.println(reactions.length() - brackets - 2 * commas - 1);
    }
}
