package advent.day19;

import java.util.Objects;

public final class Reaction {

    private final String input;
    private final String output;

    public Reaction(final String input, final String output) {
        this.input = input;
        this.output = output;
    }

    public static Reaction from(String s) {
        String[] elements = s.split(" => ");
        return new Reaction(elements[0], elements[1]);
    }

    public String getInput() {
        return input;
    }

    public String getOutput() {
        return output;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reaction reaction = (Reaction) o;
        return Objects.equals(input, reaction.input) &&
                Objects.equals(output, reaction.output);
    }

    @Override
    public int hashCode() {
        return Objects.hash(input, output);
    }
}
