package advent.day8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public final class Day8Exercise {

    private Day8Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day8exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        int totalSum = code.stream().mapToInt(String::length).sum();
        int readSum = code.stream()
                .map(value -> value.substring(1, value.length() - 1))
                .map(Day8Exercise::reduceValue).mapToInt(String::length).sum();
        System.out.println(totalSum - readSum);
    }

    private static void part2(final List<String> code) {
        int totalSum = code.stream().mapToInt(String::length).sum();
        int writeSum = code.stream()
                .map(Day8Exercise::increaseValue)
                .mapToInt(String::length).sum();
        System.out.println(writeSum - totalSum);
    }

    private static String reduceValue(final String s) {
        String result = s;
        result = result.replaceAll("\\\\\\\\", "\\\\");
        result = result.replaceAll("\\\\\"", "\"");
        result = result.replaceAll("\\\\x([0-9]|[a-f]){2}", "1");
        System.out.println("Before: " + s + "\n" + "After: " + result + "\n");
        return result;
    }

    private static String increaseValue(final String s) {
        String result = s;
        result = result.replaceAll("\\\\", "\\\\\\\\");
        result = result.replaceAll("\"", "\\\\\"");
        result = "\"" + result + "\"";
        System.out.println("Before: " + s + "\n" + "After: " + result + "\n");
        return result;
    }
}
