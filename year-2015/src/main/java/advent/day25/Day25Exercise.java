package advent.day25;

import java.math.BigInteger;

public final class Day25Exercise {

    private static final int ROW = 2981;
    private static final int COLUMN = 3075;

    private static final BigInteger A = BigInteger.valueOf(252533);
    private static final BigInteger B = BigInteger.valueOf(33554393);

    private static final BigInteger FIRST_VALUE = BigInteger.valueOf(20151125);

    private Day25Exercise() {
    }

    public static void main(String[] args) {
        part1();
        part2();
    }

    private static void part1() {
        final int requiredNumber = (ROW + COLUMN - 2) * (ROW + COLUMN - 1) / 2 + COLUMN - 1;
        int i = requiredNumber;
        BigInteger a = A;
        BigInteger result = FIRST_VALUE;
        while (i != 0) {
            if ((i & 1) == 1) {
                result = result.multiply(a).mod(B);
            }
            a = a.multiply(a).mod(B);
            i >>= 1;
        }
        System.out.println(result);
    }

    private static void part2() {

    }

}
