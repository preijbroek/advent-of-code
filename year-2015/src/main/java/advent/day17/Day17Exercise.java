package advent.day17;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static util.Comparators.minOf;

public final class Day17Exercise {

    private Day17Exercise() {
    }

    public static void main(final String[] args) throws IOException {
        List<String> code = Files.readAllLines(Paths.get("year-2015/src/main/resources/day17exercise.csv"));
        part1(code);
        part2(code);
    }

    private static void part1(final List<String> code) {
        List<Integer> containers = code.stream().mapToInt(Integer::parseInt).boxed().collect(toList());
        System.out.println(subLists(containers).stream().filter(list -> list.stream().mapToInt(Integer::intValue).sum() == 150).count());
    }

    private static void part2(final List<String> code) {
        List<Integer> containers = code.stream().mapToInt(Integer::parseInt).boxed().collect(toList());
        List<List<Integer>> options = subLists(containers).stream().filter(list -> list.stream().mapToInt(Integer::intValue).sum() == 150).collect(toList());
        int requiredSize = minOf(options, List::size).size();
        System.out.println(options.stream().filter(option -> option.size() == requiredSize).count());
    }

    private static <E> List<List<E>> subLists(final List<E> elements) {
        List<List<E>> powerList = new ArrayList<>();
        int allMasks = (1 << elements.size());
        for (int i = 1; i < allMasks; i++) {
            List<E> subList = new ArrayList<>();
            for (int j = 0; j < elements.size(); j++) {
                if ((i & (1 << j)) > 0) { //The j-th element is used
                    subList.add(elements.get(j));
                }
            }
            powerList.add(subList);
        }
        return powerList;
    }

}
