package advent.day22

data class Player(val deck: ArrayDeque<Int>) {

    fun hasCards() = deck.isNotEmpty()

    fun addCards(card1: Int, card2: Int) {
        deck.add(card1)
        deck.add(card2)
    }

    fun getFirst() = deck.removeFirst()

    fun cardCount() = deck.size

    fun subPlayer(deckSize: Int) = Player(ArrayDeque(deck.dropLast(deck.size - deckSize)))

    fun cardsString() = deck.joinToString(":")

    fun score() = deck.reversed().mapIndexed { index, it -> (index + 1) * it }.sum()
}