package advent.day22

import advent.getInputBlocks

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Int {
    val (player1, player2) = input()
    return Game(player1, player2).winner().score()
}

private fun part2(): Int {
    val (player1, player2) = input()
    return Game(player1, player2, recursive = true).winner().score()
}

private fun input(): Pair<Player, Player> {
    val players = getInputBlocks(22).asSequence()
            .map { it.split("\n") }
            .map { it.drop(1) }
            .map { it.map { value -> value.toInt() } }
            .map { ArrayDeque(it) }
            .map { Player(it) }
            .toList()
    return Pair(players[0], players[1])
}
