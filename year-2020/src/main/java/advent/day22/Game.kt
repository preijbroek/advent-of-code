package advent.day22

data class Game(val player1: Player, val player2: Player, val recursive: Boolean = false) {

    fun winner(): Player {
        val configs = HashSet<String>()
        while (player1.hasCards() && player2.hasCards()) {
            if (recursive && !configs.add("${player1.cardsString()},${player2.cardsString()}")) {
                return player1
            }
            val card1 = player1.getFirst()
            val card2 = player2.getFirst()
            when {
                recursive && player1.cardCount() >= card1 && player2.cardCount() >= card2 -> {
                    val subPlayer1 = player1.subPlayer(deckSize = card1)
                    val subPlayer2 = player2.subPlayer(deckSize = card2)
                    when (Game(subPlayer1, subPlayer2, recursive = this.recursive).winner()) {
                        subPlayer1 -> player1.addCards(card1, card2)
                        subPlayer2 -> player2.addCards(card2, card1)
                    }
                }
                card1 > card2 -> player1.addCards(card1, card2)
                card1 < card2 -> player2.addCards(card2, card1)
            }
        }
        return listOf(player1, player2).first { it.hasCards() }
    }
}