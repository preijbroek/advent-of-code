package advent.day3

import advent.getInput
import util.product

fun main() {
    println(part1())
    println(part2())
}

private fun part1() = traverse(3, 1)

private fun part2(): Long {
    return listOf(
            listOf(1, 1),
            listOf(3, 1),
            listOf(5, 1),
            listOf(7, 1),
            listOf(1, 2)
    )
            .map { traverse(it[0], it[1]) }
            .map { it.toLong() }
            .product()
}

private fun traverse(hDelta: Int, vDelta: Int): Int {
    val input = input()
    var hIndex = 0
    var count = 0
    for (vIndex in input.indices step vDelta) {
        if (input[vIndex][hIndex] == '#') {
            count++
        }
        hIndex =(hIndex + hDelta) % input[vIndex].size
    }
    return count
}

private fun input() = getInput(3).map { it.toList() }
