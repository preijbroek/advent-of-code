package advent.day12

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Int {
    val ship = Ship()
    ship.run(input())
    return ship.distanceToCentre()
}

private fun part2(): Int {
    val ship = Ship()
    ship.runWithWayPoint(input())
    return ship.distanceToCentre()
}

private fun input() = getInput(12).map { Pair(it[0], it.drop(1).toInt()) }
