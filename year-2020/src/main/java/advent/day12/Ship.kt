package advent.day12

import util.Direction
import util.Position
import util.plus
import util.times

class Ship {

    var currentPosition: Position = Position.CENTRE
        private set
    private var currentDirection: Direction = Direction.EAST

    fun distanceToCentre() = currentPosition.distanceToCentre()

    fun run(instructions: List<Pair<Char, Int>>) {
        for (instruction in instructions) {
            when (instruction.first) {
                'E' -> currentPosition = currentPosition.east(instruction.second)
                'S' -> currentPosition = currentPosition.south(instruction.second)
                'W' -> currentPosition = currentPosition.west(instruction.second)
                'N' -> currentPosition = currentPosition.north(instruction.second)
                'F' -> currentPosition = currentPosition.to(currentDirection, instruction.second)
                'L' -> {
                    for (i in 0 ..< instruction.second / 90) {
                        currentDirection = currentDirection.left()
                    }
                }
                'R' -> {
                    for (i in 0 ..< instruction.second / 90) {
                        currentDirection = currentDirection.right()
                    }
                }
            }
        }
    }

    fun runWithWayPoint(instructions: List<Pair<Char, Int>>) {
        val wayPoint = Ship()
        wayPoint.currentPosition = Position(10, 1)
        for (instruction in instructions) {
            when (instruction.first) {
                'E' -> wayPoint.currentPosition = wayPoint.currentPosition.east(instruction.second)
                'S' -> wayPoint.currentPosition = wayPoint.currentPosition.south(instruction.second)
                'W' -> wayPoint.currentPosition = wayPoint.currentPosition.west(instruction.second)
                'N' -> wayPoint.currentPosition = wayPoint.currentPosition.north(instruction.second)
                'F' -> currentPosition += (wayPoint.currentPosition * instruction.second)
                'L' -> wayPoint.currentPosition *= Matrix(0, 1, -1, 0).pow(instruction.second / 90)
                'R' -> wayPoint.currentPosition *= Matrix(0, -1, 1, 0).pow(instruction.second / 90)
            }
        }
    }
}

private operator fun Position.times(matrix: Matrix) =
    Position(
        matrix.a00 * this.x + matrix.a10 * this.y,
        matrix.a01 * this.x + matrix.a11 * this.y
    )

private data class Matrix(val a00: Int, val a01: Int, val a10: Int, val a11: Int) {
    fun pow(power: Int): Matrix =
        if (power == 1) {
            this
        } else {
            this.times(pow(power - 1))
        }

    operator fun times(matrix: Matrix) = Matrix(
        (a00 * matrix.a00) + (a01 * matrix.a10),
        (a00 * matrix.a01) + (a01 * matrix.a11),
        (a10 * matrix.a00) + (a11 * matrix.a10),
        (a10 * matrix.a01) + (a11 * matrix.a11)
    )
}