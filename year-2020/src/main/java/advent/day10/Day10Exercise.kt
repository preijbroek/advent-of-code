package advent.day10

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Int {
    val input = input()
    val indices = input.indices.drop(1)
    var threes = 0
    var ones = 0
    indices.forEach {
        when (input[it] - input[it - 1]) {
            3 -> threes++
            1 -> ones++
        }
    }
    return threes * ones
}

private fun part2(): Long {
    val input = input()
    val map = sortedMapOf<Int, Long>()
    for (i in input) {
        map[i] = listOf(i - 1, i - 2, i - 3).filter { it >= 0 }
                .mapNotNull { map[it] }
                .sum()
                .let { maxOf(it, 1L) }
    }
    return map.getValue(map.lastKey())
}

private fun input(): List<Int> {
    val input = getInput(10).map { it.toInt() }.toMutableList()
    return (input + 0 + (input.maxOrNull()!! + 3)).sorted()
}
