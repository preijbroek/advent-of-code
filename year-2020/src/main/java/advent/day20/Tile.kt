package advent.day20

data class Tile(val id: Long, var grid: List<List<Char>>) {

    operator fun get(y: Int) = grid[y]

    fun nonOrientedEdges(): List<String> = listOf(
        leftEdge(),
        topEdge(),
        rightEdge(),
        bottomEdge()
    )
        .map { it.joinToString("") }
        .map { minOf(it, it.reversed()) }

    fun leftEdge() = grid.map { it.first() }
    fun rightEdge() = grid.map { it.last() }
    fun topEdge() = grid.first()
    fun bottomEdge() = grid.last()

    fun containsMultiple(edges: Set<String>): Boolean {
        return edges.intersect(nonOrientedEdges()).size == 2
    }

    fun rotate() {
        grid = grid.flatMap { it.withIndex() }
            .groupBy({ (index, _) -> index }, { (_, c) -> c })
            .map { (_, list) -> list.reversed() }
    }

    fun flip() {
        grid = grid.reversed()
    }

    fun dropEdges() {
        grid = grid.drop(1).dropLast(1).map { it.drop(1).dropLast(1) }
    }

    fun combineHorizontally(other: Tile): Tile {
        val grid = this.grid.indices.map { this.grid[it] + other.grid[it] }
        return Tile(this.id + other.id, grid)
    }

    fun combineVertically(other: Tile): Tile {
        val grid = this.grid + other.grid
        return Tile(this.id + other.id, grid)
    }

    fun roughness() = grid.flatten().count { it == '#' } - 15 * seaMonsterCount()

    private fun seaMonsterCount(): Int {
        var count = 0
        var iterations = 0
        while (count == 0) {
            for (y in 0 ..< grid.size - 2) {
                for (x in 18 ..< grid[0].size - 1) {
                    if (listOf(
                            grid[y][x],
                            grid[y + 1][x - 18],
                            grid[y + 1][x - 13],
                            grid[y + 1][x - 7],
                            grid[y + 1][x - 6],
                            grid[y + 1][x - 1],
                            grid[y + 1][x],
                            grid[y + 1][x + 1],
                            grid[y + 2][x - 17],
                            grid[y + 2][x - 14],
                            grid[y + 2][x - 11],
                            grid[y + 2][x - 8],
                            grid[y + 2][x - 11],
                            grid[y + 2][x - 2],
                        ).all { it == '#' }
                    ) {
                        count++
                    }
                }
            }
            iterations++
            rotate()
            if (iterations % 4 == 0) {
                flip()
            }
        }
        return count
    }
}