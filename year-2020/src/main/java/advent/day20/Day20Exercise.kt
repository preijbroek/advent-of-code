package advent.day20

import advent.getInputBlocks
import util.product
import kotlin.math.sqrt

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Long {
    val input = input()
    val occurrences = input.flatMap { it.nonOrientedEdges() }
        .groupingBy { it }.eachCount().filter { it.value == 1 }.keys
    return input.filter { it.containsMultiple(occurrences) }.map { it.id }.product()
}

private fun part2(): Int {
    val input = input()
    val image = input.createImage()
    return image.roughness()
}

private fun List<Tile>.createImage(): Tile {
    val imageSize = sqrt(this.size.toDouble()).toInt()
    val edgeStrings = this.flatMap { it.nonOrientedEdges() }
        .groupingBy { it }.eachCount().filter { it.value == 1 }.keys
    val firstCorner = this.first { it.containsMultiple(edgeStrings) }
    for (i in 1..8) {
        if (edgeStrings.contains(firstCorner.topEdge().joinToString(""))
            && edgeStrings.contains(firstCorner.leftEdge().joinToString(""))
        ) {
            break
        } else if (edgeStrings.contains(firstCorner.topEdge().reversed().joinToString(""))
            && edgeStrings.contains(firstCorner.leftEdge().reversed().joinToString(""))
        ) {
            break
        }
        firstCorner.rotate()
        if (i == 4) {
            firstCorner.flip()
        }
    }
    val imageTiles: MutableList<MutableList<Tile>> = ArrayList()
    repeat(imageSize) {
        imageTiles.add(ArrayList())
    }
    for (y in 0 ..< imageSize) {
        for (x in 0 ..< imageSize) {
            if (y == 0 && x == 0) {
                imageTiles[0].add(firstCorner)
            } else if (x == 0) {
                imageTiles[y].add(findMatchingTile(this) { it != imageTiles[y - 1][x] && it.topEdge() == imageTiles[y - 1][x].bottomEdge() })
            } else {
                imageTiles[y].add(findMatchingTile(this) { it != imageTiles[y][x - 1] && it.leftEdge() == imageTiles[y][x - 1].rightEdge() })
            }
        }
    }
    imageTiles.forEach { it.forEach { tile -> tile.dropEdges() } }
    return imageTiles.map { it.reduce { acc, tile -> acc.combineHorizontally(tile) } }
        .reduce { acc, tile -> acc.combineVertically(tile) }
}

private fun findMatchingTile(input: List<Tile>, condition: (Tile) -> Boolean): Tile {
    for (tile in input) {
        for (i in 1..8) {
            if (condition(tile)) {
                return tile
            }
            tile.rotate()
            if (i % 4 == 0) {
                tile.flip()
            }
        }
    }
    throw IllegalStateException("Tile not matched: $condition")
}

private fun input() = getInputBlocks(20).map { it.split("\n") }
    .map {
        Tile(
            it[0].toTileId(),
            it.drop(1).toGrid()
        )
    }

private fun String.toTileId() = this.split(" ")[1].replace(":", "").toLong()
private fun List<String>.toGrid() = this.map { it.toList() }