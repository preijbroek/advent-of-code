package advent.day1

import advent.getInput
import util.product

fun main() {
    println("${get2000sumNumbersTwoNumbers().product()}")
    println("${get2000sumNumbersThreeNumbers().product()}")
}

private fun get2000sumNumbersTwoNumbers(): List<Int> {
    val numbers = input()
    return requiredNumbersList(numbers, 0, 0)
}

private fun get2000sumNumbersThreeNumbers(): List<Int> {
    val numbers = input()
    for (i in numbers.indices) {
        val list = requiredNumbersList(numbers, numbers[i], i+1)
        if (list.isNotEmpty()) {
            return list + numbers[i]
        }
    }
    return emptyList()
}

private fun requiredNumbersList(numbers: List<Int>, base: Int, baseIndex: Int): List<Int> {
    outer@for (i in baseIndex..numbers.lastIndex) {
        for (j in i+1..numbers.lastIndex) {
            val result = base + numbers[i] + numbers[j]
            when {
                result > 2020 -> continue@outer
                result == 2020 -> return listOf(numbers[i], numbers[j])
            }
        }
    }
    return emptyList()
}

private fun input() = getInput(1).map { it.toInt() }.sorted()
