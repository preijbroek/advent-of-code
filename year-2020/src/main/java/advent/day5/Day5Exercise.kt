package advent.day5

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1() = input().maxOrNull()

private fun part2(): Int {
    val seatIds = input()
    return (0..seatIds.maxOrNull()!!).first { i -> i !in seatIds && seatIds.contains(i - 1) && seatIds.contains(i + 1) }
}

private fun input() = getInput(5).map { BoardingPass(it).seatId() }.toSet()

private data class BoardingPass(val row: Int, val column: Int) {
    constructor(partition: String) : this(partition.toRow(), partition.toColumn())

    fun seatId() = row * 8 + column
}

private fun String.toRow() = substring(0, 7).toInt('B')

private fun String.toColumn() = substring(7).toInt('R')

private fun String.toInt(oneChar: Char) = this.map { if (it == oneChar) { '1' } else { '0' } }.joinToString("").toInt(2)
