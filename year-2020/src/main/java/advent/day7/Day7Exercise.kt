package advent.day7

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1() = input().count { it.hasRequirement("shiny gold") }

private fun part2() = input().getWithColour("shiny gold").countContents()

private fun input(): List<Bag> {
    val lines = getInput(7)
    val input = lines.map { it.toBag() }
    for (line in lines.indices) {
        val info = lines[line].split(" contain ")[1].split(", ")
        for (requirement in info) {
            val split = requirement.split(" ")
            if (split[0] != "no") {
                input[line].addRequirement(input.getWithColour("${split[1]} ${split[2]}"), split[0].toInt())
            }
        }
    }
    return input
}

private fun List<Bag>.getWithColour(colour: String) = this.first { it.hasColour(colour) }