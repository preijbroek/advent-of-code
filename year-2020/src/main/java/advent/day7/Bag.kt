package advent.day7

data class Bag(val colour: String) {
    private val requirements = HashMap<Bag, Int>()

    fun addRequirement(bag: Bag, times: Int) = requirements.put(bag, times)

    fun hasRequirement(colour: String): Boolean = hasDirectRequirement(colour) || requirements.keys.any { it.hasRequirement(colour) }

    private fun hasDirectRequirement(colour: String) = requirements.mapKeys { it.key.colour }.contains(colour)

    fun countContents(): Int = requirements.values.sum() + requirements.map { it.value * it.key.countContents() }.sum()

    fun hasColour(colour: String) = this.colour == colour
}

fun String.toBag(): Bag {
    val end = indexOf(" bags")
    return Bag(substring(0, end))
}