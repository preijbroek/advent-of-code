package advent.day13

import advent.getInput
import util.modInverse
import util.productOf

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Long {
    val (timestamp, busIds) = input()
    val firstArrivingBus = busIds.filter { it != "x" }.map { it.toLong() }
        .map { Pair(it, it - (timestamp % it)) }
        .minByOrNull { it.second }
    return firstArrivingBus!!.first * firstArrivingBus.second
}

private fun part2(): Long {
    val busses = input().second.mapIndexed { index, busId -> Pair(index.toLong(), busId) }
        .filter { it.second != "x" }
        .map { Pair(it.first, it.second.toLong()) }
        .map { it.copy(first = (it.second - it.first).mod(it.second)) }
    val product = busses.productOf { it.second }
    return busses.sumOf { bus -> crtTerm(product, bus) }.mod(product)
}

private fun crtTerm(product: Long, bus: Pair<Long, Long>) =
    (bus.first * (product / bus.second).modInverse(bus.second) * (product / bus.second)).mod(product)

private fun input(): Pair<Long, List<String>> {
    val input = getInput(13)
    return Pair(input[0].toLong(), input[1].split(","))
}
