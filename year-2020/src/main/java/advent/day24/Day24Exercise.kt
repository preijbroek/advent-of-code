package advent.day24

import advent.getInput
import util.Position

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Int {
    return flippedTiles(input()).size
}

private fun part2(): Int {
    val flippedTiles = flippedTiles(input())
    repeat(100) {
        val newBlacks = flippedTiles.asSequence()
                .map { it.hexagonalNeighbours() }
                .flatten()
                .distinct()
                .filterNot(flippedTiles::contains)
                .filter { it.hexagonalNeighbours().intersect(flippedTiles).size == 2 }
                .toSet()
        val newWhites = flippedTiles.filter { it.hexagonalNeighbours().intersect(flippedTiles).size !in 1..2 }
        flippedTiles.removeAll(newWhites.toSet())
        flippedTiles.addAll(newBlacks)
    }
    return flippedTiles.size
}

private fun flippedTiles(tilesToFlip: List<Position>): HashSet<Position> {
    val flippedTiles = HashSet<Position>()
    for (position in tilesToFlip) {
        if (flippedTiles.contains(position)) {
            flippedTiles.remove(position)
        } else {
            flippedTiles.add(position)
        }
    }
    return flippedTiles
}

private fun String.toPosition(): Position {
    var currentPosition = Position.CENTRE
    val instructionChars = ArrayList<Char>()
    for (c in this) {
        instructionChars.add(c)
        if (c == 'e' || c == 'w') {
            when (instructionChars.joinToString("")) {
                "e" -> currentPosition = currentPosition.east().east()
                "se" -> currentPosition = currentPosition.south().east()
                "sw" -> currentPosition = currentPosition.south().west()
                "w" -> currentPosition = currentPosition.west().west()
                "nw" -> currentPosition = currentPosition.north().west()
                "ne" -> currentPosition = currentPosition.north().east()
            }
            instructionChars.clear()
        }
    }
    return currentPosition
}

private fun Position.hexagonalNeighbours(): List<Position> {
    return listOf(
            this.east().east(),
            this.south().east(),
            this.south().west(),
            this.west().west(),
            this.north().west(),
            this.north().east()
    )
}

private fun input() = getInput(24).map { it.toPosition() }
