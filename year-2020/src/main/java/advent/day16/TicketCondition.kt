package advent.day16

data class TicketCondition(val name: String, val bottomRange: LongRange, val topRange: LongRange) {

    fun contains(value: Long) = bottomRange.contains(value) || topRange.contains(value)

    fun isDepartureCondition() = name.startsWith("departure")
}