package advent.day16

import advent.getInputBlocks
import util.product

fun main() {
    println(part1())
    println(part2())
}

private fun part1() : Long {
    val (conditions, _, nearbyTickets) = input()
    return nearbyTickets.flatMap { it.values }
            .filter { conditions.none { condition -> condition.contains(it) } }
            .sum()
}

private fun part2(): Long {
    var (conditions, myTicket, nearbyTickets) = input()
    nearbyTickets = nearbyTickets.filter { !it.values.any { value -> conditions.none { condition -> condition.contains(value) } } }
    val positionMap = HashMap<TicketCondition, Int>()
    val numberOfConditions = conditions.size
    while (positionMap.size != numberOfConditions) {
        for (position in myTicket.values.indices) {
            val matching = conditions.filter { condition -> nearbyTickets.map { it.values[position] }.all { condition.contains(it) } }
            if (matching.size == 1) {
                positionMap[matching[0]] = position
                conditions.remove(matching[0])
            }
        }
    }
    return positionMap.filter { it.key.isDepartureCondition() }
            .map { myTicket[it.value] }
            .product()
}

private fun input(): Triple<MutableList<TicketCondition>, Ticket, List<Ticket>> {
    val inputBlocks = getInputBlocks(16)
    val ticketConditions = inputBlocks[0].toTicketConditions()
    val myTicket = inputBlocks[1].toTicket()
    val nearbyTickets = inputBlocks[2].toTickets()
    return Triple(ticketConditions, myTicket, nearbyTickets)
}

private fun String.toTicketConditions(): MutableList<TicketCondition> {
    return this.split("\n")
            .map { it.split(": ") }
            .map {
                val ranges = it[1].split(" or ")
                        .map { range -> range.split("-").map { value -> value.toLong() } }
                        .map { numbers -> numbers[0]..numbers[1] }
                TicketCondition(it[0], ranges[0], ranges[1])
            }
            .toMutableList()
}

private fun String.toTicket() = toTickets()[0]

private fun String.toTickets() = this.split("\n").drop(1)
        .map { Ticket(it.split(",").map { value -> value.toLong() }) }
