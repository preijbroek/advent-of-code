package advent.day16

data class Ticket(val values: List<Long>) {

    operator fun get(index: Int) = values[index]
}