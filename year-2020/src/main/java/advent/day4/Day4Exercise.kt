package advent.day4

import advent.getInputBlocks

fun main() {
    println(part1())
    println(part2())
}

private fun part1() = passports().count { it.hasRequiredFieldsPresent() }
private fun part2() = passports().count { it.hasValidValues() }

private fun passports(): List<Passport> = getInputBlocks(4).map { Passport(it) }
