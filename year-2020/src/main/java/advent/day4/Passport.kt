package advent.day4

internal data class Passport(
        private val byr: String?,
        private val iyr: String?,
        private val eyr: String?,
        private val hgt: String?,
        private val hcl: String?,
        private val ecl: String?,
        private val pid: String?,
        private val cid: String?
) {

    constructor(fieldMap: Map<String, String>) : this(
            fieldMap["byr"],
            fieldMap["iyr"],
            fieldMap["eyr"],
            fieldMap["hgt"],
            fieldMap["hcl"],
            fieldMap["ecl"],
            fieldMap["pid"],
            fieldMap["cid"]
    )

    constructor(string: String) : this(
            string.split(Regex("[\n ]")).filter { it.isNotBlank() }
                    .map { it.split(":") }
                    .map { it[0] to it[1] }
                    .toMap()
    )

    fun hasRequiredFieldsPresent(): Boolean {
        return this::class.java.declaredFields
                .filter { it.name != "cid" }
                .all { it.get(this) != null }
    }

    fun hasValidValues(): Boolean {
        return hasRequiredFieldsPresent()
                && hasValidByr()
                && hasValidIyr()
                && hasValidEyr()
                && hasValidHgt()
                && hasValidHcl()
                && hasValidEcl()
                && hasValidPid()
    }

    private fun hasValidByr() = byr.inRange(1920, 2002)

    private fun hasValidIyr() = iyr.inRange(2010, 2020)

    private fun hasValidEyr() = eyr.inRange(2020, 2030)

    private fun String?.inRange(start: Int, end: Int) = this != null && this.toInt() in start..end

    private fun hasValidHgt(): Boolean {
        return hgt.matches("\\d+?(cm|in)")
                && Height(hgt!!.filter { it.isDigit() }.toInt(), hgt.filter { it.isLetter() }).isValid()
    }

    private class Height(val size: Int, val type: String) {
        fun isValid() = when (type) {
            "cm" -> size in 150..193
            "in" -> size in 59..76
            else -> false
        }
    }

    private fun hasValidHcl() = hcl.matches("#[\\da-f]{6}")

    private fun hasValidEcl() = setOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth").contains(ecl)

    private fun hasValidPid() = pid.matches("\\d{9}")

    private fun String?.matches(pattern: String): Boolean = this != null && this.matches(Regex(pattern))
}