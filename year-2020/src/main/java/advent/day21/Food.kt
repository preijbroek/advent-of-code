package advent.day21

import util.isDisjoint

data class Food(val ingredients: Set<String>, val allergens: Set<String>) {

    constructor(ingredients: String, allergens: String): this(ingredients.split(" ").toSet(), allergens.split(", ").toSet())

    fun intersect(other: Food) = Food(other.ingredients.intersect(this.ingredients), other.allergens.intersect(this.allergens))

    fun hasMatchingAllergens(other: Food) = !this.allergens.isDisjoint(other.allergens)

    fun toNoAllergensIngredients(ingredients: Collection<String>): Food {
        val noAllergensIngredients = this.ingredients.toMutableSet()
        noAllergensIngredients.removeAll(ingredients.toSet())
        return Food(noAllergensIngredients, emptySet())
    }

    fun withRemovedImpossibleAllergensIngredients(ingredients: Collection<String>): Food {
        val reducedIngredients = this.ingredients.toMutableSet()
        reducedIngredients.removeAll(ingredients.toSet())
        return Food(reducedIngredients, allergens)
    }

    fun ingredientsCount() = ingredients.size
}