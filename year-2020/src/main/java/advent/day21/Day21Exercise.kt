package advent.day21

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Int {
    val input = input()
    val ingredientsWithAllergens = foodsWithAllergens(input).flatMap { it.ingredients }.toSet()
    return input.map { it.toNoAllergensIngredients(ingredientsWithAllergens) }
            .sumBy { it.ingredientsCount() }
}

private fun part2(): String {
    val input = input()
    val foodsWithAllergens = foodsWithAllergens(input)
    while (foodsWithAllergens.any { it.ingredients.size != 1 }) {
        val determinedIngredients = foodsWithAllergens.filter { it.ingredients.size == 1 }
                .flatMap { it.ingredients }.toSet()
        val undeterminedIngredients = foodsWithAllergens.filter { it.ingredients.size != 1 }
        foodsWithAllergens.removeAll(undeterminedIngredients)
        foodsWithAllergens.addAll(undeterminedIngredients.map { it.withRemovedImpossibleAllergensIngredients(determinedIngredients) })
    }
    return foodsWithAllergens.sortedBy { it.allergens.first() }
            .joinToString(",") { it.ingredients.first() }
}

private fun foodsWithAllergens(input: Collection<Food>): HashSet<Food> {
    val foodsWithAllergens = HashSet<Food>()
    for (food in input) {
        val matchingFoods = input.filter { food.hasMatchingAllergens(it) }
        food.allergens.map {
            allergen -> matchingFoods.filter { it.allergens.contains(allergen) }
                .reduce { acc, it -> acc.intersect(it) }
        }.forEach { foodsWithAllergens.add(it) }
    }
    return foodsWithAllergens
}

private fun input() = getInput(21).map { it.split(" (contains ") }
        .map { listOf(it[0], it[1].dropLast(1)) }
        .map { Food(it[0], it[1]) }

