package advent.day9

data class Preamble(val number: Long, private val summable: List<Long>) {

    constructor(numbers: List<Long>) : this(numbers[25], numbers.subList(0, 25).sorted())

    fun isValid(): Boolean {
        for (l1 in summable.indices) {
            for (l2 in l1 + 1 ..< summable.size) {
                when {
                    summable[l1] + summable[l2] == number -> return true
                    summable[l1] + summable[l2] > number -> break
                }
            }
        }
        return false
    }
}