package advent.day9

import advent.getInput

fun main() {
    val part1 = part1()
    println(part1)
    println(part2(part1))
}

private fun part1(): Long {
    val input = input()
    val preambles = mutableListOf<Preamble>()
    for (l in 25 ..< input.size) {
        preambles.add(Preamble(input.subList(l - 25, l + 1)))
    }
    return preambles.find { !it.isValid() }?.number!!
}

private fun part2(invalidNumber: Long): Long {
    val input = input()
    val results = mutableListOf<Long>()
    for (l in input) {
        results += l
        var sum = results.sum()
        while (sum > invalidNumber) {
            sum -= results.removeFirst()
        }
        if (sum == invalidNumber) {
            return results.minOrNull()!! + results.maxOrNull()!!
        }
    }
    return Long.MIN_VALUE
}

private fun input() = getInput(9).map { it.toLong() }
