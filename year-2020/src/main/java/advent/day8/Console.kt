package advent.day8

data class Console(private val instructions: List<List<String>>) {

    var accumulator = 0
        private set

    private var currentPosition = 0

    fun runUniqueCodes() {
        val runIndices: MutableSet<Int> = mutableSetOf()
        while (runIndices.add(currentPosition) && currentPosition < instructions.size) {
            runCurrentCommand()
        }
    }

    private fun runCurrentCommand() {
        val instruction = instructions[currentPosition]
        when (instruction[0]) {
            "acc" -> {
                accumulator += instruction[1].toInt()
                currentPosition++
            }
            "jmp" -> currentPosition += instruction[1].toInt()
            "nop" -> currentPosition++
            else -> throw IllegalArgumentException("unknown instruction ${instruction.joinToString(" ")}")
        }
    }

    fun isFinished(): Boolean {
        return currentPosition >= instructions.size
    }
}