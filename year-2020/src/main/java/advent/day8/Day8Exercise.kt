package advent.day8

import advent.getInput
import util.deepModifiableCopy

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Int {
    val console = Console(input())
    console.runUniqueCodes()
    return console.accumulator
}

private fun part2(): Int {
    val input = input()
    for (line in input.indices) {
        val modifiedInput = input.deepModifiableCopy()
        val instruction = modifiedInput[line]
        when (instruction[0]) {
            "jmp" -> instruction[0] = "nop"
            "nop" -> instruction[0] = "jmp"
            else -> continue
        }
        val console = Console(modifiedInput)
        console.runUniqueCodes()
        if (console.isFinished()) {
            return console.accumulator
        }
    }
    return Int.MIN_VALUE
}

private fun input() = getInput(8).map { it.split(" ") }
