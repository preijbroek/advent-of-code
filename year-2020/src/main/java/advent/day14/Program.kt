package advent.day14

data class Program(private val input: List<String>) {

    private val memory = HashMap<Long, Long>()

    fun runValues() {
        var mask = ""
        for (s in input) {
            val instruction = s.split(" = ")
            if (instruction[0] == "mask") {
                mask = instruction[1].mask('X', 'X')
            } else {
                val memoryLocation = instruction[0].toMemoryLocation().toLong()
                val value = instruction[1].toBinaryString()
                memory[memoryLocation] = value.withMaskApplied(mask)[0].toLong(2)
            }
        }
    }

    fun runKeys() {
        var mask = ""
        for (s in input) {
            val instruction = s.split(" = ")
            if (instruction[0] == "mask") {
                mask = instruction[1].mask('Y', '0')
            } else {
                val memoryLocation = instruction[0].toMemoryLocation().toBinaryString()
                val value = instruction[1].toLong()
                memoryLocation.withMaskApplied(mask).forEach { memory[it.toLong(2)] = value }
            }
        }
    }

    private fun String.mask(padChar: Char, oldChar: Char) = this.padStart(63, padChar).replace(oldChar, 'Y')
    private fun String.toMemoryLocation() = this.replace(Regex("[me\\[\\]]"), "")

    private fun String.withMaskApplied(mask: String): List<String> {
        val builders = ArrayList<StringBuilder>()
        builders.add(StringBuilder())
        for (i in mask.indices) {
            when {
                mask[i] == 'Y' -> builders.forEach { it.append(this[i]) }
                mask[i] == 'X' -> {
                    val floating = builders.map { StringBuilder(it) }
                    builders.forEach { it.append('0') }
                    floating.forEach { it.append('1') }
                    builders.addAll(floating)
                }
                mask[i] == '1' -> builders.forEach { it.append('1') }
                mask[i] == '0' -> builders.forEach { it.append('0') }
            }
        }
        return builders.map { it.toString() }
    }

    fun sumValues() = memory.values.sum()

    private fun String.toBinaryString() = toLong().toString(2).padStart(63, '0')

}