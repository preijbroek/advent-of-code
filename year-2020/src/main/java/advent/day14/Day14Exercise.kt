package advent.day14

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Long {
    val program = Program(input())
    program.runValues()
    return program.sumValues()
}

private fun part2(): Long {
    val program = Program(input())
    program.runKeys()
    return program.sumValues()
}

private fun input() = getInput(14)
