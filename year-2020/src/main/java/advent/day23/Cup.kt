package advent.day23

import util.nonZeroMod

data class Cup(val value: Long) {

    private var next: Cup? = null

    fun setNext(next: Cup) {
        if (this.next == null) {
            this.next = next
        }
    }

    fun next() = next!!

    fun move(cups: Map<Long, Cup>): Cup {
        val pickUp = generateSequence(next) { it.next }.take(3).toList()
        this.next = pickUp.last().next
        var destinationValue = (value - 1L).nonZeroMod(cups.size.toLong())
        while (pickUp.map { it.value }.contains(destinationValue)) {
            destinationValue = (destinationValue - 1L).nonZeroMod(cups.size.toLong())
        }
        val destination = cups.getValue(destinationValue)
        pickUp.last().next = destination.next
        destination.next = pickUp.first()
        return this.next()
    }
}