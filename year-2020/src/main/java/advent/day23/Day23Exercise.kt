package advent.day23

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): String {
    var cup = playGame(numberOfCups = 9, moves = 100)
    val stringBuilder = StringBuilder()
    while (cup.value != 1L) {
        stringBuilder.append(cup.value)
        cup = cup.next()
    }
    return stringBuilder.toString()
}

private fun part2(): Long {
    val cup = playGame(numberOfCups = 1000000, moves = 10000000)
    return cup.value * cup.next().value
}

private fun playGame(numberOfCups: Int, moves: Int): Cup {
    val input = input().map { Cup(it) }.toMutableList()
    input.addAll(LongRange(10L, numberOfCups.toLong()).map { Cup(it) }.toList())
    input.forEachIndexed { index, cup ->
        run {
            val nextIndex = (index + 1).mod(numberOfCups)
            cup.setNext(input[nextIndex])
        }
    }
    val cups = input.associateBy { it.value }
    var current: Cup = input[0]
    repeat(moves) {
        current = current.move(cups)
    }
    return cups.getValue(1L).next()
}

private fun input() = getInput(23)[0].toList().map { it.toString().toLong() }
