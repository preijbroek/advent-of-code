package advent.day19

data class Rule(private val character: String?) {

    private val subRules: MutableList<List<Rule>> = mutableListOf()

    fun validates(line: String) = subRules.any { validatesSubRules(line, it) }

    private fun validatesSubRules(line: String, requiredRules: List<Rule>): Boolean {
        return when {
            line.isEmpty() -> requiredRules.isEmpty()
            requiredRules.isEmpty() -> false
            else -> {
                val firstSubRule = requiredRules.first()
                if (firstSubRule.character != null) {
                    return line.startsWith(firstSubRule.character) && validatesSubRules(line.substring(1), requiredRules.drop(1))
                }
                firstSubRule.subRules.any { validatesSubRules(line, it + requiredRules.drop(1)) }
            }
        }
    }

    fun addSubRules(subRules: List<List<Rule>>) {
        this.subRules.addAll(subRules)
    }

    fun clearRules() {
        this.subRules.clear()
    }
}