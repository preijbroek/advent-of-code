package advent.day19

import advent.getInputBlocks

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Int {
    val input = input()
    val (rules, lines) = Pair(input[0].toRules(), input[1])
    return lines.count { rules.getValue(0).validates(it) }
}

private fun part2(): Int {
    val input = input()
    val (rules, lines) = Pair(input[0].toRules(), input[1])
    val rule8 = rules.getValue(8)
    rule8.clearRules()
    rule8.addSubRules(listOf(listOf(rules.getValue(42)), listOf(rules.getValue(42), rule8)))
    val rule11 = rules.getValue(11)
    rule11.clearRules()
    rule11.addSubRules(listOf(listOf(rules.getValue(42), rules.getValue(31)), listOf(rules.getValue(42), rule11, rules.getValue(31))))
    return lines.count { rules.getValue(0).validates(it) }
}

private fun input() = getInputBlocks(19).map { it.split("\n") }

private fun List<String>.toRules(): MutableMap<Int, Rule> {
    val rules = this.map { it.split(": ") }
            .map { it[0].toInt() to Rule(if (it[1].contains("\"")) it[1][1].toString() else null) }
            .toMap().toMutableMap()
    this.combineRules(rules)
    return rules
}

private fun List<String>.combineRules(rules: MutableMap<Int, Rule>) {
    this.filter { "\"" !in it }
            .map { it.split(": ") }
            .forEach {
                rules.getValue(it[0].toInt())
                        .addSubRules(it[1].split(" | ").map { group ->
                            group.split(" ").map { rule -> rules.getValue(rule.toInt()) }
                        })
            }
}
