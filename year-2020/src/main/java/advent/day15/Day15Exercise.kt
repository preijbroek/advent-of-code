package advent.day15

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1() = playGame(2020)

private fun part2() = playGame(30000000)

private fun playGame(toNumber: Int): Int {
    val lastCalled = HashMap<Int, Int>()
    val input = input()
    input.forEachIndexed { index, i -> lastCalled[i] = index + 1 }
    return generateSequence(Pair(input.size, input.last())) { (lastIndex, previousCall) ->
        val lastCall = lastCalled[previousCall] ?: lastIndex
        lastCalled[previousCall] = lastIndex
        Pair(lastIndex + 1, lastIndex - lastCall)
    }.take(toNumber - input.lastIndex).last().second
}

private fun input() = getInput(15).first().split(",").map { it.toInt() }
