package advent.day25

import advent.getInput

fun main() {
    println(part1())
}

private fun part1(): Long {
    val input = input()
    val loopSizes = input.map { it.loopSize() }
    // Essentially this is 7L.pow(loopSizes.product()).mod(20201227L).
    // Doing that billions of times is not very efficient, though
    val encryptionKey1 = 1L.transform(times = loopSizes[1], input[0])
    val encryptionKey2 = 1L.transform(times = loopSizes[0], input[1])
    return if (encryptionKey1 == encryptionKey2) {
        encryptionKey1
    } else {
        throw IllegalArgumentException("Encryption keys are not OK! $encryptionKey1:$encryptionKey2")
    }
}

private fun Long.loopSize(): Long {
    val subjectNumber = 7L
    var loops = 0L
    var subject = 1L
    while (subject != this) {
        subject = subject.transform(times = 1L, subjectNumber)
        loops++
    }
    return loops
}

private fun Long.transform(times: Long, subjectNumber: Long): Long {
    val divisorValue = 20201227L
    var result = this
    repeat(times.toInt()) {
        result = (result * subjectNumber).mod(divisorValue)
    }
    return result
}

private fun input() = getInput(25).map { it.toLong() }
