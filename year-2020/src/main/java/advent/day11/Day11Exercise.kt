package advent.day11

import advent.getInput
import util.Position

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Int {
    return getIterations { it.nextIteration() }
}

private fun part2(): Int {
    return getIterations { it.nextTolerantIteration() }
}

private fun getIterations(iterationType: Function<WaitingRoom>): Int {
    var waitingRoom = WaitingRoom(input())
    var nextWaitingRoom = iterationType(waitingRoom)
    while (waitingRoom.occupiedSeatsSize() != nextWaitingRoom.occupiedSeatsSize()) {
        waitingRoom = nextWaitingRoom
        nextWaitingRoom = iterationType(nextWaitingRoom)
    }
    return waitingRoom.occupiedSeatsSize()
}

private fun input(): Map<Position, Char> {
    val input = getInput(11)
    return input.mapIndexed { yIndex, line -> line.mapIndexed { xIndex, char -> Position(xIndex, yIndex) to char } }
        .flatten()
        .toMap()
}

typealias Function<T> = (T) -> T
