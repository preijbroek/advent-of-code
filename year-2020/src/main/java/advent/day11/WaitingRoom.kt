package advent.day11

import util.Position
import util.plus

data class WaitingRoom(val positions: Map<Position, Char>) {

    fun nextIteration() = iteration(minOccupied = 4) { it.allNeighbours() }

    fun nextTolerantIteration() = iteration(minOccupied = 5) { it.getNeighbourSeats() }

    private fun iteration(minOccupied: Int, neighbourFunction: (Position) -> List<Position>): WaitingRoom {
        val nextMap = mutableMapOf<Position, Char>()
        for (position in positions) {
            val neighbours = neighbourFunction(position.key)
            when (position.value) {
                '.' -> nextMap[position.key] = '.'
                '#' -> nextMap[position.key] = nextIfOccupied(neighbours, minOccupied)
                'L' -> nextMap[position.key] = nextIfEmpty(neighbours)
                else -> throw IllegalArgumentException(position.toString())
            }
        }
        return WaitingRoom(nextMap)
    }

    private fun Position.getNeighbourSeats(): List<Position> {
        return listOfNotNull(
            getNeighbourSeat(Position.CENTRE.north()),
            getNeighbourSeat(Position.CENTRE.north().east()),
            getNeighbourSeat(Position.CENTRE.east()),
            getNeighbourSeat(Position.CENTRE.south().east()),
            getNeighbourSeat(Position.CENTRE.south()),
            getNeighbourSeat(Position.CENTRE.south().west()),
            getNeighbourSeat(Position.CENTRE.west()),
            getNeighbourSeat(Position.CENTRE.north().west()),
        )
    }

    private fun Position.getNeighbourSeat(direction: Position): Position? {
        var neighbourPosition = this + direction
        while (positions.containsKey(neighbourPosition)) {
            if (neighbourPosition.isSeat()) {
                return neighbourPosition
            }
            neighbourPosition += direction
        }
        return null
    }

    private fun Position.isSeat() = positions[this] != '.'

    private fun nextIfOccupied(positions: List<Position>, minOccupied: Int) =
        if (positions.count { this.positions[it] == '#' } >= minOccupied) {
            'L'
        } else {
            '#'
        }

    private fun nextIfEmpty(seats: List<Position>) =
        if (seats.any { this.positions[it] == '#' }) {
            'L'
        } else {
            '#'
        }

    fun occupiedSeatsSize() = positions.values.count { it == '#' }
}