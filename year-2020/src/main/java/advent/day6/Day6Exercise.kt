package advent.day6

import advent.getInputBlocks

fun main() {
    println(part1())
    println(part2())
}

private fun part1() = input().map { it.uniqueAnyAnswerCount() }.sum()
private fun part2() = input().map { it.uniqueAllAnswerCount() }.sum()

private fun input() = getInputBlocks(6).map { Group(it) }

private data class Group(val members: List<String>) {

    constructor(string: String): this(string.split("\n"))

    fun uniqueAnyAnswerCount() = members.flatMap { it.toList() }.distinct().count()

    fun uniqueAllAnswerCount(): Int {
        return members.map { it.toSet().toMutableSet() }
                .reduce { acc, it -> acc.apply { retainAll(it) } }
                .count()
    }

}

