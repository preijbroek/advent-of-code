package advent.day17

import util.Position

data class PocketDimension(private val grid: MutableSet<Position>) {

    fun run(neighbourFunction: (Position) -> List<Position>) {
        repeat(6) {
            runOnce(neighbourFunction)
        }
    }

    private fun runOnce(neighbourFunction: (Position) -> List<Position>) {
        val newInput = HashSet<Position>()
        val valuesToCheck = grid.flatMap { neighbourFunction(it) }.toSet()
        for (position in valuesToCheck) {
            val neighbours3D = neighbourFunction(position).toMutableSet()
            neighbours3D.retainAll(grid)
            if (shouldRemainActive(position, neighbours3D) || shouldBecomeActive(position, neighbours3D)) {
                newInput.add(position)
            }
        }
        grid.clear()
        grid.addAll(newInput)
    }

    private fun shouldRemainActive(position: Position, neighbours: Set<Position>) =
            position in grid && neighbours.size in 2..3

    private fun shouldBecomeActive(position: Position, neighbours: Set<Position>) =
            position !in grid && neighbours.size == 3

    fun activeCubes() = grid.size
}