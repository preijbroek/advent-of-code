package advent.day17

import advent.getInput
import util.toCharMap

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Int {
    val input = input()
    val pocketDimension = PocketDimension(input)
    pocketDimension.run { it.allNeighbours3d() }
    return pocketDimension.activeCubes()
}

private fun part2(): Int {
    val input = input()
    val pocketDimension = PocketDimension(input)
    pocketDimension.run { it.allNeighbours4d() }
    return pocketDimension.activeCubes()
}

private fun input() = getInput(17).toCharMap()
    .filterValues { it == '#' }
    .keys
    .toMutableSet()
