package advent.day2

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1() = input().count { it.isValidPassword1() }

private fun part2() = input().count { it.isValidPassword2() }

fun Triple<List<Int>, Char, String>.isValidPassword1(): Boolean {
    val (times, character, password) = this
    val range = times[0].rangeTo(times[1])
    val count = password.count { it == character }
    return range.contains(count)
}

fun Triple<List<Int>, Char, String>.isValidPassword2(): Boolean {
    val (times, character, password) = this
    val index1 = times[0] - 1
    val index2 = times[1] - 1
    return (password[index1] == character).xor(password[index2] == character)
}

private fun input() = getInput(2).map { it.split(" ") }.map { Triple(times(it), character(it), password(it)) }

private fun times(line: List<String>) = line[0].split("-").map { it.toInt() }
private fun character(line: List<String>) = line[1][0]
private fun password(line: List<String>) = line[2]
