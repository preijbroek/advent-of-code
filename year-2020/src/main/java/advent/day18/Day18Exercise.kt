package advent.day18

import advent.getInput

fun main() {
    println(part1())
    println(part2())
}

private fun part1(): Long {
    return input().sumOf { it.evaluateBeginner() }
}

private fun part2(): Long {
    return input().sumOf { it.evaluateAdvanced() }
}

private fun input(): List<Expression> {
    return getInput(18).map { it.replace(" ", "") }.map { Expression(it) }
}
