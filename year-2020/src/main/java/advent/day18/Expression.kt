package advent.day18

import util.product
import java.util.Stack

data class Expression(val line: String) {

    fun evaluateBeginner() = evaluate { evaluateBeginner(it) }

    fun evaluateAdvanced() = evaluate { evaluateAdvanced(it) }

    private fun evaluate(evaluateLevel: (Stack<String>) -> Unit): Long {
        val operations = Stack<String>()
        for (c in line.reversed().map { it.toString() }) {
            when (c) {
                "(" -> evaluateLevel(operations)
                else -> operations.push(c)
            }
        }
        evaluateLevel(operations)
        return operations.pop().toLong()
    }

    private fun evaluateBeginner(operations: Stack<String>) {
        var evaluation = operations.pop().toLong()
        while (operations.isNotEmpty()) {
            when (operations.pop()) {
                "+" -> evaluation += operations.pop().toLong()
                "*" -> evaluation *= operations.pop().toLong()
                ")" -> break
            }
        }
        operations.push(evaluation.toString())
    }

    private fun evaluateAdvanced(operations: Stack<String>) {
        val multiplicationStack = Stack<Long>()
        multiplicationStack.push(operations.pop().toLong())
        while (operations.isNotEmpty()) {
            when (operations.pop()) {
                "+" -> multiplicationStack.push(operations.pop().toLong() + multiplicationStack.pop())
                "*" -> multiplicationStack.push(operations.pop().toLong())
                ")" -> break
            }
        }
        operations.push(multiplicationStack.product().toString())
    }
}