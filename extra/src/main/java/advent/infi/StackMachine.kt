package advent.infi

import util.Position
import util.second
import java.util.ArrayDeque
import java.util.Collections.asLifoQueue

data class StackMachine(
    private val instructions: List<List<String>>
) {

    fun run(position: Position): Long {
        var programCounter = 0
        val stack = asLifoQueue(ArrayDeque<Long>())
        while (true) {
            val instruction = instructions[programCounter]
            when (instruction.first()) {
                "push" -> stack.offer(instruction.second().value(position))
                "add" -> stack.offer(stack.poll() + stack.poll())
                "jmpos" -> if (stack.poll() >= 0L) programCounter += instruction.second().value(position).toInt()
                "ret" -> return stack.poll()
            }
            programCounter++
        }
    }

    private fun String.value(position: Position) = when (this) {
        "X", "x" -> position.x
        "Y", "y" -> position.y
        "Z", "z" -> position.z
        else -> toLong()
    }
}
