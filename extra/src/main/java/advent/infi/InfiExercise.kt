package advent.infi

import advent.getInput
import util.Position

fun main() {
    val stackMachine = getInput("infi-exercise-2024").map { it.split(" ") }.let(::StackMachine)
    val cloudScores = (0..29).flatMap { x ->
        (0..29).flatMap { y ->
            (0..29).map { z ->
                val position = Position(x, y, z)
                position to stackMachine.run(position)
            }
        }
    }
    println("Part 1: ${part1(cloudScores)}")
    println("Part 2: ${part2(cloudScores)}")
}

private fun part1(cloudScores: List<Pair<Position, Long>>): Long {
    return cloudScores.sumOf { it.second }
}

private fun part2(cloudScores: List<Pair<Position, Long>>): Int {
    val positionsToVisit = cloudScores.filter { it.second > 0L }.mapTo(HashSet()) { it.first }
    var clouds = 0
    while (positionsToVisit.isNotEmpty()) {
        var currentCloud = setOf(positionsToVisit.first())
        positionsToVisit.removeAll(currentCloud)
        while (currentCloud.isNotEmpty()) {
            currentCloud =
                currentCloud.flatMapTo(HashSet()) { position ->
                    position.neighbours3d().filter { it in positionsToVisit }
                }
            positionsToVisit.removeAll(currentCloud)
        }
        clouds++
    }
    return clouds
}