package advent

import util.getInputBlocksFor
import util.getInputFor

fun getInput(resource: String) = getInputFor("extra", resource)

fun getInputBlocks(resource: String) = getInputBlocksFor("extra", resource)