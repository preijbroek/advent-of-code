package advent.day6

import advent.getInput

fun main() {
    val input = getInput(6).first()
    println(part1(input))
    println(part2(input))
}

private const val startOfPacketMarker = 4
private const val startOfMessageMarker = 14

private fun part1(input: String): Int {
    return input.marker(startOfPacketMarker)
}

private fun part2(input: String): Int {
    return input.marker(startOfMessageMarker)
}

private fun String.marker(size: Int): Int {
    return size + windowed(size).mapIndexed { index, s -> index to s }
        .first { (_, s) -> s.toSet().size == s.length }.first
}
