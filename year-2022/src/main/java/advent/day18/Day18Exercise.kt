package advent.day18

import advent.getInput
import util.Position
import util.second
import util.third
import java.util.ArrayDeque

fun main() {
    val cubes = getInput(18).map { inputLine ->
        inputLine.split(",").map(String::toInt).let { Position(it.first(), it.second(), it.third()) }
    }.toSet()
    println(part1(cubes))
    println(part2(cubes))
}

private fun part1(cubes: Set<Position>): Int {
    return cubes.sumOf { cube -> cube.neighbours3d().count { neighbour -> neighbour !in cubes } }
}

private fun part2(cubes: Set<Position>): Int {
    val minX = cubes.minOf(Position::x) - 1
    val maxX = cubes.maxOf(Position::x) + 1
    val minY = cubes.minOf(Position::y) - 1
    val maxY = cubes.maxOf(Position::y) + 1
    val minZ = cubes.minOf(Position::z) - 1
    val maxZ = cubes.maxOf(Position::z) + 1
    val currentCubes = ArrayDeque<Position>()
    val reachedCubes = HashSet<Position>()
    reachedCubes.add(Position(minX, minY, minZ))
    currentCubes.offer(Position(minX, minY, minZ))
    var surfaceCount = 0
    while (currentCubes.isNotEmpty()) {
        val next = currentCubes.poll()
        val possibleNeighbours = next.neighbours3d()
            .filter { it !in reachedCubes && it.isInRange(minX, maxX, minY, maxY, minZ, maxZ) }
        surfaceCount += possibleNeighbours.count { it in cubes }
        possibleNeighbours.filter { it !in cubes }.forEach {
            currentCubes.offer(it)
            reachedCubes += it
        }
    }
    return surfaceCount
}
