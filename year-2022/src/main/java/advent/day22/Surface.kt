package advent.day22

import util.Box
import util.Direction
import util.Direction.EAST
import util.Direction.NORTH
import util.Direction.SOUTH
import util.Direction.WEST
import util.Position
import util.box
import util.triple

sealed interface Surface {

    val box: Box

    fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position>

    operator fun contains(position: Position) = position in box
}

enum class SmallSurface(override val box: Box) : Surface {

    ONE(Position(9, 1) box Position(12, 4)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> FIVE to WEST triple Position(16, 13 - currentPosition.y)
                SOUTH -> THREE to SOUTH triple currentPosition.north()
                WEST -> TWO to SOUTH triple Position(4 + currentPosition.y, 5)
                NORTH -> FOUR to SOUTH triple Position(13 - currentPosition.x, 5)
            }
        }
    },
    TWO(Position(5, 5) box Position(8, 8)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> THREE to EAST triple currentPosition.east()
                SOUTH -> SIX to EAST triple Position(9, 17 - currentPosition.x)
                WEST -> FOUR to WEST triple currentPosition.west()
                NORTH -> ONE to EAST triple Position(9, currentPosition.x - 4)
            }
        }
    },
    THREE(Position(9, 5) box Position(12, 8)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> FIVE to SOUTH triple Position(21 - currentPosition.y, 9)
                SOUTH -> SIX to SOUTH triple currentPosition.north()
                WEST -> TWO to WEST triple currentPosition.west()
                NORTH -> ONE to NORTH triple currentPosition.south()
            }
        }
    },
    FOUR(Position(1, 5) box Position(4, 8)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> TWO to EAST triple currentPosition.east()
                SOUTH -> SIX to NORTH triple Position(13 - currentPosition.x, 12)
                WEST -> FIVE to NORTH triple Position(21 - currentPosition.y, 12)
                NORTH -> ONE to SOUTH triple Position(13 - currentPosition.x, 1)
            }
        }
    },
    FIVE(Position(13, 9) box Position(16, 12)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> ONE to WEST triple Position(8, 13 - currentPosition.y)
                SOUTH -> FOUR to EAST triple Position(1, 21 - currentPosition.x)
                WEST -> SIX to WEST triple currentPosition.west()
                NORTH -> THREE to WEST triple Position(12, 21 - currentPosition.x)
            }
        }
    },
    SIX(Position(9, 9) box Position(12, 12)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> FIVE to EAST triple currentPosition.east()
                SOUTH -> FOUR to NORTH triple Position(13 - currentPosition.x, 8)
                WEST -> TWO to NORTH triple Position(17 - currentPosition.y, 8)
                NORTH -> THREE to NORTH triple currentPosition.south()
            }
        }
    },
    ;
}

enum class LargeSurface(override val box: Box) : Surface {

    ONE(Position(51, 1) box Position(100, 50)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> TWO to EAST triple currentPosition.east()
                SOUTH -> THREE to SOUTH triple currentPosition.north()
                WEST -> FIVE to EAST triple Position(1, 151 - currentPosition.y)
                NORTH -> FOUR to EAST triple Position(1, currentPosition.x + 100)
            }
        }
    },
    TWO(Position(101, 1) box Position(150, 50)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> SIX to WEST triple Position(100, 151 - currentPosition.y)
                SOUTH -> THREE to WEST triple Position(100, currentPosition.x - 50)
                WEST -> ONE to WEST triple currentPosition.west()
                NORTH -> FOUR to NORTH triple Position(currentPosition.x - 100, 200)
            }
        }
    },
    THREE(Position(51, 51) box Position(100, 100)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> TWO to NORTH triple Position(50 + currentPosition.y, 50)
                SOUTH -> SIX to SOUTH triple currentPosition.north()
                WEST -> FIVE to SOUTH triple Position(currentPosition.y - 50, 101)
                NORTH -> ONE to NORTH triple currentPosition.south()
            }
        }
    },
    FOUR(Position(1, 151) box Position(50, 200)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> SIX to NORTH triple Position(currentPosition.y - 100, 150)
                SOUTH -> TWO to SOUTH triple Position(100 + currentPosition.x, 1)
                WEST -> ONE to SOUTH triple Position(currentPosition.y - 100, 1)
                NORTH -> FIVE to NORTH triple currentPosition.south()
            }
        }
    },
    FIVE(Position(1, 101) box Position(50, 150)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> SIX to EAST triple currentPosition.east()
                SOUTH -> FOUR to SOUTH triple currentPosition.north()
                WEST -> ONE to EAST triple Position(51, 151 - currentPosition.y)
                NORTH -> THREE to EAST triple Position(51, 50 + currentPosition.x)
            }
        }
    },
    SIX(Position(51, 101) box Position(100, 150)) {
        override fun to(direction: Direction, currentPosition: Position): Triple<Surface, Direction, Position> {
            return when (direction) {
                EAST -> TWO to WEST triple Position(150, 151 - currentPosition.y)
                SOUTH -> FOUR to WEST triple Position(50, 100 + currentPosition.x)
                WEST -> FIVE to WEST triple currentPosition.west()
                NORTH -> THREE to NORTH triple currentPosition.south()
            }
        }
    },

    ;

}
