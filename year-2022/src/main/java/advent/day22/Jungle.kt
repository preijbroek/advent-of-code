package advent.day22

import util.Direction
import util.Position
import util.triple

class Jungle(
    private val map: Map<Position, String>,
    smallCubes: Boolean = false
) {

    private var currentPosition = map.keys.minWith(Position.byY().thenComparing(Position.byX()))
    private var currentDirection = Direction.EAST
    private var currentSurface: Surface = if (smallCubes) SmallSurface.ONE else LargeSurface.ONE
    private val visitedPlaces = mutableMapOf(currentPosition to currentDirection.asString())

    fun password(): Int {
//        print()
        return (4 * currentPosition.x) + (1000 * currentPosition.y) + currentDirection.ordinal
    }

    fun run(instructions: String, asCube: Boolean = false) {
        val moveCounter = StringBuilder()
        instructions.forEach { c ->
            when (val instruction = c.toString()) {
                "L" -> {
                    execute(moveCounter.toString().takeIf(String::isNotEmpty)?.let(String::toInt) ?: 0, asCube, instruction)
                    moveCounter.clear()
                }
                "R" -> {
                    execute(moveCounter.toString().takeIf(String::isNotEmpty)?.let(String::toInt) ?: 0, asCube, instruction)
                    moveCounter.clear()
                }
                else -> {
                    moveCounter.append(instruction)
                }
            }
        }
        execute(moveCounter.toString().takeIf(String::isNotEmpty)?.let(String::toInt) ?: 0, asCube)
    }

    private fun execute(times: Int, asCube: Boolean, newDirection: String = "") {
        repeat(times) {
            move(asCube)
        }
        currentDirection = when (newDirection) {
            "L" -> currentDirection.left()
            "R" -> currentDirection.right()
            else -> currentDirection
        }
        visitedPlaces[currentPosition] = currentDirection.asString()
    }

    private fun move(asCube: Boolean) {
        val (nextSurface, nextDirection, nextPosition) = nextPosition(asCube)
        if (map[nextPosition] != "#") {
            currentPosition = nextPosition
            currentSurface = nextSurface
            currentDirection = nextDirection
            visitedPlaces[currentPosition] = currentDirection.asString()
//            print()
        }
    }

    private fun nextPosition(asCube: Boolean): Triple<Surface, Direction, Position> {
        return if (asCube) {
            nextCubePosition()
        } else {
            currentSurface to currentDirection triple nextFlatPosition()
        }
    }

    private fun nextCubePosition(): Triple<Surface, Direction, Position> {
        val nextOnSurface = currentPosition.toFlipped(currentDirection)
        return if (nextOnSurface in currentSurface) {
            currentSurface to currentDirection triple nextOnSurface
        } else {
            currentSurface.to(currentDirection, currentPosition)
        }
    }

    private fun nextFlatPosition(): Position {
        val nextPosition = currentPosition.toFlipped(currentDirection)
        return if (nextPosition in map) {
            nextPosition
        } else {
            when (currentDirection) {
                Direction.EAST -> {
                    map.filter { (position, _) -> position.y == currentPosition.y }.minBy { (position, _) -> position.x }.key
                }
                Direction.SOUTH -> {
                    map.filter { (position, _) -> position.x == currentPosition.x }.minBy { (position, _) -> position.y }.key
                }
                Direction.WEST -> {
                    map.filter { (position, _) -> position.y == currentPosition.y }.maxBy { (position, _) -> position.x }.key
                }
                Direction.NORTH -> {
                    map.filter { (position, _) -> position.x == currentPosition.x }.maxBy { (position, _) -> position.y }.key
                }
            }
        }
    }

    private fun print() {
        val minX = map.minOf { it.key.x }
        val maxX = map.maxOf { it.key.x }
        val minY = map.minOf { it.key.y }
        val maxY = map.maxOf { it.key.y }
        for (y in minY..maxY) {
            for (x in minX..maxX) {
                print(visitedPlaces.getOrDefault(Position(x, y), map.getOrDefault(Position(x, y), " ")))
            }
            println()
        }
        println()
    }

    private fun Direction.asString() = when(this) {
        Direction.EAST -> ">"
        Direction.SOUTH -> "v"
        Direction.WEST -> "<"
        Direction.NORTH -> "^"
    }
}
