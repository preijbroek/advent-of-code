package advent.day22

import advent.getInputBlocks
import util.Position
import util.second
import util.toCharMap

fun main() {
    val inputBlocks = getInputBlocks(22)
    println(part1(inputBlocks))
    println(part2(inputBlocks))
}

private fun String.toJungle(): Jungle {
    return split("\n").toCharMap()
        .map { (position, c) -> position.plusX().plusY() to c.toString() }
        .toMap()
        .filterValues(String::isNotBlank)
        .let { Jungle(it, smallCubes = Position(75, 75) !in it) }
}

private fun part1(inputBlocks: List<String>): Int {
    val jungle = inputBlocks.first().toJungle()
    jungle.run(inputBlocks.second(), asCube = false)
    return jungle.password()
}

private fun part2(inputBlocks: List<String>): Int {
    val jungle = inputBlocks.first().toJungle()
    jungle.run(inputBlocks.second(), asCube = true)
    return jungle.password()
}
