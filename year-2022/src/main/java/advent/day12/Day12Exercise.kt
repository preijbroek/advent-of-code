package advent.day12

import advent.getInput
import util.toCharMap

fun main() {
    val hill = getInput(12).toCharMap().mapValues { it.value.toString() }.let(::Hill)
    println(part1(hill))
    println(part2(hill))
}

private fun part1(hill: Hill): Int {
    return hill.lengthToTop()
}

private fun part2(hill: Hill): Int {
    return hill.quickestLengthToTop()
}
