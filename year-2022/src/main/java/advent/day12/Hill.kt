package advent.day12

import util.Position
import util.isDisjoint

data class Hill(
    private val grid: Map<Position, String>
) {

    private val end = grid.entries.first { it.value == "E" }.key
    private val heightGrid = grid.mapValues { (_, heightString) -> heightString.toHeight() }

    fun lengthToTop(): Int {
        return quickestRoute(setOf(grid.entries.first { it.value == "S" }.key))
    }

    fun quickestLengthToTop(): Int {
        return quickestRoute(grid.filter { (_, height) -> height == "S" || height == "a" }.keys)
    }

    private fun quickestRoute(starts: Set<Position>): Int {
        val distances = mutableMapOf(end to 0)
        val currentPositions = mutableSetOf(end)
        var steps = 0
        while (starts.isDisjoint(distances.keys)) {
            steps++
            val nextPositions = mutableSetOf<Position>()
            currentPositions.forEach {
                it.neighbours().filter(grid::containsKey)
                    .filter { neighbour -> neighbour !in distances.keys }
                    .filter { neighbour -> heightGrid.getValue(it) - heightGrid.getValue(neighbour) <= 1 }
                    .forEach { neighbour ->
                        distances[neighbour] = steps
                        nextPositions += neighbour
                    }
            }
            currentPositions.clear()
            currentPositions += nextPositions
        }
        return steps
    }

    private fun String.toHeight(): Int {
        return when (val value = first()) {
            'S' -> 1
            'E' -> 26
            else -> value.code - 'a'.code + 1
        }
    }
}
