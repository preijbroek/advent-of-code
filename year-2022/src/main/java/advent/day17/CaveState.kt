package advent.day17

data class CaveState(
    val maxHeight: Int,
    val rocksLanded: Long,
)
