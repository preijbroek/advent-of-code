package advent.day17

data class CaveHash(
    val move: Int,
    val rockIndex: Int,
    val distancesToFormation: List<Int>,
)
