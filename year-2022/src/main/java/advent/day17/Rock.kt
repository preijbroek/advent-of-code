package advent.day17

import util.Direction
import util.Position

data class Rock(
    private val position: Set<Position>
) {

    companion object {

        operator fun invoke(count: Int, height: Int): Rock {
            return when (count) {
                0 -> (3..6).map { x -> Position(x = x, y = height + 1) }.toSet()
                1 -> setOf(
                    Position(x = 3, y = height + 2),
                    Position(x = 4, y = height + 3),
                    Position(x = 4, y = height + 2),
                    Position(x = 4, y = height + 1),
                    Position(x = 5, y = height + 2),
                )
                2 -> setOf(
                    Position(x = 3, y = height + 1),
                    Position(x = 4, y = height + 1),
                    Position(x = 5, y = height + 3),
                    Position(x = 5, y = height + 2),
                    Position(x = 5, y = height + 1),
                )
                3 -> setOf(
                    Position(x = 3, y = height + 4),
                    Position(x = 3, y = height + 3),
                    Position(x = 3, y = height + 2),
                    Position(x = 3, y = height + 1),
                )
                4 -> setOf(
                    Position(x = 3, y = height + 2),
                    Position(x = 3, y = height + 1),
                    Position(x = 4, y = height + 2),
                    Position(x = 4, y = height + 1),
                )
                else -> throw IllegalArgumentException(count.toString())
            }.let(::Rock)
        }
    }

    fun fill(cave: MutableSet<Position>) = cave.addAll(position)

    fun moveTo(direction: Direction) = Rock(position.map { it.to(direction) }.toSet())

    fun canMove(direction: Direction, cave: Set<Position>) = moveTo(direction).position.all { it.x in 1..7 && it !in cave }

    fun maxHeight() = position.maxOf(Position::y)

    fun differencesToCeiling(ceiling: Int): List<Int> {
        return position.map { ceiling - it.y }
    }

    fun heights(): List<Int> {
        return (1..7).map { index -> position.filter { it.x == index }.maxOfOrNull(Position::y) ?: 0 }
    }
}
