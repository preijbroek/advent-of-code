package advent.day17

import util.Direction
import util.Position
import java.util.Collections.nCopies
import kotlin.math.max

data class Cave(
    private val steam: List<Direction>,
    private val filled: MutableSet<Position> = (1..7).map { x -> Position(x, 0) }.toMutableSet()
) {

    private var steamIndex = 0
    private var rocksLanded = 0L
    private var shape = 0
    private var currentMaxHeight = 0
    private var currentRock = nextRock()
    private var maxFixedHeights: List<Int> = nCopies(7, 0)

    private fun landCurrentRock() {
        currentMaxHeight = max(currentMaxHeight, currentRock.maxHeight())
        maxFixedHeights = nextMaxFixedHeights()
        currentRock.fill(filled)
        rocksLanded++
        shape = (shape + 1) % 5
        currentRock = nextRock()
    }

    private fun nextMaxFixedHeights(): List<Int> {
        return currentRock.heights().mapIndexed { index, height -> max(height, maxFixedHeights[index]) }
    }

    private fun nextRock() = Rock(shape, currentMaxHeight + 3)

    private fun canMoveCurrentRock(direction: Direction = Direction.SOUTH) = currentRock.canMove(direction, filled)

    private fun moveCurrentRock(direction: Direction = Direction.SOUTH) {
        currentRock = currentRock.moveTo(direction)
    }

    private fun hash(): CaveHash {
        return CaveHash(
            steamIndex,
            shape,
            maxFixedHeights.map { currentMaxHeight - it }
        )
    }

    fun run(totalDroppedRocks: Long): Long {
        val caveStates = mutableMapOf<CaveHash, CaveState>()
        var hash = hash()
        while (!caveStates.containsKey(hash)) {
            caveStates[hash] = CaveState(currentMaxHeight, rocksLanded)
            drop()
            hash = hash()
        }
        val rocksPerCycle = rocksLanded - caveStates.getValue(hash).rocksLanded
        val heightPerCycle = currentMaxHeight - caveStates.getValue(hash).maxHeight
        val cycles = (totalDroppedRocks - rocksLanded) / rocksPerCycle
        val remainingRocks = (totalDroppedRocks - rocksLanded) - (cycles * rocksPerCycle)
        rocksLanded = 0
        while (rocksLanded < remainingRocks) {
            drop()
        }
        return currentMaxHeight.toLong() + (heightPerCycle * cycles)
    }

    private fun drop() {
        val direction = steam[steamIndex]
        if (canMoveCurrentRock(direction)) {
            moveCurrentRock(direction)
        }
        if (canMoveCurrentRock()) {
            moveCurrentRock()
        } else {
            landCurrentRock()
        }
        steamIndex = (steamIndex + 1) % steam.size
    }
}
