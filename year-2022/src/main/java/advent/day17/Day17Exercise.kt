package advent.day17

import advent.getInput
import util.Direction

fun main() {
    val steam = getInput(17).first().map {
        when (it) {
            '>' -> Direction.EAST
            '<' -> Direction.WEST
            else -> throw IllegalArgumentException(it.toString())
        }
    }
    println(part1(steam))
    println(part2(steam))
}

private fun part1(steam: List<Direction>): Long {
    return Cave(steam).run(totalDroppedRocks = 2022L)
}

private fun part2(steam: List<Direction>): Long {
    return Cave(steam).run(totalDroppedRocks = 1_000_000_000_000L)
}

