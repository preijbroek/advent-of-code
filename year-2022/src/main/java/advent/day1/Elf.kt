package advent.day1

data class Elf(
    val calories: List<Int>
) {

    val totalCalories = calories.sum()
}
