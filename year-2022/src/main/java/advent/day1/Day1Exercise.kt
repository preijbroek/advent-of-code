package advent.day1

import advent.getInputBlocks

fun main() {
    val elves = getInputBlocks(1).map {
        it.split("\n").map(String::toInt).let(::Elf)
    }
    println(part1(elves))
    println(part2(elves))
}

private fun part1(elves: List<Elf>): Int {
    return elves.maxOf(Elf::totalCalories)
}

private fun part2(elves: List<Elf>): Int {
    return elves.sortedByDescending(Elf::totalCalories)
        .take(3)
        .sumOf(Elf::totalCalories)
}
