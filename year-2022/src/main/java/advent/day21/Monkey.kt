package advent.day21

import util.second
import util.third

data class Monkey(
    val id: String,
    val value: () -> Long,
    val dependsOn: List<() -> Monkey>,
    val operation: String?
) {

    companion object {

        operator fun invoke(id: String, operation: String, monkeys: Map<String, Monkey>): Monkey {
            val operationComponents = operation.split(" ")
            val dependentMonkeys = mutableListOf<() -> Monkey>()
            val operations: Pair<() -> Long, String?> = when (operationComponents.size) {
                1 -> {
                    { operationComponents.first().toLong() } to null
                }
                3 -> {
                    val first: () -> Monkey = { monkeys.getValue(operationComponents.first()) }
                    val second: () -> Monkey = { monkeys.getValue(operationComponents.third()) }
                    dependentMonkeys.add(first)
                    dependentMonkeys.add(second)
                    when (val binaryOperation = operationComponents.second()) {
                        "+" -> {
                            { first().value() + second().value() } to binaryOperation
                        }
                        "*" -> {
                            { first().value() * second().value() } to binaryOperation
                        }
                        "-" -> {
                            { first().value() - second().value() } to binaryOperation
                        }
                        "/" -> {
                            { first().value() / second().value() } to binaryOperation
                        }
                        else -> throw IllegalArgumentException(binaryOperation)
                    }
                }
                else -> throw IllegalArgumentException(operation)
            }
            return Monkey(id, operations.first, dependentMonkeys, operations.second)
        }
    }

    fun requiredHumanNumber(): Long {
        var humanChain = chainToHuman()
        var currentInChain = humanChain.first()
        var expectedValue = dependsOn.first { it() != currentInChain }().value()
        while (!currentInChain.isHuman()) {
            humanChain = humanChain.drop(1)
            val known = currentInChain.dependsOn.first { humanChain.first() != it() }()
            val isKnownFirstArgument = currentInChain.dependsOn.first()() == known
            expectedValue = expectedValue.updateExpectedValue(currentInChain.operation, isKnownFirstArgument, known)
            currentInChain = humanChain.first()
        }
        return expectedValue
    }

    private fun chainToHuman(): List<Monkey> {
        var current = this
        val chain = mutableListOf<Monkey>()
        while (!current.isHuman()) {
            current = current.dependsOn.first { it().hasHumanInTree() }()
            chain.add(current)
        }
        return chain
    }

    private fun Long.updateExpectedValue(operation: String?, isKnownFirstArgument: Boolean, known: Monkey): Long {
        return when (operation) {
            "+" -> this - known.value()
            "*" -> this / known.value()
            "-" -> if (isKnownFirstArgument) {
                known.value() - this
            } else {
                this + known.value()
            }
            "/" -> if (isKnownFirstArgument) {
                known.value() / this
            } else {
                this * known.value()
            }
            else -> throw IllegalArgumentException(operation)
        }
    }

    private fun hasHumanInTree(): Boolean = isHuman() || dependsOn.any { it().hasHumanInTree() }

    private fun isHuman() = id == "humn"
}
