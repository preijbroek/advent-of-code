package advent.day21

import advent.getInput
import util.second

fun main() {
    val monkeys = getInput(21).toMonkeys()
    println(part1(monkeys))
    println(part2(monkeys))
}

private fun List<String>.toMonkeys(): List<Monkey> {
    val monkeyMap = mutableMapOf<String, Monkey>()
    forEach {
        val components = it.split(": ")
        Monkey(components.first(), components.second(), monkeyMap).run {
            monkeyMap[id] = this
        }
    }
    return monkeyMap.values.toList()
}

private fun part1(monkeys: List<Monkey>): Long {
    return monkeys.first { it.id == "root" }.value()
}

private fun part2(monkeys: List<Monkey>): Long {
    return monkeys.first { it.id == "root" }.requiredHumanNumber()
}
