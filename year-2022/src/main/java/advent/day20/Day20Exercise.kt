package advent.day20

import advent.getInput
import util.modulo
import util.nonZeroMod

fun main() {
    val input = getInput(20).map(String::toLong)
    println(part1(input))
    println(part2(input))
}

private const val encryptionKey = 811589153L

private fun part1(input: List<Long>): Long {
    return input.mixed(times = 1).grooveCoordinates().sum()
}

private fun part2(input: List<Long>): Long {
    return input.map { it * encryptionKey }.mixed(times = 10).grooveCoordinates().sum()
}

private fun List<Long>.mixed(times: Int): List<Long> {
    val mixedInput = mapIndexed { index, number -> index to number }.toMutableList()
    repeat(times) {
        forEachIndexed { index, number ->
            val indexOfElementToMove = mixedInput.indexOf(index to number)
            mixedInput.removeAt(indexOfElementToMove)
            val newIndex = (indexOfElementToMove + number) nonZeroMod mixedInput.size
            mixedInput.add((newIndex), index to number)
        }
    }
    return mixedInput.map { (_, number) -> number }
}

private fun List<Long>.grooveCoordinates(): Sequence<Long> {
    val indexOfZero = indexOfFirst { it == 0L }
    return sequenceOf(1000, 2000, 3000)
        .map { it + indexOfZero }
        .map { it modulo size }
        .map(this::get)
}
