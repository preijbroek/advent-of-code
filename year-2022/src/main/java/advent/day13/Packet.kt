package advent.day13

sealed interface Packet : Comparable<Packet> {

    companion object {

        operator fun invoke(s: String): Packet {
            return if (s.toIntOrNull() != null) {
                ValuePacket(s.toInt())
            } else if (s == "[]") {
                ListPacket(emptyList())
            } else {
                val unwrappedString = s.drop(1).dropLast(1)
                val subPackets = mutableListOf<Packet>()
                var depth = 0
                val subPacketBuilder = StringBuilder()
                unwrappedString.forEach { c ->
                    when (c) {
                        '[' -> depth++
                        ']' -> depth--
                        ',' -> if (depth == 0) {
                            subPackets.add(Packet(subPacketBuilder.toString()))
                            subPacketBuilder.clear()
                            return@forEach
                        }
                    }
                    subPacketBuilder.append(c)
                }
                subPackets.add(Packet(subPacketBuilder.toString()))
                ListPacket(subPackets)
            }
        }
    }

    override fun compareTo(other: Packet): Int
}

data class ValuePacket(
    private val value: Int
) : Packet {

    fun toList() = ListPacket(listOf(this))

    override fun compareTo(other: Packet): Int {
        return when (other) {
            is ValuePacket -> this.value - other.value
            is ListPacket -> this.toList().compareTo(other)
        }
    }

    override fun toString(): String {
        return value.toString()
    }

}

data class ListPacket(
    private val packets: List<Packet>
) : Packet {

    override fun compareTo(other: Packet): Int {
        return when (other) {
            is ValuePacket -> this.compareTo(other.toList())
            is ListPacket -> this.compareTo(other)
        }
    }

    private fun compareTo(other: ListPacket): Int {
        val minSize = minOf(this.packets.size, other.packets.size)
        return (0 ..< minSize).map { this.packets[it].compareTo(other.packets[it]) }.firstOrNull { it != 0 }
            ?: (this.packets.size - other.packets.size)

    }

    override fun toString(): String {
        return packets.toString()
    }

}

