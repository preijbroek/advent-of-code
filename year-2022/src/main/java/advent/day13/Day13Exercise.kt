package advent.day13

import advent.getInputBlocks
import util.oneBasedIndexOf
import util.second
import util.times

fun main() {
    val packetPairs = getInputBlocks(13).map { block ->
        block.split("\n").map { Packet(it) }
    }
    println(part1(packetPairs))
    println(part2(packetPairs))
}

private fun part1(packetPairs: List<List<Packet>>): Int {
    return packetPairs.map { it.first() <= it.second() }
        .foldIndexed(0) { index, acc, isInRightOrder ->
            acc + (isInRightOrder * (index + 1))
        }
}

private fun part2(packetPairs: List<List<Packet>>): Int {
    val dividerPacket1 = Packet("[[2]]")
    val dividerPacket2 = Packet("[[6]]")
    val allPackets = packetPairs.flatten() + listOf(dividerPacket1, dividerPacket2)
    val sortedPackets = allPackets.sorted()
    return sortedPackets.oneBasedIndexOf(dividerPacket1) * sortedPackets.oneBasedIndexOf(dividerPacket2)
}
