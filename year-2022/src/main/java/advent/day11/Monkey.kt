package advent.day11

import java.util.ArrayDeque

data class Monkey(
    val operation: Long.() -> Long,
    val test: Long.() -> Boolean,
    val successTarget: () -> Monkey,
    val failureTarget: () -> Monkey,
) {

    private val itemsInPossession = ArrayDeque<Long>()
    private var activityLevel = 0L

    fun throwAll(worryLevelManager: Long.() -> Long) {
        val totalItems = itemsInPossession.size
        while (itemsInPossession.isNotEmpty()) {
            val next = inspectNext(worryLevelManager)
            `throw`(next)
        }
        activityLevel += totalItems
    }

    private fun inspectNext(manageWorryLevel: Long.() -> Long): Long {
        val item = itemsInPossession.poll()
        return item.operation().manageWorryLevel()
    }

    private fun `throw`(item: Long) {
        if (item.test()) {
            successTarget().catch(item)
        } else {
            failureTarget().catch(item)
        }
    }

    fun catch(item: Long) {
        itemsInPossession.add(item)
    }

    fun activityLevel() = activityLevel
}
