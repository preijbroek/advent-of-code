package advent.day11

import advent.getInputBlocks
import util.fourth
import util.product
import util.second
import util.secondLast
import util.third

fun main() {
    val input = getInputBlocks(11)
    println(part1(input.toMonkeys()))
    println(part2(input.toMonkeys()))
}

private fun List<String>.toMonkeys(): List<Monkey> {
    val monkeys = mutableMapOf<Int, Monkey>()
    return map {
        val details = it.split("\n")
        val monkeyNumber = details.first().secondLast().digitToInt()
        val successTarget = details.secondLast().last().digitToInt()
        val failureTarget = details.last().last().digitToInt()
        val testNumber = details.fourth().split(" ").last().toLong()
        val monkey = Monkey(
            operation = details.third().substringAfter("  Operation: new = ").toOperation(),
            test = { this % testNumber == 0L },
            successTarget = { monkeys.getValue(successTarget) },
            failureTarget = { monkeys.getValue(failureTarget) }
        )
        details.second().substringAfter("Starting items: ").split(", ")
            .map(String::toLong).forEach(monkey::catch)
        monkeys[monkeyNumber] = monkey
        monkey
    }
}

private fun String.toOperation(): Long.() -> Long = {
    val items = split(" ")
    val secondOperand = when (val operand = items.third()) {
        "old" -> this
        else -> operand.toLong()
    }
    when (items.second()) {
        "+" -> this + secondOperand
        "*" -> this * secondOperand
        else -> throw IllegalArgumentException(items.second())
    }
}

private fun part1(monkeys: List<Monkey>): Long {
    repeat(20) {
        monkeys.forEach { it.throwAll { this / 3 } }
    }
    return monkeys.map(Monkey::activityLevel).sortedDescending().take(2).product()
}

private fun part2(monkeys: List<Monkey>): Long {
    val worryLevelDivisor = monkeys.map { monkey -> (1L..23L).first { monkey.test(it) } }.product()
    repeat(10000) {
        monkeys.forEach { it.throwAll { this % worryLevelDivisor } }
    }
    return monkeys.map(Monkey::activityLevel).sortedDescending().take(2).product()
}
