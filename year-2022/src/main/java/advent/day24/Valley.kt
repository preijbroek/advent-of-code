package advent.day24

import util.Direction
import util.Position
import util.modulo

data class Valley(
    val possibleBlizzards: Map<Int, Blizzards>,
    val currentIndex: Int = 0,
    val me: Position = start,
    val target: Position,
    val end: Position = target,
) {

    companion object {
        val start = Position(0, -1)
    }

    private val nextIndex = (currentIndex + 1) modulo possibleBlizzards.size

    fun hash() = "$currentIndex $me"

    fun next(): List<Valley> {
        val nextBlizzards = possibleBlizzards.getValue(nextIndex)
        return me.withNeighbours().filter {
            (it == start || it == end || nextBlizzards.mayContain(it)) && it !in nextBlizzards
        }.map { copy(currentIndex = nextIndex, me = it) }
    }

    fun draw() {
        val nextBlizzards = possibleBlizzards.getValue(currentIndex)
        for (y in (-1 .. nextBlizzards.maxY + 1)) {
            for (x in (-1 .. nextBlizzards.maxX + 1)) {
                val position = Position(x, y)
                val string = if ((position == me)) {
                    "E"
                } else if (position == start || position == target) {
                    "."
                } else if (position.x == -1 || position.x == nextBlizzards.maxY + 1 || position.y == -1 || position.y == nextBlizzards.maxX + 1)
                    "#"
                else {
                    nextBlizzards[position].toDrawString()
                }
                print(string)
            }
            println()
        }
        println()
    }

    private fun List<Direction>?.toDrawString(): String {
        return when {
            this == null -> "."
            size > 1 -> "$size"
            else -> when (first()) {
                Direction.NORTH -> "^"
                Direction.EAST -> ">"
                Direction.SOUTH -> "v"
                Direction.WEST -> "<"
            }
        }
    }
}
