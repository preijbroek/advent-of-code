package advent.day24

import util.Direction
import util.Position
import util.rem

data class Blizzards(
    private val grid: Map<Position, List<Direction>>,
    val maxX: Int,
    val maxY: Int,
) {

    fun next(): Blizzards {
        val nextGrid = grid.entries.flatMap { (blizzardPosition, blizzardDirections) ->
            blizzardDirections.map { blizzardDirection ->
                blizzardPosition.toFlipped(blizzardDirection) % Position(maxX + 1, maxY + 1) to blizzardDirection
            }
        }.groupBy(Pair<Position, Direction>::first, Pair<Position, Direction>::second)
        return copy(grid = nextGrid)
    }

    operator fun contains(position: Position): Boolean {
        return position in grid
    }

    operator fun get(position: Position) = grid[position]

    fun mayContain(position: Position): Boolean {
        return position.isInRange(0, maxX, 0, maxY)
    }
}
