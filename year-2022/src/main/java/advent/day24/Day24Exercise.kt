package advent.day24

import advent.getInput
import util.Direction
import util.Position
import java.util.ArrayDeque

fun main() {
    val blizzards = getInput(24).drop(1).dropLast(1).flatMapIndexed { y, s ->
        s.drop(1).dropLast(1).mapIndexed { x, c -> x to c }.filter { (_, c) -> c != '.' }
            .map { (x, c) ->
                Position(x, y) to listOf(
                    when (c) {
                        '>' -> Direction.EAST
                        'v' -> Direction.SOUTH
                        '<' -> Direction.WEST
                        '^' -> Direction.NORTH
                        else -> throw IllegalArgumentException(c.toString())
                    }
                )
            }
    }.toMap().let {
        Blizzards(it, it.keys.maxOf(Position::x), it.keys.maxOf(Position::y))
    }
    val blizzardsMap = mutableMapOf(0 to blizzards)
    (1 ..< (blizzards.maxX + 1) * (blizzards.maxY + 1)).forEach { index ->
        blizzardsMap[index] = blizzardsMap.getValue(index - 1).next()
    }
    val valley = Valley(
        possibleBlizzards = blizzardsMap,
        target = Position(blizzards.maxX, blizzards.maxY + 1)
    )
    println(part1(valley))
    println(part2(valley))
}

private fun part1(valley: Valley): Int {
    return valley.moveThrough().second
}

private fun part2(valley: Valley): Int {
    val (valleyAfterFirstBrowse, timeForFirstRun) = valley.moveThrough()
    val (valleyAfterRunningBackToStart, timeForBackRun) = valleyAfterFirstBrowse.copy(target = Valley.start)
        .moveThrough()
    val (_, timeForSecondRun) = valleyAfterRunningBackToStart.copy(target = valleyAfterFirstBrowse.target).moveThrough()
    return timeForFirstRun + timeForBackRun + timeForSecondRun
}

private fun Valley.moveThrough(): Pair<Valley, Int> {
    val valleys = ArrayDeque<Pair<Valley, Int>>()
    valleys.offer(this to 0)
    val seenValleys = mutableSetOf(hash())
    while (valleys.isNotEmpty()) {
        val (currentValley, minutes) = valleys.poll()
        currentValley.next().filter { it.hash() !in seenValleys }.forEach {
            if (it.me == it.target) {
                return it to minutes + 1
            }
            valleys.offer(it to minutes + 1)
            seenValleys.add(it.hash())
        }
    }
    throw IllegalStateException("Could not find path to exit")
}
