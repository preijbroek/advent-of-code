package advent.day2

data class Battle(
    val elfHandShape: HandShape,
    val myInstruction: String,
) {

    fun shapeRightScore(execution: (HandShape, String) -> HandShape) = execution(elfHandShape, myInstruction).score(elfHandShape)
}

sealed interface HandShape {

    fun score(other: HandShape): Int

    fun draws() = this

    fun wins(): HandShape

    fun loses(): HandShape

    companion object {

        operator fun invoke(s: String): HandShape {
            return when (s) {
                "A" -> Rock
                "B" -> Paper
                "C" -> Scissors
                else -> throw UnsupportedOperationException(s)
            }
        }
    }
}

object Rock : HandShape {

    override fun score(other: HandShape): Int {
        return 1 + when (other) {
            Rock -> 3
            Paper -> 0
            Scissors -> 6
        }
    }

    override fun wins() = Scissors

    override fun loses() = Paper
}

object Paper : HandShape {

    override fun score(other: HandShape): Int {
        return 2 + when (other) {
            Rock -> 6
            Paper -> 3
            Scissors -> 0
        }
    }

    override fun wins() = Rock

    override fun loses() = Scissors
}

object Scissors : HandShape {

    override fun score(other: HandShape): Int {
        return 3 + when (other) {
            Rock -> 0
            Paper -> 6
            Scissors -> 3
        }
    }

    override fun wins() = Paper

    override fun loses() = Rock
}
