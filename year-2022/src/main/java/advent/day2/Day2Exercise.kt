package advent.day2

import advent.getInput
import util.second

fun main() {
    val input = getInput(2).map { s ->
        s.split(" ").let { HandShape(it.first()) to it.second() }
            .let { Battle(it.first, it.second) }
    }
    println(part1(input))
    println(part2(input))
}

private fun part1(battles: List<Battle>): Int {
    return battles.sumOf {
        it.shapeRightScore { _, myInstruction ->
            when (myInstruction) {
                "X" -> Rock
                "Y" -> Paper
                "Z" -> Scissors
                else -> throw UnsupportedOperationException(myInstruction)
            }
        }
    }
}

private fun part2(battles: List<Battle>): Int {
    return battles.sumOf {
        it.shapeRightScore { elfHandShape, myInstruction ->
            when(myInstruction) {
                "X" -> elfHandShape.wins()
                "Y" -> elfHandShape.draws()
                "Z" -> elfHandShape.loses()
                else -> throw UnsupportedOperationException(myInstruction)
            }
        }
    }
}