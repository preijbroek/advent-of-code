package advent.day14

import advent.getInput
import util.Position
import util.second


fun main() {

    fun String.toPosition(): Position {
        val coordinates = split(",").map(String::toInt)
        return Position(coordinates.first(), coordinates.second())
    }

    val rocks = getInput(14).flatMap { line ->
        line.split(" -> ")
            .map(String::toPosition)
            .windowed(size = 2)
            .map { it.first()..it.second() }
            .flatten()
    }.toSet()
    println(part1(rocks))
    println(part2(rocks))
}

private fun part1(rocks: Set<Position>): Int {
    val infiniteDrop = rocks.maxOf { it.y } + 1
    val startPosition = Position(500, 0)
    var currentDropPosition = startPosition
    val stoppedDrops = mutableSetOf<Position>()

    fun Position.isFilled(): Boolean {
        return this in rocks || this in stoppedDrops
    }

    fun Position.drop(): Position? {
        return listOf(down(), down().left(), down().right()).firstOrNull { !it.isFilled() }
    }

    while (currentDropPosition.y != infiniteDrop) {
        with(currentDropPosition) {
            currentDropPosition = currentDropPosition.drop() ?: startPosition.also {
                stoppedDrops.add(this)
            }
        }
    }
    return stoppedDrops.size
}

private fun part2(rocks: Set<Position>): Int {
    val floorY = rocks.maxOf { it.y } + 2
    val startPosition = Position(500, 0)
    var currentDropPosition = startPosition
    val stoppedDrops = mutableSetOf<Position>()

    fun Position.isFilled(): Boolean {
        return this in rocks || this in stoppedDrops || y == floorY
    }

    fun Position.drop(): Position? {
        return listOf(down(), down().left(), down().right()).firstOrNull { !it.isFilled() }
    }

    while (startPosition !in stoppedDrops) {
        with(currentDropPosition) {
            currentDropPosition = currentDropPosition.drop() ?: startPosition.also {
                stoppedDrops.add(this)
            }
        }
    }
    return stoppedDrops.size
}

private fun Position.down() = north()

private fun Position.left() = west()

private fun Position.right() = east()
