package advent.day7

sealed interface Resource {

    val parent: Directory?
    val name: String

    fun subResource(name: String): Resource

    fun size(): Int

    fun sizesSum(sizeLimit: Int): Int

    fun smallestSubDirOfAtLeastSize(limit: Int): Directory?
}

sealed class Directory(
    override val parent: Directory?,
    override val name: String,
) : Resource {

    private val resources = mutableSetOf<Resource>()

    override fun size() = resources.sumOf { it.size() }

    override fun sizesSum(sizeLimit: Int): Int {
        val size = size()
        return resources.filterIsInstance<Directory>().sumOf { it.sizesSum(sizeLimit) } + if (size < sizeLimit) size else 0
    }

    override fun smallestSubDirOfAtLeastSize(limit: Int): Directory? {
        return if (this.size() < limit) null
        else resources.filterIsInstance<Directory>()
            .mapNotNull { it.smallestSubDirOfAtLeastSize(limit) }
            .minByOrNull { it.size() }
            ?: this
    }

    override fun subResource(name: String): Resource {
        return when (name) {
            "/" -> {
                var resource: Directory = this
                while (resource is SubDirectory) resource = resource.parent
                resource
            }
            ".." -> parent!!
            else -> resources.first { it.name == name }
        }
    }

    fun add(resource: Resource) {
        resources.add(resource)
    }
}

data class Root(
    override val name: String = "/",
) : Directory(parent = null, name)

data class SubDirectory(
    override val parent: Directory,
    override val name: String
) : Directory(parent, name)

data class File(
    override val parent: Directory,
    override val name: String,
    private val size: Int
) : Resource {

    override fun subResource(name: String): Resource {
        throw UnsupportedOperationException("Cannot do subResource on type File")
    }

    override fun size(): Int {
        return size
    }

    override fun sizesSum(sizeLimit: Int): Int {
        return size
    }

    override fun smallestSubDirOfAtLeastSize(limit: Int): Directory {
        throw UnsupportedOperationException("Cannot do smallestSubDirOfAtLeastSize on type File")
    }
}