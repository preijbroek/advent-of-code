package advent.day7

import advent.getInput
import util.second
import util.third

fun main() {
    val root = getInput(7).toFileTree()
    println(part1(root))
    println(part2(root))
}

private fun List<String>.toFileTree(): Resource {
    val root = Root()
    var currentDirectory: Directory = root
    for (line in this) {
        val arguments = line.split(" ")
        when (arguments.first()) {
            "$" -> when (arguments.second()) {
                "cd" -> currentDirectory = currentDirectory.subResource(arguments.third()) as Directory
                "ls" -> continue
            }
            "dir" -> currentDirectory.add(SubDirectory(parent = currentDirectory, name = arguments.second()))
            else -> currentDirectory.add(
                File(parent = currentDirectory, name = arguments.second(), size = arguments.first().toInt())
            )
        }
    }
    return root
}

private fun part1(root: Resource): Int {
    return root.sizesSum(sizeLimit = 100000)
}

private fun part2(root: Resource): Int {
    val totalSize = root.size()
    val unusedSpace = 70000000 - totalSize
    val requiredSpace = 30000000 - unusedSpace
    return root.smallestSubDirOfAtLeastSize(requiredSpace)!!.size()
}