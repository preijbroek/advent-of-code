package advent.day5

import advent.getInputBlocks
import util.second
import util.third

fun main() {
    val input = getInputBlocks(5)
    println(part1(input.toInstructions()))
    println(part2(input.toInstructions()))
}

private fun List<String>.toInstructions(): Pair<List<Stack>, List<Movement>> {
    val boxLines = first().split("\n")
    val numberOfStacks = boxLines.first().length / 4 + 1
    val stacks: Map<Int, Stack> = (1..numberOfStacks).associateWith { Stack(it) }
    boxLines.dropLast(1)
        .map { it.chunked(size = 4)}
        .reversed()
        .forEach {
            it.forEachIndexed { index, s ->
                if (s.isNotBlank()) stacks.getValue(index + 1).add(s.second().toString())
            }
        }
    val movements = second().split("\n").map {
        it.substringAfter("move ").split("( from )|( to )".toRegex()).map(String::toInt)
    }.map {
        Movement(stacks.getValue(it.second()), stacks.getValue(it.third()), it.first())
    }
    return stacks.values.toList() to movements
}

private fun part1(instructions: Pair<List<Stack>, List<Movement>>): String {
    val (stacks, movements) = instructions
    movements.forEach(Movement::execute)
    return stacks.sortedBy(Stack::id).joinToString(separator = "", transform = Stack::top)
}

private fun part2(instructions: Pair<List<Stack>, List<Movement>>): String {
    val (stacks, movements) = instructions
    movements.forEach { it.execute { reversed() } }
    return stacks.sortedBy(Stack::id).joinToString(separator = "", transform = Stack::top)
}