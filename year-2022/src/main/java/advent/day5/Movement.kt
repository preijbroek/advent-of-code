package advent.day5

data class Movement(
    val source: Stack,
    val target: Stack,
    val times: Int,
) {

    fun execute(order: List<String>.() -> List<String> = { this }) {
        target.add(source.take(times).order())
    }
}
