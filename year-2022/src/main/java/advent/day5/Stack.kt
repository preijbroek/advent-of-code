package advent.day5

import java.util.ArrayDeque
import java.util.Collections.asLifoQueue
import java.util.Queue

data class Stack(
    val id: Int,
    val boxes: Queue<String>
) {

    constructor(id: Int, vararg boxes: String) : this(id, asLifoQueue(ArrayDeque<String>(boxes.toList().reversed())))

    fun add(box: String) = boxes.offer(box)

    fun add(boxes: List<String>) = boxes.forEach(this::add)

    fun take(amount: Int) = generateSequence { boxes.poll() }.take(amount).toList()

    fun top(): String = boxes.peek()
}
