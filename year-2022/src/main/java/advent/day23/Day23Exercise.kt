package advent.day23

import advent.getInput
import util.Position

fun main() {
    val input = getInput(23)
    val lastIndex = input.lastIndex
    val elves = (lastIndex downTo 0).flatMap { y ->
        input[lastIndex - y].mapIndexed { x, c ->
            Position(x, y) to c.toString()
        }
    }.toMap()
        .filterValues { it == "#" }
        .mapValues { (position, _) -> Elf(position) }
    println(part1(elves))
    println(part2(elves))
}

private fun part1(elves: Map<Position, Elf>): Int {
    val movingGrid = elves.toMutableMap()
    var preferredDirection = 0
    repeat(10) {
        movingGrid.moveElves(preferredDirection)
        preferredDirection++
    }
    return movingGrid.emptySpaces()
}

private fun part2(elves: Map<Position, Elf>): Int {
    val movingGrid = elves.toMutableMap()
    var lastGridHash = ""
    var preferredDirection = 0
    while (lastGridHash != movingGrid.toString()) {
        lastGridHash = movingGrid.toString()
        movingGrid.moveElves(preferredDirection)
        preferredDirection++
    }
    return preferredDirection
}

private fun MutableMap<Position, Elf>.moveElves(preferredDirection: Int) {
    values.filterNot { it.isLonely(this) }
        .map { it to it.suggestedMove(preferredDirection, this) }
        .groupBy({ (_, destination) -> destination }, { (elf, _) -> elf.position })
        .filter { (destination, originalPosition) -> destination != null && originalPosition.size == 1 }
        .forEach { (destination, originalPosition) ->
            remove(originalPosition.first())
            this[destination!!] = Elf(destination)
        }
}

private fun Map<Position, Elf>.emptySpaces(): Int {
    val minX = keys.minOf(Position::x)
    val maxX = keys.maxOf(Position::x)
    val minY = keys.minOf(Position::y)
    val maxY = keys.maxOf(Position::y)
    return (maxY downTo minY).flatMap { y ->
        (minX..maxX).map { x -> Position(x, y) }
    }.count { it !in this }
}
