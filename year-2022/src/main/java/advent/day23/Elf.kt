package advent.day23

import util.Direction
import util.Position

data class Elf(
    val position: Position
) {

    companion object {

        private val preferredDirections = listOf(Direction.NORTH, Direction.SOUTH, Direction.WEST, Direction.EAST)
    }

    fun isLonely(field: Map<Position, Elf>): Boolean {
        return position.allNeighbours().none { it in field }
    }

    fun suggestedMove(preferredDirection: Int, field: Map<Position, Elf>): Position? {
        return (preferredDirection..(preferredDirection + 3))
            .map { preferredDirections[it % 4] }
            .firstOrNull { canMove(it, field) }
            ?.let { position.to(it) }
    }

    private fun canMove(direction: Direction, field: Map<Position, Elf>): Boolean {
        return when(direction) {
            Direction.NORTH -> setOf(position.north().east(), position.north(), position.north().west()).none { it in field }
            Direction.SOUTH -> setOf(position.south().east(), position.south(), position.south().west()).none { it in field }
            Direction.WEST -> setOf(position.west().north(), position.west(), position.west().south()).none { it in field }
            Direction.EAST -> setOf(position.east().north(), position.east(), position.east().south()).none { it in field }
        }
    }
}
