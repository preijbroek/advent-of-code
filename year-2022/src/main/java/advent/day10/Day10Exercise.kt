package advent.day10

import advent.getInput

fun main() {
    val results = getInput(10).map { it.split(" ") }.let(::Programme).execute()
    println(part1(results))
    println(part2(results))
}

private fun part1(results: Outputs): Int {
    return results.signalStrength
}

private fun part2(results: Outputs): String {
    return results.image
}
