package advent.day10

import util.second

data class Programme(
    private val instructions: List<List<String>>
) {

    fun execute(): Outputs {
        var cycle = 0
        var registerX = 1
        var signalStrength = 0
        val imageBuilder = StringBuilder()

        fun sprite() = registerX - 1..registerX + 1

        fun runCycle() {
            imageBuilder.append(if (cycle % 40 in sprite()) '#' else ' ')
            cycle++
            when (cycle % 40) {
                0 -> imageBuilder.append('\n')
                20 -> signalStrength += cycle * registerX
            }
        }

        instructions.forEach {
            when (it.first()) {
                "noop" -> runCycle()
                "addx" -> {
                    repeat(2) { runCycle() }
                    registerX += it.second().toInt()
                }
            }
        }
        return Outputs(signalStrength, imageBuilder.toString())
    }
}

data class Outputs(
    val signalStrength: Int,
    val image: String
)
