package advent

import util.getInputBlocksForYear
import util.getInputForYear

fun getInput(day: Int) = getInputForYear(2022, day)

fun getInputBlocks(day: Int): List<String> = getInputBlocksForYear(2022, day)