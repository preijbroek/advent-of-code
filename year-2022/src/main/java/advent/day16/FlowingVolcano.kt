package advent.day16

import kotlin.math.max

data class FlowingVolcano(
    private val flowingValves: List<FlowingValve>,
    private val currentValve: FlowingValve = flowingValves.first(FlowingValve::isStart),
    private val closedValves: Set<FlowingValve> = flowingValves.filterNot(FlowingValve::isStart).toSet()
) {

    fun remainingPressure(minutes: Int, withElephant: Boolean = false, originalMinutes: Int = minutes): Int {
        return if (closedValves.isEmpty() || minutes <= 0) 0
        else {
            maxOf(
                a = closedValves.maxOf { flowingValve ->
                    val newMinutes = max(0, minutes - (currentValve.minutesTo(flowingValve) + 1))
                    newMinutes * flowingValve.flowRate + copy(
                        currentValve = flowingValve,
                        closedValves = closedValves - flowingValve
                    ).remainingPressure(newMinutes, withElephant, originalMinutes)
                },
                b = 0.takeUnless { withElephant } ?: copy(
                    currentValve = flowingValves.first(FlowingValve::isStart),
                    closedValves = closedValves
                ).remainingPressure(originalMinutes, withElephant = false)
            )
        }
    }
}
