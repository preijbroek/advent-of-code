package advent.day16

data class Valve(
    val name: String,
    val flowRate: Int,
    val connections: List<() -> Valve>,
) {

    fun connections() = connections.map { it() }

    fun canFlow() = flowRate > 0

    fun isStart() = name == "AA"
}
