package advent.day16

data class FlowingValve(
    val name: String,
    val flowRate: Int,
    private val connections: Map<() -> FlowingValve, Int>
) {

    private val initializedConnections by lazy { connections.mapKeys { it.key() } }

    fun minutesTo(flowingValve: FlowingValve) = initializedConnections.getValue(flowingValve)

    fun isStart() = name == "AA"

}