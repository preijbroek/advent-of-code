package advent.day16

import advent.getInput
import util.second
import util.third

fun main() {
    val input = getInput(16).toValves().toFlowingValves()
    println(part1(input))
    println(part2(input))
}

private fun List<String>.toValves(): List<Valve> {
    val valvesMap = mutableMapOf<String, Valve>()
    return map {
        val components = it.substringAfter("Valve ")
            .split("( has flow rate=)|(; tunnels lead to valves )|(; tunnel leads to valve )".toRegex())
        val name = components.first()
        val flowRate = components.second().toInt()
        val connections = components.third().split(", ").map { { valvesMap.getValue(it) } }
        val valve = Valve(name, flowRate, connections)
        valvesMap[valve.name] = valve
        valve
    }
}

private fun List<Valve>.toFlowingValves(): List<FlowingValve> {
    val flowingValvesMap = mutableMapOf<String, FlowingValve>()
    return filter { it.canFlow() || it.isStart() }
        .map { valve ->
            val reachedValves = mutableSetOf(valve)
            val connections = mutableMapOf<() -> FlowingValve, Int>()
            var distance = 0
            while (size != reachedValves.size) {
                distance++
                val newConnections = reachedValves.flatMap { it.connections() }
                    .filter { it !in reachedValves }
                reachedValves.addAll(newConnections)
                newConnections.filter { it.canFlow() || it.isStart() }.forEach {
                    connections[{ flowingValvesMap.getValue(it.name) }] = distance
                }
            }
            FlowingValve(valve.name, valve.flowRate, connections).also {
                flowingValvesMap[it.name] = it
            }
        }
}

private fun part1(valves: List<FlowingValve>): Int {
    return FlowingVolcano(valves).remainingPressure(minutes = 30)
}

private fun part2(valves: List<FlowingValve>): Int {
    return FlowingVolcano(valves).remainingPressure(minutes = 26, withElephant = true)
}
