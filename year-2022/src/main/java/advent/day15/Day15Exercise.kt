package advent.day15

import advent.getInput
import util.*

fun main() {
    val input = getInput(15).map {
        val sensor = it.substring(it.indexOf("x"), it.indexOf(":")).toPosition()
        val closestBeacon = it.substring(it.lastIndexOf("x")).toPosition()
        PositionRangeFromCentre(sensor, sensor.distanceTo(closestBeacon)) to closestBeacon
    }
    println(part1(input))
    println(part2(input))
}

private fun String.toPosition(): Position {
    val coordinates = split(", ").map { it.drop(2).toInt() }
    return Position(coordinates.first(), coordinates.second())
}

private fun part1(sensorsAndBeacons: List<Pair<PositionRangeFromCentre, Position>>): Long {
    val knownBeacons = sensorsAndBeacons.map(Pair<PositionRangeFromCentre, Position>::second).toSet()
    val sensors = sensorsAndBeacons.map(Pair<PositionRangeFromCentre, Position>::first)
    val segments = sensors.segments(2000000)
    return segments.sumOf { it.last - it.first + 1 } - knownBeacons.filter { it.y == 2000000L }.size
}

private fun part2(sensorsAndBeacons: List<Pair<PositionRangeFromCentre, Position>>): Long {
    val sensors = sensorsAndBeacons.map(Pair<PositionRangeFromCentre, Position>::first)
    val distressBeacon = sensors.asSequence()
        .flatMap(PositionRangeFromCentre::outerBorder2d)
        .first {
            it.isInRange(0, 4000000, 0, 4000000) && sensors.none { sensor -> it in sensor }
        }
    return 4000000L * distressBeacon.x.toLong() + distressBeacon.y.toLong()
}

private fun List<PositionRangeFromCentre>.segments(y: Long): List<LongRange> = map { range ->
    range.intersectY(y)
}.sortedBy(LongRange::first).fold(listOf()) { acc: List<LongRange>, range ->
    if (acc.isEmpty()) {
        listOf(range)
    } else if (acc.last() canMergeWith range) {
        acc.dropLast(1) with (acc.last() mergeWith range)
    } else {
        acc with range
    }
}

// 5127797
// 12518502636475