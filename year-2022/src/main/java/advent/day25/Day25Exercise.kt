package advent.day25

import advent.getInput
import util.modulo
import util.pow

fun main() {
    val snafuNumbers = getInput(25)
    println(part1(snafuNumbers))
}

private fun part1(snafuNumbers: List<String>): String {
    return snafuNumbers.sumOf(String::toDecimal).toSnafuNumber()
}

private fun String.toDecimal(): Long {
    return indices.sumOf { index ->
        (5L pow index) * this[lastIndex - index].toDecimalDigit()
    }
}

private fun Char.toDecimalDigit(): Long {
    return when (this) {
        '2' -> 2L
        '1' -> 1L
        '0' -> 0L
        '-' -> -1L
        '=' -> -2L
        else -> throw IllegalArgumentException(toString())
    }
}

private fun Long.toSnafuNumber(): String {
    if (this == 0L) return ""
    return ((this + 2) / 5).toSnafuNumber() + lastEntryOfSnafuString()
}

private fun Long.lastEntryOfSnafuString(): String {
    return when (this modulo 5) {
        0 -> "0"
        1 -> "1"
        2 -> "2"
        3 -> "="
        4 -> "-"
        else -> throw IllegalArgumentException(toString())
    }
}


