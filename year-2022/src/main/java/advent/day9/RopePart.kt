package advent.day9

import util.Direction
import util.Position
import util.abs

data class RopePart(
    private var position: Position = Position.CENTRE,
    private val follows: RopePart?,
) {

    fun move(direction: Direction) {
        position = position.to(direction)
    }

    fun follow() {
        if (follows != null) {
            if (position !in follows.position.withAllNeighbours()) {
                position = follows.position.pull(position)
            }
        }
    }

    fun currentPosition() = position

    private fun Position.pull(positionT: Position): Position {
        return Position(
            x = pullAxis(positionT, Position::x),
            y = pullAxis(positionT, Position::y),
        )
    }

    private fun Position.pullAxis(positionT: Position, axis: Position.() -> Int): Int {
        return if ((axis() - positionT.axis()).abs() == 1) {
            axis()
        } else {
            (axis() + positionT.axis()) / 2
        }
    }
}
