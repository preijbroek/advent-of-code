package advent.day9

import advent.getInput
import util.Direction
import util.Position
import util.second

fun main() {
    val instructions = getInput(9).map {
        it.split(" ").let { instruction -> instruction.first().toDirection() to instruction.second().toInt() }
    }
    println(part1(instructions))
    println(part2(instructions))
}

private fun String.toDirection() =
    when (this) {
        "D" -> Direction.SOUTH
        "U" -> Direction.NORTH
        "R" -> Direction.EAST
        "L" -> Direction.WEST
        else -> throw UnsupportedOperationException(this)
    }

private fun part1(instructions: List<Pair<Direction, Int>>): Int {
    return countSquaresOfTail(instructions, ropeLength = 2)
}

private fun part2(instructions: List<Pair<Direction, Int>>): Int {
    return countSquaresOfTail(instructions, ropeLength = 10)
}

private fun countSquaresOfTail(instructions: List<Pair<Direction, Int>>, ropeLength: Int): Int {
    val head = RopePart(follows = null)
    val ropeParts = generateSequence(head) { RopePart(follows = it) }.take(ropeLength).toList()
    val positionsReachedByTail = mutableSetOf(Position.CENTRE)
    instructions.forEach { (direction, times) ->
        repeat(times) {
            head.move(direction)
            ropeParts.forEach(RopePart::follow)
            positionsReachedByTail.add(ropeParts.last().currentPosition())
        }
    }
    return positionsReachedByTail.size
}
