package advent.day3

data class Rucksack(
    val data: String
) {

    private val leftCompartment = data.substring(0, data.length / 2)
    private val rightCompartment = data.substringAfter(leftCompartment)

    fun duplicate() = (leftCompartment intersect rightCompartment).first()

    infix fun intersect(other: Rucksack) = Rucksack((data intersect other.data).joinToString(separator = ""))

    private infix fun String.intersect(other: String) = toSet() intersect other.toSet()
}
