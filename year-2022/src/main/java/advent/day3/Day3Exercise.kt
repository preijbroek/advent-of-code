package advent.day3

import advent.getInput

fun main() {
    val rucksacks = getInput(3).map(::Rucksack)
    println(part1(rucksacks))
    println(part2(rucksacks))
}

private fun part1(rucksacks: List<Rucksack>): Int {
    return rucksacks.map(Rucksack::duplicate).sumOf(Char::toPriority)
}

private fun part2(rucksacks: List<Rucksack>): Int {
    return rucksacks.chunked(size = 3).sumOf(List<Rucksack>::badge)
}

private fun List<Rucksack>.badge() = let { (rucksack1, rucksack2, rucksack3) ->
    (rucksack1 intersect rucksack2 intersect rucksack3).toPriority()
}

private fun Rucksack.toPriority() = data.toPriority()

private fun String.toPriority() = first().toPriority()

private fun Char.toPriority() =
    if (isLowerCase()) {
        this.code - 96
    } else {
        this.code - 38
    }

