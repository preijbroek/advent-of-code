package advent.day4

import advent.getInput
import util.second

fun main() {
    val sections = getInput(4).map { elves ->
        elves.split(",").map { sections ->
            sections.split("-").map(String::toInt).let { it.first()..it.second() }
        }.let { Sections(it.first(), it.second()) }
    }
    println(part1(sections))
    println(part2(sections))
}

private fun part1(sections: List<Sections>): Int {
    return sections.count(Sections::hasSubsetSection)
}

private fun part2(sections: List<Sections>): Int {
    return sections.count(Sections::hasOverlappingSection)
}
