package advent.day4

data class Sections(
    val section1: IntRange,
    val section2: IntRange
) {

    fun hasSubsetSection(): Boolean {
        val intersection = intersection()
        return intersection == section1.toSet() || intersection == section2.toSet()
    }

    fun hasOverlappingSection(): Boolean {
        return intersection().isNotEmpty()
    }

    private fun intersection() = section1.intersect(section2)
}
