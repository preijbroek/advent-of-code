package advent.day19

import java.util.UUID
import kotlin.math.max

class BluePrint(
    private val id: Int,
    private val resources: List<Amounts>
) {

    companion object {

        const val ORE = 0
        const val CLAY = 1
        const val OBSIDIAN = 2
        const val GEODE = 3

        operator fun invoke(s: String): BluePrint {
            val description = s.split(" ")
            val id = description[1].dropLast(1).toInt()
            val resources = listOf(
                Amounts(ore = description[6].toInt(), clay = 0, obsidian = 0),
                Amounts(ore = description[12].toInt(), clay = 0, obsidian = 0),
                Amounts(ore = description[18].toInt(), clay = description[21].toInt(), obsidian = 0),
                Amounts(ore = description[27].toInt(), clay = 0, obsidian = description[30].toInt()),
            )
            return BluePrint(id, resources)
        }
    }

    private val maxGeodeIds = HashMap<String, Int>()

    fun qualityScore(minutes: Int): Int {
        return id * createGeodes(minutes)
    }

    fun createGeodes(minutes: Int) = maxGeodes(minutes)

    private val maxGeodeHashes = HashMap<String, Int>()

    private fun maxGeodes(
        minutes: Int,
        maxGeodeId: String = UUID.randomUUID().toString(),
        currentResources: Amounts = Amounts(ore = 0, clay = 0, obsidian = 0, geode = 0),
        currentRobots: Robots = Robots(Amounts(ore = 1, clay = 0, obsidian = 0, geode = 0)),
    ): Int {
        val hash = "$minutes ${currentRobots.amounts} $currentResources"
        if (hash in maxGeodeHashes) {
            return maxGeodeHashes.getValue(hash)
        }
        if (!maxGeodeIds.containsKey(maxGeodeId)) {
            maxGeodeIds[maxGeodeId] = Int.MIN_VALUE
        }
        if (minutes == 0) {
            return currentResources.geode.also {
                maxGeodeIds[maxGeodeId] = max(it, maxGeodeIds.getValue(maxGeodeId))
            }
        }
        if (maxGeodeIds.getValue(maxGeodeId) >= maxPossibleGeodes(minutes, currentResources, currentRobots.amounts)) {
            return Int.MIN_VALUE
        }
        val possiblePaths = mutableListOf<Pair<Robots, Amounts>>()
        if (canBuildRobot(GEODE, currentResources)) {
            possiblePaths += (currentRobots + Amounts.GEODE) to (currentResources - resources[GEODE])
        } else {
            var canRefuseObsidian = false
            var canRefuseClay = false
            var canRefuseOre = false
            if (canBuildRobot(OBSIDIAN, currentResources) && mightBuildObsidianRobot(minutes, currentResources, currentRobots)) {
                possiblePaths += (currentRobots + Amounts.OBSIDIAN) to (currentResources - resources[OBSIDIAN])
                canRefuseObsidian = true
            }
            if (canBuildRobot(CLAY, currentResources) && mightBuildClayRobot(minutes, currentResources, currentRobots)) {
                possiblePaths += (currentRobots + Amounts.CLAY) to (currentResources - resources[CLAY])
                canRefuseClay = true
            }
            if (canBuildRobot(ORE, currentResources) && mightBuildOreRobot(minutes, currentResources, currentRobots)) {
                possiblePaths += (currentRobots + Amounts.ORE) to (currentResources - resources[ORE])
                canRefuseOre = true
            }
            possiblePaths += currentRobots.copy(
                refusedObsidian = currentRobots.refusedObsidian || canRefuseObsidian,
                refusedClay = currentRobots.refusedClay || canRefuseClay,
                refusedOre = currentRobots.refusedOre || canRefuseOre
            ) to currentResources
        }
        return possiblePaths.maxOf { (robots, resources) ->
           maxGeodes(minutes - 1, maxGeodeId, resources + currentRobots.amounts, robots)
        }.also {
            maxGeodeHashes[hash] = it
        }
    }

    private fun maxPossibleGeodes(minutes: Int, currentResources: Amounts, currentRobots: Amounts): Int {
        val maxGeodesToCreateByNonExistentRobots = minutes * (minutes + 1) / 2
        return currentResources.geode + minutes * currentRobots.geode + maxGeodesToCreateByNonExistentRobots
    }

    private fun canBuildRobot(robot: Int, currentResources: Amounts): Boolean {
        return resources[robot] in currentResources
    }

    private fun mightBuildObsidianRobot(minutes: Int, currentResources: Amounts, currentRobots: Robots): Boolean {
        return !currentRobots.refusedObsidian
                && currentResources.obsidian <= minutes * (resources[GEODE].obsidian - currentRobots.amounts.obsidian)
    }

    private fun mightBuildClayRobot(minutes: Int, currentResources: Amounts, currentRobots: Robots): Boolean {
        return !currentRobots.refusedClay
                && currentResources.clay <= minutes * (resources[OBSIDIAN].clay - currentRobots.amounts.clay)
    }

    private fun mightBuildOreRobot(minutes: Int, currentResources: Amounts, currentRobots: Robots): Boolean {
        val maxOreNeeded = maxOf(resources[GEODE].ore, resources[OBSIDIAN].ore, resources[CLAY].ore, resources[ORE].ore)
        return !currentRobots.refusedOre
                && currentResources.ore <= minutes * (maxOreNeeded - currentRobots.amounts.ore)
    }


}