package advent.day19

data class Amounts(
    val ore: Int,
    val clay: Int,
    val obsidian: Int,
    val geode: Int = 0,
) {

    companion object {

        val ORE = Amounts(ore = 1, clay = 0, obsidian = 0, geode = 0)
        val CLAY = Amounts(ore = 0, clay = 1, obsidian = 0, geode = 0)
        val OBSIDIAN = Amounts(ore = 0, clay = 0, obsidian = 1, geode = 0)
        val GEODE = Amounts(ore = 0, clay = 0, obsidian = 0, geode = 1)
    }

    operator fun contains(amounts: Amounts): Boolean {
        return ore >= amounts.ore
                && clay >= amounts.clay
                && obsidian >= amounts.obsidian
                && geode >= amounts.geode
    }

    operator fun minus(amounts: Amounts): Amounts {
        return Amounts(
            ore = ore - amounts.ore,
            clay = clay - amounts.clay,
            obsidian = obsidian - amounts.obsidian,
            geode = geode - amounts.geode
        )
    }

    operator fun plus(amounts: Amounts): Amounts {
        return Amounts(
            ore = ore + amounts.ore,
            clay = clay + amounts.clay,
            obsidian = obsidian + amounts.obsidian,
            geode = geode + amounts.geode
        )
    }
}
