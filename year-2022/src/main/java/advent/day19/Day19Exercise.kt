package advent.day19

import advent.getInput
import util.productOf

fun main() {
    val blueprints = getInput(19).map { BluePrint(it) }
    println(part1(blueprints))
    println(part2(blueprints))
}

private fun part1(blueprints: List<BluePrint>): Int {
    return blueprints.sumOf { it.qualityScore(minutes = 24) }
}

private fun part2(blueprints: List<BluePrint>): Int {
    return blueprints.take(3).productOf { it.createGeodes(minutes = 32).toLong() }.toInt()
}
