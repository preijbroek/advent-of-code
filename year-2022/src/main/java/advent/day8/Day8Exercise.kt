package advent.day8

import advent.getInput
import util.Position
import util.toCharMap

fun main() {
    val forest = getInput(8).toCharMap().mapValues { Tree(it.key, it.value.digitToInt()) }
    println(part1(forest))
    println(part2(forest))
}

private fun part1(forest: Map<Position, Tree>): Int {
    val (maxX, maxY, _, _) = forest.keys.maxWith(Position.byX().thenComparing(Position.byY()))
    return forest.values.count { it.isVisibleIn(forest, maxX, maxY) }
}

private fun part2(forest: Map<Position, Tree>): Int {
    val (maxX, maxY, _, _) = forest.keys.maxWith(Position.byX().thenComparing(Position.byY()))
    return forest.values.maxOf { it.scenicScoreIn(forest, maxX, maxY) }
}