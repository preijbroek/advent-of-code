package advent.day8

import util.Position
import util.takeWhileInclusive

data class Tree(
    val position: Position,
    val height: Int,
) {

    fun isVisibleIn(forest: Map<Position, Tree>, maxX: Int, maxY: Int): Boolean {
        return lowerXRange().none { forest.getWithX(it).height >= height }
                || lowerYRange().none { forest.getWithY(it).height >= height }
                || higherXRange(maxX).none { forest.getWithX(it).height >= height }
                || higherYRange(maxY).none { forest.getWithY(it).height >= height }
    }

    fun scenicScoreIn(forest: Map<Position, Tree>, maxX: Int, maxY: Int): Int {
        return lowerXRange().asSequence().takeWhileInclusive { forest.getWithX(it).height < height }.count() *
                lowerYRange().asSequence().takeWhileInclusive { forest.getWithY(it).height < height }.count() *
                higherXRange(maxX).asSequence().takeWhileInclusive { forest.getWithX(it).height < height }.count() *
                higherYRange(maxY).asSequence().takeWhileInclusive { forest.getWithY(it).height < height }.count()
    }

    private fun lowerXRange() = (0 ..< position.x).reversed()

    private fun lowerYRange() = (0 ..< position.y).reversed()

    private fun higherXRange(maxX: Int) = position.x + 1..maxX

    private fun higherYRange(maxY: Int) = position.y + 1..maxY

    private fun Map<Position, Tree>.getWithX(x: Int) = getValue(position.copy(x = x))

    private fun Map<Position, Tree>.getWithY(y: Int) =
        getValue(position.copy(y = y))


}
